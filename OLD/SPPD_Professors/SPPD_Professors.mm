<map version="0.8.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1180458850577" ID="Freemind_Link_8150358" MODIFIED="1180470045853" TEXT="USC SPPD Professors">
<node COLOR="#ff0000" CREATED="1180676561251" ID="Freemind_Link_529186155" MODIFIED="1180676669626" POSITION="left" TEXT="email to send">
<arrowlink DESTINATION="Freemind_Link_529186155" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Freemind_Arrow_Link_29974942" STARTARROW="None" STARTINCLINATION="0;0;"/>
<node CREATED="1180676591416" LINK="/Users/durantschoon/Documents/GradSchools/USC/RA_request.rtf" MODIFIED="1180676591416" TEXT="RA_request.rtf"/>
</node>
<node COLOR="#669900" CREATED="1180462989859" ID="Freemind_Link_1291904855" MODIFIED="1180463141875" POSITION="left" TEXT="Third world">
<cloud/>
<node CREATED="1180458875900" FOLDED="true" ID="Freemind_Link_85994913" MODIFIED="1180459240495" TEXT="Tridib Banerjee">
<arrowlink DESTINATION="Freemind_Link_85994913" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Freemind_Arrow_Link_1421704638" STARTARROW="None" STARTINCLINATION="0;0;"/>
<icon BUILTIN="bookmark"/>
<node CREATED="1180458881804" LINK="http://www.usc.edu/schools/sppd/faculty/detail.php?id=2" MODIFIED="1180458881804" TEXT="usc.edu &gt; Schools &gt; Sppd &gt; Faculty &gt; Detail ? ..."/>
<node CREATED="1180459207663" ID="_" MODIFIED="1180459227381" TEXT="&lt;html&gt;&lt;img src=&quot;images_SPPD_Professors/tbanerje.jpg&quot;&gt;">
<arrowlink DESTINATION="_" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Freemind_Arrow_Link_1117284647" STARTARROW="None" STARTINCLINATION="0;0;"/>
</node>
<node CREATED="1180463128093" ID="Freemind_Link_772590161" MODIFIED="1180469786788" TEXT="Professor&#xa;&#xd;James Irvine Chair in Urban and Regional Planning"/>
<node CREATED="1180459269498" ID="Freemind_Link_258703341" MODIFIED="1180462597418" TEXT="&lt;html&gt;&#xa;&lt;b&gt;Expertise&lt;/b&gt;&lt;br&gt;&#xa;&#xd;Urban design, comparative urbanism, urban sprawl, third world urbanization, planning theory"/>
<node COLOR="#0033ff" CREATED="1180490988434" ID="Freemind_Link_1550740873" MODIFIED="1180491059612" TEXT="EMAILED about RA 5/29/07"/>
</node>
</node>
<node COLOR="#669900" CREATED="1180469723122" ID="Freemind_Link_1560284956" MODIFIED="1180469792619" POSITION="right" TEXT="Economics">
<cloud/>
<node CREATED="1180469729382" FOLDED="true" ID="Freemind_Link_366583865" MODIFIED="1180469729382" TEXT="Raphael Bostic">
<node CREATED="1180469736046" LINK="http://www.usc.edu/schools/sppd/faculty/detail.php?id=3" MODIFIED="1180469736046" TEXT="usc.edu &gt; Schools &gt; Sppd &gt; Faculty &gt; Detail ? ..."/>
<node CREATED="1180469741731" ID="Freemind_Link_1179508755" MODIFIED="1180469749425" TEXT="&lt;html&gt;&lt;img src=&quot;images_SPPD_Professors/bostic.jpg&quot;&gt;"/>
<node CREATED="1180469766808" ID="Freemind_Link_329511337" MODIFIED="1180469775935" TEXT="Associate Professor &#xa;Director, Master of Real Estate Development Program"/>
<node CREATED="1180491312500" MODIFIED="1180491312500" TEXT="His research on financial markets and institutions has focused on banks in community development, the role and effects of regulation in banking, housing and homeownership, urban economic growth, wage and earnings profiles, affordable housing, and policy analysis."/>
</node>
</node>
<node COLOR="#669900" CREATED="1180469850715" ID="Freemind_Link_575753221" MODIFIED="1180469929312" POSITION="right" TEXT="administrative corruption, public accountability">
<cloud/>
<node CREATED="1180469860629" FOLDED="true" ID="Freemind_Link_1412227342" MODIFIED="1180469860629" TEXT="Gerald Caiden">
<node CREATED="1180469866312" LINK="http://www.usc.edu/schools/sppd/faculty/detail.php?id=5" MODIFIED="1180469866312" TEXT="usc.edu &gt; Schools &gt; Sppd &gt; Faculty &gt; Detail ? ..."/>
<node CREATED="1180469880773" ID="Freemind_Link_1800617558" MODIFIED="1180469888750" TEXT="&lt;html&gt;&lt;img src=&quot;images_SPPD_Professors/caiden.jpg&quot;&gt;"/>
<node CREATED="1180469896611" ID="Freemind_Link_684298641" MODIFIED="1180469898097" TEXT="Professor"/>
<node CREATED="1180469921682" ID="Freemind_Link_1318838823" MODIFIED="1180469922839" TEXT="He is responsible for over 29 books and over 270 academic articles on diverse topics, such as administrative corruption, public accountability, auditing, ombudsman, public service ethics, comparative administrative cultures, and public management systems"/>
</node>
<node CREATED="1180476662676" FOLDED="true" ID="Freemind_Link_1484168624" MODIFIED="1180476662676" TEXT="Joseph S. Wholey">
<node CREATED="1180476766887" LINK="http://www.usc.edu/schools/sppd/faculty/detail.php?id=43" MODIFIED="1180476766887" TEXT="usc.edu &gt; Schools &gt; Sppd &gt; Faculty &gt; Detail ? ..."/>
<node CREATED="1180476685955" ID="Freemind_Link_557494549" MODIFIED="1180476685955" TEXT="Joseph S. Wholey"/>
<node CREATED="1180476708361" ID="Freemind_Link_1753244050" MODIFIED="1180476718700" TEXT="&lt;html&gt;&lt;img src=&quot;images_SPPD_Professors/wholey.jpg&quot;&gt;"/>
<node CREATED="1180476735856" MODIFIED="1180476735856" TEXT="Professor"/>
<node CREATED="1180476662677" ID="Freemind_Link_1854542909" MODIFIED="1180476662677" TEXT="Wholey, Joseph S., &quot;Improving Performance and Accountability&quot;; in S.I. Donaldson, and M. Scriven, eds. Evaluating Social Programs and Problems. Mahwah, NJ: Erlbaum, pp. 43-61; 2003"/>
<node CREATED="1180476662679" MODIFIED="1180476662679" TEXT="Wholey, Joseph, &quot;Defining, Improving, and Communicating Program Quality&quot;; in A. Benson, D.M. Hinn, and C. Lloyd, eds., The Promise and Perils of Representing Quality in Program Evaluation. Stamford, Conn: JAI Press, pp. 201-218; 2001"/>
</node>
<node COLOR="#0033ff" CREATED="1180677341438" ID="Freemind_Link_1982679735" MODIFIED="1180677360759" TEXT="Email over quota 5/31/07">
<edge COLOR="#808080" WIDTH="thin"/>
</node>
</node>
<node COLOR="#669900" CREATED="1180469957095" ID="Freemind_Link_709987607" MODIFIED="1180470028439" POSITION="left" TEXT="Participation">
<cloud/>
<node CREATED="1180469965978" FOLDED="true" ID="Freemind_Link_82629617" MODIFIED="1180469965978" TEXT="Terry L. Cooper">
<node CREATED="1180469972152" LINK="http://www.usc.edu/schools/sppd/faculty/detail.php?id=6" MODIFIED="1180469972152" TEXT="usc.edu &gt; Schools &gt; Sppd &gt; Faculty &gt; Detail ? ..."/>
<node CREATED="1180469985681" ID="Freemind_Link_1685593124" MODIFIED="1180469994451" TEXT="&lt;html&gt;&lt;img src=&quot;images_SPPD_Professors/tlcooper.jpg&quot;&gt;"/>
<node CREATED="1180470003548" ID="Freemind_Link_1093813657" MODIFIED="1180470012048" TEXT="Professor &#xa;The Maria B. Crutcher Professor in Citizenship and Democratic Values"/>
<node CREATED="1180470025508" MODIFIED="1180470025508" TEXT="Administrative ethics, administrative theory, citizen participation, neighborhood organizations, role of citizens"/>
</node>
<node CREATED="1180470751519" FOLDED="true" ID="Freemind_Link_1970668236" MODIFIED="1180470751519" TEXT="Clara Iraz&#xe1;bal">
<node CREATED="1180470762480" LINK="http://www.usc.edu/schools/sppd/faculty/detail.php?id=17" MODIFIED="1180470762480" TEXT="usc.edu &gt; Schools &gt; Sppd &gt; Faculty &gt; Detail ? ..."/>
<node CREATED="1180470778793" ID="Freemind_Link_1768421030" MODIFIED="1180470788796" TEXT="&lt;html&gt;&lt;img src=&quot;images_SPPD_Professors/irazabal.jpg&quot;&gt;"/>
<node CREATED="1180471309286" MODIFIED="1180471309286" TEXT="Assistant Professor"/>
<node CREATED="1180471329925" MODIFIED="1180471329925" TEXT="Comparative urbanism, urban design and development theory and politics, Latino and Latin American studies"/>
<node CREATED="1180471329927" MODIFIED="1180471329927" TEXT="She has worked as consultant, researcher, and professor in Venezuela, Brazil, Colombia, Mexico, and the USA"/>
<node CREATED="1180471329928" MODIFIED="1180471329928" TEXT="&quot;A Planned City Coming of Age: Rethinking Ciudad Guayana Today,&quot;"/>
<node CREATED="1180471329928" MODIFIED="1180471329928" TEXT="the Venezuelan transition to a socialist participatory democracy."/>
</node>
<node CREATED="1180473108244" FOLDED="true" ID="Freemind_Link_1291119973" MODIFIED="1180473108244" TEXT="Juliet Ann Musso">
<node CREATED="1180473142126" LINK="http://www.usc.edu/schools/sppd/faculty/detail.php?id=24" MODIFIED="1180473142126" TEXT="usc.edu &gt; Schools &gt; Sppd &gt; Faculty &gt; Detail ? ..."/>
<node CREATED="1180473156397" ID="Freemind_Link_1196087398" MODIFIED="1180473169785" TEXT="&lt;html&gt;&lt;img src=&quot;images_SPPD_Professors/musso.jpg&quot;&gt;"/>
<node CREATED="1180473202450" ID="Freemind_Link_1868753009" MODIFIED="1180473202450" TEXT="Associate Professor"/>
<node CREATED="1180473220945" ID="Freemind_Link_33959649" MODIFIED="1180473220945" TEXT="Director, Master of Public Policy Program"/>
<node CREATED="1180473192998" MODIFIED="1180473192998" TEXT="Federalism, urban political economy, fiscal policy, community governance, neighborhood organizations"/>
<node CREATED="1180473192998" MODIFIED="1180473192998" TEXT="Musso, Juliet with Woody Stanley and Chris Weare, &quot;Participation, Deliberative Democracy, and the Internet: Lessons from a National Forum on Commercial Vehicle Safety&quot;; Peter Shane, Ed. Democracy Online: The Prospects for Democratic Renewal through the Internet.; 2004"/>
</node>
</node>
<node COLOR="#669900" CREATED="1180470083733" ID="Freemind_Link_574525246" MODIFIED="1180470565426" POSITION="right" TEXT="Cultural Economy">
<cloud/>
<node CREATED="1180470093256" FOLDED="true" ID="Freemind_Link_480884666" MODIFIED="1180470093256" TEXT="Elizabeth Currid">
<node CREATED="1180470103277" LINK="http://www.usc.edu/schools/sppd/faculty/detail.php?id=53" MODIFIED="1180470103277" TEXT="usc.edu &gt; Schools &gt; Sppd &gt; Faculty &gt; Detail ? ..."/>
<node CREATED="1180470114416" ID="Freemind_Link_521170394" MODIFIED="1180470121803" TEXT="&lt;html&gt;&lt;img src=&quot;images_SPPD_Professors/currid.jpg&quot;&gt;"/>
<node CREATED="1180470135433" MODIFIED="1180470135433" TEXT="Assistant Professor"/>
<node CREATED="1180470155895" ID="Freemind_Link_254139037" MODIFIED="1180470155895" TEXT="Economic development, cultural economy, social networks, urban growth"/>
<node CREATED="1180470155896" MODIFIED="1180470155896" TEXT="(what makes a city fun?)"/>
</node>
</node>
<node COLOR="#669900" CREATED="1180470202466" ID="Freemind_Link_84336042" MODIFIED="1180470335149" POSITION="left" TEXT="Philanthropy, Non-profit">
<cloud/>
<node CREATED="1180470212184" FOLDED="true" ID="Freemind_Link_246795255" MODIFIED="1180470216691" TEXT="James M. Ferris">
<node CREATED="1180470225165" LINK="http://www.usc.edu/schools/sppd/faculty/detail.php?id=9" MODIFIED="1180470225165" TEXT="usc.edu &gt; Schools &gt; Sppd &gt; Faculty &gt; Detail ? ..."/>
<node CREATED="1180470237434" ID="Freemind_Link_1072899919" MODIFIED="1180470245880" TEXT="&lt;html&gt;&lt;img src=&quot;images_SPPD_Professors/jferris.jpg&quot;&gt;"/>
<node CREATED="1180470621074" ID="Freemind_Link_422482367" MODIFIED="1180470654428" TEXT="Professor&#xa;&#xd;Emery Evans Olson Chair in Non-Profit Entrepreneurship and Public Policy&#xa;&#xd;Director, Center on Philanthropy and Public Policy"/>
<node CREATED="1180470265700" ID="Freemind_Link_240028869" MODIFIED="1180470265700" TEXT="Emery Evans Olson Chair in Non-Profit Entrepreneurship and Public Policy"/>
<node CREATED="1180470265706" MODIFIED="1180470265706" TEXT="the causes and consequences of the conversion of nonprofit health care organizations to for-profit status,"/>
</node>
<node CREATED="1180472995410" FOLDED="true" ID="Freemind_Link_15003210" MODIFIED="1180472995410" TEXT="Michael Moody">
<node CREATED="1180473002637" LINK="http://www.usc.edu/schools/sppd/faculty/detail.php?id=22" MODIFIED="1180473002637" TEXT="usc.edu &gt; Schools &gt; Sppd &gt; Faculty &gt; Detail ? ..."/>
<node CREATED="1180473025804" ID="Freemind_Link_764618057" MODIFIED="1180473036355" TEXT="&lt;html&gt;&lt;img src=&quot;images_SPPD_Professors/mmoody.jpg&quot;&gt;"/>
<node CREATED="1180473043801" MODIFIED="1180473043801" TEXT="Assistant Professor"/>
<node CREATED="1180473057269" ID="Freemind_Link_1646572027" MODIFIED="1180473057269" TEXT="Philanthropy, political culture, advocacy, environmental policy, nonprofit organizations"/>
<node CREATED="1180473057270" MODIFIED="1180473057270" TEXT="His recent research paper, &quot;The Construction and Evolution of Venture Philanthropy&quot; is hosted on the CPPP website available for viewing."/>
</node>
</node>
<node COLOR="#669900" CREATED="1180470453257" ID="Freemind_Link_1617507015" MODIFIED="1180470461407" POSITION="right" TEXT="Real Estate">
<cloud/>
<node CREATED="1180470459178" FOLDED="true" ID="Freemind_Link_1981498897" MODIFIED="1180470459178" TEXT="Stuart A. Gabriel">
<node CREATED="1180470472271" LINK="http://www.usc.edu/schools/sppd/faculty/detail.php?id=10" MODIFIED="1180470472271" TEXT="usc.edu &gt; Schools &gt; Sppd &gt; Faculty &gt; Detail ? ..."/>
<node CREATED="1180470489157" ID="Freemind_Link_603409969" MODIFIED="1180470498306" TEXT="&lt;html&gt;&lt;img src=&quot;images_SPPD_Professors/sgabriel.jpg&quot;&gt;"/>
<node CREATED="1180470687094" ID="Freemind_Link_369146444" MODIFIED="1180491370036" TEXT="Professor"/>
<node CREATED="1180470528049" ID="Freemind_Link_1885547775" MODIFIED="1180470558069" TEXT="&lt;html&gt;&#xa;Urban economics, reagional economics, housing markets, mortgage markets, &lt;b&gt;urban quality of life&lt;/b&gt;"/>
<node CREATED="1180470528049" MODIFIED="1180470528049" TEXT="(real estate valuation)"/>
</node>
<node CREATED="1180473278785" FOLDED="true" ID="Freemind_Link_1584470424" MODIFIED="1180473281654" TEXT="Christian L. Redfearn">
<node CREATED="1180473290199" LINK="http://www.usc.edu/schools/sppd/faculty/detail.php?id=31" MODIFIED="1180473290199" TEXT="usc.edu &gt; Schools &gt; Sppd &gt; Faculty &gt; Detail ? ..."/>
<node CREATED="1180473302924" ID="Freemind_Link_1370688865" MODIFIED="1180473312688" TEXT="&lt;html&gt;&lt;img src=&quot;images_SPPD_Professors/redfearn.jpg&quot;&gt;"/>
<node CREATED="1180476433180" MODIFIED="1180476433180" TEXT="Assistant Professor"/>
<node CREATED="1180476444167" MODIFIED="1180476444167" TEXT="International Real Estate"/>
<node CREATED="1180476444167" MODIFIED="1180476444167" TEXT="He is currently involved in both domestic and international research projects, including Swedish housing markets, residential real estate markets in Singapore, and Los Angeles Basin Real Estate Submarket Dynamics."/>
</node>
</node>
<node COLOR="#669900" CREATED="1180476496368" ID="Freemind_Link_359062497" MODIFIED="1180476510190" POSITION="right" TEXT="Globalization and Urban planning">
<cloud/>
<node CREATED="1180476513488" FOLDED="true" ID="Freemind_Link_560781757" MODIFIED="1180476513488" TEXT="Harry W. Richardson">
<node CREATED="1180476525815" LINK="http://www.usc.edu/schools/sppd/faculty/detail.php?id=32" MODIFIED="1180476525815" TEXT="usc.edu &gt; Schools &gt; Sppd &gt; Faculty &gt; Detail ? ..."/>
<node CREATED="1180476541346" ID="Freemind_Link_1045880310" MODIFIED="1180476551287" TEXT="&lt;html&gt;&lt;img src=&quot;images_SPPD_Professors/hrichard.jpg&quot;&gt;"/>
<node CREATED="1180476565452" MODIFIED="1180476565452" TEXT="Professor"/>
<node CREATED="1180476580218" ID="Freemind_Link_1805789063" MODIFIED="1180476580218" TEXT="Globalization, density, urban planning"/>
<node CREATED="1180476580219" MODIFIED="1180476580219" TEXT="Richardson, Harry with P. Gordon, &quot;Sustainable Portland? A Critique, and the Los Angeles Counterpoint&quot;; A. Sorensen, P.J. Marcutullio and J. Grant (eds.), Towards Sustainable Cities: East Asian, North American and European Perspectives on Managing Urban Regions; 2004"/>
<node CREATED="1180476580221" MODIFIED="1180476580221" TEXT="Richardson, H. W., &amp; Bae, C.-H. C. (Eds.), &quot;The impacts of globalization on urban development&quot;; Annals of Regional Science, 37(3).; 2004"/>
</node>
</node>
<node COLOR="#669900" CREATED="1180472841001" FOLDED="true" ID="Freemind_Link_379176623" MODIFIED="1180472956177" POSITION="left" TEXT="Sustainability">
<cloud/>
<node CREATED="1180472858119" ID="Freemind_Link_624529579" MODIFIED="1180472858119" TEXT="Daniel A. Mazmanian">
<node CREATED="1180472865611" LINK="http://www.usc.edu/schools/sppd/faculty/detail.php?id=20" MODIFIED="1180472865611" TEXT="usc.edu &gt; Schools &gt; Sppd &gt; Faculty &gt; Detail ? ..."/>
<node CREATED="1180472879688" ID="Freemind_Link_1010852725" MODIFIED="1180472892337" TEXT="&lt;html&gt;&lt;img src=&quot;images_SPPD_Professors/mazmania.jpg&quot;&gt;"/>
<node CREATED="1180472908811" MODIFIED="1180472908811" TEXT="Professor"/>
<node CREATED="1180472925411" MODIFIED="1180472925411" TEXT="Environmental policy, policy implementation, sustainable communities, political science"/>
<node CREATED="1180472952965" MODIFIED="1180472952965" TEXT="examples (research/background) of his expertise?"/>
</node>
</node>
</node>
</map>
