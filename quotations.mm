<map version="0.8.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1268397162289" ID="Freemind_Link_210365645" LINK="../../Documents/mindmaps/MetaMap.mm" MODIFIED="1268397182682" TEXT="quotations.mm">
<node CREATED="1268397189459" ID="_" MODIFIED="1268397190676" POSITION="right" TEXT="duro">
<node CREATED="1268397191214" ID="Freemind_Link_790764265" MODIFIED="1268397208232" TEXT="Libertarians are anarchists who like money."/>
<node CREATED="1319649928551" ID="Freemind_Link_1863738361" MODIFIED="1319649970539" TEXT="Unless we dream, what is possible becomes limited to what has already been done."/>
</node>
<node CREATED="1268397277310" ID="Freemind_Link_1222176532" MODIFIED="1268397285806" POSITION="right" TEXT="ideas waiting to be quotes">
<node CREATED="1268397208972" ID="Freemind_Link_1239837463" MODIFIED="1268397314789" TEXT="What distinguishes a utopia is that ALL problems are solved."/>
<node CREATED="1292822101657" ID="Freemind_Link_442443286" MODIFIED="1292822109396" TEXT="&quot;Utopia: a place where all problems are solved&quot;"/>
</node>
<node CREATED="1325943558629" ID="Freemind_Link_223089405" MODIFIED="1325943560273" POSITION="left" TEXT="others">
<node CREATED="1325943561104" ID="Freemind_Link_641463548" MODIFIED="1325943564667" TEXT="&quot;First, we make our habits, then our habits make us.&quot; -Charles C. Noble"/>
<node CREATED="1325943606873" ID="Freemind_Link_1808912871" MODIFIED="1325943606873" TEXT="John Kenneth Galbraith">
<node CREATED="1325943619128" MODIFIED="1325943619128" TEXT="&quot;Under capitalism, man exploits man. Under communism, it&apos;s just the opposite.&quot;"/>
</node>
</node>
</node>
</map>
