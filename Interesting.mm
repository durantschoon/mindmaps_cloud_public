<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1381439828408" ID="ID_561240796" LINK="MetaMap.mm" MODIFIED="1385944677018" STYLE="fork" TEXT="Interesting">
<node CREATED="1381439838913" ID="ID_1954760731" MODIFIED="1385944677018" POSITION="right" TEXT="Video">
<node CREATED="1382476947058" ID="ID_1283281153" MODIFIED="1385944677018" TEXT="Explanation">
<node CREATED="1382476954506" ID="ID_997136868" MODIFIED="1385944677019" TEXT="David Deutsch: A new way to explain explanation">
<node CREATED="1382476961394" ID="ID_1435810050" LINK="http://www.ted.com/talks/david_deutsch_a_new_way_to_explain_explanation.html" MODIFIED="1385944677019" TEXT="ted.com &gt; Talks &gt; David deutsch a new way to explain explanation"/>
</node>
</node>
<node CREATED="1381439842257" ID="ID_202046297" MODIFIED="1385944677019" TEXT="Robots">
<node CREATED="1381442664111" ID="ID_1561400458" MODIFIED="1385944677020" TEXT="Baxter">
<node CREATED="1381442678454" ID="ID_1663828761" MODIFIED="1385944677020" TEXT="Rodney Brooks: Why we will rely on robots">
<node CREATED="1381442672962" ID="ID_586170101" LINK="http://www.ted.com/talks/rodney_brooks_why_we_will_rely_on_robots.html" MODIFIED="1385944677020" TEXT="ted.com &gt; Talks &gt; Rodney brooks why we will rely on robots"/>
</node>
</node>
<node CREATED="1381439865825" ID="ID_515903981" MODIFIED="1385944677020" TEXT="Hod Lipson: Robots that are &quot;self-aware&quot;">
<node CREATED="1381439870878" ID="ID_1023986244" LINK="http://www.youtube.com/watch?annotation_id=annotation_247411&amp;feature=iv&amp;src_vid=nA-J0510Pxs&amp;v=lMkHYE9-R0A" MODIFIED="1385944677021" TEXT="youtube.com &gt; Watch ? ..."/>
<node CREATED="1381439964055" ID="ID_1295536539" LINK="http://www.ted.com/talks/hod_lipson_builds_self_aware_robots.html" MODIFIED="1385944677021" TEXT="ted.com &gt; Talks &gt; Hod lipson builds self aware robots"/>
</node>
</node>
<node CREATED="1381439918090" ID="ID_1965726643" MODIFIED="1385944677021" TEXT="Algorithms">
<node CREATED="1381439921683" ID="ID_1737522908" MODIFIED="1385944677022" TEXT="Kevin Slavin: How algorithms shape our world">
<node CREATED="1381439931438" ID="ID_1573176441" LINK="http://www.ted.com/talks/lang/en/kevin_slavin_how_algorithms_shape_our_world.html" MODIFIED="1385944677022" TEXT="ted.com &gt; Talks &gt; Lang &gt; En &gt; Kevin slavin how algorithms shape our world"/>
</node>
</node>
<node CREATED="1381440009757" ID="ID_636471609" MODIFIED="1385944677022" TEXT="Open Source Ecology">
<node CREATED="1381440036078" ID="ID_841484451" MODIFIED="1385944677022" TEXT="Marcin Jakubowski: Open-sourced blueprints for civilization">
<node CREATED="1381440041258" ID="ID_1317820526" LINK="http://www.ted.com/talks/marcin_jakubowski.html" MODIFIED="1385944677023" TEXT="ted.com &gt; Talks &gt; Marcin jakubowski"/>
</node>
</node>
<node CREATED="1381440070367" ID="ID_756207075" MODIFIED="1385944677023" TEXT="Stroke">
<node CREATED="1381440083135" ID="ID_732207389" MODIFIED="1385944677023" TEXT="Jill Bolte Taylor: My stroke of insight">
<node CREATED="1381440088644" ID="ID_1818041514" LINK="http://www.ted.com/talks/jill_bolte_taylor_s_powerful_stroke_of_insight.html" MODIFIED="1385944677023" TEXT="ted.com &gt; Talks &gt; Jill bolte taylor s powerful stroke of insight"/>
</node>
</node>
<node CREATED="1381440125504" ID="ID_910386329" MODIFIED="1385944677024" TEXT="Music &amp; Passion">
<node CREATED="1381440141392" ID="ID_1238942257" MODIFIED="1385944677024" TEXT="Benjamin Zander: The transformative power of classical music">
<node CREATED="1381440148587" ID="ID_1833044537" LINK="http://www.ted.com/talks/benjamin_zander_on_music_and_passion.html" MODIFIED="1385944677024" TEXT="ted.com &gt; Talks &gt; Benjamin zander on music and passion"/>
</node>
</node>
<node CREATED="1381440247763" ID="ID_1365247474" MODIFIED="1385944677024" TEXT="Farming">
<node CREATED="1381440252420" ID="ID_1740762063" MODIFIED="1385944677025" TEXT="Dan Barber: How I fell in love with a fish">
<node CREATED="1381440261487" ID="ID_924391614" LINK="http://www.ted.com/talks/lang/en/dan_barber_how_i_fell_in_love_with_a_fish.html" MODIFIED="1385944677025" TEXT="ted.com &gt; Talks &gt; Lang &gt; En &gt; Dan barber how i fell in love with a fish"/>
<node CREATED="1381440297530" ID="ID_1616432052" MODIFIED="1385944677025" TEXT="See also foie gras">
<node CREATED="1381440314829" ID="ID_186757459" LINK="http://www.ted.com/talks/dan_barber_s_surprising_foie_gras_parable.html" MODIFIED="1385944677025" TEXT="ted.com &gt; Talks &gt; Dan barber s surprising foie gras parable"/>
</node>
</node>
</node>
<node CREATED="1381440442921" ID="ID_481385649" MODIFIED="1386090938633" TEXT="Happiness">
<font BOLD="true" ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1381440449850" ID="ID_1649480894" MODIFIED="1385944677026" TEXT="Matthieu Ricard: The habits of happiness">
<node CREATED="1381440460229" ID="ID_232647337" LINK="http://www.ted.com/talks/matthieu_ricard_on_the_habits_of_happiness.html" MODIFIED="1385944677026" TEXT="ted.com &gt; Talks &gt; Matthieu ricard on the habits of happiness"/>
</node>
</node>
<node CREATED="1381442053114" ID="ID_749172979" MODIFIED="1385944677026" TEXT="Energy">
<node CREATED="1381442075523" ID="ID_1083033784" MODIFIED="1385944677027" TEXT="Saul Griffith: High-altitude wind energy from kites!">
<node CREATED="1381442086494" ID="ID_1745136018" LINK="http://www.ted.com/talks/saul_griffith_on_kites_as_the_future_of_renewable_energy.html" MODIFIED="1385944677027" TEXT="ted.com &gt; Talks &gt; Saul griffith on kites as the future of renewable energy"/>
</node>
</node>
</node>
<node CREATED="1385944637950" HGAP="80" ID="ID_1748304654" LINK="../../Public/duro_public_mindmaps/Beautiful.mm" MODIFIED="1385944691572" POSITION="left" STYLE="bubble" TEXT="Beautiful" VSHIFT="-190">
<arrowlink DESTINATION="ID_1748304654" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Arrow_ID_802290179" STARTARROW="None" STARTINCLINATION="0;0;"/>
<linktarget COLOR="#b0b0b0" DESTINATION="ID_1748304654" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Arrow_ID_802290179" SOURCE="ID_1748304654" STARTARROW="None" STARTINCLINATION="0;0;"/>
</node>
<node CREATED="1383370064844" ID="ID_1418993342" MODIFIED="1385944677027" POSITION="left" TEXT="Lessons">
<node CREATED="1383370067372" ID="ID_1721511282" MODIFIED="1385944677027" TEXT="Autodesk Sustainability">
<node CREATED="1383370075995" LINK="http://www.youtube.com/user/autodeskecoworkshop?feature=results_main" MODIFIED="1385944677027" TEXT="youtube.com &gt; User &gt; Autodeskecoworkshop ? ..."/>
</node>
</node>
</node>
</map>
