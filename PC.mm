<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1492463230767" ID="ID_1398720859" LINK="../../Documents/mindmaps/MetaMap.mm" MODIFIED="1492463260766" TEXT="PC.mm">
<node CREATED="1482989418733" ID="ID_1015680891" MODIFIED="1492463313858" POSITION="right" TEXT="Build a VR/ML PC with Carl">
<node CREATED="1494630779857" ID="ID_1505352118" MODIFIED="1494631562212" TEXT="Remember" VSHIFT="-26">
<node CREATED="1494631562212" ID="ID_459614412" MODIFIED="1494631562214" TEXT=" use Price Rewind for tracking">
<node CREATED="1494630825599" ID="ID_660952026" LINK="https://www.citipricerewind.com/" MODIFIED="1494631565838" TEXT="https://www.citipricerewind.com/"/>
</node>
<node CREATED="1494631568117" ID="ID_1427248962" MODIFIED="1494631588973" TEXT="Try a VR system at work before building this"/>
</node>
<node CREATED="1495674824443" ID="ID_1090839413" MODIFIED="1495674828140" TEXT="my build">
<node CREATED="1497322010391" ID="ID_966566306" LINK="https://pcpartpicker.com/list/rpZbzM" MODIFIED="1497322010391" TEXT="https://pcpartpicker.com/list/rpZbzM"/>
</node>
<node CREATED="1495676229533" ID="ID_706357832" MODIFIED="1495676233521" TEXT="complete VR systems">
<node CREATED="1495676233967" ID="ID_1771648784" LINK="http://www.logicalincrements.com/articles/vrguide" MODIFIED="1495676233967" TEXT="logicalincrements.com &gt; Articles &gt; Vrguide">
<node CREATED="1495685697502" ID="ID_1852011448" MODIFIED="1495685700709" TEXT="Extreme VR Build"/>
</node>
<node CREATED="1495690883945" ID="ID_1321281239" LINK="http://www.logicalincrements.com/" MODIFIED="1495690883945" TEXT="logicalincrements.com">
<node CREATED="1495690897114" ID="ID_1259577119" MODIFIED="1495690899938" TEXT="Extremist"/>
</node>
</node>
<node CREATED="1482989429390" ID="ID_672932958" LINK="https://pcpartpicker.com/guide/" MODIFIED="1482989429390" TEXT="https://pcpartpicker.com/guide/"/>
<node CREATED="1482989439168" ID="ID_1861967558" MODIFIED="1495833494361" TEXT="">
<node CREATED="1482989440806" ID="ID_1494137342" MODIFIED="1495675182141" TEXT="CPU Intel (choose best performance/$)">
<node CREATED="1495675250768" ID="ID_783903624" MODIFIED="1495675251590" TEXT="CPU: Intel Core i7-7700K ">
<node CREATED="1495675257652" LINK="http://www.logicalincrements.com/articles/vrguide" MODIFIED="1495675257652" TEXT="logicalincrements.com &gt; Articles &gt; Vrguide"/>
</node>
</node>
<node CREATED="1482989440807" ID="ID_268178894" MODIFIED="1495675309388" TEXT="CPU Cooler (we won&apos;t overclock, so we don&apos;t need a special one)"/>
<node CREATED="1482989440805" ID="ID_1082083189" MODIFIED="1482989440805" TEXT="750W or 1000W power supply should be fine">
<node CREATED="1495675119247" ID="ID_943933363" LINK="https://www.highgroundgaming.com/best-power-supply-for-gaming/" MODIFIED="1495675119247" TEXT="https://www.highgroundgaming.com/best-power-supply-for-gaming/"/>
<node CREATED="1495675129930" ID="ID_1468928211" MODIFIED="1495675130556" TEXT="EVGA 750 GQ">
<node CREATED="1495675131092" ID="ID_1928728925" MODIFIED="1495675134180" TEXT="G3 OK?"/>
</node>
</node>
<node CREATED="1482989440806" ID="ID_1526072640" MODIFIED="1482989440806" TEXT="Intermediate motherboards are fine, eg. $100">
<node CREATED="1495685053427" ID="ID_828805967" MODIFIED="1495685056898" TEXT="incompatibilities">
<node CREATED="1495685288026" FOLDED="true" ID="ID_1053477813" MODIFIED="1497556363193" TEXT="MSI - Z170A GAMING M5 ATX LGA1151 Motherboard $129.99&#x9;-$30.00&#x9;$2.99&#x9;&#x9;$102.98&#x9;Newegg">
<node CREATED="1495685304497" ID="ID_597953584" MODIFIED="1495685343622" TEXT="Some Intel Z170 chipset motherboards may need a BIOS update prior to using Kaby Lake-S CPUs. Upgrading the BIOS may require a different CPU that is supported by older BIOS revisions. T"/>
<node CREATED="1495685343625" ID="ID_1561274635" MODIFIED="1495685343626" TEXT="he motherboard M.2 slot #0 shares bandwidth with SATA 6.0 Gb/s ports. When the M.2 slot is populated, two SATA 6Gb/s ports are disabled."/>
</node>
<node CREATED="1495685275921" ID="ID_1684969730" MODIFIED="1495685277611" TEXT="warning">
<node CREATED="1495685068442" FOLDED="true" ID="ID_1166503387" MODIFIED="1497556368653" TEXT="MSI - Z270 SLI PLUS ATX LGA1151 Motherboard $139.89&#x9;-$10.00&#x9;&#x9;&#x9;$129.89">
<icon BUILTIN="button_ok"/>
<node CREATED="1495685080081" ID="ID_122314496" MODIFIED="1495685348440" TEXT="The motherboard M.2 slot #0 shares bandwidth with a SATA 6.0 Gb/s port. When the M.2 slot is populated, one SATA 6Gb/s port is disabled. "/>
<node CREATED="1495685348442" ID="ID_614863451" MODIFIED="1495685348443" TEXT="The motherboard M.2 slot #1 shares bandwidth with SATA 6.0 Gb/s ports. When the M.2 slot is populated, two SATA 6Gb/s ports are disabled."/>
</node>
<node CREATED="1495685375457" FOLDED="true" ID="ID_891823843" MODIFIED="1497556396945" TEXT="MSI - Z270 GAMING PRO CARBON ATX LGA1151 Motherboard $173.89&#x9;-$10.00&#x9;&#x9;&#x9;$163.89&#x9;OutletPC">
<node CREATED="1495685080081" ID="ID_1375461092" MODIFIED="1495685348440" TEXT="The motherboard M.2 slot #0 shares bandwidth with a SATA 6.0 Gb/s port. When the M.2 slot is populated, one SATA 6Gb/s port is disabled. "/>
<node CREATED="1495685348442" ID="ID_1841968510" MODIFIED="1495685348443" TEXT="The motherboard M.2 slot #1 shares bandwidth with SATA 6.0 Gb/s ports. When the M.2 slot is populated, two SATA 6Gb/s ports are disabled."/>
</node>
</node>
</node>
</node>
<node CREATED="1482989440807" ID="ID_569916592" MODIFIED="1482989440807" TEXT="Compare DDR4 to DDR3 for RAM (not as important as VRAM)">
<node CREATED="1494630877873" ID="ID_1966435749" MODIFIED="1494630879267" TEXT="Note">
<node CREATED="1494630882289" ID="ID_1144781956" MODIFIED="1494630927346" TEXT="GTX 1080 Ti">
<node CREATED="1494630898074" ID="ID_1216272012" MODIFIED="1495676306789" TEXT="GDDR5X">
<node CREATED="1494630914068" ID="ID_954254280" MODIFIED="1494630915341" TEXT="Video memory: 11GB GDDR5X"/>
<node CREATED="1494630924336" ID="ID_1128825660" LINK="http://www.techradar.com/reviews/nvidia-geforce-gtx-1080-ti" MODIFIED="1494630924336" TEXT="techradar.com &gt; Reviews &gt; Nvidia-geforce-gtx-1080-ti"/>
</node>
</node>
</node>
<node CREATED="1495676416973" ID="ID_1007919582" MODIFIED="1495676421120" TEXT="DDR4-3000"/>
</node>
<node CREATED="1482989440807" ID="ID_485725383" MODIFIED="1482989440807" TEXT="Disk">
<node CREATED="1482989440808" ID="ID_930293743" MODIFIED="1482989440808" TEXT="512GB Solid State"/>
<node CREATED="1482989440808" ID="ID_964849557" MODIFIED="1497733307886" TEXT="4TB HD"/>
</node>
<node CREATED="1482989440809" ID="ID_406466925" MODIFIED="1497556283943" TEXT="Case w/cool lights! Should be large enough for heat dissipation and easier to reconfigure  ">
<node CREATED="1497556315640" FOLDED="true" ID="ID_727519914" MODIFIED="1497556325626" TEXT="motherboard">
<node CREATED="1497556322397" ID="ID_1706807630" LINK="https://www.msi.com/Motherboard/Z270-SLI-PLUS.html#productFeature-section" MODIFIED="1497556324052" TEXT="https://www.msi.com/Motherboard/Z270-SLI-PLUS.html#productFeature-section"/>
<node CREATED="1497556295127" ID="ID_1972783988" MODIFIED="1497556295984" TEXT="MSI&apos;s Mystic Light Extension pinheader provides an intuitive way to control additional RGB strips and other RGB peripherals added to a system, without needing a separate RGB controller. By simply connecting any 12V RGB LED strip to the 4-pin Mystic Light Extension RGB-strip header gamers can sync colors to any style they choose."/>
</node>
</node>
<node CREATED="1482989440809" ID="ID_1433211920" LINK="http://www.phanteks.com/Enthoo-Evolv-ATX.html" MODIFIED="1482989440809" TEXT="phanteks.com &gt; Enthoo-Evolv-ATX">
<node CREATED="1496856648380" FOLDED="true" ID="ID_471345579" MODIFIED="1496856723676" TEXT="Liquid Cooling Basics in The Phanteks EVOLV ATX">
<node CREATED="1496856659159" LINK="https://www.youtube.com/watch?v=FfLJfP0XyaE" MODIFIED="1496856659159" TEXT="https://www.youtube.com/watch?v=FfLJfP0XyaE"/>
</node>
<node CREATED="1496856837494" FOLDED="true" ID="ID_413723473" MODIFIED="1496856851188" TEXT="I think these lights should work">
<node CREATED="1496856850596" LINK="https://www.amazon.com/s/ref=nb_sb_noss_2?url=search-alias%3Daps&amp;field-keywords=PH-LEDkt_m1" MODIFIED="1496856850596" TEXT="https://www.amazon.com/s/ref=nb_sb_noss_2?url=search-alias%3Daps&amp;field-keywords=PH-LEDkt_m1"/>
</node>
<node CREATED="1494631035607" FOLDED="true" ID="ID_595851538" MODIFIED="1496856719834" TEXT="what&apos;s wrong with Coursair?">
<node CREATED="1494631045522" ID="ID_1523523670" MODIFIED="1494631047000" TEXT="Corsair Crystal Series 570X RGB - Tempered Glass, Premium ATX Mid-Tower Case Cases CC-9011098-WW"/>
<node CREATED="1494631039552" ID="ID_1008062652" LINK="https://www.amazon.com/Corsair-Crystal-570X-RGB-CC-9011098-WW/dp/B01LE0ZKR2/ref=sr_1_4?ie=UTF8&amp;qid=1494630967&amp;sr=8-4&amp;keywords=enthoo+evolv+atx+glass" MODIFIED="1494631039552" TEXT="https://www.amazon.com/Corsair-Crystal-570X-RGB-CC-9011098-WW/dp/B01LE0ZKR2/ref=sr_1_4?ie=UTF8&amp;qid=1494630967&amp;sr=8-4&amp;keywords=enthoo+evolv+atx+glass"/>
<node CREATED="1496856714920" ID="ID_275557235" MODIFIED="1496856716951" TEXT="cooling">
<node CREATED="1496856718598" LINK="https://www.amazon.com/Corsair-Extreme-Performance-Liquid-Cooler/dp/B019EXSSBG/ref=pd_bxgy_147_3?_encoding=UTF8&amp;pd_rd_i=B019EXSSBG&amp;pd_rd_r=P2RHG4BH1TY79MY5MK51&amp;pd_rd_w=v3Hsn&amp;pd_rd_wg=r5Ehs&amp;psc=1&amp;refRID=P2RHG4BH1TY79MY5MK51" MODIFIED="1496856718598" TEXT="https://www.amazon.com/Corsair-Extreme-Performance-Liquid-Cooler/dp/B019EXSSBG/ref=pd_bxgy_147_3?_encoding=UTF8&amp;pd_rd_i=B019EXSSBG&amp;pd_rd_r=P2RHG4BH1TY79MY5MK51&amp;pd_rd_w=v3Hsn&amp;pd_rd_wg=r5Ehs&amp;psc=1&amp;refRID=P2RHG4BH1TY79MY5MK51"/>
</node>
</node>
</node>
<node CREATED="1492463420961" ID="ID_387201365" MODIFIED="1492463422900" TEXT="GPU">
<node CREATED="1492463423419" ID="ID_614627823" MODIFIED="1492463425651" TEXT="April 2017">
<node CREATED="1492463501174" ID="ID_306502611" MODIFIED="1492463503319" TEXT="GEFORCE GTX 1080 Ti">
<node CREATED="1492463444295" ID="ID_425369813" MODIFIED="1492463446387" TEXT="$699.99"/>
<node CREATED="1492463436529" ID="ID_740446934" LINK="https://www.nvidia.com/en-us/geforce/products/10series/geforce-gtx-1080-ti/" MODIFIED="1492463518493" TEXT="https://www.nvidia.com/en-us/geforce/products/10series/geforce-gtx-1080-ti/"/>
<node CREATED="1492463519758" ID="ID_320613277" MODIFIED="1492463519758" TEXT="">
<node CREATED="1492463511426" ID="ID_802461364" MODIFIED="1492463512829" TEXT="The GeForce&#xae; GTX 1080 Ti is NVIDIA&apos;s new flagship gaming GPU, based on the NVIDIA Pascal&#x2122; architecture. The latest addition to the ultimate gaming platform, this card is packed with extreme gaming horsepower, next-gen 11 Gbps GDDR5X memory, and a massive 11 GB frame buffer. #GameReady."/>
</node>
<node CREATED="1495674661684" ID="ID_226548240" MODIFIED="1495674666127" TEXT="iCX">
<node CREATED="1495674666991" ID="ID_511150451" MODIFIED="1495674669638" TEXT="fancy cooling">
<node CREATED="1495674669901" ID="ID_1322011629" MODIFIED="1495674671565" TEXT="$70 more"/>
<node CREATED="1495674733368" ID="ID_1620468349" MODIFIED="1495674736114" TEXT="good for overclocking"/>
</node>
<node CREATED="1495674706862" FOLDED="true" ID="ID_1042857113" MODIFIED="1495674731764" TEXT="info">
<node CREATED="1495674690554" ID="ID_555845155" LINK="https://forums.evga.com/What-is-the-difference-between-the-1080-Ti-Black-Edition-and-the-SC-Gaming-Edition-m2636243.aspx" MODIFIED="1495674703495" TEXT="https://forums.evga.com/What-is-the-difference-between-the-1080-Ti-Black-Edition-and-the-SC-Gaming-Edition-m2636243.aspx"/>
<node CREATED="1495674678249" ID="ID_963502147" MODIFIED="1495674679108" TEXT="From Lee&apos;s post:   &quot;The SC Black Edition features everything from the iCX Cooler: Die-cast baseplate/backplate Pin fins, multiple contact points between baseplate and heatsink Pin holes and half-open fins to direct airflow around and through the card. However, the card does not feature the iCX Technology, which means: Fans are not asynchronous (only 1 fan header, not two) Only the Nvidia GPU sensor (standard on all Nvidia cards) Standard PCB, without any of the exclusive iCX PCB features (with the exception of a safety fuse). Think of the GTX 1080 Ti Black Edition this way:  it&apos;s got all of the best new cooling features from iCX, but without the sensors or the asynchronous fans.  If you&apos;re the type of person that isn&apos;t worried about the temperatures around the PCB - especially with the newer cooling features on the iCX Cooler - then you can save a little money and go with this card, compared to the iCX version.  Similarly, the 1080 Ti SC Black Edition uses a reference PCB, which may be important to you if you&apos;re looking for aftermarket cooling, such as watercooling.  If you want the full iCX Technology with the sensors and asynchronous fans, however, then you&apos;ll want to go with the 1080 Ti SC2.&quot;"/>
</node>
</node>
</node>
<node CREATED="1492463590558" ID="ID_417899797" MODIFIED="1494630504224" TEXT="GEFORCE GTX SLI HB BRIDGE">
<node CREATED="1492463598283" ID="ID_1668941415" MODIFIED="1492463598979" TEXT="$ 39.99"/>
<node CREATED="1492463608581" ID="ID_110854183" LINK="https://www.nvidia.com/en-us/geforce/products/10series/geforce-gtx-1080-ti/" MODIFIED="1492463608581" TEXT="https://www.nvidia.com/en-us/geforce/products/10series/geforce-gtx-1080-ti/"/>
</node>
</node>
<node CREATED="1495692138018" ID="ID_780044933" MODIFIED="1495692138705" TEXT="EVGA is nVidia&apos;s personal retail brand so the boards are stock specifications">
<node CREATED="1495692143379" LINK="http://ask.metafilter.com/65904/Which-graphics-card-manufacturer-should-I-choose-and-does-it-make-a-difference" MODIFIED="1495692143379" TEXT="ask.metafilter.com &gt; 65904 &gt; Which-graphics-card-manufacturer-should-I-choose-and-does-it-make-a-difference"/>
</node>
</node>
</node>
<node CREATED="1495833494973" ID="ID_120137289" MODIFIED="1495833497160" TEXT="ordered"/>
<node CREATED="1497321922812" FOLDED="true" ID="ID_30096953" MODIFIED="1497733321362" TEXT="Trouble Shooting">
<node CREATED="1497321927274" LINK="http://www.pcworld.com/article/202038/how_to_troubleshoot_your_home_built_pc.html" MODIFIED="1497321927274" TEXT="pcworld.com &gt; Article &gt; 202038 &gt; How to troubleshoot your home built pc"/>
<node CREATED="1497322071448" ID="ID_1619430840" LINK="http://www.tomshardware.com/answers/id-3404275/msi-z270-sli-leds-flashing-troubleshooting.html" MODIFIED="1497322071448" TEXT="tomshardware.com &gt; Answers &gt; Id-3404275 &gt; Msi-z270-sli-leds-flashing-troubleshooting"/>
<node CREATED="1497548838821" ID="ID_1594647052" MODIFIED="1497548841392" TEXT="Official MSI">
<node CREATED="1497548842323" ID="ID_1103302582" LINK="https://service.msicomputer.com/msi_user/support/troublest.aspx" MODIFIED="1497548842323" TEXT="https://service.msicomputer.com/msi_user/support/troublest.aspx"/>
<node CREATED="1497555719850" ID="ID_474738236" MODIFIED="1497555723774" TEXT="how to contact MSI">
<node CREATED="1497555727757" LINK="https://forum-en.msi.com/index.php?topic=107326.0" MODIFIED="1497555727757" TEXT="https://forum-en.msi.com/index.php?topic=107326.0"/>
</node>
<node CREATED="1497548984828" FOLDED="true" ID="ID_1951601475" MODIFIED="1497555745831" TEXT="Reply detail for Case Number:  TS061517022740">
<node CREATED="1497555744913" LINK="https://service.msicomputer.com/msi_user/support/ts_form.aspx?email=simulus@mindspring.com&amp;case_id=17022740" MODIFIED="1497555744913" TEXT="https://service.msicomputer.com/msi_user/support/ts_form.aspx?email=simulus@mindspring.com&amp;case_id=17022740"/>
</node>
<node CREATED="1497548995687" ID="ID_818518374" MODIFIED="1497548996264" TEXT="MSI Tech.">
<node CREATED="1497549000093" ID="ID_1351757106" MODIFIED="1497549001073" TEXT="06/15/2017">
<node CREATED="1497549008022" ID="ID_1717101377" MODIFIED="1497549008571" TEXT="check and make sure there is no bent pin inside the cpu socket. Please try with clearing the cmos setting on Jbat1 jumper/SW1 Switch or removing the battery on the mb out for 1 minute and put it back in with power off. Please test out the mb and components outside the case with mb box to support the board. This can prevent any short or grounding problem to the mb and the components. Try with swapping other components like the processor/memory/power supply/video card with other working system components to limit down and defective components installed into the system board. Remove all connection/installation of add on cards, drives, and extra cable connection from the mb. Try with bare minimum components installed only. If all steps have been tried and problem still persists then please kindly refer to our website for manufacturer warranty RMA info/request: http://service.msicomputer.com/msi_user/rmaform_p.aspx"/>
</node>
</node>
</node>
<node CREATED="1497395570592" ID="ID_1972237345" MODIFIED="1497395576105" TEXT="debugging my specific errors">
<node CREATED="1497395593599" ID="ID_1328159164" MODIFIED="1497395595084" TEXT="usb device over current status detected will shutdown in 15 seconds">
<node CREATED="1497501566666" ID="ID_275759011" LINK="https://superuser.com/questions/957565/usb-device-overcurrent" MODIFIED="1497501566666" TEXT="https://superuser.com/questions/957565/usb-device-overcurrent"/>
<node CREATED="1497502335530" ID="ID_204853890" MODIFIED="1497502346643" TEXT="unbending pins might fix it">
<node CREATED="1497502347872" LINK="http://expertcomputing.co.uk/blog/2014/11/error-usb-device-current-status-detected-system-will-shut-15-seconds-asus-motherboard-solved/" MODIFIED="1497502347872" TEXT="expertcomputing.co.uk &gt; Blog &gt; 2014 &gt; 11 &gt; Error-usb-device-current-status-detected-system-will-shut-15-seconds-asus-motherboard-solved"/>
</node>
<node CREATED="1497501682278" ID="ID_1751201271" MODIFIED="1497501686157" TEXT="see if I can flip a fuse">
<node CREATED="1497501709926" ID="ID_1570509756" MODIFIED="1497501710977" TEXT="Usually there are fuses on the USB port but their main function is to protect from a overcurrent."/>
<node CREATED="1497501715620" LINK="https://superuser.com/questions/863464/ran-power-through-a-usb-port-am-i-out-of-luck/863494#863494" MODIFIED="1497501715620" TEXT="https://superuser.com/questions/863464/ran-power-through-a-usb-port-am-i-out-of-luck/863494#863494"/>
</node>
<node CREATED="1497502882932" ID="ID_1875650829" MODIFIED="1497502898000" TEXT="maybe worth a shot to replace the CMOS (?) battery">
<node CREATED="1497502902058" LINK="http://www.tomshardware.co.uk/forum/314200-30-device-current-status-detected-shut-seconds?_ga=2.15640318.952612591.1497486503-998827184.1495685415" MODIFIED="1497502902058" TEXT="tomshardware.co.uk &gt; Forum &gt; 314200-30-device-current-status-detected-shut-seconds ? ..."/>
<node CREATED="1497502921948" ID="ID_748070276" MODIFIED="1497502922694" TEXT="So I shut it down with power button and removed the CMOS battery (the small 2032 battery, like 25&#xa2; coin) and put it back, that forced the CMOS to reset and enable the USB ports again. So I turned the computer on again, and... tchan!!! It worked."/>
</node>
</node>
</node>
</node>
<node CREATED="1498024515945" ID="ID_476606488" MODIFIED="1498024520702" TEXT="need optical drive"/>
<node CREATED="1498024522021" ID="ID_457717331" MODIFIED="1498024528426" TEXT="booting from DVD">
<node CREATED="1498024568513" ID="ID_1504471050" LINK="https://www.lifewire.com/how-to-boot-from-a-cd-dvd-or-bd-disc-2626090" MODIFIED="1498024568513" TEXT="https://www.lifewire.com/how-to-boot-from-a-cd-dvd-or-bd-disc-2626090"/>
</node>
<node CREATED="1482989481755" ID="ID_413778438" MODIFIED="1497733430561" TEXT="FPGAs">
<node CREATED="1497733434039" ID="ID_913780920" MODIFIED="1497733437047" TEXT="mining">
<node CREATED="1497733436360" LINK="https://cryptojunction.com/glossary/fpga-mining/" MODIFIED="1497733436360" TEXT="https://cryptojunction.com/glossary/fpga-mining/"/>
</node>
<node CREATED="1482989653820" ID="ID_14063072" MODIFIED="1482989655363" TEXT="">
<node CREATED="1482989485659" ID="ID_604089812" LINK="http://electronics.stackexchange.com/questions/140618/can-fpga-out-perform-a-multi-core-pc" MODIFIED="1482989659913" TEXT="electronics.stackexchange.com &gt; Questions &gt; 140618 &gt; Can-fpga-out-perform-a-multi-core-pc"/>
<node CREATED="1482989518686" ID="ID_1059923802" MODIFIED="1482989665564" TEXT="">
<node CREATED="1482989538305" ID="ID_1514906654" MODIFIED="1482989540782" TEXT="FPGAs would outperform a PC once you had more than about 100 independent, integer tasks that would fit in the FPGA."/>
<node CREATED="1482989522527" ID="ID_1689316631" MODIFIED="1482989545762" TEXT="For floating point tasks GPGPU beat FPGA throughout. "/>
<node CREATED="1482989557420" ID="ID_404466518" MODIFIED="1482989558403" TEXT="For narrow multithreading or SIMD operation then CPUs are extremely optimised and run at a higher clock speed than FPGAs typically achieve."/>
<node CREATED="1482989616920" ID="ID_1374064021" MODIFIED="1482989617487" TEXT="If you have to keep the workload in DRAM then that will be the bottleneck rather than the processor."/>
</node>
<node CREATED="1482989669811" ID="ID_950589926" MODIFIED="1482989669811" TEXT="">
<node CREATED="1482989674156" ID="ID_252834104" MODIFIED="1482989675080" TEXT="For Artificial Neural Networks the FPGA is a great choice. There is a lot of ongoing research in this area."/>
<node CREATED="1482989700446" ID="ID_167705646" MODIFIED="1482989701175" TEXT="FPGA development is often done using languages like Verilog or VHDL which describe behavior rather than implementation--a fact which is sometimes useful but can sometimes greatly complicate the design of asynchronous sequential logic. "/>
</node>
<node CREATED="1482989934123" ID="ID_254930922" MODIFIED="1482989935662" TEXT="Neural Network simulator in FPGA?">
<node CREATED="1482989940203" LINK="http://stackoverflow.com/questions/2190470/neural-network-simulator-in-fpga" MODIFIED="1482989940203" TEXT="stackoverflow.com &gt; Questions &gt; 2190470 &gt; Neural-network-simulator-in-fpga"/>
<node CREATED="1482989959297" ID="ID_1103026882" MODIFIED="1482989961962" TEXT="Verilog">
<node CREATED="1482989999495" ID="ID_715429913" LINK="http://vol.verilog.com/" MODIFIED="1482990051406" TEXT="vol.verilog.com"/>
<node CREATED="1482989969272" ID="ID_995654753" LINK="https://www.amazon.com/dp/0134516753/?tag=stackoverfl08-20" MODIFIED="1482989969272" TEXT="https://www.amazon.com/dp/0134516753/?tag=stackoverfl08-20"/>
</node>
<node CREATED="1482990131423" ID="ID_1429822390" MODIFIED="1482990144431" TEXT="FPGA Implementations&#xa;of Neural Networks">
<node CREATED="1482990133369" ID="ID_1989586202" LINK="http://lab.fs.uni-lj.si/lasin/wp/IMIT_files/neural/doc/Omondi2006.pdf" MODIFIED="1482990133369" TEXT="lab.fs.uni-lj.si &gt; Lasin &gt; Wp &gt; IMIT files &gt; Neural &gt; Doc &gt; Omondi2006"/>
<node CREATED="1482990352881" ID="ID_1032965802" MODIFIED="1482990357137" TEXT="downloaded">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1482990487078" ID="ID_815334773" MODIFIED="1482990488727" TEXT="2006"/>
</node>
</node>
<node CREATED="1482990703336" ID="ID_1151632017" MODIFIED="1482990704726" TEXT="2015">
<node CREATED="1482991070751" ID="ID_1380678634" MODIFIED="1482991072740" TEXT="">
<node CREATED="1482990718050" ID="ID_385755453" LINK="https://pdfs.semanticscholar.org/2ffc/74bec88d8762a613256589891ff323123e99.pdf" MODIFIED="1482990719491" TEXT="https://pdfs.semanticscholar.org/2ffc/74bec88d8762a613256589891ff323123e99.pdf"/>
<node CREATED="1482990710917" ID="ID_1527074860" LINK="http://dl.acm.org/citation.cfm?doid=2684746.2689060" MODIFIED="1482990710917" TEXT="dl.acm.org &gt; Citation ? ..."/>
</node>
<node CREATED="1482991173077" ID="ID_811205375" MODIFIED="1482991173077" TEXT="">
<node CREATED="1482991082229" FOLDED="true" ID="ID_1932088718" MODIFIED="1483897863853" TEXT="DNNWEAVER: &#xa;From High-Level Deep Network Models &#xa;to FPGA Acceleration">
<node CREATED="1482991091960" LINK="http://www.cc.gatech.edu/~hadi/doc/paper/2016-cogarch-dnn_weaver.pdf" MODIFIED="1482991091960" TEXT="cc.gatech.edu &gt; Hadi &gt; Doc &gt; Paper &gt; 2016-cogarch-dnn weaver"/>
<node CREATED="1482991174640" ID="ID_1209111405" MODIFIED="1482991175737" TEXT="In comparison, the generated accelerators deliver significantly superior performance compared to CPU platforms, and superior efficiency compared to GPU platforms; without requiring the programmers to participate in the arduous task of hardware design. These results suggest that DNNWEAVER takes an effective step in making FPGAs available to a broader community of DNN developers who do not often possess hardware expertise."/>
<node CREATED="1482991288565" LINK="http://act-lab.org/artifacts/dnnweaver/" MODIFIED="1482991288565" TEXT="act-lab.org &gt; Artifacts &gt; Dnnweaver"/>
</node>
</node>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1492470897802" ID="ID_684972420" MODIFIED="1492470901507" POSITION="right" TEXT="Pelican Travel Case">
<node CREATED="1492470902450" LINK="http://www.pelican.com/us/en/product/watertight-storm-hard-cases/large-case/travel-case/iM2750/" MODIFIED="1492470902450" TEXT="pelican.com &gt; Us &gt; En &gt; Product &gt; Watertight-storm-hard-cases &gt; Large-case &gt; Travel-case &gt; IM2750"/>
</node>
</node>
</map>
