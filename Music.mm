<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1290727104529" ID="Freemind_Link_1359872242" LINK="../../Documents/mindmaps/MetaMap.mm" MODIFIED="1290727119876" TEXT="Music.mm">
<node CREATED="1290727122849" ID="_" MODIFIED="1290727126381" POSITION="right" TEXT="classical">
<node CREATED="1290727126761" ID="Freemind_Link_260093901" MODIFIED="1290727129349" TEXT="isabelle rucks">
<node CREATED="1290727183200" ID="Freemind_Link_1538605719" MODIFIED="1290727197894" TEXT="Francesco Geminiani &#xa;(1680-1762) - &#xa;Concerto grosso n.12 in re minore &#xa;&quot;La follia&quot; (part one)">
<node CREATED="1290727173806" LINK="http://www.youtube.com/watch?v=OVx-PHFc4HI" MODIFIED="1290727173806" TEXT="youtube.com &gt; Watch ? ..."/>
</node>
<node CREATED="1290727247110" ID="Freemind_Link_315002615" MODIFIED="1290727247809" TEXT="also">
<node CREATED="1290727250254" ID="Freemind_Link_1871816324" MODIFIED="1290727259339" TEXT="Francesco Geminiani &#xa;(1680-1762) - &#xa;Concerto grosso n.12 in re minore &#xa;&quot;La follia&quot; (part two) ">
<node CREATED="1290727264377" LINK="http://www.youtube.com/watch?v=J6ZqvKmnwDI" MODIFIED="1290727264377" TEXT="youtube.com &gt; Watch ? ..."/>
</node>
</node>
</node>
</node>
<node CREATED="1343387873171" HGAP="14" ID="ID_1706231361" MODIFIED="1343387896661" POSITION="left" TEXT="TODO" VSHIFT="-59">
<node CREATED="1343387883658" ID="ID_1764146854" MODIFIED="1343387891366" TEXT="buy music but put it on external drive"/>
<node CREATED="1343387900408" ID="ID_1392265483" MODIFIED="1343387903141" TEXT="after NZ"/>
</node>
<node CREATED="1342246296378" ID="ID_262123809" MODIFIED="1342246297965" POSITION="left" TEXT="pandora">
<node CREATED="1342684224675" ID="ID_597009605" MODIFIED="1342684225959" TEXT="trance">
<node CREATED="1342771578685" FOLDED="true" ID="ID_399725153" MODIFIED="1348654009630" TEXT="Adagio For Strings (Phynn Remix)">
<node CREATED="1342771587325" ID="ID_1416235309" LINK="http://www.pandora.com/tiesto/klub-life/adagio-for-strings-phynn-remix" MODIFIED="1342771587325" TEXT="pandora.com &gt; Tiesto &gt; Klub-life &gt; Adagio-for-strings-phynn-remix"/>
</node>
<node CREATED="1345547479087" FOLDED="true" ID="ID_1514761764" MODIFIED="1348653819605" TEXT="As The Rush Comes (Gabriel &amp; Dresden Chill Out Mix)">
<node CREATED="1345547488068" ID="ID_353237148" LINK="http://www.pandora.com/motorcycle/perfecto-chills-vol-1/as-rush-comes-gabriel-dresden-chill-out-mix" MODIFIED="1345547488068" TEXT="pandora.com &gt; Motorcycle &gt; Perfecto-chills-vol-1 &gt; As-rush-comes-gabriel-dresden-chill-out-mix"/>
</node>
<node CREATED="1345844787127" FOLDED="true" ID="ID_1658688343" MODIFIED="1348653848647" TEXT="Center of the Sun">
<node CREATED="1345844790684" ID="ID_1807139394" LINK="http://www.pandora.com/conjure-1/pure-dance/center-of-sun-junkie-xl-remix" MODIFIED="1345844790684" TEXT="pandora.com &gt; Conjure-1 &gt; Pure-dance &gt; Center-of-sun-junkie-xl-remix"/>
</node>
<node CREATED="1345353959470" FOLDED="true" ID="ID_1891697669" MODIFIED="1348653851384" TEXT="Daydream by Christopher Lawrence">
<node CREATED="1345353966549" ID="ID_1484716502" LINK="http://www.pandora.com/christopher-lawrence/subculture-01/daydream" MODIFIED="1345353966549" TEXT="pandora.com &gt; Christopher-lawrence &gt; Subculture-01 &gt; Daydream"/>
</node>
<node CREATED="1348653868382" FOLDED="true" ID="ID_1047619454" MODIFIED="1348654003735" TEXT="For An Angel (2009) (Filo &amp; Peri Remix)  by Paul Van Dyk on 2009 For An Angel EP">
<node CREATED="1348653998314" ID="ID_1813395884" LINK="http://www.pandora.com/paul-van-dyk/2009-for-angel-ep/for-angel-2009-filo-peri-remix" MODIFIED="1348653998314" TEXT="pandora.com &gt; Paul-van-dyk &gt; 2009-for-angel-ep &gt; For-angel-2009-filo-peri-remix"/>
</node>
<node CREATED="1352446224608" ID="ID_1963459811" MODIFIED="1352446228575" TEXT="Heaven Scent">
<node CREATED="1352446235336" ID="ID_78678640" MODIFIED="1352446235336" TEXT="by Bedrock on Select Singles: Bedrock: Compiled &amp; Mixed By John Digweed"/>
<node CREATED="1352446241352" ID="ID_1883338893" LINK="http://www.pandora.com/bedrock/gatecrasher-classics-2/heaven-scent" MODIFIED="1352446241352" TEXT="pandora.com &gt; Bedrock &gt; Gatecrasher-classics-2 &gt; Heaven-scent"/>
</node>
<node CREATED="1350157652268" FOLDED="true" ID="ID_1224684502" MODIFIED="1350157672053" TEXT="Juice By Gms by Gms on No Rules">
<node CREATED="1350157658867" ID="ID_1963425193" LINK="http://www.pandora.com/gms/no-rules/juice-by-gms" MODIFIED="1350157660339" TEXT="pandora.com &gt; Gms &gt; No-rules &gt; Juice-by-gms"/>
<node CREATED="1350157658866" ID="ID_652494795" MODIFIED="1350157658866" TEXT="faster">
<node CREATED="1350157664732" ID="ID_100144735" MODIFIED="1350157670826" TEXT="Requiem for a Dream?"/>
</node>
</node>
<node CREATED="1350157626012" FOLDED="true" ID="ID_346998776" MODIFIED="1350157636956" TEXT="Mr Tiddles by Sasha on Airdrawndagger">
<node CREATED="1350157632962" ID="ID_189551606" LINK="http://www.pandora.com/sasha/airdrawndagger/mr-tiddles" MODIFIED="1350157635889" TEXT="pandora.com &gt; Sasha &gt; Airdrawndagger &gt; Mr-tiddles"/>
<node CREATED="1350157632961" ID="ID_831866270" MODIFIED="1350157632961" TEXT="relaxed, hypnotic"/>
</node>
<node CREATED="1345016893535" FOLDED="true" ID="ID_1329847760" MODIFIED="1348653841871" TEXT="New York City (Greg Downey Remix) by Paul Van Dyk">
<node CREATED="1345016904120" ID="ID_836692893" LINK="http://www.pandora.com/paul-van-dyk/hands-on-in-between/new-york-city-greg-downey-remix" MODIFIED="1345016904120" TEXT="pandora.com &gt; Paul-van-dyk &gt; Hands-on-in-between &gt; New-york-city-greg-downey-remix"/>
</node>
<node CREATED="1348653800444" FOLDED="true" ID="ID_105729082" MODIFIED="1348653913197" TEXT="Now Is The Time by Darren Tate on Horizons 01">
<node CREATED="1348653912683" ID="ID_1683003326" LINK="http://www.pandora.com/darren-tate/horizons-01/now-is-time" MODIFIED="1348653912683" TEXT="pandora.com &gt; Darren-tate &gt; Horizons-01 &gt; Now-is-time"/>
</node>
<node CREATED="1350157605820" FOLDED="true" ID="ID_223054831" MODIFIED="1350157614292" TEXT="Point Zero by Li Kwan on Creamfields">
<node CREATED="1350157613921" ID="ID_1769730756" LINK="http://www.pandora.com/li-kwan/creamfields/point-0" MODIFIED="1350157613921" TEXT="pandora.com &gt; Li-kwan &gt; Creamfields &gt; Point-0"/>
</node>
<node CREATED="1347880923401" FOLDED="true" ID="ID_1095530929" MODIFIED="1347880933166" TEXT="Ready Steady Go by Paul Oakenfold on Bunkka">
<node CREATED="1347880932111" ID="ID_24207645" LINK="http://www.pandora.com/paul-oakenfold/bunkka/ready-steady-go" MODIFIED="1347880932111" TEXT="pandora.com &gt; Paul-oakenfold &gt; Bunkka &gt; Ready-steady-go"/>
</node>
<node CREATED="1348653931377" FOLDED="true" ID="ID_642311408" MODIFIED="1348653959318" TEXT="Resurrection (Robots Outro)  by PPK on Best Of Chillout Hits">
<node CREATED="1342246307522" ID="ID_1167729777" LINK="http://www.pandora.com/ppk/best-of-chillout-hits/resurrection-robots-outro" MODIFIED="1342246307522" TEXT="pandora.com &gt; Ppk &gt; Best-of-chillout-hits &gt; Resurrection-robots-outro"/>
</node>
<node CREATED="1346319184383" FOLDED="true" ID="ID_365612501" MODIFIED="1348653896914" TEXT="Runaway (Ford Remix) by IIO on Runaway [Maxi-Single]">
<node CREATED="1346319192437" ID="ID_1225712682" LINK="http://www.pandora.com/iio/ultra-dance-06/runaway-ford-remix" MODIFIED="1346319192437" TEXT="pandora.com &gt; Iio &gt; Ultra-dance-06 &gt; Runaway-ford-remix"/>
</node>
<node CREATED="1342684242552" FOLDED="true" ID="ID_128669459" MODIFIED="1348653966886" TEXT="take me away">
<node CREATED="1342684248660" ID="ID_1482992430" LINK="http://www.pandora.com/4-strings/take-me-away-into-night-2009-remixes/take-me-away-into-night-katie-j-remix" MODIFIED="1342684248660" TEXT="pandora.com &gt; 4-strings &gt; Take-me-away-into-night-2009-remixes &gt; Take-me-away-into-night-katie-j-remix"/>
</node>
</node>
<node CREATED="1352446343858" ID="ID_1867698132" MODIFIED="1352446349904" TEXT="trance (mellow)">
<node CREATED="1352446350689" FOLDED="true" ID="ID_1927359905" MODIFIED="1352446375202" TEXT="Nobody Seems To Care &#xa;by 16 Bit Lolitas on Murder Weapon EP">
<node CREATED="1352446374810" ID="ID_495228579" LINK="http://www.pandora.com/16-bit-lolitas/murder-weapon-ep/nobody-seems-to-care" MODIFIED="1352446374810" TEXT="pandora.com &gt; 16-bit-lolitas &gt; Murder-weapon-ep &gt; Nobody-seems-to-care"/>
</node>
</node>
<node CREATED="1342684226410" ID="ID_1784853485" MODIFIED="1342684232812" TEXT="dubstep">
<node CREATED="1342684303337" FOLDED="true" ID="ID_809945070" MODIFIED="1348653813759" TEXT="Blood Red">
<node CREATED="1342684308247" ID="ID_674279388" LINK="http://www.pandora.com/feed-me/feed-mes-big-adventure/blood-red" MODIFIED="1342684308247" TEXT="pandora.com &gt; Feed-me &gt; Feed-mes-big-adventure &gt; Blood-red"/>
</node>
<node CREATED="1342684276384" FOLDED="true" ID="ID_363790645" MODIFIED="1348653815647" TEXT="Caribbean Heat">
<node CREATED="1342684281500" ID="ID_1291672476" LINK="http://www.pandora.com/samples-dance/10-nae-presents-acid-crunk-volume-2/caribbean-heat" MODIFIED="1342684281500" TEXT="pandora.com &gt; Samples-dance &gt; 10-nae-presents-acid-crunk-volume-2 &gt; Caribbean-heat"/>
</node>
<node CREATED="1342684262181" FOLDED="true" ID="ID_1994137879" MODIFIED="1348653808801" TEXT="Eyes On Fire (Zeds Dead Remix)">
<node CREATED="1342684267875" ID="ID_1195783267" LINK="http://www.pandora.com/blue-foundation/sound-of-dubstep-worldwide/eyes-on-fire-zeds-dead-remix" MODIFIED="1342684267875" TEXT="pandora.com &gt; Blue-foundation &gt; Sound-of-dubstep-worldwide &gt; Eyes-on-fire-zeds-dead-remix"/>
</node>
<node CREATED="1342684291183" FOLDED="true" ID="ID_52052764" MODIFIED="1348653809881" TEXT="Frogger">
<node CREATED="1342684295773" ID="ID_1181436566" LINK="http://www.pandora.com/chewie/dubsteppers-for-haiti-volume-1/frogger" MODIFIED="1342684295773" TEXT="pandora.com &gt; Chewie &gt; Dubsteppers-for-haiti-volume-1 &gt; Frogger"/>
</node>
<node CREATED="1352446091285" ID="ID_234724324" MODIFIED="1352446096074" TEXT="Miracle">
<node CREATED="1352446102277" ID="ID_33165539" MODIFIED="1352446102277" TEXT="by Blackmill on Miracle"/>
<node CREATED="1352446107854" ID="ID_580203257" LINK="http://www.pandora.com/blackmill/miracle/miracle" MODIFIED="1352446107854" TEXT="pandora.com &gt; Blackmill &gt; Miracle &gt; Miracle"/>
</node>
<node CREATED="1349424630474" FOLDED="true" ID="ID_1766391329" MODIFIED="1349424648936" TEXT="Predator &#xa;by Chrispy on Predator (Single)">
<node CREATED="1349424647861" ID="ID_1123156211" LINK="http://www.pandora.com/chrispy/predator-single/predator" MODIFIED="1349424647861" TEXT="pandora.com &gt; Chrispy &gt; Predator-single &gt; Predator"/>
</node>
<node CREATED="1349424601301" FOLDED="true" ID="ID_1008097036" MODIFIED="1349424642463" TEXT="Sirius &#xa;by Mimosa on Flux For Life">
<node CREATED="1349424607635" ID="ID_1648148629" LINK="http://www.pandora.com/mimosa/flux-for-life/sirius" MODIFIED="1349424607635" TEXT="pandora.com &gt; Mimosa &gt; Flux-for-life &gt; Sirius"/>
</node>
</node>
<node CREATED="1348647769822" ID="ID_897956712" MODIFIED="1348647770535" TEXT="[arpeggio dub step]">
<node CREATED="1348647780102" FOLDED="true" ID="ID_1869548204" MODIFIED="1348647790930" TEXT="Crystallize by Lindsey Stirling on Crystallize (Single)">
<node CREATED="1348647788770" ID="ID_1699644775" LINK="http://www.pandora.com/lindsey-stirling/crystallize-single/crystallize" MODIFIED="1348647788770" TEXT="pandora.com &gt; Lindsey-stirling &gt; Crystallize-single &gt; Crystallize"/>
</node>
<node CREATED="1352446607566" FOLDED="true" ID="ID_1051777557" MODIFIED="1352446621903" TEXT="Gangsteppin &#xa;by MartyParty on An-Ten-Nae Presents Acid Crunk Volume 2 ">
<node CREATED="1352446621101" ID="ID_1680924630" LINK="http://www.pandora.com/martyparty/10-nae-presents-acid-crunk-volume-2/gangsteppin" MODIFIED="1352446621101" TEXT="pandora.com &gt; Martyparty &gt; 10-nae-presents-acid-crunk-volume-2 &gt; Gangsteppin"/>
</node>
<node CREATED="1352446542797" FOLDED="true" ID="ID_1447393659" MODIFIED="1352446562926" TEXT="Rapid Cognition &#xa;by Phutureprimitive on Kinetik">
<node CREATED="1352446562693" ID="ID_1504790813" LINK="http://www.pandora.com/phutureprimitive/kinetik/rapid-cognition" MODIFIED="1352446562693" TEXT="pandora.com &gt; Phutureprimitive &gt; Kinetik &gt; Rapid-cognition"/>
</node>
<node CREATED="1348647805386" FOLDED="true" ID="ID_31649735" MODIFIED="1352446556278" TEXT="Ripple Effect &#xa;by Phutureprimitive on Kinetik">
<node CREATED="1348647813014" ID="ID_949795769" LINK="http://www.pandora.com/phutureprimitive/kinetik/ripple-effect" MODIFIED="1348647813014" TEXT="pandora.com &gt; Phutureprimitive &gt; Kinetik &gt; Ripple-effect"/>
</node>
<node CREATED="1348647887417" FOLDED="true" ID="ID_1525977195" MODIFIED="1348647894174" TEXT="Fortune Days by The Glitch Mob on Drink The Sea">
<node CREATED="1348647893524" ID="ID_219701203" LINK="http://www.pandora.com/glitch-mob/drink-sea/fortune-days" MODIFIED="1348647893524" TEXT="pandora.com &gt; Glitch-mob &gt; Drink-sea &gt; Fortune-days"/>
</node>
<node CREATED="1350466173021" FOLDED="true" ID="ID_1966384510" MODIFIED="1350466198087" TEXT="In For The Kill (Skrillex Remix)">
<node CREATED="1350466186655" ID="ID_435478446" MODIFIED="1350466186655" TEXT="by La Roux on In For The Kill (Remix Single)"/>
<node CREATED="1350466196236" ID="ID_687936495" LINK="http://www.pandora.com/la-roux/in-for-kill-remix-single/in-for-kill-skrillex-remix" MODIFIED="1350466196236" TEXT="pandora.com &gt; La-roux &gt; In-for-kill-remix-single &gt; In-for-kill-skrillex-remix"/>
</node>
<node CREATED="1352446575134" FOLDED="true" ID="ID_676185264" MODIFIED="1352446588966" TEXT="Insomnia &#xa;by Gladkill on Disciples Of Headtron: Vol. I ">
<node CREATED="1352446586901" ID="ID_1161949706" LINK="http://www.pandora.com/gladkill/disciples-of-headtron-vol-i/insomnia" MODIFIED="1352446586901" TEXT="pandora.com &gt; Gladkill &gt; Disciples-of-headtron-vol-i &gt; Insomnia"/>
</node>
<node CREATED="1350466230205" FOLDED="true" ID="ID_1237721803" MODIFIED="1350466249928" TEXT="Xotica">
<node CREATED="1350466238048" ID="ID_972483590" MODIFIED="1350466240871" TEXT="by Phuture primitive on Kinetik"/>
<node CREATED="1350466249366" ID="ID_666210721" LINK="http://www.pandora.com/phutureprimitive/kinetik/xotica" MODIFIED="1350466249366" TEXT="pandora.com &gt; Phutureprimitive &gt; Kinetik &gt; Xotica"/>
</node>
</node>
</node>
<node CREATED="1343387848077" ID="ID_1826187105" MODIFIED="1343387849906" POSITION="left" TEXT="itunes">
<node CREATED="1343387850317" ID="ID_1206428771" MODIFIED="1343387852882" TEXT="emily portman">
<node CREATED="1343387853156" ID="ID_327259916" MODIFIED="1343387855282" TEXT="see popular">
<node CREATED="1343387855700" ID="ID_1662842047" MODIFIED="1343387857530" TEXT="two sisters"/>
<node CREATED="1343387857844" ID="ID_396473936" MODIFIED="1343387859642" TEXT="stick stock"/>
</node>
<node CREATED="1343387939915" ID="ID_1853280790" MODIFIED="1343387939915" TEXT="Tongue Tied"/>
</node>
</node>
</node>
</map>
