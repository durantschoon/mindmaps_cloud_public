<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1374346632572" ID="ID_867148394" LINK="../../Documents/mindmaps/MetaMap.mm" MODIFIED="1374346640759" TEXT="Math">
<node CREATED="1492473599181" ID="ID_1301919520" MODIFIED="1492473605219" POSITION="right" TEXT="for mathemeticians">
<node CREATED="1491758937137" HGAP="21" ID="ID_1877553222" MODIFIED="1491758955343" TEXT="Numberphile" VSHIFT="-48">
<node CREATED="1491758942625" ID="ID_210799904" MODIFIED="1491758944151" TEXT="best of">
<node CREATED="1491758963061" ID="ID_668437176" MODIFIED="1491758963637" TEXT="The Opposite of Infinity">
<node CREATED="1491758967969" LINK="https://www.youtube.com/watch?v=WYijIV5JrKg" MODIFIED="1491758967969" TEXT="https://www.youtube.com/watch?v=WYijIV5JrKg"/>
</node>
</node>
<node CREATED="1491760461994" ID="ID_229987945" MODIFIED="1491760467380" TEXT="interesting">
<node CREATED="1491760470106" ID="ID_911051720" MODIFIED="1491760475739" TEXT="Billionaire Mathematician ">
<node CREATED="1491760477531" ID="ID_1800317773" MODIFIED="1491760489974" TEXT="John Harris Simmons">
<node CREATED="1491765468166" ID="ID_922665497" MODIFIED="1491765469709" TEXT="funds">
<node CREATED="1491765470293" ID="ID_765817919" MODIFIED="1491765475168" TEXT="Math for America"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1492473608033" ID="ID_1544511920" MODIFIED="1492473609714" TEXT="The Princeton Companion to Mathematics">
<node CREATED="1492473630634" ID="ID_1760403231" MODIFIED="1492473635484" TEXT="book">
<node CREATED="1492473632509" ID="ID_398738860" MODIFIED="1492473634196" TEXT="$99.50">
<node CREATED="1492473616211" ID="ID_1221679928" LINK="http://press.princeton.edu/titles/8350.html" MODIFIED="1492473637293" TEXT="press.princeton.edu &gt; Titles &gt; 8350"/>
</node>
</node>
</node>
</node>
<node CREATED="1374346642987" ID="ID_43142784" MODIFIED="1374346655459" POSITION="right" TEXT="relationship">
<node CREATED="1374373178167" ID="ID_737664133" MODIFIED="1374373191233" TEXT="factorial to trigonometric identities">
<node CREATED="1374373242897" ID="ID_1131491210" MODIFIED="1374373245190" TEXT="think about">
<node CREATED="1374373245544" ID="ID_1584136629" MODIFIED="1374373411482" TEXT="a periodic function can be defined by an infinite sum of sign-alternating (diminishing?) terms">
<node CREATED="1374373413244" ID="ID_212495091" MODIFIED="1374373423389" TEXT="diminishing becuase factorial outpaces exponential"/>
</node>
</node>
</node>
<node CREATED="1374373195435" ID="ID_164695290" MODIFIED="1374373200511" TEXT="weak relationship actually">
<node CREATED="1374346655623" ID="ID_1222388924" MODIFIED="1374346682653" TEXT="binomial coefficients to trigonometic identities">
<node CREATED="1374359381900" ID="ID_1839174233" LINK="http://en.wikipedia.org/wiki/Binomial_theorem#Multiple_angle_identities" MODIFIED="1374359381900" TEXT="en.wikipedia.org &gt; Wiki &gt; Binomial theorem#Multiple angle identities"/>
<node CREATED="1374359426213" ID="ID_836522660" MODIFIED="1374359426882" TEXT="For the complex numbers the binomial theorem can be combined with De Moivre&apos;s formula to yield multiple-angle formulas for the sine and cosine"/>
</node>
<node CREATED="1374359383414" ID="ID_794756038" MODIFIED="1374359392361" TEXT="binomial coefficients to e">
<node CREATED="1374359404317" ID="ID_1321630557" LINK="http://en.wikipedia.org/wiki/Binomial_theorem#Series_for_e" MODIFIED="1374359404317" TEXT="en.wikipedia.org &gt; Wiki &gt; Binomial theorem#Series for e"/>
<node CREATED="1374359725126" ID="ID_1915181407" MODIFIED="1374359743629" TEXT="expanding a function to the nth power means using binomial coefficients for the polynomial expansion"/>
<node CREATED="1374359752065" ID="ID_300584852" MODIFIED="1374359766035" TEXT="in this case lim[n-&gt;inf](1 + 1/n)^n"/>
</node>
</node>
<node CREATED="1374373591087" ID="ID_78535499" MODIFIED="1374373602010" TEXT="factorial to infinite series and derivatives">
<node CREATED="1374373602165" ID="ID_167953275" MODIFIED="1374373608289" TEXT="Taylor Series expansion"/>
</node>
<node CREATED="1374373904972" ID="ID_1040308957" MODIFIED="1374373915705" TEXT="n factorial to pi and e">
<node CREATED="1374373822094" ID="ID_852092184" MODIFIED="1374373829442" TEXT="Stirling&apos;s approximation">
<node CREATED="1374373818127" ID="ID_911077413" MODIFIED="1374373928957" TEXT="n factorial is about sqrt(2pi*n) * n^n * e^-n"/>
</node>
</node>
</node>
<node CREATED="1374346721119" ID="ID_1069325726" MODIFIED="1374346727090" POSITION="right" TEXT="combinatorics">
<node CREATED="1374346803008" ID="ID_261400200" MODIFIED="1374346805124" TEXT="factorials">
<node CREATED="1374346831158" ID="ID_873952713" LINK="https://en.wikipedia.org/wiki/Rising_factorial#Alternate_notations" MODIFIED="1374346831158" TEXT="https://en.wikipedia.org/wiki/Rising_factorial#Alternate_notations"/>
<node CREATED="1374346833618" ID="ID_453008567" MODIFIED="1374347438365" TEXT="for rising/falling, there are k multipliers, &#xa;just start increasing/decreasing">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1374346805288" ID="ID_118009806" MODIFIED="1374346838821" TEXT="rising">
<node CREATED="1374347049145" ID="ID_1761143927" MODIFIED="1374347169503" TEXT="div by k! is multiset coefficient">
<node CREATED="1374347183618" ID="ID_275117637" LINK="https://en.wikipedia.org/wiki/Combination#Number_of_combinations_with_repetition" MODIFIED="1374347183618" TEXT="https://en.wikipedia.org/wiki/Combination#Number_of_combinations_with_repetition"/>
</node>
<node CREATED="1374347288839" ID="ID_1540512403" MODIFIED="1374347304192" TEXT="w/ replacement">
<node CREATED="1374347307659" ID="ID_1014925892" MODIFIED="1374347396123" TEXT="n^k (not-rising) is also order-unimportant"/>
</node>
</node>
<node CREATED="1374346806535" ID="ID_1727086766" MODIFIED="1374346838819" TEXT="falling">
<node CREATED="1374346892146" ID="ID_654210998" MODIFIED="1374346899781" TEXT="same as permutation"/>
<node CREATED="1374347049145" ID="ID_258409809" MODIFIED="1374347056931" TEXT="div by k! is combination"/>
<node CREATED="1374347288839" ID="ID_43642476" MODIFIED="1374347291243" TEXT="w/o replacement"/>
</node>
</node>
</node>
</node>
<node CREATED="1476421802300" ID="ID_1846576226" MODIFIED="1476421804949" POSITION="right" TEXT="Probability">
<node CREATED="1476421773549" ID="ID_194792014" LINK="http://www.nature.com/nmeth/journal/v12/n4/images/nmeth.3335-F1.jpg" MODIFIED="1476421773549" TEXT="nature.com &gt; Nmeth &gt; Journal &gt; V12 &gt; N4 &gt; Images &gt; Nmeth.3335-F1"/>
</node>
<node CREATED="1374372905383" ID="ID_82516465" MODIFIED="1374372906795" POSITION="right" TEXT="series">
<node CREATED="1374372907046" ID="ID_10291000" MODIFIED="1374372908611" TEXT="infinite">
<node CREATED="1374372909254" ID="ID_1053676502" MODIFIED="1374372911114" TEXT="see ch 2">
<node CREATED="1374372911785" ID="ID_1447051822" LINK="http://www.physics.miami.edu/~nearing/mathmethods/mathematical_methods-three.pdf" MODIFIED="1374372911785" TEXT="physics.miami.edu &gt; Nearing &gt; Mathmethods &gt; Mathematical methods-three"/>
</node>
<node CREATED="1374373118319" ID="ID_1404059395" MODIFIED="1374373120179" TEXT="geometric"/>
<node CREATED="1374373123645" ID="ID_1311100265" MODIFIED="1374373125322" TEXT="e^x"/>
<node CREATED="1374373128116" ID="ID_300770826" MODIFIED="1374373131449" TEXT="sin(x)"/>
<node CREATED="1374373131659" ID="ID_955389952" MODIFIED="1374373133161" TEXT="cos(x)"/>
<node CREATED="1374373140380" ID="ID_995904342" MODIFIED="1374373146102" TEXT="ln(1+x)"/>
<node CREATED="1374373152183" ID="ID_1285042652" MODIFIED="1374373156276" TEXT="(1+x)^a"/>
<node CREATED="1376174311446" ID="ID_207741860" MODIFIED="1494703846916" TEXT="visual proofs">
<arrowlink DESTINATION="ID_1102424107" ENDARROW="Default" ENDINCLINATION="155;0;" ID="Arrow_ID_1898067366" STARTARROW="None" STARTINCLINATION="155;0;"/>
<node CREATED="1376174347732" ID="ID_1361315223" MODIFIED="1376174355650" TEXT="f = 1/(1-x)">
<node CREATED="1376174356514" ID="ID_1419025961" LINK="http://www41.homepage.villanova.edu/robert.styer/Bouncingball/geomet1.gif" MODIFIED="1376174356514" TEXT="www41.homepage.villanova.edu &gt; Robert.styer &gt; Bouncingball &gt; Geomet1"/>
</node>
</node>
</node>
<node CREATED="1374373518803" ID="ID_1901265096" MODIFIED="1374373520433" TEXT="Taylor"/>
<node CREATED="1374373743735" ID="ID_586003884" MODIFIED="1374373750647" TEXT="convergence">
<node CREATED="1374373750648" ID="ID_596429019" MODIFIED="1374373751771" TEXT="tests"/>
</node>
</node>
<node CREATED="1374360698938" ID="ID_723368355" MODIFIED="1374369033058" POSITION="left">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      weird <b>formulas</b>&#xa0;for common values
    </p>
  </body>
</html></richcontent>
<node CREATED="1374360701617" ID="ID_1315614581" MODIFIED="1374360702774" TEXT="e"/>
<node CREATED="1374368993238" FOLDED="true" ID="ID_643112595" MODIFIED="1493661773665" TEXT="pi">
<node CREATED="1374369038253" ID="ID_679235073" MODIFIED="1374369039048" TEXT="Vi&#xe8;te formula">
<node CREATED="1374369044534" ID="ID_1013287991" LINK="http://en.wikipedia.org/wiki/Vi%C3%A8te" MODIFIED="1374369044534" TEXT="en.wikipedia.org &gt; Wiki &gt; Vi%C3%A8te&apos;s formula"/>
</node>
</node>
<node CREATED="1374360703113" ID="ID_604252341" MODIFIED="1374360705102" TEXT="sin(x)"/>
<node CREATED="1374360705312" ID="ID_1997759957" MODIFIED="1374360706797" TEXT="cos(x)"/>
</node>
<node CREATED="1375595196889" ID="ID_1575092174" LINK="../../Public/duro_public_mindmaps/LinearAlgebra.mm" MODIFIED="1376174071162" POSITION="left" STYLE="bubble" TEXT="LinearAlgebra">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1475439540696" ID="ID_1559303614" MODIFIED="1492025439357" STYLE="fork" TEXT="Currently contains Geometric Algebra&#xa;(Grassman Algebras)">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1492253041703" ID="ID_271250098" MODIFIED="1492253043746" POSITION="left" TEXT="Tensors">
<node CREATED="1493589769164" ID="ID_1417726541" MODIFIED="1493589858404" TEXT="Great, simple, visual explanation of Tensors">
<node CREATED="1493589831140" ID="ID_1163454146" MODIFIED="1493589842357" TEXT="What&apos;s a Tensor? "/>
<node CREATED="1493589842358" ID="ID_1570582697" MODIFIED="1493589842358" TEXT="Dan Fleisch"/>
<node CREATED="1492253047302" ID="ID_1211116196" LINK="https://www.youtube.com/watch?v=f5liqUk0ZTw" MODIFIED="1493589872500" STYLE="fork" TEXT="https://www.youtube.com/watch?v=f5liqUk0ZTw">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
<node CREATED="1503778898353" ID="ID_937229454" MODIFIED="1503778903314" TEXT="Another Tensor explanation">
<node CREATED="1503778904044" LINK="https://www.youtube.com/watch?v=CliW7kSxxWU" MODIFIED="1503778904044" TEXT="https://www.youtube.com/watch?v=CliW7kSxxWU"/>
<node CREATED="1503779066307" ID="ID_1718712866" MODIFIED="1503779071774" TEXT="good for understanding">
<node CREATED="1503779072315" ID="ID_327307953" MODIFIED="1503779075974" TEXT="covariance"/>
<node CREATED="1503779076508" ID="ID_176216881" MODIFIED="1503779078720" TEXT="contravariance"/>
<node CREATED="1503779078960" ID="ID_1581187109" MODIFIED="1503779079744" TEXT="rank"/>
</node>
</node>
<node CREATED="1494015042808" ID="ID_221719024" MODIFIED="1494015057968" TEXT="1:01:38">
<node CREATED="1494015047527" ID="ID_20344803" MODIFIED="1494015048308" TEXT="What Tensors Are For!">
<node CREATED="1494015052657" LINK="https://www.youtube.com/watch?v=e0eJXttPRZI" MODIFIED="1494015052657" TEXT="https://www.youtube.com/watch?v=e0eJXttPRZI"/>
</node>
</node>
<node CREATED="1494730801412" ID="ID_1814668822" MODIFIED="1494730804464" TEXT="Pavel Grinfeld">
<node CREATED="1494730805212" LINK="https://en.wikipedia.org/wiki/Pavel_Grinfeld" MODIFIED="1494730805212" TEXT="https://en.wikipedia.org/wiki/Pavel_Grinfeld"/>
<node CREATED="1494703870511" ID="ID_490379055" LINK="https://www.youtube.com/watch?v=e0eJXttPRZI" MODIFIED="1494703870511" TEXT="https://www.youtube.com/watch?v=e0eJXttPRZI"/>
</node>
</node>
<node CREATED="1478674061589" ID="ID_809655146" MODIFIED="1478674068333" POSITION="left" TEXT="Differential Geometry">
<node CREATED="1478674087521" ID="ID_550996477" LINK="https://en.wikipedia.org/wiki/Differential_geometry" MODIFIED="1478674090413" TEXT="https://en.wikipedia.org/wiki/Differential_geometry"/>
<node CREATED="1478674070123" ID="ID_1493615756" LINK="https://en.wikipedia.org/wiki/Pushforward_" MODIFIED="1478674070123" TEXT="https://en.wikipedia.org/wiki/Pushforward_(differential)"/>
<node CREATED="1478674115173" LINK="https://en.wikipedia.org/wiki/Differential_geometry#Lie_groups" MODIFIED="1478674115173" TEXT="https://en.wikipedia.org/wiki/Differential_geometry#Lie_groups"/>
</node>
<node CREATED="1478986885993" FOLDED="true" ID="ID_1877312098" MODIFIED="1478986914632" POSITION="left" TEXT="polynomials  ">
<node CREATED="1478986902011" ID="ID_1563976366" MODIFIED="1478986906984" TEXT="For each N points in a plane there exists a polynomial of degree at most N-1, which intersects them."/>
</node>
<node CREATED="1466360318662" ID="ID_166827300" MODIFIED="1466363219579" POSITION="left" TEXT="Number Theory">
<node CREATED="1466360322513" ID="ID_1786524258" MODIFIED="1478986927846" TEXT="P-adic Numbers">
<node CREATED="1466360479267" ID="ID_1107291449" MODIFIED="1466360480582" TEXT="njwildberger">
<node CREATED="1466360340517" ID="ID_1588498903" MODIFIED="1466360484808" TEXT="Repeating Decimals&#xa;&quot;Reversimals&quot;&#xa;and Euler&apos;s infinite series = 0">
<node CREATED="1466360513079" ID="ID_202557697" LINK="https://www.youtube.com/watch?v=XXRwlo_MHnI" MODIFIED="1466360513079" TEXT="https://www.youtube.com/watch?v=XXRwlo_MHnI"/>
<node CREATED="1466360552739" ID="ID_1642681890" MODIFIED="1466360557164" TEXT="12:30">
<node CREATED="1466360567087" ID="ID_1717185943" MODIFIED="1466360579514" TEXT="Double Infiinity Identity"/>
</node>
<node CREATED="1466360752622" ID="ID_1075477790" MODIFIED="1466360755005" TEXT="16:25">
<node CREATED="1466360755360" ID="ID_1730859042" MODIFIED="1466360764356" TEXT="Consequences">
<node CREATED="1466360765110" ID="ID_333639638" MODIFIED="1466360804489" TEXT="Part to the left of the decimal = - part to the right">
<node CREATED="1466360785355" ID="ID_768506620" MODIFIED="1466360794392" TEXT="Because the sum is zero"/>
<node CREATED="1466360808384" ID="ID_1883436202" MODIFIED="1466360872585" TEXT="NOTE: Of course you could split it anywhere.&#xa;The decimal point is arbitrary, isn&apos;t it."/>
</node>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1485665274354" ID="ID_1054847849" MODIFIED="1485665276050" TEXT="Permutohedron">
<node CREATED="1485665267709" ID="ID_2855120" LINK="https://en.wikiversity.org/wiki/Symmetric_group_S4#Lattice_of_subgroups" MODIFIED="1485665267709" TEXT="https://en.wikiversity.org/wiki/Symmetric_group_S4#Lattice_of_subgroups"/>
</node>
</node>
<node CREATED="1503777206131" ID="ID_1235741409" MODIFIED="1503777213485" POSITION="left" TEXT="Lattices">
<node CREATED="1503777214656" ID="ID_623672527" MODIFIED="1503777220353" TEXT="Distributive Lattice">
<node CREATED="1503777220663" ID="ID_608844108" MODIFIED="1503777224600" TEXT="Cool Image"/>
<node CREATED="1503777230022" LINK="https://www.youtube.com/watch?v=Z41WZXeh2yY" MODIFIED="1503777230022" TEXT="https://www.youtube.com/watch?v=Z41WZXeh2yY"/>
</node>
<node CREATED="1503927790303" ID="ID_468347236" MODIFIED="1503927800302" TEXT="are posets?">
<node CREATED="1503927800605" ID="ID_1639770606" MODIFIED="1503927805831" TEXT="partially ordered sets"/>
<node CREATED="1503927813750" ID="ID_724632715" MODIFIED="1503927816283" TEXT="57 min">
<node CREATED="1503927809170" ID="ID_1117596297" LINK="https://www.youtube.com/watch?v=qPtGlrb_sXg" MODIFIED="1503927809170" TEXT="https://www.youtube.com/watch?v=qPtGlrb_sXg"/>
<node CREATED="1503927819637" ID="ID_971032798" MODIFIED="1503927821008" TEXT="IIT"/>
</node>
</node>
</node>
<node CREATED="1471580012696" ID="ID_367350983" MODIFIED="1473220779688" POSITION="left" TEXT="Mizar">
<node CREATED="1471580031510" FOLDED="true" ID="ID_627251296" MODIFIED="1473220801627" TEXT="15A75">
<node CREATED="1471580035685" ID="ID_1882628990" LINK="http://wiki.mizar.org/twiki/bin/view/Mizar/15A" MODIFIED="1473220779694" TEXT="wiki.mizar.org &gt; Twiki &gt; Bin &gt; View &gt; Mizar &gt; 15A">
<node CREATED="1471580018114" ID="ID_899669727" LINK="http://wiki.mizar.org/twiki/bin/edit/Mizar/15A75?twiki_redirect_cache=09bead2e251617153a3a050826e3ecd6" MODIFIED="1471580039569" TEXT="wiki.mizar.org &gt; Twiki &gt; Bin &gt; Edit &gt; Mizar &gt; 15A75 ? ..."/>
</node>
</node>
<node CREATED="1471580081497" FOLDED="true" ID="ID_1511013501" MODIFIED="1473220800876" TEXT="emacs">
<node CREATED="1471580083632" LINK="http://wiki.mizar.org/twiki/bin/view/Mizar/MizarMode" MODIFIED="1471580083632" TEXT="wiki.mizar.org &gt; Twiki &gt; Bin &gt; View &gt; Mizar &gt; MizarMode"/>
<node CREATED="1471580116450" ID="ID_263863916" MODIFIED="1473220779701" TEXT="pdf">
<node CREATED="1471580113962" ID="ID_1656195978" LINK="http://jurban.github.io/mizarmode/doc/mizmode.pdf" MODIFIED="1471580113962" TEXT="jurban.github.io &gt; Mizarmode &gt; Doc &gt; Mizmode"/>
</node>
</node>
</node>
<node CREATED="1471761237935" ID="ID_415675469" MODIFIED="1471761240236" POSITION="right" TEXT="dimensions">
<node CREATED="1471761278029" ID="ID_191771747" LINK="http://www.georgehart.com/research/multanal.html" MODIFIED="1471761278029" TEXT="georgehart.com &gt; Research &gt; Multanal">
<node CREATED="1471761278827" ID="ID_1060563484" MODIFIED="1471761281859" TEXT="from">
<node CREATED="1471761288351" LINK="http://blog.sigfpe.com/2016/08/dimensionful-matrices.html" MODIFIED="1471761288351" TEXT="blog.sigfpe.com &gt; 2016 &gt; 08 &gt; Dimensionful-matrices"/>
</node>
</node>
</node>
<node CREATED="1473220748180" ID="ID_1897249224" MODIFIED="1473220757843" POSITION="right" TEXT="Homotopy Type Theory (HoTT)">
<node CREATED="1473220761794" LINK="https://en.wikipedia.org/wiki/Homotopy_type_theory" MODIFIED="1473220761794" TEXT="https://en.wikipedia.org/wiki/Homotopy_type_theory"/>
<node CREATED="1473307633737" ID="ID_893142723" LINK="file:/Users/durantschoon/DocumentsOthers/HoTT/Homotopy-Type-Theory_Univalent-Foundations-of-Mathematics.pdf" MODIFIED="1473307672238" TEXT="Homotopy-Type-Theory_Univalent-Foundations-of-Mathematics.pdf"/>
</node>
<node CREATED="1494703826137" ID="ID_1153287559" MODIFIED="1494703827699" POSITION="right" TEXT="Proofs">
<node CREATED="1494703837930" ID="ID_1102424107" MODIFIED="1494729513861" TEXT="visual proofs">
<linktarget COLOR="#b0b0b0" DESTINATION="ID_1102424107" ENDARROW="Default" ENDINCLINATION="155;0;" ID="Arrow_ID_1898067366" SOURCE="ID_207741860" STARTARROW="None" STARTINCLINATION="155;0;"/>
<node CREATED="1494703864976" ID="ID_106509384" MODIFIED="1494729516435" TEXT="seen in">
<node CREATED="1494703870511" ID="ID_1631474940" LINK="https://www.youtube.com/watch?v=e0eJXttPRZI" MODIFIED="1494703870511" TEXT="https://www.youtube.com/watch?v=e0eJXttPRZI"/>
<node CREATED="1494704443591" ID="ID_1505980850" MODIFIED="1494729526172" TEXT="geometric">
<node CREATED="1494703848207" ID="ID_425915554" MODIFIED="1494729520022" TEXT="Heron&apos;s Problem" VSHIFT="22"/>
<node CREATED="1494729562639" ID="ID_312784230" MODIFIED="1494729576373" TEXT="Torricelli Problem"/>
<node CREATED="1494729528489" ID="ID_134855762" MODIFIED="1494729538728" TEXT="Steiner&apos;s Proof"/>
</node>
</node>
</node>
</node>
<node CREATED="1464654194329" ID="ID_877173513" MODIFIED="1483945526457" POSITION="left" TEXT="Category Theory">
<node CREATED="1464463133933" ID="ID_1374824667" LINK="http://ocw.mit.edu/courses/mathematics/18-s996-category-theory-for-scientists-spring-2013/" MODIFIED="1464463133933" TEXT="ocw.mit.edu &gt; Courses &gt; Mathematics &gt; 18-s996-category-theory-for-scientists-spring-2013"/>
<node CREATED="1464654212147" LINK="https://en.wikibooks.org/wiki/Haskell/Category_theory" MODIFIED="1464654212147" TEXT="https://en.wikibooks.org/wiki/Haskell/Category_theory"/>
<node CREATED="1464654530694" ID="ID_1673395484" LINK="http://category-theory.mitpress.mit.edu/" MODIFIED="1464654530694" TEXT="category-theory.mitpress.mit.edu"/>
<node CREATED="1464654612704" ID="ID_1407667472" MODIFIED="1474833827604" TEXT="Physics, Topology, Logic and Computation: A Rosetta Stone">
<node CREATED="1464654604814" ID="ID_501858723" LINK="http://arxiv.org/abs/0903.0340" MODIFIED="1464654604814" TEXT="arxiv.org &gt; Abs &gt; 0903"/>
</node>
</node>
<node CREATED="1471316782298" ID="ID_964122650" MODIFIED="1471316786059" POSITION="right" TEXT="Theorem Proving">
<node CREATED="1471316792710" ID="ID_1190040324" MODIFIED="1471316795572" TEXT="deep learning">
<node CREATED="1471316801181" LINK="https://arxiv.org/pdf/1606.04442v1.pdf" MODIFIED="1471316801181" TEXT="https://arxiv.org/pdf/1606.04442v1.pdf"/>
</node>
<node CREATED="1483945572881" ID="ID_50832952" MODIFIED="1483945577196" TEXT="languages/systems">
<node CREATED="1464654261649" ID="ID_1606380866" LINK="https://en.wikipedia.org/wiki/Coq" MODIFIED="1483945580162" TEXT="https://en.wikipedia.org/wiki/Coq"/>
<node CREATED="1483945578256" ID="ID_1554482511" LINK="https://en.wikipedia.org/wiki/Agda_" MODIFIED="1483945578256" TEXT="https://en.wikipedia.org/wiki/Agda_(programming_language)"/>
</node>
<node CREATED="1497587438195" ID="ID_1894310747" MODIFIED="1497587439250" TEXT="Formal Reasoning About Programs">
<node CREATED="1497587663075" ID="ID_501473960" MODIFIED="1497587664411" TEXT="Adam Chlipala">
<node CREATED="1497587669831" LINK="http://adam.chlipala.net/" MODIFIED="1497587669831" TEXT="adam.chlipala.net"/>
</node>
<node CREATED="1497587500040" ID="ID_977769387" LINK="http://adam.chlipala.net/frap/" MODIFIED="1497587502404" TEXT="adam.chlipala.net &gt; Frap"/>
<node CREATED="1497587443925" LINK="https://frap.csail.mit.edu/main" MODIFIED="1497587443925" TEXT="https://frap.csail.mit.edu/main"/>
<node CREATED="1497587483727" ID="ID_1501866596" LINK="https://github.com/mit-frap/spring17" MODIFIED="1497587485052" TEXT="https://github.com/mit-frap/spring17"/>
<node CREATED="1497587454335" ID="ID_1180055205" MODIFIED="1497587455229" TEXT="coq"/>
</node>
<node CREATED="1511879651310" ID="ID_278242818" MODIFIED="1511879652592" TEXT="MIT">
<node CREATED="1492471627311" ID="ID_327410818" MODIFIED="1511881044638" TEXT="Mathematics for Computer Science">
<node CREATED="1492471636957" ID="ID_1396989593" MODIFIED="1492475564154" TEXT="MIT OCW"/>
<node CREATED="1492471654306" ID="ID_1974752300" MODIFIED="1492471656594" TEXT="See also PDF"/>
<node CREATED="1511879667234" ID="ID_1335243242" LINK="https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-042j-mathematics-for-computer-science-spring-2015/" MODIFIED="1511881050926" TEXT="6.042J / 18.062J&#xa;Fall 2015">
<node CREATED="1511880976998" MODIFIED="1511880976998" TEXT="1. Fundamental concepts of mathematics: Definitions, proofs, sets, functions, relations."/>
<node CREATED="1511880977000" MODIFIED="1511880977000" TEXT="2. Discrete structures: graphs, state machines, modular arithmetic, counting."/>
<node CREATED="1511880977001" ID="ID_833925410" MODIFIED="1511880977001" TEXT="3. Discrete probability theory."/>
<node CREATED="1511881263618" ID="ID_256341428" MODIFIED="1511881264744" TEXT="Adam Chlipala">
<node CREATED="1511881288872" LINK="http://adam.chlipala.net/" MODIFIED="1511881288872" TEXT="adam.chlipala.net"/>
<node CREATED="1511881299299" ID="ID_1421137847" MODIFIED="1511881300584" TEXT="fiat">
<node CREATED="1511881304879" LINK="http://plv.csail.mit.edu/fiat/" MODIFIED="1511881304879" TEXT="plv.csail.mit.edu &gt; Fiat"/>
<node CREATED="1511881310466" ID="ID_870790208" MODIFIED="1511881311275" TEXT="coq"/>
</node>
</node>
</node>
</node>
</node>
</node>
</node>
</map>
