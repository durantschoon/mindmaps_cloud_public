<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1374044023062" ID="ID_1424952079" LINK="../../Documents/mindmaps/MetaMap.mm" MODIFIED="1374044041276" TEXT="UltraLearning">
<node CREATED="1373957180312" ID="ID_1472618304" LINK="http://calnewport.com/blog/2012/10/26/mastering-linear-algebra-in-10-days-astounding-experiments-in-ultra-learning/" MODIFIED="1513980517528" POSITION="left" TEXT="Mastering Linear Algebra in 10 Days:&#xa;Ultra-Learning">
<node CREATED="1374044084522" ID="ID_771334107" MODIFIED="1374044085755" TEXT="Scott Young"/>
</node>
<node CREATED="1374044146449" ID="ID_738978022" MODIFIED="1374044150858" POSITION="right" TEXT="Instead of memorizing, I had to find a way to speed up the process of understanding itself.  ">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1374044173707" ID="ID_1274622251" MODIFIED="1374044246638" POSITION="right" TEXT="Getting insights to deepen your understanding &#xa;largely amounts to two things:">
<node CREATED="1374044773036" ID="ID_557033268" MODIFIED="1374044774770" TEXT="1">
<node CREATED="1374044186789" ID="ID_290741905" MODIFIED="1374044191799" TEXT="Making connections"/>
</node>
<node CREATED="1374044777364" ID="ID_1967077677" MODIFIED="1374044777882" TEXT="2">
<node CREATED="1374044191799" FOLDED="true" ID="ID_1582564765" MODIFIED="1513980446860" TEXT="Debugging errors">
<node CREATED="1374044237999" ID="ID_266328628" MODIFIED="1374044262208" TEXT="Debugging errors is also important because &#xa;often you make mistakes because &#xa;you&#x2019;re missing knowledge &#xa;or have an incorrect picture."/>
<node CREATED="1374044285741" ID="ID_1875682961" MODIFIED="1374340074078" TEXT="If you can debug yourself in an efficient way, &#xa;you can greatly accelerate the learning process."/>
</node>
</node>
</node>
<node CREATED="1374044318382" ID="ID_1916218613" MODIFIED="1374044320442" POSITION="right" TEXT="Methods">
<node CREATED="1374044322246" FOLDED="true" ID="ID_825117249" MODIFIED="1513980450702" TEXT="The Drilldown Method: &#xa;A Strategy for Learning Faster">
<node CREATED="1374044407815" ID="ID_620588897" MODIFIED="1374281443455" TEXT="Coverage">
<node CREATED="1374044423388" ID="ID_1334755028" MODIFIED="1374044424200" TEXT="get a general sense of what you need to learn."/>
<node CREATED="1374044447598" ID="ID_1927876857" MODIFIED="1374044456791" TEXT="For self-learning it might mean ">
<node CREATED="1374044456791" ID="ID_506509002" MODIFIED="1374044462465" TEXT="reading several books on the topic and "/>
<node CREATED="1374044462465" ID="ID_410532000" MODIFIED="1374044462466" TEXT="doing research."/>
</node>
<node CREATED="1374044516300" ID="ID_1305326126" MODIFIED="1374281164903" TEXT="coverage is the least efficient stage">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1374044528700" ID="ID_1397322319" MODIFIED="1374340090831" TEXT="low learning per unit time"/>
<node CREATED="1374044543532" ID="ID_73732265" MODIFIED="1374044546514" TEXT="speed this up"/>
<node CREATED="1374044546740" ID="ID_1457588315" MODIFIED="1374044552657" TEXT="spend more time on next two">
<node CREATED="1374044642526" ID="ID_457421313" MODIFIED="1374044658216" TEXT="watch videos with VLC">
<node CREATED="1374044648919" ID="ID_580865940" MODIFIED="1374044649868" TEXT="at 1.5x or 2x"/>
</node>
</node>
</node>
<node CREATED="1374044679474" ID="ID_779996630" MODIFIED="1374044680224" TEXT="NO">
<node CREATED="1374044680522" ID="ID_1537972438" MODIFIED="1374044682055" TEXT="highlighting"/>
</node>
<node CREATED="1374044682834" ID="ID_290895588" MODIFIED="1374044683815" TEXT="YES">
<node CREATED="1374044684130" ID="ID_1559377834" MODIFIED="1374044701632" TEXT="paragraph summary after a major section"/>
</node>
</node>
<node CREATED="1374044407816" ID="ID_1290095724" MODIFIED="1374044407816" TEXT="Practice">
<node CREATED="1374281201682" ID="ID_851153493" MODIFIED="1374281202532" TEXT="huge for boosting your understanding"/>
<node CREATED="1374281203817" ID="ID_1381970386" MODIFIED="1374281205021" TEXT="traps">
<node CREATED="1374281214551" ID="ID_1796330632" MODIFIED="1374281215491" TEXT="#1 &#x2013; Not Getting Immediate Feedback">
<node CREATED="1374281247726" ID="ID_1021350904" MODIFIED="1374281258049" TEXT="practice problems immediately for feedback"/>
</node>
<node CREATED="1374281221365" ID="ID_375830130" MODIFIED="1374281222314" TEXT="#2 &#x2013; Grinding Problems">
<node CREATED="1374340142694" ID="ID_513303880" MODIFIED="1374340150409" TEXT="grinding through is also slow and inefficient."/>
<node CREATED="1374281295852" ID="ID_1561109085" MODIFIED="1374281305815" TEXT="highlight areas [for which] you need to develop a better intuition">
<node CREATED="1374340198275" ID="ID_1173173565" MODIFIED="1374340198800" TEXT="me">
<node CREATED="1374340168664" ID="ID_1039764646" MODIFIED="1374340209189" TEXT="use problems (mostly) for finding out &#xa;where you need to improve your intuition"/>
</node>
</node>
<node CREATED="1374340230947" ID="ID_3083943" MODIFIED="1374340232928" TEXT="use">
<node CREATED="1374340233115" ID="ID_1928767446" MODIFIED="1374340242162" TEXT="Feynman technique">
<arrowlink DESTINATION="ID_812521786" ENDARROW="Default" ENDINCLINATION="486;0;" ID="Arrow_ID_1227007917" STARTARROW="None" STARTINCLINATION="486;0;"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1374281392750" ID="ID_1952491295" MODIFIED="1374361701042">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Use <i>Coverage</i>&#xa0;and <i>Practice</i>&#xa0;to learn&#xa0;<br/>what you don't understand
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1374044407816" ID="ID_1502507027" MODIFIED="1374044407816" TEXT="Insight">
<node CREATED="1374281357003" ID="ID_812521786" MODIFIED="1374340242162" TEXT="Feynman technique">
<linktarget COLOR="#b0b0b0" DESTINATION="ID_812521786" ENDARROW="Default" ENDINCLINATION="486;0;" ID="Arrow_ID_1227007917" SOURCE="ID_1928767446" STARTARROW="None" STARTINCLINATION="486;0;"/>
<node CREATED="1374281445812" ID="ID_36199380" MODIFIED="1374340250564" TEXT="narrow down the gaps of what &#xa;you don&apos;t understand even further" VSHIFT="50"/>
<node CREATED="1374281802086" ID="ID_890656281" MODIFIED="1374281885804">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      
    </p>
    <ol charset="utf-8">
      <p style="font-weight: normal; white-space: normal; text-transform: none; text-align: start; margin-bottom: 15px; text-indent: 0px; font-style: normal; background-color: rgb(255, 255, 254); margin-right: 0px; word-spacing: 0px; margin-left: 0px; font-family: Trebuchet MS, Tahoma, Arial, Verdana; color: rgb(51, 51, 51); font-variant: normal; letter-spacing: normal; font-size: 12px; margin-top: 0px; line-height: 17.600000381469727px">
        The technique is simple:
      </p>
      <li>
        Get a piece of paper
      </li>
      <li>
        Write at the top the idea or process you want to understand
      </li>
      <li>
        Explain the idea, as if you were teaching it to someone else
      </li>
    </ol>
  </body>
</html></richcontent>
<node CREATED="1374281912438" ID="ID_400868222" MODIFIED="1374281917287" TEXT="What&#x2019;s crucial is that the third step will likely repeat some areas of the idea you already understand. However, eventually you&#x2019;ll reach a stopping point where you can&#x2019;t explain. That&#x2019;s the precise gap in your understanding that you need to fill." VSHIFT="55"/>
<node CREATED="1374340339157" ID="ID_606806787" MODIFIED="1374340341282" TEXT="my version">
<node CREATED="1374340341508" ID="ID_1403370773" MODIFIED="1374340348201" TEXT="use a mindmap instead of a piece of paper"/>
</node>
</node>
</node>
<node CREATED="1374281961549" ID="ID_1721559849" MODIFIED="1374281962034" TEXT="For Ideas You Don&#x2019;t Get At All">
<node CREATED="1374281964429" ID="ID_1717879453" MODIFIED="1374340291286" TEXT="use Feynman technique, but">
<node CREATED="1374281969307" ID="ID_1701331276" MODIFIED="1374340439486" TEXT="with textbook open"/>
<node CREATED="1374340441455" ID="ID_930210871" MODIFIED="1374340447963" TEXT="copy the authors description">
<node CREATED="1374340448213" ID="ID_1216947699" MODIFIED="1374340454851" TEXT="WITH own clarifying ideas"/>
</node>
</node>
</node>
<node CREATED="1374340487646" ID="ID_1836208224" MODIFIED="1374340490562" TEXT="Procedures">
<node CREATED="1374340490860" ID="ID_331494952" MODIFIED="1374340502174" TEXT="explain for each step">
<node CREATED="1374340493044" ID="ID_1793693779" MODIFIED="1374340495089" TEXT="how"/>
<node CREATED="1374340495299" ID="ID_479541935" MODIFIED="1374340497465" TEXT="why"/>
</node>
</node>
<node CREATED="1374340662713" ID="ID_1157850240" MODIFIED="1374340663494" TEXT="Formulas">
<node CREATED="1374340665368" ID="ID_1989563863" MODIFIED="1374340671900" TEXT="understand, don&apos;t just memorize">
<node CREATED="1374340681292" ID="ID_571389666" MODIFIED="1374340693767" TEXT="if necessary, walk through each part with a Feynman"/>
</node>
</node>
<node CREATED="1374354263722" ID="ID_433428918" MODIFIED="1374354268926" TEXT="For Checking Your Memory">
<node CREATED="1374354285772" ID="ID_992717402" MODIFIED="1374354289194" TEXT="test your memory">
<node CREATED="1374354289419" ID="ID_1813058551" MODIFIED="1374354302425" TEXT="do a feynman w/o checking src material"/>
</node>
</node>
</node>
</node>
<node CREATED="1374361095885" FOLDED="true" ID="ID_5191422" MODIFIED="1513980452200" TEXT="Developing a Deeper Intuition">
<node CREATED="1374361123397" ID="ID_732403259" MODIFIED="1374361123908" TEXT="Understanding an idea intuitively">
<node CREATED="1374361141587" ID="ID_665838257" MODIFIED="1374361193377" TEXT="Analogies">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1374361171874" ID="ID_1361974737" MODIFIED="1374361171875" TEXT="You understand an idea by correctly recognizing an important similarity between it and an easier-to-understand idea."/>
</node>
<node CREATED="1374361149482" ID="ID_1979971022" MODIFIED="1374361194039" TEXT="Visualizations">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1374361178703" ID="ID_491192958" MODIFIED="1374361178704" TEXT="Abstract ideas often become useful intuitions when we can form a mental picture of them. Even if the picture is just an incomplete representation of a larger, and more varied, idea."/>
</node>
<node CREATED="1374361161408" ID="ID_978397452" MODIFIED="1374361194727" TEXT="Simplifications">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1374361185863" ID="ID_538885539" MODIFIED="1374361185864" TEXT="A famous scientist once said that if you couldn&#x2019;t explain something to your grandmother, you don&#x2019;t fully understand it. Simplification is the art of strengthening those connections between basic components and complex ideas.">
<node CREATED="1374361241913" ID="ID_1878442203" MODIFIED="1374361245686" TEXT="Albert Einstein"/>
</node>
</node>
</node>
<node CREATED="1374361314416" ID="ID_570730097" MODIFIED="1374361344417" TEXT="After Feynman, describe the idea using the above anlogies, visualizations and simplifications."/>
<node CREATED="1374361358401" ID="ID_295519668" MODIFIED="1374361358816" TEXT="The Strategy to Learn Faster">
<node CREATED="1374364252354" ID="ID_1664973567" MODIFIED="1374364256265" TEXT="overlap and repeat">
<node CREATED="1374364256441" ID="ID_396805530" MODIFIED="1374364261206" TEXT="coverage"/>
<node CREATED="1374364261456" ID="ID_1881642098" MODIFIED="1374364264096" TEXT="practice"/>
<node CREATED="1374364264287" ID="ID_1584797124" MODIFIED="1374364265839" TEXT="insight"/>
</node>
<node CREATED="1374364278804" ID="ID_1595350199" MODIFIED="1374364282731" TEXT="example loop">
<node CREATED="1374364282915" ID="ID_203166940" MODIFIED="1374364283947" TEXT="read"/>
<node CREATED="1374364284147" ID="ID_307131095" MODIFIED="1374364285921" TEXT="practice"/>
<node CREATED="1374364286123" ID="ID_1716642532" MODIFIED="1374364292354" TEXT="re-read to fill in missing gaps"/>
</node>
</node>
</node>
</node>
</node>
</map>
