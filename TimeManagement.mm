<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1293178633920" ID="Freemind_Link_796344288" LINK="../../Documents/mindmaps/MetaMap.mm" MODIFIED="1293178653351" TEXT="TimeManagement.mm">
<node CREATED="1293178673305" ID="Freemind_Link_1197013569" MODIFIED="1293178675389" POSITION="right" TEXT="Time Boxing">
<node CREATED="1293178748320" ID="Freemind_Link_104711931" LINK="http://litemind.com/time-boxing/" MODIFIED="1293178748320" TEXT="litemind.com &gt; Time-boxing"/>
<node CREATED="1293178694466" ID="Freemind_Link_1285474450" MODIFIED="1293178701685" TEXT="refers to &quot;littles&quot; as mosquito tasks"/>
<node CREATED="1293179157584" ID="Freemind_Link_1464355055" MODIFIED="1293179157584" TEXT="15 specific ways that it can help you too be more productive">
<node CREATED="1293178760623" FOLDED="true" ID="Freemind_Link_1362680704" MODIFIED="1293178760623" TEXT="1. Make a Dent in Big Tasks">
<node CREATED="1293178769886" MODIFIED="1293178769886" TEXT="The most obvious use of a time box is to make progress on big tasks. On the one hand, it enables you to continuously make progress on these intimidating tasks. On the other hand, it makes sure working on these tasks won&#x2019;t overrun the rest of your day."/>
</node>
<node CREATED="1293178804019" FOLDED="true" ID="Freemind_Link_1421112642" MODIFIED="1293178804019" TEXT="2. Get Rid of &#x2018;Mosquito Tasks&#x2019;">
<node CREATED="1293178976862" MODIFIED="1293178976862" TEXT="Time boxes are a great way to tackle those annoying, tiny tasks that keep bugging you (pun intended). The problem with these pesky little tasks is that each of them, alone, may be regarded as insignificant enough to be postponed. After a while, however, there are enough of them to drain a significant amount of your mental energy. A good strategy to claim back that energy is to create a time box and tackle all of them at one sitting."/>
</node>
<node CREATED="1293178812035" FOLDED="true" ID="Freemind_Link_208891672" MODIFIED="1293178812035" TEXT="3. Overcome Procrastination">
<node CREATED="1293179016755" MODIFIED="1293179016755" TEXT="If you&#x2019;re procrastinating on a task, forget about completing it: just put it in a time box instead. You overcome your resistance towards the task and chances are that when the time is up you&#x2019;ll have built enough momentum to continue working on it much longer."/>
<node CREATED="1293179016755" MODIFIED="1293179016755" TEXT="That&#x2019;s right, if procrastination is your problem, feel free to ignore the timer when it buzzes. That&#x2019;s what I call an &#x2018;open time box&#x2019;: you set a minimum period of work, which you may extend as you like. For such type of time boxes, I like to configure my timer with a round of applause sound as a little incentive to keep me going."/>
<node CREATED="1293179016761" MODIFIED="1293179016761" TEXT="Links">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1293179016762" LINK="http://freesound.iua.upf.edu/samplesViewSingle.php?id=44312" MODIFIED="1293179016762" TEXT="round of applause sound"/>
</node>
</node>
<node CREATED="1293178817771" FOLDED="true" ID="Freemind_Link_1059812442" MODIFIED="1293178821878" TEXT="4. Conquer Perfectionism">
<icon BUILTIN="bookmark"/>
<node CREATED="1293179031098" MODIFIED="1293179031098" TEXT="Perfectionism is the flip side of procrastination. Instead of avoiding a task, you dwell on it for so long that when you notice, all your time is gone. To avoid perfectionism and the effects of diminishing returns, having a definite cut-off time for a task is one of the best strategies you can use."/>
<node CREATED="1293179031099" MODIFIED="1293179031099" TEXT="Dealing with perfectionism demands what I call a &#x2018;closed time box&#x2019;: setting a maximum period of work. When dealing with these time boxes, I like to configure my timer with a disruptive, annoying buzz sound to remind me drop the task immediately."/>
<node CREATED="1293179031104" MODIFIED="1293179031104" TEXT="Links">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1293179031105" LINK="http://freesound.iua.upf.edu/samplesViewSingle.php?id=1523" MODIFIED="1293179031105" TEXT="disruptive, annoying buzz sound"/>
</node>
</node>
<node CREATED="1293178830971" FOLDED="true" ID="Freemind_Link_1447620808" MODIFIED="1293178830971" TEXT="5. Sharpen Your Focus">
<node CREATED="1293179041126" MODIFIED="1293179041126" TEXT="Time boxing a particular task helps excluding other tasks and unrelated thoughts from your radar during that particular time window. Reducing mental clutter is essential if you want to be fully productive."/>
<node CREATED="1293179041126" MODIFIED="1293179041126" TEXT="Also, by organizing your work in time boxes you have the structure you need to properly prepare for your tasks. By taking care of potential distractions beforehand you maximize your chances of getting fully in flow."/>
</node>
<node CREATED="1293178839031" FOLDED="true" ID="Freemind_Link_681280211" MODIFIED="1293178839031" TEXT="6. Increase Efficiency">
<node CREATED="1293179049683" ID="Freemind_Link_1998838813" MODIFIED="1293179049683" TEXT="Isn&#x2019;t it true that you get much more accomplished in one of those pre-vacation Fridays than on any other normal workday? For some reason, it seems that our most efficient work is usually done at the end of a time period when there&#x2019;s a very well-defined cut-off point."/>
<node CREATED="1293179049683" MODIFIED="1293179049683" TEXT="Time boxes give you just enough of this healthy time pressure, enabling you to take full advantage of this &#x2018;end effect&#x2019;, so make sure that timer is visible and you can see the time going by as you work on your task."/>
</node>
<node CREATED="1293178846009" FOLDED="true" ID="Freemind_Link_1871754647" MODIFIED="1293178846009" TEXT="7. Boost Motivation">
<node CREATED="1293179060163" MODIFIED="1293179060163" TEXT="Big tasks, no matter how important, can be demotivating: you simply need to work for too long to see their outcomes. We may prefer deferring important tasks so we plow through many quick and easy tasks, just for the sake of the false perception of accomplishment."/>
<node CREATED="1293179060163" MODIFIED="1293179060163" TEXT="But just like the simple act of crossing off items from your to-do list can be motivating, so is successfully completing a time box. Completing a time box works as a visible sign of progress."/>
<node CREATED="1293179060163" MODIFIED="1293179060163" TEXT="Another idea on how to use time boxes to boost motivation is to make a game or challenge out of them: How many prospects can you call during one hour? Why not trying to beat your own record?"/>
</node>
<node CREATED="1293178853058" FOLDED="true" ID="Freemind_Link_533695820" MODIFIED="1293178853058" TEXT="8. Work on &#x2018;Fuzzy&#x2019; Goals">
<node CREATED="1293179070440" MODIFIED="1293179070440" TEXT="Although some people may want to stone me for this, I don&#x2019;t agree that we should have SMART goals for everything (SMART meaning &#x2018;specific, measurable, actionable, realistic and timely&#x2019;)."/>
<node CREATED="1293179070440" MODIFIED="1293179070440" TEXT="Sometimes I enjoy the freedom of an open outcome. Sometimes I just want to improve something &#x2014; no specific, precise goal in mind. This has been especially the case since I started experimenting with smaller, gradual goals). I think it&#x2019;s healthy not being 100% objective all the time."/>
<node CREATED="1293179070440" MODIFIED="1293179070440" TEXT="That said, you can&#x2019;t afford these &#x201c;relaxed goals&#x201d; to overrun your daily plan &#x2014; you know, when you actually need to be fully focused on things such as deadlines and SMART goals. Corner your fuzzy goals with a time box and get the best of both worlds."/>
</node>
<node CREATED="1293178859466" FOLDED="true" ID="Freemind_Link_980500545" MODIFIED="1293178859466" TEXT="9. Kick-Off Creative Exploration">
<node CREATED="1293179081349" MODIFIED="1293179081349" TEXT="Projects that require a high amount of creativity are not best tackled start-to-finish. The most effective way to deal with a project like this is to have an initial phase of immersion &#x2014; a period when you generate a burst of ideas &#x2014; and then forget about them for a while. By letting it go, you give time for the subconscious mind to work on the problem."/>
<node CREATED="1293179081349" MODIFIED="1293179081349" TEXT="This initial brainstorming phase is a perfect candidate for time boxing since there&#x2019;s usually no precise outcome for it. (Unless you define a precise outcome for the creative process, such as in a List of 100 or Idea Quota)."/>
<node CREATED="1293179081355" MODIFIED="1293179081355" TEXT="Links">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1293179081356" LINK="http://litemind.com/tackle-any-issue-with-a-list-of-100/" MODIFIED="1293179081356" TEXT="List of 100"/>
<node CREATED="1293179081356" LINK="http://litemind.com/get-mentally-fit-with-an-idea-quota/" MODIFIED="1293179081356" TEXT="Idea Quota"/>
</node>
</node>
<node CREATED="1293178866074" FOLDED="true" ID="Freemind_Link_1553083262" MODIFIED="1293178866074" TEXT="10. Raise Time Awareness">
<node CREATED="1293179122139" MODIFIED="1293179122139" TEXT="How many times have you wondered at the end of the day where did all your time go? As reader Iain Hamp suggested in a comment, performing time audits is an extremely valuable activity to diagnose your time, as well as aligning your time and values. Structuring your day in time boxes makes these audits super-easy to do."/>
<node CREATED="1293179122139" MODIFIED="1293179122139" TEXT="Also, being more aware of how much you can really fit in your time is liberating, as it helps you saying &#x2018;no&#x2019; to unimportant things more often."/>
<node CREATED="1293179122145" MODIFIED="1293179122145" TEXT="Links">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1293179122146" LINK="http://100lists.blogspot.com/" MODIFIED="1293179122146" TEXT="Iain Hamp"/>
<node CREATED="1293179122147" LINK="http://litemind.com/boost-brainstorm-effectiveness-why-habit/#comment-1548" MODIFIED="1293179122147" TEXT="comment"/>
</node>
</node>
<node CREATED="1293178873340" FOLDED="true" ID="Freemind_Link_1044967826" MODIFIED="1293178873340" TEXT="11. Create a Work Rhythm">
<node CREATED="1293179135721" MODIFIED="1293179135721" TEXT="You only get maximum effectiveness if you properly balance periods of work and rest. Time boxes provide a great framework to allow this balance to happen. The key is to find your own rhythm."/>
<node CREATED="1293179135721" MODIFIED="1293179135721" TEXT="Alternating between different types of time boxes (such as work/rest, or hard/easy tasks) maximizes your use of energy and enables you to accomplish much more."/>
<node CREATED="1293179135721" MODIFIED="1293179135721" TEXT="My favorite work rhythm is alternating between blocks of 50 minutes of work and 10 minutes of rest. For quick sprints, Merlin Mann&#x2019;s remarkably effective (10+2)*5 hack is also a great option."/>
<node CREATED="1293179135727" MODIFIED="1293179135727" TEXT="Links">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1293179135728" LINK="http://successbeginstoday.org/wordpress/2006/09/the-focused-50/" MODIFIED="1293179135728" TEXT="50 minutes of work and 10 minutes of rest"/>
<node CREATED="1293179135729" LINK="http://www.43folders.com/2005/10/11/procrastination-hack-1025" MODIFIED="1293179135729" TEXT="(10+2)*5 hack"/>
</node>
</node>
<node CREATED="1293178885819" FOLDED="true" ID="Freemind_Link_374504127" MODIFIED="1293178885819" TEXT="12. Get Meaningful Work Done First">
<node CREATED="1293178892193" MODIFIED="1293178892193" TEXT="Working on your most important projects first thing in the morning is a classic tip to guarantee that you do meaningful work in your day. Create a time box to work on your dreams every day &#x2014; before the world out there has a chance to disrupt your plans."/>
<node CREATED="1293178892193" MODIFIED="1293178892193" TEXT="If you don&#x2019;t plan to use time boxes for anything else, please consider applying just this one tip. This is perhaps the single, most effective thing you can do in pursuit of your dreams."/>
</node>
<node CREATED="1293178901881" FOLDED="true" ID="Freemind_Link_322654796" MODIFIED="1293178911465" TEXT="13. Balance Your Life">
<node CREATED="1293178901881" ID="Freemind_Link_1960314867" MODIFIED="1293178911460" TEXT="It&#x2019;s common to become too focused on a specific area of our lives at the expense of others."/>
<node CREATED="1293178901881" ID="Freemind_Link_401630779" MODIFIED="1293178911460" TEXT="Remember, you don&#x2019;t need to use time boxing only for work-related tasks: you can block time for anything that matters to you: leisure, family, hobbies &#x2014; anything."/>
<node CREATED="1293178901881" ID="Freemind_Link_1733687153" MODIFIED="1293178911463" TEXT="Pre-allocating time boxes for the things that matter most is an excellent strategy to help you live a balanced life. In fact, planned time boxes are at the heart of the prioritization system I use. I plan to explore it more deeply in a new article."/>
</node>
<node CREATED="1293178926136" FOLDED="true" ID="Freemind_Link_1543877418" MODIFIED="1293178926136" TEXT="14. Plug Time Sinks">
<node CREATED="1293178926137" MODIFIED="1293178926137" TEXT="Stop kicking yourself; all you need to do is to put a time box around them to reclaim the time back."/>
<node CREATED="1293178926137" MODIFIED="1293178926137" TEXT="You know what I am talking about: channel-surfing, web-surfing, games, feeds, e-mail &#x2014; everybody seems to have a time drain in their lives. StumbleUpon, anyone?"/>
<node CREATED="1293178926149" ID="Freemind_Link_1699663095" MODIFIED="1293178926149" TEXT="Links">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1293178926150" LINK="http://www.stumbleupon.com/" MODIFIED="1293178926150" TEXT="StumbleUpon"/>
</node>
</node>
<node CREATED="1293178953244" FOLDED="true" ID="Freemind_Link_15838591" MODIFIED="1293178953244" TEXT="15. Reward Yourself">
<node CREATED="1293178953244" MODIFIED="1293178953244" TEXT="If you tie your rewards to the completion of tasks, you may find yourself doing only quick and easy tasks, and avoiding the important ones. Why not get yourself a little reward after you complete a time box instead?"/>
<node CREATED="1293178953244" MODIFIED="1293178953244" TEXT="A personal example: Checking e-mail multiple times a day is an old addiction I haven&#x2019;t yet managed to cure. While the ideal solution would be to the task of checking for e-mail in a time box, I now use it as a reward for completing time boxes &#x2014; an extra incentive that works wonders!"/>
<node CREATED="1293178953244" MODIFIED="1293178953244" TEXT="Rewards can be as tiny as a glass of water or a deep breath at the window (for more about tiny rewards and their powerful effect, I recommend the chapter &#x2018;Bestow Small Rewards&#x2019; in One Small Step Can Change Your Life &#x2014; check the full book summary)."/>
<node CREATED="1293178953255" MODIFIED="1293178953255" TEXT="Links">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1293178953255" LINK="http://www.amazon.com/exec/obidos/ASIN/0761129235/phaedrus0b" MODIFIED="1293178953255" TEXT="One Small Step Can Change Your Life"/>
<node CREATED="1293178953256" LINK="http://litemind.com/one-small-step-can-change-your-life/" MODIFIED="1293178953256" TEXT="check the full book summary"/>
</node>
</node>
</node>
</node>
<node CREATED="1293178678457" ID="Freemind_Link_899664055" MODIFIED="1293178679829" POSITION="left" TEXT="with Vio">
<node CREATED="1293178666562" ID="_" MODIFIED="1293178672661" TEXT="Bigs vs. Littles">
<node CREATED="1293178684921" ID="Freemind_Link_1552401761" MODIFIED="1293178693021" TEXT="separate tasks into bigs vs. littles"/>
<node CREATED="1293178706370" ID="Freemind_Link_1099107564" MODIFIED="1293178717757" TEXT="work on bigs first, for 2 to 3 hours"/>
<node CREATED="1293178718202" ID="Freemind_Link_999593560" MODIFIED="1293178725542" TEXT="littles are after (like afternoon)"/>
</node>
</node>
</node>
</map>
