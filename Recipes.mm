<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1273420864097" ID="Freemind_Link_754712875" LINK="../../Documents/mindmaps/MetaMap.mm" MODIFIED="1273420881288" TEXT="Recipes.mm">
<node CREATED="1273420931725" ID="Freemind_Link_1108155497" MODIFIED="1337033738906" POSITION="right" TEXT="Beverages">
<node CREATED="1273420903915" ID="Freemind_Link_1978013401" MODIFIED="1455677707659" TEXT="">
<node CREATED="1273420887091" ID="_" MODIFIED="1273420903454" TEXT="stevia">
<node CREATED="1273420922438" ID="Freemind_Link_669907500" MODIFIED="1273420927339" TEXT="plant based sweetener"/>
</node>
<node CREATED="1273420908923" ID="Freemind_Link_1022244851" MODIFIED="1273420913082" TEXT="lemon juice"/>
<node CREATED="1273420913522" ID="Freemind_Link_1365325360" MODIFIED="1273420917409" TEXT="tangerine juice"/>
<node CREATED="1273420917858" ID="Freemind_Link_715789620" MODIFIED="1273420920440" TEXT="sparkling water"/>
</node>
</node>
<node CREATED="1337033689750" ID="ID_83413811" MODIFIED="1337033693804" POSITION="right" TEXT="Vegetarian">
<node CREATED="1273817069888" FOLDED="true" ID="Freemind_Link_1268319769" MODIFIED="1455677702991" TEXT="roasted cauliflower">
<node CREATED="1294708544695" ID="Freemind_Link_487714222" MODIFIED="1294708548564" TEXT="1) slice as thin as possible (just start cutting the whole thing like a loaf of bread)&#xa;2) separate onto two cookie sheets (metal not pyrex): crumbs on one and bigger flower slices on the other&#xa;3) mix in oil (or bacon grease) with fingers and sprinkle with salt&#xa;4) cook in over (350? 400?) until some start to blacken (ie better dark than light) and the rest is brown&#xa;&#x9;timing is important&#xa;&#x9;set 20 minutes and check&#xa;&#x9;then 10&#xa;&#x9;then every 5&#xa;&#x9;don&apos;t forget them at the end and let them burn!&#xa;5) crumbs will be done first (that&apos;s the point) Yummy / Crispy!"/>
</node>
<node CREATED="1455408280358" ID="ID_1981741692" MODIFIED="1455408284742" TEXT="Stuffed zucchini recipe">
<node CREATED="1455408295598" ID="ID_1386003750" MODIFIED="1455408297625" TEXT="ingredients">
<node CREATED="1455408298560" ID="ID_1503123107" MODIFIED="1455408316098" TEXT="about 6 zucchini (more or less according to size. 6 -8 are what I use.)"/>
<node CREATED="1455408298561" MODIFIED="1455408298561" TEXT="1 cup grated jack cheese"/>
<node CREATED="1455408298562" MODIFIED="1455408298562" TEXT="parmesan cheese"/>
<node CREATED="1455408298562" MODIFIED="1455408298562" TEXT="4 - 6 eggs"/>
<node CREATED="1455408298563" MODIFIED="1455408298563" TEXT="1 pkg thawed frozen chopped spinach or one bag raw spinach wilted in microwave and chopped."/>
<node CREATED="1455408298563" MODIFIED="1455408298563" TEXT="salt, black pepper, nutmeg"/>
</node>
<node CREATED="1455408299469" ID="ID_561252975" MODIFIED="1455408301184" TEXT="instructions">
<node CREATED="1455408307589" ID="ID_1232442659" MODIFIED="1455408308311" TEXT="I par-boil the zucchini, slice in half, and scoop out insides with a spoon. You can fry the insides in olive oil to make it extra rich. I just chop up the insides with the spinach, add cheese, add eggs one at a time. stir. add salt, black pepper, and fresh ground nutmeg. olive oil the bottom of a glass cake pan, arrange the shells, fill them and sprinkle the parmesan on top Bake at 350 about half an hour (until brown on top (I go by when it smells done, but brown on top works just as well :-)  --  Andrea Leonardi "/>
</node>
</node>
<node CREATED="1327014459407" ID="Freemind_Link_468791131" MODIFIED="1365615454893" TEXT="vegan">
<node CREATED="1382042585349" FOLDED="true" ID="ID_1849287975" MODIFIED="1493770387118" TEXT="crunchy salad : mom&apos;s asian dressing">
<node CREATED="1382042943660" ID="ID_43203888" MODIFIED="1382042945093" TEXT="tools">
<node CREATED="1382042945692" ID="ID_366950446" MODIFIED="1382042951275" TEXT="salad shooter pro"/>
<node CREATED="1382042951500" ID="ID_567957861" MODIFIED="1382042953331" TEXT="slap chop"/>
</node>
<node CREATED="1382042741670" ID="ID_1845262019" MODIFIED="1382042743407" TEXT="shake">
<node CREATED="1382042596419" ID="ID_1888300508" MODIFIED="1382042605977" TEXT="1-2 slices ginger"/>
<node CREATED="1382042606203" ID="ID_866595930" MODIFIED="1382042639582" TEXT="1/3 - 1/2 cup seasoned rice wine vinegar"/>
<node CREATED="1382042642476" ID="ID_1026534276" MODIFIED="1384973524981" TEXT="1/4 tsp soy sauce"/>
<node CREATED="1382042650116" ID="ID_1113633572" MODIFIED="1384973530037" TEXT="2 drops worcestershire sauce"/>
<node CREATED="1382042717110" ID="ID_438720706" MODIFIED="1382042728029" TEXT="1/2 - 1 tsp 5 spice">
<node CREATED="1493767002803" FOLDED="true" ID="ID_549728750" MODIFIED="1493770384821" TEXT="wing it">
<node CREATED="1493766867379" ID="ID_1333516629" MODIFIED="1493766876607" TEXT="cinnamon"/>
<node CREATED="1493766877131" ID="ID_745268954" MODIFIED="1493766879010" TEXT="cardamom"/>
<node CREATED="1493766879227" ID="ID_1689685672" MODIFIED="1493766880873" TEXT="anise"/>
<node CREATED="1493766923292" ID="ID_112018331" MODIFIED="1493766926105" TEXT="fennel seeds"/>
<node CREATED="1493766952681" ID="ID_1495811939" MODIFIED="1493766956516" TEXT="cloves"/>
<node CREATED="1493766969771" ID="ID_1162834256" MODIFIED="1493766971883" TEXT="ginger"/>
<node CREATED="1493766932037" ID="ID_74282199" MODIFIED="1493766934731" TEXT="pepper corns">
<node CREATED="1493766935973" ID="ID_1875504022" MODIFIED="1493766944727" TEXT="szechuan peppercorns"/>
<node CREATED="1493766950386" ID="ID_1161177353" MODIFIED="1493766951043" TEXT="whole black peppercorns"/>
</node>
</node>
</node>
<node CREATED="1382042728678" ID="ID_1547697095" MODIFIED="1382042740662" TEXT="1/2 - 1 tsp sesame oil"/>
</node>
<node CREATED="1382042753991" ID="ID_67123368" MODIFIED="1382042755735" TEXT="serve">
<node CREATED="1382042755943" ID="ID_88265664" MODIFIED="1382071132286" TEXT="over">
<node CREATED="1382042806464" ID="ID_1918834514" MODIFIED="1382042809817" TEXT="grated">
<node CREATED="1382042810064" ID="ID_1108018514" MODIFIED="1382042811927" TEXT="carrots"/>
<node CREATED="1382042812200" ID="ID_1859782282" MODIFIED="1382042815688" TEXT="red beets"/>
<node CREATED="1382042876650" ID="ID_1140747198" MODIFIED="1382042883626" TEXT="root vegetables"/>
<node CREATED="1382042820912" ID="ID_554030451" MODIFIED="1382042822176" TEXT="radish"/>
<node CREATED="1382042855433" ID="ID_1582770079" MODIFIED="1382042858215" TEXT="broccoli stems"/>
</node>
<node CREATED="1382042905691" ID="ID_737370282" MODIFIED="1382042908514" TEXT="sliced">
<node CREATED="1382042816176" ID="ID_676638633" MODIFIED="1382042820687" TEXT="daikon"/>
<node CREATED="1382042912531" ID="ID_1977476080" MODIFIED="1382042920523" TEXT="Jicama?"/>
</node>
<node CREATED="1382042842225" ID="ID_856551629" MODIFIED="1382042849129" TEXT="sprouts (sprouted beans)"/>
<node CREATED="1382042851065" ID="ID_1515936523" MODIFIED="1382042854642" TEXT="edamame"/>
</node>
<node CREATED="1382042757399" ID="ID_334699" MODIFIED="1382042758087" TEXT="with">
<node CREATED="1382042758335" ID="ID_1898807777" MODIFIED="1382042786944" TEXT="fresh cut (scissors) ">
<node CREATED="1382042786945" ID="ID_1068562641" MODIFIED="1382042786945" TEXT="cilantro"/>
<node CREATED="1382042789552" ID="ID_1918218659" MODIFIED="1382042801562" TEXT="green onion (white &amp; green parts)"/>
</node>
<node CREATED="1382042768143" ID="ID_679830518" MODIFIED="1382042773918" TEXT="chopped peanuts">
<node CREATED="1382042774111" ID="ID_243968815" MODIFIED="1382042775910" TEXT="slap chop"/>
</node>
<node CREATED="1382049711344" ID="ID_1409629020" MODIFIED="1382049717936" TEXT="fresh lime juice"/>
</node>
</node>
</node>
<node CREATED="1327012069328" FOLDED="true" ID="Freemind_Link_1385100269" MODIFIED="1381971078468" TEXT="vegan sauces">
<node CREATED="1327012073928" ID="Freemind_Link_1789930309" MODIFIED="1327012075092" TEXT="untried">
<node CREATED="1327012081575" LINK="http://webcache.googleusercontent.com/search?q=cache:http://vegetarian.about.com/od/saucesdipsspreads/Sauces_Dips_Spreads.htm" MODIFIED="1327012081575" TEXT="webcache.googleusercontent.com &gt; Search?q=cache:http: &gt;  &gt; Vegetarian.about.com &gt; Od &gt; Saucesdipsspreads &gt; Sauces Dips Spreads"/>
<node CREATED="1327012090083" FOLDED="true" ID="Freemind_Link_1876938577" MODIFIED="1327012090083" TEXT="Vegan buerre blanc sauce with blood oranges">
<node CREATED="1327012096179" LINK="http://vegetarian.about.com/od/saucesdipsspreads/r/veganbuerreblanc.htm" MODIFIED="1327012096179" TEXT="vegetarian.about.com &gt; Od &gt; Saucesdipsspreads &gt; R &gt; Veganbuerreblanc"/>
<node CREATED="1327012129140" MODIFIED="1327012129140" TEXT="A gourmet French buerre blanc sauce with blood oranges and completely vegan, dairy-free and cholesterol-free. Buerre blanc is a gourmet white French sauce made from white wine. Recipe by Chef Scot J. Jones."/>
<node CREATED="1327012129140" MODIFIED="1327012129140" TEXT="Ingredients:">
<node CREATED="1327012129140" MODIFIED="1327012129140" TEXT="&#x2022;        Sea salt"/>
<node CREATED="1327012129140" MODIFIED="1327012129140" TEXT="&#x2022;        1 teaspoon extra-virgin olive oil"/>
<node CREATED="1327012129140" MODIFIED="1327012129140" TEXT="&#x2022;        2 shallots, minced"/>
<node CREATED="1327012129140" MODIFIED="1327012129140" TEXT="&#x2022;        3 whole blood oranges, segmented (save the juice)"/>
<node CREATED="1327012129140" MODIFIED="1327012129140" TEXT="&#x2022;        1/2 cup white wine"/>
<node CREATED="1327012129140" MODIFIED="1327012129140" TEXT="&#x2022;        1 teaspoon sugar"/>
<node CREATED="1327012129140" ID="Freemind_Link_1589295589" MODIFIED="1327012129140" TEXT="&#x2022;        8 tablespoons Earth Balance, cut into tablespoon-sized pieces">
<node CREATED="1327012197262" ID="Freemind_Link_904575261" MODIFIED="1327012200738" TEXT="vegan margarine">
<node CREATED="1327012226241" LINK="http://vegetarian.about.com/od/ingredientsandadditives/qt/transfatdetail.htm" MODIFIED="1327012226241" TEXT="As an added bonus, it is also gluten-free, non-GMO, and, unlike most margarines, it has no hydrogenated oils."/>
</node>
</node>
<node CREATED="1327012129140" MODIFIED="1327012129140" TEXT="&#x2022;        1 teaspoon chopped fresh thyme, finely chopped"/>
<node CREATED="1327012129140" MODIFIED="1327012129140" TEXT="&#x2022;        Freshly ground black pepper"/>
</node>
<node CREATED="1327012129141" ID="Freemind_Link_1586375564" MODIFIED="1327012129141" TEXT="Preparation:">
<node CREATED="1327012129141" MODIFIED="1327012129141" TEXT="Place a medium saut&#xe9; pan over medium heat. Sprinkle the bottom with a pinch of salt and heat for 1 minute. Add the oil and heat for 30 seconds, being careful not to let it smoke."/>
<node CREATED="1327012129141" MODIFIED="1327012129141" TEXT="Reduce the heat to low. Add the shallots and saut&#xe9; until translucent but not browned, 2 to 3 minutes. Add the blood orange segments, 1/2 of the orange juice and wine and cook until reduced in half."/>
<node CREATED="1327012129141" MODIFIED="1327012129141" TEXT="Remove from the heat. Whisk in the Earth Balance 1 tablespoon at a time, then stir in the rest of the orange juice and the thyme. Finish with sugar, salt and pepper to taste."/>
<node CREATED="1327012129141" ID="Freemind_Link_938384388" MODIFIED="1327012129141" TEXT="Use this gourmet French buerre blanc to make Chef Scot&apos;s Pine nut crusted scallopini with braised escarole and buerre blanc">
<node CREATED="1327012322135" LINK="http://vegetarian.about.com/od/maindishentreerecipes/r/gardeinscalloppini.htm" MODIFIED="1327012322135" TEXT="vegetarian.about.com &gt; Od &gt; Maindishentreerecipes &gt; R &gt; Gardeinscalloppini"/>
</node>
</node>
</node>
<node CREATED="1327013374750" FOLDED="true" ID="Freemind_Link_1680872285" MODIFIED="1327013374750" TEXT="Vegan basil lime sauce">
<node CREATED="1327013379685" LINK="http://vegetarian.about.com/od/saucesdipsspreads/r/basillimesauce.htm" MODIFIED="1327013379685" TEXT="vegetarian.about.com &gt; Od &gt; Saucesdipsspreads &gt; R &gt; Basillimesauce"/>
<node CREATED="1327013395181" MODIFIED="1327013395181" TEXT="A simple yet elegant creamy dairy-free and vegan basil lime sauce simmered stovetop. Use as a gourmet topping for pasta, risottos or veggies."/>
<node CREATED="1327013395181" MODIFIED="1327013395181" TEXT="This gourmet vegetarian recipe is by Chef J. Scot Jones."/>
<node CREATED="1327013395181" MODIFIED="1327013395181" TEXT="Ingredients:">
<node CREATED="1327013395181" MODIFIED="1327013395181" TEXT="&#x2022;        2 cups dry white wine"/>
<node CREATED="1327013395181" MODIFIED="1327013395181" TEXT="&#x2022;        1 clove of garlic, minced"/>
<node CREATED="1327013395181" MODIFIED="1327013395181" TEXT="&#x2022;        1 tablespoon minced shallot"/>
<node CREATED="1327013395181" MODIFIED="1327013395181" TEXT="&#x2022;        2 teaspoons sugar"/>
<node CREATED="1327013395181" MODIFIED="1327013395181" TEXT="&#x2022;        2 tablespoons fresh lime juice"/>
<node CREATED="1327013395181" MODIFIED="1327013395181" TEXT="&#x2022;        1 cup fresh basil, chopped"/>
<node CREATED="1327013395181" MODIFIED="1327013395181" TEXT="&#x2022;        3 tablespoons chilled Earth Balance, cut into bits"/>
</node>
<node CREATED="1327013395181" ID="Freemind_Link_632005424" MODIFIED="1327013395181" TEXT="Preparation:">
<node CREATED="1327013395181" MODIFIED="1327013395181" TEXT="Place a skillet over medium-high heat and add the wine, garlic, shallot, sugar, and lime juice. Bring to a simmer and cook until the sauce is reduced by half, about 8 minutes."/>
<node CREATED="1327013395182" MODIFIED="1327013395182" TEXT="Remove from the heat and slowly stir in the Earth Balance, one piece at a time. Remove from the heat. When the sauce has cooled and thickened slightly, stir in the basil."/>
<node CREATED="1327013395182" ID="Freemind_Link_1350766143" MODIFIED="1327013395182" TEXT="Use your vegan basil lime sauce to make Chef Scot&apos;sHot Italian peppers stuffed with herbed risotto.">
<node CREATED="1327013412505" LINK="http://vegetarian.about.com/od/maindishentreerecipes/r/stuffedpeppers2.htm" MODIFIED="1327013412505" TEXT="vegetarian.about.com &gt; Od &gt; Maindishentreerecipes &gt; R &gt; Stuffedpeppers2"/>
</node>
</node>
</node>
<node CREATED="1327014295966" FOLDED="true" ID="Freemind_Link_619198375" MODIFIED="1327014295966" TEXT="Thai Peanut Sauce with Ginger">
<node CREATED="1327014314067" MODIFIED="1327014314067" TEXT="This is a mild Thai peanut sauce recipe, perfect for kids who love peanut butter. If you prefer a spicier version, you can add extra red pepper flakes or even fresh diced chilies. Serve your Thai peanut sauce over pasta or noodles, or use as a dip for veggies. You can also use Thai peanut sauce as a salad dressing if you add some extra liquid to thin it out a bit. This Thai peanut sauce with ginger recipe is both vegetarian and vegan."/>
<node CREATED="1327014314068" MODIFIED="1327014314068" TEXT="More vegetarian recipes"/>
<node CREATED="1327014314068" ID="Freemind_Link_763561875" MODIFIED="1327014314068" TEXT="Ingredients:">
<node CREATED="1327014314068" MODIFIED="1327014314068" TEXT="&#x2022;        1/3 cup peanut butter, softened"/>
<node CREATED="1327014314068" MODIFIED="1327014314068" TEXT="&#x2022;        3 tbsp water"/>
<node CREATED="1327014314068" MODIFIED="1327014314068" TEXT="&#x2022;        2 tbsp soy sauce"/>
<node CREATED="1327014314068" MODIFIED="1327014314068" TEXT="&#x2022;        juice from 2 limes"/>
<node CREATED="1327014314068" MODIFIED="1327014314068" TEXT="&#x2022;        1 tbsp fresh minced ginger"/>
<node CREATED="1327014314068" MODIFIED="1327014314068" TEXT="&#x2022;        1/2 tsp brown sugar"/>
<node CREATED="1327014314068" MODIFIED="1327014314068" TEXT="&#x2022;        1 clove garlic, minced"/>
<node CREATED="1327014314068" MODIFIED="1327014314068" TEXT="&#x2022;        1/4 tsp red pepper flakes, or to taste"/>
<node CREATED="1327014314068" MODIFIED="1327014314068" TEXT="&#x2022;        salt to taste"/>
<node CREATED="1327014350641" ID="Freemind_Link_811367855" MODIFIED="1327014351820" TEXT="durant">
<node CREATED="1327014352081" ID="Freemind_Link_1092114739" MODIFIED="1327014354580" TEXT="from other recipe">
<node CREATED="1327013686812" ID="Freemind_Link_796781708" MODIFIED="1327013693968" TEXT="&#x2022;        2 tbsp rice vinegar">
<icon BUILTIN="bookmark"/>
</node>
</node>
</node>
</node>
<node CREATED="1327014314068" ID="Freemind_Link_651049419" MODIFIED="1327014314068" TEXT="Preparation:">
<node CREATED="1327014314068" MODIFIED="1327014314068" TEXT="Whisk together all ingredients in a small bowl. If needed, you can microwave the peanut butter for a few seconds first to soften it up."/>
</node>
</node>
<node CREATED="1327013794303" FOLDED="true" ID="Freemind_Link_1485693907" MODIFIED="1327013868703" TEXT="Coconut and Tofu Curry Recipe&#xa;(requires blender / food processor)">
<node CREATED="1327013799311" LINK="http://vegetarian.about.com/od/tofurecipes/r/coconutcurry.htm" MODIFIED="1327013799311" TEXT="vegetarian.about.com &gt; Od &gt; Tofurecipes &gt; R &gt; Coconutcurry"/>
<node CREATED="1327013814799" MODIFIED="1327013814799" TEXT="Looking for easy tofu recipes? Here is a complete list of tofu recipes."/>
<node CREATED="1327013814800" MODIFIED="1327013814800" TEXT="Although this vegetarian and vegan curry recipe isn&apos;t authentic to any ethnic cuisine per se, I find that the mild flavor really gets absorbed into the tofu and blends well with the rich coconut flavor. When served over rice, tofu curry makes for a great entree. If you&apos;re bored with the usual vegan recipes, or have some tofu on hand you need to use up, this easy dish is just right. Related video: How to Cook with Tofu"/>
<node CREATED="1327013814800" MODIFIED="1327013814800" TEXT="Prep Time: 10&#xa0;minutes"/>
<node CREATED="1327013814800" MODIFIED="1327013814800" TEXT="Cook Time: 20&#xa0;minutes"/>
<node CREATED="1327013814800" MODIFIED="1327013814800" TEXT="Total Time: 30&#xa0;minutes"/>
<node CREATED="1327013814800" MODIFIED="1327013814800" TEXT="Ingredients:">
<node CREATED="1327013814800" MODIFIED="1327013814800" TEXT="&#x2022;        1/2 teaspoon fresh ginger (about 2 slices)"/>
<node CREATED="1327013814800" MODIFIED="1327013814800" TEXT="&#x2022;        5 cloves garlic"/>
<node CREATED="1327013814800" MODIFIED="1327013814800" TEXT="&#x2022;        1 onion"/>
<node CREATED="1327013814800" MODIFIED="1327013814800" TEXT="&#x2022;        2 stalks fresh lemongrass (optional)"/>
<node CREATED="1327013814800" MODIFIED="1327013814800" TEXT="&#x2022;        3 tbsp olive oil"/>
<node CREATED="1327013814800" MODIFIED="1327013814800" TEXT="&#x2022;        dash red pepper flakes"/>
<node CREATED="1327013814800" MODIFIED="1327013814800" TEXT="&#x2022;        1 block tofu, drained and sliced into approx 1 inch squares"/>
<node CREATED="1327013814800" MODIFIED="1327013814800" TEXT="&#x2022;        1 1/2 tbsp curry powder"/>
<node CREATED="1327013814800" MODIFIED="1327013814800" TEXT="&#x2022;        14 oz coconut milk"/>
<node CREATED="1327013814800" MODIFIED="1327013814800" TEXT="&#x2022;        1 cup water"/>
</node>
<node CREATED="1327013814800" ID="Freemind_Link_1812639150" MODIFIED="1327013814800" TEXT="Preparation:">
<node CREATED="1327013814800" MODIFIED="1327013814800" TEXT="In a blender or food processor, process the ginger, garlic, onion and lemongrass until smooth, adding a bit of olive oil if needed."/>
<node CREATED="1327013814800" MODIFIED="1327013814800" TEXT="Heat the olive oil in a large skillet over medium-high heat and add the blended garlic mixture. Sautee this mixture for 1-3 minutes, then add the tofu and red pepper flakes, stirring gently to mix the tofu with the garlic mixture for another 3-5 minutes, adding more olive oil if needed."/>
<node CREATED="1327013814800" MODIFIED="1327013814800" TEXT="Reduce heat to medium low and add coconut, water, and curry, stirring well to combine. Cover the skillet and allow to simmer for 20 minutes, stirring occasionally."/>
<node CREATED="1327013814801" MODIFIED="1327013814801" TEXT="Serve hot over rice and enjoy!"/>
</node>
</node>
</node>
</node>
<node CREATED="1327014054105" ID="Freemind_Link_438324286" MODIFIED="1327014057068" TEXT="tofu desserts">
<node CREATED="1327014075508" FOLDED="true" ID="Freemind_Link_1110107418" MODIFIED="1327014075508" TEXT="5 Ways to Substitute Tofu in Desserts">
<node CREATED="1327014063520" ID="Freemind_Link_1623385896" MODIFIED="1327014066052" TEXT="video"/>
<node CREATED="1327014096691" MODIFIED="1327014096691" TEXT="Transcript:5 Ways to Substitute Tofu in Desserts"/>
<node CREATED="1327014096691" MODIFIED="1327014096691" TEXT="Hi! My name is Rocky Hunter and I am at Local Sprouts Cooperative Cafe in Portland, Maine. Today I am going talk a little bit about substituting tofu in your desserts."/>
<node CREATED="1327014096691" ID="Freemind_Link_437712822" MODIFIED="1327014096691" TEXT="Silken Tofu Is a Good Way to Substitute Tofu in Desserts">
<node CREATED="1327014096691" MODIFIED="1327014096691" TEXT="When using tofu in your baked goods instead of eggs or dairy, typically you will be working with silken tofu. It&apos;s the softest of all the tofu varieties and really lends itself to being blended in things smoothed out. It works really well for a lot of baking projects and holds a lot of the same principles where normally eggs or dairy would be used."/>
</node>
<node CREATED="1327014096691" ID="Freemind_Link_1470227035" MODIFIED="1327014096691" TEXT="Substitute Silken Tofu in Chocolate Mousse">
<node CREATED="1327014096691" MODIFIED="1327014096691" TEXT="Today, I am going to be putting together a very basic chocolate mousse using tofu instead of dairy. As I said earlier, the tofu really lends itself to being blended. So what you will see is starting with chocolate, sweetener, adding just a little bit of vegetable oil and then blending the silken tofu right in. As you can see at the end of the process it&apos;s very smooth, very pourable, holds a lot of the same principles that a dairy chocolate mousse would have."/>
</node>
<node CREATED="1327014096691" ID="Freemind_Link_114207702" MODIFIED="1327014096691" TEXT="Substitute Tofu in Pie Fillings">
<node CREATED="1327014096691" MODIFIED="1327014096691" TEXT="Similar ideas can be applied to something to be used for pie filling, if we were to take that same kind of mixture where you are using chocolate, some kind of sweetener oil and silken tofu to kind of emulsify it all together may be throw in peanut butter and that&apos;s like a chocolate peanut butter pie right there. You could certainly add other elements like raspberry or fruit and that would be filling for a vegan pie crust."/>
</node>
<node CREATED="1327014096692" ID="Freemind_Link_764317510" MODIFIED="1327014096692" TEXT="Substitute Tofu in Frostings for Cake">
<node CREATED="1327014096692" MODIFIED="1327014096692" TEXT="The next time that you wanted to frost a vegan cake, tofu could actually be involved in the topping process -- adding the right amount of silken tofu and sweetener, with maybe a non-diary vegan butter product, you could emulate a lot of those same characteristics and flavors."/>
</node>
<node CREATED="1327014096692" ID="Freemind_Link_1611680496" MODIFIED="1327014096692" TEXT="Substitute Tofu for Whipped Cream">
<node CREATED="1327014096692" MODIFIED="1327014096692" TEXT="For the baking buffs out there that have the right home appliances, playing with the amount of silken tofu that you include in a recipe could yield things like a whipped cream topping, something that&apos;s really light and fluffy, again that would something with like a stand mixer and kind of playing with how much silken tofu that you are adding to your baking projects."/>
</node>
<node CREATED="1327014096692" ID="Freemind_Link_635412431" MODIFIED="1327014096692" TEXT="Substitute Tofu in Cheesecakes">
<node CREATED="1327014096692" MODIFIED="1327014096692" TEXT="And last but not least is my personal favorite, the tofu cheesecake. Obviously most cheesecakes are going to have dairy products in them, aren&apos;t very vegan friendly, but using silken tofu and possibly a vegan brand cream cheese, you could emulate a lot of those same flavors just by mixing it in your food processor and pouring it into a pie crust and letting it solidify overnight. And the next day when you open up the fridge is a tasty vegan treat."/>
</node>
<node CREATED="1327014096692" ID="Freemind_Link_1536077554" MODIFIED="1327014096692" TEXT="Those are some tips for substituting tofu in your desserts."/>
</node>
</node>
<node CREATED="1327014384776" ID="Freemind_Link_188711074" MODIFIED="1327014389891" TEXT="vegan pie crust">
<node CREATED="1327014404156" FOLDED="true" ID="Freemind_Link_892794065" MODIFIED="1327014404156" TEXT="Flaky Vegan Pie Crust Recipe">
<node CREATED="1327014397515" LINK="http://vegetarian.about.com/od/piecrustrecipes/r/flakypiecrust.htm" MODIFIED="1327014397515" TEXT="vegetarian.about.com &gt; Od &gt; Piecrustrecipes &gt; R &gt; Flakypiecrust"/>
<node CREATED="1327014413457" MODIFIED="1327014413457" TEXT="An easy vegan pie crust recipe made from margarine and oil, instead of Crisco or solid shortening."/>
<node CREATED="1327014413457" MODIFIED="1327014413457" TEXT="Ingredients:"/>
<node CREATED="1327014413457" MODIFIED="1327014413457" TEXT="2 cups flour"/>
<node CREATED="1327014413457" MODIFIED="1327014413457" TEXT="1 1/2 tbsp sugar"/>
<node CREATED="1327014413457" MODIFIED="1327014413457" TEXT="1/2 tsp salt"/>
<node CREATED="1327014413457" MODIFIED="1327014413457" TEXT="1/2 cup plus 2 tbsp vegan margarine"/>
<node CREATED="1327014413457" MODIFIED="1327014413457" TEXT="2 tbsp vegetable oil"/>
<node CREATED="1327014413457" MODIFIED="1327014413457" TEXT="3 tbsp cold water"/>
<node CREATED="1327014413457" MODIFIED="1327014413457" TEXT="Preparation:"/>
<node CREATED="1327014413457" MODIFIED="1327014413457" TEXT="Combine the flour, sugar and salt in a large bowl. Cut in the vegan margarine, mixing until crumbly."/>
<node CREATED="1327014413457" MODIFIED="1327014413457" TEXT="In a separate bowl, whisk together the oil and water, then add to the flour mixture, mixing just until a dough forms."/>
<node CREATED="1327014413458" MODIFIED="1327014413458" TEXT="Cover the dough with plastic wrap, then chill for 30 minutes."/>
<node CREATED="1327014413458" MODIFIED="1327014413458" TEXT="Roll out onto a lightly floured surface to about 1/4 inch thickness and gently press into a pie tin."/>
<node CREATED="1327014413465" MODIFIED="1327014413465" TEXT="Links">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1327014413466" LINK="http://vegetarian.about.com/od/guideproductpicks/qt/veganmargarine.htm" MODIFIED="1327014413466" TEXT="vegan margarine"/>
</node>
</node>
</node>
<node CREATED="1327014943241" FOLDED="true" ID="Freemind_Link_1940997276" MODIFIED="1380230103770" TEXT="Compare&#xa;Mushroom Pate">
<node CREATED="1327014863959" ID="Freemind_Link_1865259931" MODIFIED="1329276823754" TEXT="Vegan Walnut Mushroom Pate (with tofu)&#xa;(requires food processor)&#xa;">
<node CREATED="1327014893199" FOLDED="true" ID="Freemind_Link_32002490" LINK="http://vegetarian.about.com/od/saucesdipsspreads/r/walnutpate.htm" MODIFIED="1380230100987" TEXT="vegetarian.about.com &gt; Od &gt; Saucesdipsspreads &gt; R &gt; Walnutpate">
<node CREATED="1327014909287" ID="ID_166863553" MODIFIED="1327014909287" TEXT="A vegetarian and vegan pate recipe made from mushrooms and walnuts for plenty of texture and flavor. Vegetarian pate is much healthier and lower in fat than the real thing. Serve your vegetarian and vegan walnut pate chilled with bread or crackers."/>
</node>
<node CREATED="1327014909287" ID="Freemind_Link_1366239548" MODIFIED="1327014909287" TEXT="Ingredients:">
<node CREATED="1327014909287" MODIFIED="1327014909287" TEXT="&#x2022;        3 tbsp olive oil"/>
<node CREATED="1327014909287" MODIFIED="1327014909287" TEXT="&#x2022;        3/4 pound sliced fresh mushrooms"/>
<node CREATED="1327014909287" MODIFIED="1327014909287" TEXT="&#x2022;        1/2 onion, sliced"/>
<node CREATED="1327014909287" MODIFIED="1327014909287" TEXT="&#x2022;        2 cloves garlic, minced"/>
<node CREATED="1327014909287" MODIFIED="1327014909287" TEXT="&#x2022;        1/4 pound firm tofu, mashed"/>
<node CREATED="1327014909287" MODIFIED="1327014909287" TEXT="&#x2022;        1/2 cup walnuts"/>
<node CREATED="1327014909287" MODIFIED="1327014909287" TEXT="&#x2022;        1/4 tsp salt"/>
<node CREATED="1327014909287" MODIFIED="1327014909287" TEXT="&#x2022;        1/4 tsp black pepper"/>
</node>
<node CREATED="1327014909287" ID="Freemind_Link_1714101567" MODIFIED="1327014909287" TEXT="Preparation:">
<node CREATED="1327014909287" MODIFIED="1327014909287" TEXT="Sautee the mushrooms, onion and garlic in olive oil until the onions turn soft, about 5 minutes."/>
<node CREATED="1327014909287" MODIFIED="1327014909287" TEXT="Put the mushrooms mixture and tofu in a food processor or blender and puree until smooth."/>
<node CREATED="1327014909287" MODIFIED="1327014909287" TEXT="Add the walnuts, salt and pepper, and puree again until smooth."/>
<node CREATED="1327014909288" ID="ID_1905060701" MODIFIED="1327014909288" TEXT="Chill the vegetarian pate thoroughly before serving. Serve your pate with bread or crackers."/>
</node>
</node>
<node CREATED="1327014948502" ID="Freemind_Link_545741581" MODIFIED="1327014974357" TEXT="Herbed Mushroom Pate Recipe&#xa;(requires food processor)&#xa;(warning, raw garlic)">
<node CREATED="1327015046392" FOLDED="true" ID="Freemind_Link_524488876" LINK="http://vegetarian.about.com/od/saucesdipsspreads/r/herbedpate.htm" MODIFIED="1380230102779" TEXT="vegetarian.about.com &gt; Od &gt; Saucesdipsspreads &gt; R &gt; Herbedpate">
<node CREATED="1327015049472" ID="ID_1063088055" MODIFIED="1327015049472" TEXT="No need to eat chopped liver (ew!); try a vegetarian pate made from mushrooms and fresh herbs instead. Yum! Not sure what to do with pate? Spread it on bagels for a casual lazy breakfast, serve it with artisan bread or crackers for a party appetizer for a holiday or Christmas party. This herbed mushroom pate is vegetarian, vegan (if you use a vegan margarine) and cholesterol-free!"/>
</node>
<node CREATED="1327015049472" ID="Freemind_Link_349167849" MODIFIED="1327015049472" TEXT="Ingredients:">
<node CREATED="1327015049472" MODIFIED="1327015049472" TEXT="&#x2022;        1 onion, chopped"/>
<node CREATED="1327015049472" ID="Freemind_Link_1822664401" MODIFIED="1327015644086" TEXT="&#x2022;        1 tbsp (vegan) margarine"/>
<node CREATED="1327015049472" MODIFIED="1327015049472" TEXT="&#x2022;        2 pounds fresh mushrooms, finely chopped"/>
<node CREATED="1327015049472" MODIFIED="1327015049472" TEXT="&#x2022;        4 cloves garlic, minced"/>
<node CREATED="1327015049472" MODIFIED="1327015049472" TEXT="&#x2022;        2 tbsp chopped fresh parsley"/>
<node CREATED="1327015049472" MODIFIED="1327015049472" TEXT="&#x2022;        3/4 tsp rosemary"/>
<node CREATED="1327015049473" ID="ID_747210091" MODIFIED="1327015049473" TEXT="&#x2022;        1 1/2 cups bread crumbs"/>
<node CREATED="1327015049473" MODIFIED="1327015049473" TEXT="&#x2022;        2 tbsp lemon juice"/>
<node CREATED="1327015049473" MODIFIED="1327015049473" TEXT="&#x2022;        salt and pepper to taste"/>
</node>
<node CREATED="1327015049473" ID="Freemind_Link_1095904312" MODIFIED="1327015049473" TEXT="Preparation:">
<node CREATED="1327015049473" ID="Freemind_Link_696288789" MODIFIED="1329277866911" TEXT="In a large saucepan, heat the margarine and cook the onion [DURANT and garlic] for 5-6 minutes, until soft."/>
<node CREATED="1327015049473" MODIFIED="1327015049473" TEXT="Add the mushroom and cook for another 8-10 minutes, until well cooked. Remove the mixture from the heat and add the remaining ingredients."/>
<node CREATED="1327015049473" MODIFIED="1327015049473" TEXT="Transfer to a blender or food processor, and pulse until desired consistency is achieved."/>
</node>
</node>
</node>
<node CREATED="1327015849169" FOLDED="true" ID="Freemind_Link_438458407" MODIFIED="1380230104636" TEXT="mushroom pie">
<node CREATED="1327014456028" ID="Freemind_Link_1579283830" MODIFIED="1327014456028" TEXT="wild mushroom pie with sage, caramelized onion and white truffle oil">
<node CREATED="1327014486347" MODIFIED="1327014486347" TEXT="add 1 Tbsp of fresh minced sage and 1 tsp of raw cider vinegar to the pie crust recipe"/>
<node CREATED="1329002093544" ID="Freemind_Link_101247846" MODIFIED="1329002094083" TEXT="from">
<node CREATED="1329002086286" ID="Freemind_Link_660299691" LINK="http://vegetarian.about.com/od/piecrustrecipes/r/flakypiecrust.htm" MODIFIED="1329002086286" TEXT="vegetarian.about.com &gt; Od &gt; Piecrustrecipes &gt; R &gt; Flakypiecrust"/>
</node>
</node>
<node CREATED="1329002760034" ID="Freemind_Link_1784314176" MODIFIED="1329003155755" TEXT="Vegan Mushroom Pie&#xa;Veggie Num Num">
<node CREATED="1327015929713" LINK="http://www.veggienumnum.com/2010/08/mushroom-pie/" MODIFIED="1327015929713" TEXT="veggienumnum.com &gt; 2010 &gt; 08 &gt; Mushroom-pie"/>
<node CREATED="1329002763870" MODIFIED="1329002763870" TEXT="for the filling">
<node CREATED="1329002763870" MODIFIED="1329002763870" TEXT="&#x25aa;        50g (2oz) assorted dried mushrooms"/>
<node CREATED="1329002763870" MODIFIED="1329002763870" TEXT="&#x25aa;        350g (12oz) button mushrooms, sliced"/>
<node CREATED="1329002763870" MODIFIED="1329002763870" TEXT="&#x25aa;        250g (9oz) Swiss brown mushrooms, sliced"/>
<node CREATED="1329002763870" MODIFIED="1329002763870" TEXT="&#x25aa;        2 tbs olive oil"/>
<node CREATED="1329002763870" MODIFIED="1329002763870" TEXT="&#x25aa;        200g (7oz) baby onions (approx. 5), sliced"/>
<node CREATED="1329002763870" MODIFIED="1329002763870" TEXT="&#x25aa;        3 garlic cloves, diced"/>
<node CREATED="1329002763870" MODIFIED="1329002763870" TEXT="&#x25aa;        &#xbd; cup dry white wine"/>
<node CREATED="1329002763870" MODIFIED="1329002763870" TEXT="&#x25aa;        1 stock cube (I used a no-animal-content &#x2018;beef&#x2019; stock)"/>
<node CREATED="1329002763870" MODIFIED="1329002763870" TEXT="&#x25aa;        2 tsp cornflour (corn starch)"/>
<node CREATED="1329002763870" MODIFIED="1329002763870" TEXT="&#x25aa;        &#xbc; cup soy cream or natural set yoghurt"/>
</node>
<node CREATED="1329002763870" MODIFIED="1329002763870" TEXT="for the pastry">
<node CREATED="1329002763870" ID="Freemind_Link_1803129426" MODIFIED="1329002763870" TEXT="&#x25aa;        225g (8oz) flour">
<node CREATED="1330574150994" ID="Freemind_Link_596940736" MODIFIED="1330574155425" TEXT="1 cup"/>
</node>
<node CREATED="1329002763870" ID="Freemind_Link_195053592" MODIFIED="1329002763870" TEXT="&#x25aa;        115g (4oz) vegan margarine or butter">
<node CREATED="1330574163946" ID="Freemind_Link_1785512936" MODIFIED="1330574168927" TEXT="1/2 cup">
<node CREATED="1330574246148" ID="Freemind_Link_341639072" MODIFIED="1330574255644" TEXT="8 Tbsp (dry)"/>
</node>
</node>
<node CREATED="1329002763870" MODIFIED="1329002763870" TEXT="&#x25aa;        1 tsp dried thyme"/>
<node CREATED="1329002763870" MODIFIED="1329002763870" TEXT="&#x25aa;        &#xbc; cup cold water"/>
<node CREATED="1329002763870" MODIFIED="1329002763870" TEXT="&#x25aa;        1 tsp black sesame seeds"/>
</node>
<node CREATED="1329102665738" ID="Freemind_Link_1878291407" MODIFIED="1329102669774" TEXT="method">
<node CREATED="1329102684728" MODIFIED="1329102684728" TEXT="Place the dried mushrooms in a heat proof bowl and cover with hot water. Allow to soak for 15-30 minutes until tender. Drain reserving liquid, slice the mushrooms and set aside."/>
<node CREATED="1329102684728" MODIFIED="1329102684728" TEXT="Combine 1 cup of the reserved liquid with the stock cube and cornflour (cornstarch) until dissolved, set aside."/>
<node CREATED="1329102684728" MODIFIED="1329102684728" TEXT="Heat the olive oil in a good sized frypan over a medium heat, add the sliced baby onions and fry for 3-4 minutes until soft, add the garlic and cook for a further minute."/>
<node CREATED="1329102684728" MODIFIED="1329102684728" TEXT="Add the button mushrooms and continue to cook over a medium heat until they start to soften, add the Swiss mushrooms and cook for a further few minuted. Add the sliced assorted mushrooms and bring the heat to high, add the wine and allow to simmer for 1 minute before adding the reserved liquid, stock and cornflour mixture."/>
<node CREATED="1329102684728" MODIFIED="1329102684728" TEXT="Turn down the heat to low and let the mushrooms cook over a very gentle heat for 10-15 minutes."/>
<node CREATED="1329102684728" ID="Freemind_Link_986573271" MODIFIED="1330571977605" TEXT="Once the mushroom mixture has gently cooked for 10-15minutes and is reduced, remove from the heat and stir through the soy cream or yoghurt. Check seasoning and add salt if needed and lots of fresh cracked pepper to taste."/>
<node CREATED="1330574994748" ID="Freemind_Link_1148639311" MODIFIED="1330574999920" TEXT="crust">
<node CREATED="1329102684729" MODIFIED="1329102684729" TEXT="Meanwhile pre-heat the oven to 180&#xb0;C/356&#xb0;F"/>
<node CREATED="1329102684729" ID="ID_303270111" MODIFIED="1329102684729" TEXT="Rub the margarine or butter together with the sifted flour in a large bowl until it resembles fine bread crumbs. Add the dried thyme and a pinch of salt, combine."/>
<node CREATED="1329102684729" MODIFIED="1329102684729" TEXT="Drop by drop add the cold water until the mixture forms a dough, you may not need all the water. Roll the dough into a smooth ball, cover with plastic wrap and refrigerate for 10 minutes."/>
<node CREATED="1329102684729" MODIFIED="1329102684729" TEXT="Grease a 35cm pie dish and set aside."/>
<node CREATED="1329102684729" MODIFIED="1329102684729" TEXT="Remove the pastry from the fridge and cut into two pieces, one slightly larger than the other."/>
<node CREATED="1329102684729" MODIFIED="1329102684729" TEXT="Roll the larger ball out on a floured surface large enough to line the base and sides of your pie dish. Lay the pastry into to prepared dish prick with a fork a couple of times and return the dish to the fridge for another 10 minutes. Roll out the remainder of the pastry to form the pie lid."/>
</node>
<node CREATED="1329102684729" ID="Freemind_Link_1414388363" MODIFIED="1329102684729" TEXT="Pour the mushroom filling into the prepared pie crust, trim the edges of the pastry from the dish and top with the pie lid. Seal the edges of the pastry case by gentle pressing drown with a fork or your fingers. Cut a cross in the middle of the pie lid, sprinkle with black sesame seeds and wash with non-dairy milk."/>
<node CREATED="1329102684729" ID="Freemind_Link_971241190" MODIFIED="1330572040643" TEXT="Place in the pre-heated oven for 20-25minutes until pastry is golden and pie bubbling. Remove from the oven and allow to sit for a few minutes before devouring."/>
<node CREATED="1329102684729" ID="Freemind_Link_244883796" MODIFIED="1330572181740" TEXT="Serve with blanched spinach or steamed green vegetables for a wonderful elegant vegan/vegetarian meal."/>
</node>
</node>
<node CREATED="1329002958000" ID="Freemind_Link_1923261845" MODIFIED="1329003187749" TEXT="Vegan Mushroom Pie&#xa;Village Voice (UK) Recipe Book">
<node CREATED="1327015853967" ID="Freemind_Link_1892995197" LINK="http://www.veganvillage.co.uk/recipes/mushale.htm" MODIFIED="1327015853967" TEXT="veganvillage.co.uk &gt; Recipes &gt; Mushale">
<node CREATED="1327016059185" ID="Freemind_Link_837408516" MODIFIED="1327016079436" TEXT="great idea to cook the pie lid separately on a greased pan on the top shelf of the oven"/>
</node>
<node CREATED="1329002959909" ID="Freemind_Link_1873794027" MODIFIED="1329002959909" TEXT="Ingredients">
<node CREATED="1329003012187" MODIFIED="1329003012187" TEXT="1 onion"/>
<node CREATED="1329003012187" MODIFIED="1329003012187" TEXT="&#x2028;2 cloves garlic"/>
<node CREATED="1329003012187" MODIFIED="1329003012187" TEXT="&#x2028;1 lb leeks sliced into one inch slices and washed thoroughly"/>
<node CREATED="1329003012188" MODIFIED="1329003012188" TEXT="&#x2028;1 lb mushrooms"/>
<node CREATED="1329003012188" MODIFIED="1329003012188" TEXT="&#x2028;&#xbd; bottle stout (e.g. Samuel Smith&apos;s Imperial Stout or try Black Sheep Ale)"/>
<node CREATED="1329003012188" MODIFIED="1329003012188" TEXT="&#x2028;Soy sauce"/>
<node CREATED="1329003012188" MODIFIED="1329003012188" TEXT="&#x2028;1 teaspoon yeast extract"/>
<node CREATED="1329003012188" MODIFIED="1329003012188" TEXT="&#x2028;1 oz flour"/>
<node CREATED="1329003012188" MODIFIED="1329003012188" TEXT="&#x2028;&#xbd; tbsp sugar"/>
<node CREATED="1329003012188" MODIFIED="1329003012188" TEXT="&#x2028;&#xbc; lb bag garden peas"/>
<node CREATED="1329003012188" MODIFIED="1329003012188" TEXT="&#x2028;Salt and pepper"/>
<node CREATED="1329003012188" MODIFIED="1329003012188" TEXT="&#x2028;&#xbd; block jus roll puff pastry"/>
</node>
<node CREATED="1329002959909" ID="Freemind_Link_105697173" MODIFIED="1329002959909" TEXT="Notes">
<node CREATED="1329002959909" MODIFIED="1329002959909" TEXT="Serve with chips, peas and tomato sauce!"/>
</node>
</node>
</node>
<node CREATED="1329277993564" ID="Freemind_Link_239523507" MODIFIED="1329278027760" TEXT="mushroom pate + mushroom pie + roasted nuts (walnuts or hazelnuts)"/>
<node CREATED="1329001922242" FOLDED="true" ID="Freemind_Link_1407963962" MODIFIED="1329001926328" TEXT="bock choi">
<node CREATED="1329001926580" ID="Freemind_Link_1076017361" MODIFIED="1329001935029" TEXT="mirin">
<node CREATED="1329001935388" ID="Freemind_Link_1789327248" MODIFIED="1329001937032" TEXT="rice wine"/>
</node>
<node CREATED="1329001938013" ID="Freemind_Link_858251048" MODIFIED="1329001941360" TEXT="soy sauce"/>
<node CREATED="1329001941732" ID="Freemind_Link_265576203" MODIFIED="1329001945280" TEXT="sesame oil"/>
</node>
<node CREATED="1365615462327" ID="ID_302525826" MODIFIED="1365615601835" TEXT="puee of eggplant (&amp; (sweet) potato)">
<node CREATED="1365615501968" ID="ID_1485745464" MODIFIED="1365615509680" TEXT="garlic and onions in oil w/salt"/>
<node CREATED="1365615538025" ID="ID_341392655" MODIFIED="1365615539649" TEXT="maybe">
<node CREATED="1365615539953" ID="ID_336713336" MODIFIED="1365615542753" TEXT="chili peppers"/>
</node>
<node CREATED="1365615511233" ID="ID_117438455" MODIFIED="1365615516895" TEXT="add veggies">
<node CREATED="1365615591667" ID="ID_1388410843" MODIFIED="1365615593724" TEXT="cook until soft"/>
</node>
<node CREATED="1365615523545" ID="ID_26296695" MODIFIED="1365615524296" TEXT="blend"/>
<node CREATED="1365615525977" ID="ID_520636461" MODIFIED="1365615532435" TEXT="olive oil"/>
<node CREATED="1365615532665" ID="ID_1997968281" MODIFIED="1365615533808" TEXT="salt"/>
<node CREATED="1365615472359" ID="ID_627476031" MODIFIED="1365615478095" TEXT="at end">
<node CREATED="1365615478320" ID="ID_335779140" MODIFIED="1365615480311" TEXT="fresh corn"/>
<node CREATED="1365615480911" ID="ID_537624083" MODIFIED="1365615484087" TEXT="fresh green onion"/>
</node>
</node>
</node>
<node CREATED="1384890675933" FOLDED="true" ID="ID_1563078443" MODIFIED="1455252256107" TEXT="lightly sweetened roated yam cubes">
<node CREATED="1384890814661" ID="ID_1854058817" MODIFIED="1384890816608" TEXT="3 yams"/>
<node CREATED="1384890846702" ID="ID_952277332" MODIFIED="1384890852420" TEXT="melt in microwave 20 seconds">
<node CREATED="1384890816956" ID="ID_884013484" MODIFIED="1384890824216" TEXT="1T">
<node CREATED="1384890831157" ID="ID_1252095776" MODIFIED="1384890833856" TEXT="olive oil"/>
</node>
<node CREATED="1384890816956" ID="ID_243651509" MODIFIED="1384890824216" TEXT="1T">
<node CREATED="1384890835070" ID="ID_414126499" MODIFIED="1384890836224" TEXT="butter"/>
</node>
<node CREATED="1384890816956" ID="ID_1350403715" MODIFIED="1384890839841" TEXT="2T">
<node CREATED="1384890841533" ID="ID_984450891" MODIFIED="1384890844416" TEXT="brown sugar"/>
</node>
<node CREATED="1384890858925" ID="ID_627692273" MODIFIED="1384890861097" TEXT="1t">
<node CREATED="1384890861293" ID="ID_328656583" MODIFIED="1384890864273" TEXT="cinnamon"/>
</node>
<node CREATED="1384890865030" ID="ID_1089839781" MODIFIED="1384890866824" TEXT="1/4t">
<node CREATED="1384890867238" ID="ID_282845664" MODIFIED="1384890870433" TEXT="nutmeg">
<node CREATED="1384890870637" ID="ID_422044686" MODIFIED="1384890872785" TEXT="6 gratings"/>
</node>
</node>
<node CREATED="1384890876967" ID="ID_1569035335" MODIFIED="1384890878753" TEXT="pinch">
<node CREATED="1384890879286" ID="ID_1566431625" MODIFIED="1384890883681" TEXT="ginger powder"/>
</node>
</node>
<node CREATED="1384890887478" ID="ID_1834835688" MODIFIED="1384890890634" TEXT="cook at 350"/>
<node CREATED="1384890891110" ID="ID_1659590253" MODIFIED="1384890892465" TEXT="45">
<node CREATED="1384890893647" ID="ID_1482546303" MODIFIED="1384890895578" TEXT="minutes"/>
</node>
<node CREATED="1384890896855" ID="ID_1962678210" MODIFIED="1384890901945" TEXT="stir 3x"/>
</node>
<node CREATED="1384890688186" FOLDED="true" ID="ID_28781851" MODIFIED="1455252263135" TEXT="roasted rosemary potatoes">
<node CREATED="1384890699282" ID="ID_1764513031" MODIFIED="1384890704557" TEXT="cut potatoes into cubes"/>
<node CREATED="1384890705034" ID="ID_1406848087" MODIFIED="1384890714198" TEXT="shake in tupperware with">
<node CREATED="1384890714419" ID="ID_737333256" MODIFIED="1384890717430" TEXT="olive oil"/>
<node CREATED="1384890717763" ID="ID_958921181" MODIFIED="1384890719262" TEXT="salt"/>
<node CREATED="1384890719507" ID="ID_1932001261" MODIFIED="1384890722805" TEXT="pepper">
<node CREATED="1384890723169" ID="ID_340456206" MODIFIED="1384890793344" TEXT="or McCormicks(?) seasoning"/>
</node>
<node CREATED="1384890735387" ID="ID_219311598" MODIFIED="1384890740302" TEXT="fresh rosemary"/>
</node>
<node CREATED="1384890799540" ID="ID_871179885" MODIFIED="1384890809410" TEXT="roast on cookie sheet at 350 stirring a couple of times"/>
</node>
<node CREATED="1326672104300" FOLDED="true" ID="Freemind_Link_21403543" MODIFIED="1455252268040" TEXT="vegetables a la Las Toninas">
<linktarget COLOR="#b0b0b0" DESTINATION="Freemind_Link_21403543" ENDARROW="Default" ENDINCLINATION="216;0;" ID="Arrow_ID_1666818639" SOURCE="ID_112815093" STARTARROW="None" STARTINCLINATION="216;0;"/>
<node CREATED="1326672117374" ID="Freemind_Link_724997742" MODIFIED="1326672129612" TEXT="sauces">
<node CREATED="1326673017886" ID="Freemind_Link_789685193" MODIFIED="1326673017886" TEXT="">
<node CREATED="1326673025267" MODIFIED="1326673025267" TEXT="lemon"/>
<node CREATED="1326673025268" MODIFIED="1326673025268" TEXT="oil"/>
<node CREATED="1326673025268" MODIFIED="1326673025268" TEXT="herbs"/>
<node CREATED="1326673025268" ID="Freemind_Link_908686343" MODIFIED="1326673025268" TEXT="maybe">
<node CREATED="1326673025268" MODIFIED="1326673025268" TEXT="balsamic vinegar"/>
<node CREATED="1326737468775" ID="Freemind_Link_599063250" MODIFIED="1326737474274" TEXT="heat">
<node CREATED="1326737474639" ID="Freemind_Link_979527189" MODIFIED="1326737480531" TEXT="red chili flakes"/>
<node CREATED="1326737481471" ID="Freemind_Link_1631678547" MODIFIED="1326737486675" TEXT="chili powder"/>
</node>
<node CREATED="1326737463918" ID="Freemind_Link_227874891" MODIFIED="1326737466650" TEXT="peanut butter"/>
<node CREATED="1326737537293" ID="Freemind_Link_1281194142" MODIFIED="1326737538867" TEXT="ginger"/>
<node CREATED="1326737553142" ID="Freemind_Link_1404206516" MODIFIED="1326737555578" TEXT="lemon grass"/>
</node>
<node CREATED="1326673025268" ID="Freemind_Link_1038234066" MODIFIED="1326673025268" TEXT="choice">
<node CREATED="1326673025268" MODIFIED="1326673025268" TEXT="salt"/>
<node CREATED="1326673025268" MODIFIED="1326673025268" TEXT="soy sauce"/>
</node>
</node>
<node CREATED="1332382424316" ID="Freemind_Link_1177341155" MODIFIED="1332382425135" TEXT="try">
<node CREATED="1332382426732" ID="Freemind_Link_1250216326" MODIFIED="1332382429535" TEXT="peanut butter"/>
<node CREATED="1332384299535" ID="Freemind_Link_551629436" MODIFIED="1332384301290" TEXT="water"/>
<node CREATED="1332382430059" ID="Freemind_Link_1065654856" MODIFIED="1332382432431" TEXT="soy sauce"/>
<node CREATED="1332382432916" ID="Freemind_Link_1011596256" MODIFIED="1332382439248" TEXT="rice wine vinegar"/>
<node CREATED="1332382474964" ID="Freemind_Link_1045616386" MODIFIED="1332382478080" TEXT="coq sauce"/>
</node>
<node CREATED="1326672156614" ID="Freemind_Link_1464172900" MODIFIED="1326672156614" TEXT="">
<node CREATED="1326672161366" ID="Freemind_Link_354963225" MODIFIED="1326672164690" TEXT="garlic"/>
<node CREATED="1326672166179" ID="Freemind_Link_415755202" MODIFIED="1326672171915" TEXT="tomato">
<node CREATED="1326672173742" ID="Freemind_Link_1516632260" MODIFIED="1326672180866" TEXT="romas are easiest to cut"/>
</node>
<node CREATED="1326672184966" ID="Freemind_Link_532124839" MODIFIED="1332382420463" TEXT="fresh corn"/>
</node>
<node CREATED="1326672131846" ID="Freemind_Link_1937199045" MODIFIED="1326672131846" TEXT="">
<node CREATED="1326672140270" ID="Freemind_Link_572695553" MODIFIED="1326672142778" TEXT="garlic"/>
<node CREATED="1326672144279" ID="Freemind_Link_1282943372" MODIFIED="1326672147817" TEXT="wine"/>
<node CREATED="1326672150535" ID="Freemind_Link_1114424615" MODIFIED="1326672153101" TEXT="cream"/>
<node CREATED="1326672192022" ID="Freemind_Link_869433327" MODIFIED="1326672195340" TEXT="herb">
<node CREATED="1326672197364" ID="Freemind_Link_516427710" MODIFIED="1326672201154" TEXT="thyme"/>
<node CREATED="1326672202991" ID="Freemind_Link_500266268" MODIFIED="1326672206666" TEXT="rosemary?"/>
</node>
</node>
<node CREATED="1327097074801" ID="Freemind_Link_608857407" MODIFIED="1327097074801" TEXT="">
<node CREATED="1327097075673" ID="Freemind_Link_1597769671" MODIFIED="1327097080052" TEXT="balsamic vinegar"/>
<node CREATED="1327097081017" ID="Freemind_Link_1876804318" MODIFIED="1327097087445" TEXT="marmalade">
<node CREATED="1327097087849" ID="Freemind_Link_1626100781" MODIFIED="1327097089300" TEXT="fig!"/>
</node>
<node CREATED="1327097090289" ID="Freemind_Link_1194198413" MODIFIED="1327097092804" TEXT="dash of oil"/>
<node CREATED="1327097093465" ID="Freemind_Link_1255961885" MODIFIED="1327097096500" TEXT="great with fish"/>
<node CREATED="1327097113537" ID="Freemind_Link_800112220" MODIFIED="1327097116356" TEXT="heat">
<node CREATED="1326737474639" ID="Freemind_Link_1951803120" MODIFIED="1326737480531" TEXT="red chili flakes"/>
</node>
</node>
<node CREATED="1326672922026" ID="Freemind_Link_1946548207" MODIFIED="1326672927525" TEXT="julie">
<node CREATED="1326672947167" ID="Freemind_Link_1561348339" MODIFIED="1326672947167" TEXT="1 part each">
<node CREATED="1326672947167" MODIFIED="1326672947167" TEXT="lemon juice"/>
<node CREATED="1326672947167" MODIFIED="1326672947167" TEXT="soy sauce"/>
<node CREATED="1326672947167" MODIFIED="1326672947167" TEXT="olive oil"/>
</node>
</node>
</node>
<node CREATED="1326672215910" ID="Freemind_Link_1369427735" MODIFIED="1326672219722" TEXT="combine">
<node CREATED="1326672221543" ID="Freemind_Link_66596058" MODIFIED="1326672221543" TEXT="">
<node CREATED="1326672227159" ID="Freemind_Link_1510553274" MODIFIED="1326672231541" TEXT="lemon">
<node CREATED="1326672246959" ID="Freemind_Link_260337418" MODIFIED="1326672251595" TEXT="not more than half"/>
</node>
<node CREATED="1326672233623" ID="Freemind_Link_601711223" MODIFIED="1326672239154" TEXT="green apple"/>
<node CREATED="1326672241015" ID="Freemind_Link_1721061472" MODIFIED="1326672244315" TEXT="beet"/>
<node CREATED="1326672255167" ID="Freemind_Link_1917502662" MODIFIED="1326672261896" TEXT="great with fish">
<node CREATED="1326672263823" ID="Freemind_Link_1137884937" MODIFIED="1326672275107" TEXT="can be canned (oil better than water)"/>
</node>
</node>
</node>
<node CREATED="1326672294031" ID="Freemind_Link_1774550845" MODIFIED="1326672301613" TEXT="cook in stages">
<node CREATED="1326672303519" ID="Freemind_Link_479941014" MODIFIED="1326672306834" TEXT="pre">
<node CREATED="1326672333320" MODIFIED="1326672333320" TEXT="garlic"/>
<node CREATED="1326672333320" MODIFIED="1326672333320" TEXT="onions"/>
<node CREATED="1326672333320" MODIFIED="1326672333320" TEXT="oil"/>
<node CREATED="1326672333320" MODIFIED="1326672333320" TEXT="salt"/>
</node>
<node CREATED="1326672810960" ID="Freemind_Link_862640641" MODIFIED="1326672886017" TEXT="15 minute long cook stage optional">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1326672887456" MODIFIED="1326672887456" TEXT="lentils"/>
<node CREATED="1326672887456" MODIFIED="1326672887456" TEXT="osso bucco"/>
</node>
<node CREATED="1326672350436" ID="Freemind_Link_1046610954" MODIFIED="1326672350436" TEXT="5 minutes">
<node CREATED="1326672377942" MODIFIED="1326672377942" TEXT="potato"/>
<node CREATED="1326672378587" ID="ID_349903163" MODIFIED="1326672378587" TEXT="sweet potato"/>
<node CREATED="1326672378588" MODIFIED="1326672378588" TEXT="carrot"/>
<node CREATED="1326672651732" ID="Freemind_Link_1173483962" MODIFIED="1326672651732" TEXT="squash (can actually leave skin on, becomes like apple skin)"/>
<node CREATED="1326672378588" ID="Freemind_Link_939830796" MODIFIED="1326672453719" TEXT="eggplant"/>
<node CREATED="1326672697478" MODIFIED="1326672697478" TEXT="beet"/>
<node CREATED="1326672697478" MODIFIED="1326672697478" TEXT="purple cabbage (great with cream)"/>
<node CREATED="1326672697478" MODIFIED="1326672697478" TEXT="chorizo"/>
<node CREATED="1326672422799" ID="Freemind_Link_790789101" MODIFIED="1326672422799" TEXT="white part of green onion"/>
<node CREATED="1326672422799" MODIFIED="1326672422799" TEXT="leek (entire, including green stem)"/>
</node>
<node CREATED="1382230494148" ID="ID_1232825501" MODIFIED="1382230516482" TEXT="20 minutes w/lid"/>
<node CREATED="1326672352404" ID="Freemind_Link_1820810873" MODIFIED="1326672774644" TEXT="last 5 minutes cooking">
<node CREATED="1326672511070" ID="Freemind_Link_1388605336" MODIFIED="1326672511070" TEXT="tomato"/>
<node CREATED="1326672511070" ID="ID_1796606645" MODIFIED="1326672511070" TEXT="zuchinni"/>
<node CREATED="1326672511070" MODIFIED="1326672511070" TEXT="zapallito"/>
<node CREATED="1326672534078" MODIFIED="1326672534078" TEXT="red pepper"/>
</node>
<node CREATED="1326672465205" ID="Freemind_Link_1356993426" MODIFIED="1326672784558" TEXT="last 5 minutes (off w/lid on)">
<node CREATED="1326672470577" ID="Freemind_Link_1285048098" MODIFIED="1326672474860" TEXT="corn"/>
<node CREATED="1326672493726" MODIFIED="1326672493726" TEXT="green part of green onion"/>
<node CREATED="1326672545322" ID="Freemind_Link_1473646633" MODIFIED="1326672550365" TEXT="cream"/>
</node>
</node>
<node CREATED="1326673068478" ID="Freemind_Link_359019085" MODIFIED="1326673074197" TEXT="eat with">
<node CREATED="1326673099700" MODIFIED="1326673099700" TEXT="crackers">
<node CREATED="1326673099701" MODIFIED="1326673099701" TEXT="try various"/>
<node CREATED="1326673099701" MODIFIED="1326673099701" TEXT="akmaks"/>
<node CREATED="1326673099701" MODIFIED="1326673099701" TEXT="wheat thins"/>
<node CREATED="1326673100276" MODIFIED="1326673100276" TEXT="seeded"/>
</node>
<node CREATED="1326673116369" MODIFIED="1326673116369" TEXT="rice"/>
<node CREATED="1326673116369" MODIFIED="1326673116369" TEXT="pasta"/>
<node CREATED="1326673116369" ID="ID_523889957" MODIFIED="1326673116369" TEXT="fresh bread"/>
<node CREATED="1326673116369" MODIFIED="1326673116369" TEXT="toast"/>
</node>
</node>
<node CREATED="1349162298010" FOLDED="true" ID="ID_1424202269" MODIFIED="1381971183766" TEXT="vegetables US,NZ">
<node CREATED="1349162302742" ID="ID_610569600" MODIFIED="1349162318685" TEXT="curry veggies"/>
<node CREATED="1349162332946" ID="ID_1484358414" MODIFIED="1349162335216" TEXT="grilled cheese veggies"/>
</node>
</node>
<node CREATED="1337033703086" ID="ID_112815093" MODIFIED="1381971186001" POSITION="right" TEXT="Sauces">
<arrowlink DESTINATION="Freemind_Link_21403543" ENDARROW="Default" ENDINCLINATION="216;0;" ID="Arrow_ID_1666818639" STARTARROW="None" STARTINCLINATION="216;0;"/>
<node CREATED="1328995102956" FOLDED="true" ID="Freemind_Link_646094903" MODIFIED="1455252281720" TEXT="curries">
<node CREATED="1328995266903" ID="Freemind_Link_1356375471" MODIFIED="1328995267867" TEXT="book">
<node CREATED="1328995449724" FOLDED="true" ID="Freemind_Link_668399103" MODIFIED="1328995449724" TEXT="Curry Cuisine">
<node CREATED="1328995413455" LINK="http://www.amazon.com/Curry-Cuisine-David-Thompson/dp/0756662079/ref=sr_1_1?ie=UTF8&amp;qid=1328995403&amp;sr=8-1" MODIFIED="1328995413455" TEXT="amazon.com &gt; Curry-Cuisine-David-Thompson &gt; Dp &gt; 0756662079 &gt; Ref=sr 1 1 ? ..."/>
<node CREATED="1328995485084" MODIFIED="1328995485084" TEXT="David Thompson (Author), Corinne Trang (Author), Sri Owen (Author), Vivek Singh (Author), Mahmood Akbar (Author), Judy Bastyra (Author),Roopa Gulati (Author), Yasuko Fukuoka (Author), Das Sreedharam (Author)"/>
</node>
<node CREATED="1328997817108" ID="Freemind_Link_213403975" MODIFIED="1328997836523" TEXT="coconut cream">
<node CREATED="1328997819732" ID="Freemind_Link_514234348" MODIFIED="1328997821608" TEXT="cracked">
<node CREATED="1328997821844" ID="Freemind_Link_1523469109" MODIFIED="1328997830491" TEXT="means cooked until oil begins to separate"/>
</node>
</node>
<node CREATED="1328997901220" FOLDED="true" ID="Freemind_Link_1844436564" MODIFIED="1329001532643" TEXT="dry roasting spices&#xa;p. 322">
<node CREATED="1328997936914" ID="Freemind_Link_1827540403" MODIFIED="1328997937869" TEXT="1">
<node CREATED="1328997950799" ID="Freemind_Link_672359363" MODIFIED="1328997956643" TEXT="roast spices in a pan"/>
<node CREATED="1328997967254" ID="Freemind_Link_1440555136" MODIFIED="1328997968786" TEXT="1 minute"/>
<node CREATED="1328997969118" ID="Freemind_Link_1773181987" MODIFIED="1328997980475" TEXT="constantly stir/shake to avoid scorchin"/>
</node>
<node CREATED="1328997987543" ID="Freemind_Link_1267214890" MODIFIED="1328997987842" TEXT="2">
<node CREATED="1328997988247" ID="Freemind_Link_999880483" MODIFIED="1328997990130" TEXT="remove when">
<node CREATED="1328997990367" ID="Freemind_Link_875853428" MODIFIED="1328997992922" TEXT="start to darken"/>
</node>
<node CREATED="1328998007077" ID="Freemind_Link_1800836564" MODIFIED="1328998012049" TEXT="pour onto a plate too cool"/>
</node>
<node CREATED="1328998016694" ID="Freemind_Link_1863555122" MODIFIED="1328998017097" TEXT="3">
<node CREATED="1328998017437" ID="Freemind_Link_1265288636" MODIFIED="1328998019113" TEXT="grind">
<node CREATED="1328998019461" ID="Freemind_Link_1194928762" MODIFIED="1328998023017" TEXT="food processor"/>
</node>
</node>
<node CREATED="1328998026854" ID="Freemind_Link_1781340840" MODIFIED="1328998027529" TEXT="4">
<node CREATED="1328998027838" ID="Freemind_Link_361406520" MODIFIED="1328998029217" TEXT="optional">
<node CREATED="1328998029461" ID="Freemind_Link_28053302" MODIFIED="1328998030530" TEXT="sift"/>
</node>
</node>
</node>
<node CREATED="1328997915076" FOLDED="true" ID="Freemind_Link_88933880" MODIFIED="1329001536083" TEXT="tamarind water&#xa;p. 322">
<node CREATED="1328998156055" ID="Freemind_Link_49271475" MODIFIED="1328998157883" TEXT="ingredients">
<node CREATED="1328998068364" ID="Freemind_Link_892485727" MODIFIED="1328998075720" TEXT="tamarind pulp">
<node CREATED="1328998076157" ID="Freemind_Link_1871267444" MODIFIED="1328998081376" TEXT="1 oz">
<node CREATED="1328998081877" ID="Freemind_Link_523869790" MODIFIED="1328998083960" TEXT="30g"/>
</node>
</node>
<node CREATED="1328998091285" ID="Freemind_Link_1668445112" MODIFIED="1328998092128" TEXT="water">
<node CREATED="1328998092509" ID="Freemind_Link_1633889082" MODIFIED="1328998094625" TEXT="1/2C"/>
</node>
</node>
<node CREATED="1328998158111" ID="Freemind_Link_750189817" MODIFIED="1328998159067" TEXT="method">
<node CREATED="1328998336031" ID="Freemind_Link_974895063" MODIFIED="1328998336307" TEXT="1">
<node CREATED="1328998258585" ID="Freemind_Link_1951591692" MODIFIED="1328998263349" TEXT="soak in hot water">
<node CREATED="1328998263521" ID="Freemind_Link_451445217" MODIFIED="1328998266245" TEXT="10-15 minutes">
<node CREATED="1328998317655" ID="Freemind_Link_1239646598" MODIFIED="1328998327778" TEXT="or until soft (loosen with fingers)"/>
</node>
</node>
</node>
<node CREATED="1328998336847" ID="Freemind_Link_1044910829" MODIFIED="1328998337227" TEXT="2">
<node CREATED="1328998341311" ID="Freemind_Link_581983515" MODIFIED="1328998343907" TEXT="strain in sieve"/>
</node>
</node>
<node CREATED="1328998356086" ID="Freemind_Link_518228855" MODIFIED="1328998357889" TEXT="storage">
<node CREATED="1328998358149" ID="Freemind_Link_1718518253" MODIFIED="1328998363790" TEXT="can store in fridge for 2 weeks"/>
</node>
</node>
<node CREATED="1328996166048" FOLDED="true" ID="Freemind_Link_592864604" MODIFIED="1328996170676" TEXT="adjusting curry flavors">
<node CREATED="1328996323853" ID="Freemind_Link_426337123" MODIFIED="1328996326353" TEXT="needs salt">
<node CREATED="1328996326597" ID="Freemind_Link_840125412" MODIFIED="1328996330880" TEXT="add fish sauce">
<node CREATED="1328996518708" ID="Freemind_Link_286274205" MODIFIED="1328996523809" TEXT="last step because hard to undo"/>
</node>
</node>
<node CREATED="1328996171144" ID="Freemind_Link_1102968340" MODIFIED="1328996176396" TEXT="too spicey">
<node CREATED="1328996176800" ID="Freemind_Link_595115406" MODIFIED="1328996229796" TEXT="add lime"/>
</node>
<node CREATED="1328996234712" ID="Freemind_Link_1260203322" MODIFIED="1328996236148" TEXT="too sour">
<node CREATED="1328996236528" ID="Freemind_Link_113182458" MODIFIED="1328996238316" TEXT="add sugar"/>
</node>
</node>
<node CREATED="1329001605112" ID="Freemind_Link_881141864" MODIFIED="1329001606860" TEXT="Green Curry">
<node CREATED="1328995165115" FOLDED="true" ID="Freemind_Link_225379288" MODIFIED="1328995521864" TEXT="Thai&#xa;Green Curry Paste&#xa;p. 217">
<node CREATED="1328996016532" ID="Freemind_Link_606939494" MODIFIED="1328996018863" TEXT="ingredients">
<node CREATED="1328995165115" MODIFIED="1328995165115" TEXT="1 heaped tbsp bird&apos;s eye chilies"/>
<node CREATED="1328995165115" MODIFIED="1328995165115" TEXT="1-2 long green chilies, deseeded"/>
<node CREATED="1328995165115" MODIFIED="1328995165115" TEXT="pinch of salt"/>
<node CREATED="1328995165115" ID="Freemind_Link_1226457159" MODIFIED="1328995165115" TEXT="1 rounded tbsp chopped galangal">
<node CREATED="1328995178941" ID="Freemind_Link_136547483" MODIFIED="1328995182409" TEXT="looks like ginger"/>
<node CREATED="1328995182877" ID="Freemind_Link_199277746" MODIFIED="1328995187625" TEXT="also called &quot;malangal&quot;"/>
</node>
<node CREATED="1328995165115" ID="Freemind_Link_1413235579" MODIFIED="1328995165115" TEXT="2-1/2 tbsp chopped lemongrass">
<node CREATED="1328995191150" ID="Freemind_Link_1924381138" MODIFIED="1328995193233" TEXT="stalks"/>
</node>
<node CREATED="1328995165115" MODIFIED="1328995165115" TEXT="1 tsp chopped kaffir lime zest"/>
<node CREATED="1328995165116" ID="Freemind_Link_1807043030" MODIFIED="1328995165116" TEXT="2 tsp chopped cilantro root">
<node CREATED="1328995198494" ID="Freemind_Link_922518336" MODIFIED="1328995201138" TEXT="or just cilantro"/>
</node>
<node CREATED="1328995165116" ID="Freemind_Link_1974633940" MODIFIED="1328995165116" TEXT="2 tsp chopped red turmeric">
<node CREATED="1328995202590" ID="Freemind_Link_618538057" MODIFIED="1328995210850" TEXT="looks like smaller reddish ginger">
<node CREATED="1328995211118" ID="Freemind_Link_488116607" MODIFIED="1328995213826" TEXT="frozen was ok"/>
</node>
<node CREATED="1328996390772" ID="Freemind_Link_856443856" MODIFIED="1328996395880" TEXT="if ground, dried">
<node CREATED="1328996396316" ID="Freemind_Link_1686756915" MODIFIED="1328996398672" TEXT="only use 1/3"/>
</node>
</node>
<node CREATED="1328995165116" ID="Freemind_Link_489245237" MODIFIED="1328995165116" TEXT="1 rounded tbsp chopped wild ginger">
<node CREATED="1328996407628" ID="Freemind_Link_855732427" MODIFIED="1328996416288" TEXT="pencil-like">
<node CREATED="1328996417189" ID="Freemind_Link_1815888941" MODIFIED="1328996421025" TEXT="called &quot;Chinese keys&quot;"/>
</node>
<node CREATED="1328996430821" ID="Freemind_Link_1367985475" MODIFIED="1328996435905" TEXT="peel &amp; trim"/>
</node>
<node CREATED="1328995165116" MODIFIED="1328995165116" TEXT="2-1/2 tbsp chopped red shallots"/>
<node CREATED="1328995165116" ID="Freemind_Link_655484698" MODIFIED="1328995165116" TEXT="2-/2 tbsp chopped garlic">
<node CREATED="1328995362385" ID="Freemind_Link_1571060481" MODIFIED="1328995364348" TEXT="for durant">
<node CREATED="1328995364577" ID="Freemind_Link_831535167" MODIFIED="1328995371477" TEXT="try cooling until golden brown">
<node CREATED="1328995372025" ID="Freemind_Link_1788084535" MODIFIED="1328995376389" TEXT="in peanut oil maybe?"/>
</node>
</node>
</node>
<node CREATED="1328995165116" MODIFIED="1328995165116" TEXT="1 tsp Thai shrimp paste"/>
<node CREATED="1328995165116" MODIFIED="1328995165116" TEXT="1 tsp white peppercorns"/>
<node CREATED="1328995165116" MODIFIED="1328995165116" TEXT="1 tsp roasted coriander seeds"/>
<node CREATED="1328995165116" MODIFIED="1328995165116" TEXT="a few blades of mace, roasted (optional)"/>
</node>
<node CREATED="1328996025860" ID="Freemind_Link_1933021315" MODIFIED="1328996030008" TEXT="method">
<node CREATED="1328996042829" ID="Freemind_Link_763787224" MODIFIED="1328996052808" TEXT="maybe fry garlic first for durant"/>
<node CREATED="1328996030492" ID="Freemind_Link_1675894652" MODIFIED="1328996037657" TEXT="blend all in food processor"/>
</node>
</node>
<node CREATED="1328995225326" FOLDED="true" ID="Freemind_Link_649733873" MODIFIED="1328995560928" TEXT="Green curry of heart of coconut&#xa;p. 240">
<node CREATED="1328995250423" ID="Freemind_Link_1255070712" MODIFIED="1328995251635" TEXT="seasoning">
<node CREATED="1328995244673" ID="Freemind_Link_1295250215" MODIFIED="1328995244673" TEXT="Kaffir lime leaves">
<node CREATED="1328995685050" ID="Freemind_Link_984726756" MODIFIED="1328995685853" TEXT="torn"/>
<node CREATED="1328996680138" ID="Freemind_Link_3563485" MODIFIED="1328996682629" TEXT="go on top at end"/>
</node>
<node CREATED="1328995244673" ID="Freemind_Link_903627555" MODIFIED="1328995741954" TEXT="Thai basil leaves">
<node CREATED="1328995741955" ID="Freemind_Link_759776216" MODIFIED="1328995741956" TEXT="Handful "/>
</node>
<node CREATED="1328995638355" ID="Freemind_Link_1720244959" MODIFIED="1328995651127" TEXT="2-1/5 Tbsp fish sauce or to taste"/>
<node CREATED="1328995716698" ID="Freemind_Link_742162666" MODIFIED="1328995723758" TEXT="3 young green chilies">
<node CREATED="1328995724075" ID="Freemind_Link_1867169105" MODIFIED="1328995726582" TEXT="deseeded"/>
<node CREATED="1328995726843" ID="Freemind_Link_693832620" MODIFIED="1328995729454" TEXT="thinly sliced"/>
</node>
</node>
<node CREATED="1328995663804" ID="Freemind_Link_1435123742" MODIFIED="1328995665807" TEXT="liquids">
<node CREATED="1328995666084" ID="Freemind_Link_184810082" MODIFIED="1328995671959" TEXT="2 cups coconut milk"/>
</node>
<node CREATED="1328995241823" ID="Freemind_Link_1597309579" MODIFIED="1328995243971" TEXT="vegetables">
<node CREATED="1328995595458" ID="Freemind_Link_1472313538" MODIFIED="1328995618293" TEXT="heart of coconut">
<node CREATED="1328995618294" ID="Freemind_Link_1836797638" MODIFIED="1328995618296" TEXT="6oz (150g) elegantly cut "/>
</node>
<node CREATED="1328995244673" ID="Freemind_Link_1073463831" MODIFIED="1328995244673" TEXT="Baby corn">
<node CREATED="1328995625291" ID="Freemind_Link_287127199" MODIFIED="1328995632455" TEXT="4-5 cut lengthwise"/>
</node>
<node CREATED="1328995244673" MODIFIED="1328995244673" TEXT="Bamboo shoots"/>
<node CREATED="1328995244673" ID="Freemind_Link_580469044" MODIFIED="1328995244673" TEXT="Heart of coconut"/>
<node CREATED="1328995693410" ID="Freemind_Link_1766557268" MODIFIED="1328995706934" TEXT="pea eggplants (optional)">
<node CREATED="1328995707186" ID="Freemind_Link_642842581" MODIFIED="1328995709118" TEXT="a few picked"/>
</node>
</node>
<node CREATED="1328995776828" ID="Freemind_Link_91469823" MODIFIED="1328995778119" TEXT="method">
<node CREATED="1328996007564" ID="Freemind_Link_145884603" MODIFIED="1328996010648" TEXT="make curry paste"/>
<node CREATED="1328996067869" ID="Freemind_Link_1564208445" MODIFIED="1328996618843" TEXT="fry the curry paste &#xa;in the coconut (cream) &#xa;for 5 minutes">
<node CREATED="1328996097302" ID="Freemind_Link_303727648" MODIFIED="1328996097754" TEXT="add">
<node CREATED="1328996086670" ID="Freemind_Link_1564416953" MODIFIED="1328996096394" TEXT="heart of coconut"/>
<node CREATED="1328996100198" ID="Freemind_Link_142254050" MODIFIED="1328996101242" TEXT="corn"/>
</node>
<node CREATED="1328996635149" ID="Freemind_Link_189156386" MODIFIED="1328996637289" TEXT="keep stirring"/>
<node CREATED="1328996112839" ID="Freemind_Link_276162561" MODIFIED="1328996122811" TEXT="until looks scrambled and smells cooked"/>
<node CREATED="1328996123199" ID="Freemind_Link_1083625750" MODIFIED="1328996129146" TEXT="season with fish sauce"/>
<node CREATED="1328996557272" ID="Freemind_Link_1736468892" MODIFIED="1328996588773" TEXT="until &quot;piercingly aromatic&quot;"/>
</node>
<node CREATED="1328996137831" ID="Freemind_Link_1497978069" MODIFIED="1328996145535" TEXT="moisten with coconut milk"/>
<node CREATED="1328996146015" ID="Freemind_Link_1341621754" MODIFIED="1328996148739" TEXT="bring to a boil"/>
<node CREATED="1328996150888" ID="Freemind_Link_655771404" MODIFIED="1328996158275" TEXT="add remaining ingredients"/>
</node>
</node>
</node>
<node CREATED="1329001611481" ID="Freemind_Link_1969196008" MODIFIED="1329001613892" TEXT="Mussaman Curry">
<node CREATED="1328996827070" ID="Freemind_Link_982265547" MODIFIED="1328996847359" TEXT="Mussaman curry paste&#xa;p. 248">
<node CREATED="1328996851694" ID="Freemind_Link_563300933" MODIFIED="1328996853722" TEXT="ingredients">
<node CREATED="1329004226352" ID="Freemind_Link_645314696" MODIFIED="1329004226352" TEXT="dried red chillies">
<node CREATED="1328996864423" ID="Freemind_Link_1749967745" MODIFIED="1329004224867" TEXT="8-12 "/>
</node>
<node CREATED="1328996878919" ID="Freemind_Link_1237103212" MODIFIED="1328996882163" TEXT="coriander seeds">
<node CREATED="1328996874183" ID="Freemind_Link_371093920" MODIFIED="1328996878339" TEXT="5 Tbsp"/>
</node>
<node CREATED="1328996930461" ID="Freemind_Link_1727755053" MODIFIED="1328996932128" TEXT="cumin seeds">
<node CREATED="1328996933477" ID="Freemind_Link_40147227" MODIFIED="1328996941640" TEXT="2-1/2 Tbsp"/>
</node>
<node CREATED="1328997091297" ID="Freemind_Link_931996015" MODIFIED="1328997093933" TEXT="cardomom pods">
<node CREATED="1328997096505" ID="Freemind_Link_720258060" MODIFIED="1328997102917" TEXT="1 rounded Tbsp"/>
</node>
<node CREATED="1328997104209" ID="Freemind_Link_1313158124" MODIFIED="1328997105045" TEXT="cloves">
<node CREATED="1328997105321" ID="Freemind_Link_507468352" MODIFIED="1328997109605" TEXT="5 or so"/>
</node>
<node CREATED="1328997111282" ID="Freemind_Link_588433847" MODIFIED="1328997113381" TEXT="cassia">
<node CREATED="1328997113977" ID="Freemind_Link_14595436" MODIFIED="1328997116109" TEXT="2 pieces"/>
<node CREATED="1328997141282" ID="Freemind_Link_1257699462" MODIFIED="1328997155979" TEXT="&quot;chinese cinnamon&quot;">
<icon BUILTIN="help"/>
<node CREATED="1328997148494" LINK="http://en.wikipedia.org/wiki/Cinnamomum_aromaticum" MODIFIED="1328997148494" TEXT="en.wikipedia.org &gt; Wiki &gt; Cinnamomum aromaticum"/>
</node>
</node>
<node CREATED="1328997206109" ID="Freemind_Link_560672173" MODIFIED="1328997208249" TEXT="star anise">
<node CREATED="1328997208469" ID="Freemind_Link_1306188609" MODIFIED="1328997208977" TEXT="2"/>
</node>
<node CREATED="1328997214813" ID="Freemind_Link_735299360" MODIFIED="1328997217177" TEXT="unpeeled shallots">
<node CREATED="1328997229589" ID="Freemind_Link_463997427" MODIFIED="1328997236321" TEXT="6 oz (150 g)"/>
</node>
<node CREATED="1328997240446" ID="Freemind_Link_9274972" MODIFIED="1328997247322" TEXT="unpeeled garlic cloves">
<node CREATED="1328997229589" ID="Freemind_Link_1614773214" MODIFIED="1328997236321" TEXT="6 oz (150 g)"/>
</node>
<node CREATED="1328997255766" ID="Freemind_Link_154792703" MODIFIED="1328997259258" TEXT="chopped lemon grass">
<node CREATED="1328997229589" ID="Freemind_Link_1801398006" MODIFIED="1328997265818" TEXT="2 oz (60 g)"/>
</node>
<node CREATED="1328997270479" ID="Freemind_Link_1203931416" MODIFIED="1328997272794" TEXT="galangal">
<node CREATED="1328997273159" ID="Freemind_Link_1216952984" MODIFIED="1328997280739" TEXT="5 Tbsp"/>
</node>
</node>
<node CREATED="1328996854046" ID="Freemind_Link_110472183" MODIFIED="1328997301124" TEXT="method">
<node CREATED="1328997301632" ID="Freemind_Link_1588462736" MODIFIED="1328997313172" TEXT="roast chilies"/>
<node CREATED="1328997318313" ID="Freemind_Link_114571625" MODIFIED="1328997326093" TEXT="remove seeds from cardomom pods"/>
<node CREATED="1328997332097" ID="Freemind_Link_203730318" MODIFIED="1328997333957" TEXT="grind all spices"/>
<node CREATED="1328997337457" ID="Freemind_Link_983980405" MODIFIED="1328997338885" TEXT="sift"/>
<node CREATED="1328997343553" ID="Freemind_Link_1639285073" MODIFIED="1328997348701" TEXT="combine all ingredients">
<node CREATED="1328997349034" ID="Freemind_Link_601831877" MODIFIED="1328997351590" TEXT="roast in wok">
<node CREATED="1328997361274" ID="Freemind_Link_1849845922" MODIFIED="1328997368333" TEXT="with water to prevent burning"/>
<node CREATED="1328997374490" ID="Freemind_Link_172569024" MODIFIED="1328997376190" TEXT="until">
<node CREATED="1328997376458" ID="Freemind_Link_1318750400" MODIFIED="1328997377318" TEXT="brown"/>
<node CREATED="1328997377610" ID="Freemind_Link_194896492" MODIFIED="1328997379909" TEXT="fragrant"/>
</node>
</node>
<node CREATED="1328997387499" ID="Freemind_Link_1245662082" MODIFIED="1328997389190" TEXT="allow to cool"/>
</node>
<node CREATED="1329001574608" ID="Freemind_Link_858372092" MODIFIED="1329001576211" TEXT="meanwhile">
<node CREATED="1328997390491" ID="Freemind_Link_1571932759" MODIFIED="1328997394302" TEXT="peel">
<node CREATED="1328997394715" ID="Freemind_Link_499515665" MODIFIED="1328997396502" TEXT="shallots"/>
<node CREATED="1328997396947" ID="Freemind_Link_1772605907" MODIFIED="1328997398406" TEXT="garlic"/>
</node>
<node CREATED="1328997415544" ID="Freemind_Link_948255248" MODIFIED="1328997418819" TEXT="grind everything"/>
</node>
</node>
</node>
<node CREATED="1328997565207" FOLDED="true" ID="Freemind_Link_1216446159" MODIFIED="1328997589881" TEXT="Mussaman curry [duck] with potatoes and onions&#xa;p. 248">
<node CREATED="1328997590511" ID="Freemind_Link_743056207" MODIFIED="1328997593939" TEXT="skipping duck"/>
<node CREATED="1328997623607" ID="Freemind_Link_160435946" MODIFIED="1328997626323" TEXT="ingredients">
<node CREATED="1328997640952" ID="Freemind_Link_319503570" MODIFIED="1328997643635" TEXT="dark soy sauce">
<node CREATED="1328997635592" ID="Freemind_Link_714919389" MODIFIED="1328997640619" TEXT="4t"/>
</node>
<node CREATED="1328997649728" ID="Freemind_Link_433450627" MODIFIED="1328997658916" TEXT="pototoes, peeled &amp; cut">
<node CREATED="1328997660224" ID="Freemind_Link_1531846873" MODIFIED="1328997662220" TEXT="4"/>
</node>
<node CREATED="1328997664521" ID="Freemind_Link_912828880" MODIFIED="1328997674756" TEXT="vege oil for deep frying"/>
<node CREATED="1328997680129" ID="Freemind_Link_582242527" MODIFIED="1328997687829" TEXT="pickling onions or shallots">
<node CREATED="1328997687986" ID="Freemind_Link_1158749822" MODIFIED="1328997689604" TEXT="8 small"/>
<node CREATED="1328997689841" ID="Freemind_Link_308331858" MODIFIED="1328997690844" TEXT="peeled"/>
</node>
<node CREATED="1328997695393" ID="Freemind_Link_983910330" MODIFIED="1328997696485" TEXT="peanuts">
<node CREATED="1328997696713" ID="Freemind_Link_250343067" MODIFIED="1328997699109" TEXT="1/4 cup"/>
</node>
<node CREATED="1328997704233" ID="Freemind_Link_1337773446" MODIFIED="1328997706477" TEXT="coconut milk">
<node CREATED="1328997706834" ID="Freemind_Link_32698265" MODIFIED="1328997708197" TEXT="2 cups"/>
</node>
<node CREATED="1328997801052" ID="Freemind_Link_204516005" MODIFIED="1328997808399" TEXT="cracked coconut cream">
<node CREATED="1328997808908" ID="Freemind_Link_1789503989" MODIFIED="1328997810223" TEXT="2 cups"/>
</node>
<node CREATED="1328997712618" ID="Freemind_Link_673137138" MODIFIED="1328997716037" TEXT="pinch of salt"/>
<node CREATED="1328997757511" ID="Freemind_Link_1400612951" MODIFIED="1328997760654" TEXT="cardomom pods">
<node CREATED="1328997760931" ID="Freemind_Link_1519008359" MODIFIED="1328997762686" TEXT="5 or so"/>
<node CREATED="1328997762914" ID="Freemind_Link_1297302899" MODIFIED="1328997766854" TEXT="roasted"/>
</node>
<node CREATED="1328997775203" ID="Freemind_Link_1913107419" MODIFIED="1328997785602" TEXT="Thai cardomom leaves, roasted">
<node CREATED="1328997786011" ID="Freemind_Link_1213903076" MODIFIED="1328997786766" TEXT="4"/>
</node>
<node CREATED="1328997722386" ID="Freemind_Link_693993510" MODIFIED="1328997733814" TEXT="1 piece cassia, roasted">
<node CREATED="1328997734218" ID="Freemind_Link_1254460665" MODIFIED="1328997746622" TEXT="3/4 x 1-1/4"/>
<node CREATED="1328997747274" ID="Freemind_Link_1582239772" MODIFIED="1328997749582" TEXT="2x3 cm"/>
</node>
<node CREATED="1328997853369" ID="Freemind_Link_570717106" MODIFIED="1328997854924" TEXT="palm sugar">
<node CREATED="1328997859161" ID="Freemind_Link_1270485318" MODIFIED="1328997862645" TEXT="9-11 oz">
<node CREATED="1328997864665" ID="Freemind_Link_1778256370" MODIFIED="1328997869644" TEXT="250-325g"/>
</node>
</node>
<node CREATED="1328997876073" ID="Freemind_Link_1107325262" MODIFIED="1328997878877" TEXT="tamarind water">
<node CREATED="1328997879201" ID="Freemind_Link_857690230" MODIFIED="1328997880773" TEXT="1C"/>
</node>
</node>
<node CREATED="1328997626680" ID="Freemind_Link_382702476" MODIFIED="1328997627979" TEXT="method">
<node CREATED="1328997611191" ID="Freemind_Link_1675188913" MODIFIED="1328997611191" TEXT="">
<node CREATED="1328997597439" ID="Freemind_Link_294285246" MODIFIED="1328997605572" TEXT="rinse potatoes to remove excess starch"/>
<node CREATED="1328997608359" ID="Freemind_Link_1896799735" MODIFIED="1328997609763" TEXT="drain"/>
<node CREATED="1328997610103" ID="Freemind_Link_575230990" MODIFIED="1328997610819" TEXT="dry"/>
</node>
<node CREATED="1328998467808" ID="Freemind_Link_1540888451" MODIFIED="1328998467808" TEXT="">
<node CREATED="1328998448808" ID="Freemind_Link_1140144399" MODIFIED="1328998450899" TEXT="heat oil"/>
<node CREATED="1328998451176" ID="Freemind_Link_1394373012" MODIFIED="1328998552331" TEXT="deep fry">
<node CREATED="1328998553668" MODIFIED="1328998553668" TEXT="pototoes until golden"/>
<node CREATED="1328998555698" ID="Freemind_Link_1826872728" MODIFIED="1328998562318" TEXT="onions"/>
<node CREATED="1328998563313" ID="Freemind_Link_1335103292" MODIFIED="1328998564517" TEXT="peanuts"/>
</node>
</node>
<node CREATED="1328998590216" ID="Freemind_Link_1560188650" MODIFIED="1328998590216" TEXT="">
<node CREATED="1328998592096" ID="Freemind_Link_1401547208" MODIFIED="1328998598907" TEXT="bring coconut milk and salt to a boil"/>
<node CREATED="1328998639761" ID="Freemind_Link_1417164542" MODIFIED="1328998642573" TEXT="add veggies"/>
<node CREATED="1328998643377" ID="Freemind_Link_529886692" MODIFIED="1328998646317" TEXT="simmer until cooked"/>
</node>
<node CREATED="1328998655858" ID="Freemind_Link_1469977292" MODIFIED="1328998657565" TEXT="meanwhile">
<node CREATED="1328998658297" ID="Freemind_Link_1607336595" MODIFIED="1328998663733" TEXT="medium pot"/>
<node CREATED="1328998664034" ID="Freemind_Link_899647309" MODIFIED="1328998669941" TEXT="heat">
<node CREATED="1328998670202" ID="Freemind_Link_278394818" MODIFIED="1328998673453" TEXT="coconut cream"/>
<node CREATED="1328998673802" ID="Freemind_Link_185665895" MODIFIED="1328998677285" TEXT="curry paste">
<node CREATED="1328998677473" ID="Freemind_Link_345506231" MODIFIED="1328998679389" TEXT="2 cups"/>
</node>
</node>
<node CREATED="1328998686458" ID="Freemind_Link_897178844" MODIFIED="1328998687798" TEXT="simmer">
<node CREATED="1328998687985" ID="Freemind_Link_305795247" MODIFIED="1328998690741" TEXT="at least 10 minutes"/>
<node CREATED="1328998713183" ID="Freemind_Link_1161378127" MODIFIED="1328998715514" TEXT="stir regularly"/>
<node CREATED="1328998727071" ID="Freemind_Link_1123544957" MODIFIED="1328998729483" TEXT="until redolent"/>
</node>
<node CREATED="1328998743687" ID="Freemind_Link_448784737" MODIFIED="1328998748370" TEXT="when oily, hot, sizzling">
<node CREATED="1328998749079" ID="Freemind_Link_1551949651" MODIFIED="1328998753154" TEXT="season with palm sugar"/>
<node CREATED="1328998762535" ID="Freemind_Link_1032826942" MODIFIED="1328998769563" TEXT="keep simmering until sugar darkens sauce"/>
</node>
<node CREATED="1328998773839" ID="Freemind_Link_71038855" MODIFIED="1328998776787" TEXT="add tamarind water"/>
<node CREATED="1328998785608" ID="Freemind_Link_982082425" MODIFIED="1328998788603" TEXT="add fish sauce"/>
<node CREATED="1328998809357" ID="Freemind_Link_1549626245" MODIFIED="1328998812321" TEXT="add cooked ingredients"/>
<node CREATED="1328998816157" ID="Freemind_Link_897463234" MODIFIED="1328998823221" TEXT="set aside for a couple of minutes for flavors to deepen"/>
</node>
</node>
</node>
</node>
<node CREATED="1329001619769" FOLDED="true" ID="Freemind_Link_1446796518" MODIFIED="1329001620996" TEXT="Red Curry">
<node CREATED="1328999840784" FOLDED="true" ID="Freemind_Link_1902737677" MODIFIED="1328999851154" TEXT="red curry paste&#xa;p. 231&#xa;">
<node CREATED="1328999857989" MODIFIED="1328999857989" TEXT="large dried chilies">
<node CREATED="1328999857989" MODIFIED="1328999857989" TEXT="deseeded"/>
<node CREATED="1328999857989" MODIFIED="1328999857989" TEXT="soaked"/>
<node CREATED="1328999857989" MODIFIED="1328999857989" TEXT="chopped"/>
</node>
<node CREATED="1328999857989" MODIFIED="1328999857989" TEXT="pinch of salt"/>
<node CREATED="1328999857989" MODIFIED="1328999857989" TEXT="chopped lemon grass">
<node CREATED="1328999857989" MODIFIED="1328999857989" TEXT="1/4 cup"/>
</node>
<node CREATED="1328999857989" MODIFIED="1328999857989" TEXT="slices galangal">
<node CREATED="1328999857989" MODIFIED="1328999857989" TEXT="6"/>
</node>
<node CREATED="1328999857989" MODIFIED="1328999857989" TEXT="chopped wild ginger">
<node CREATED="1328999857990" MODIFIED="1328999857990" TEXT="2-1/2 T"/>
</node>
<node CREATED="1328999857990" MODIFIED="1328999857990" TEXT="chopped cilantro root">
<node CREATED="1328999857990" MODIFIED="1328999857990" TEXT="1 rounded T"/>
</node>
<node CREATED="1328999857990" MODIFIED="1328999857990" TEXT="chopped red shallots">
<node CREATED="1328999857990" MODIFIED="1328999857990" TEXT="2-1/2 T"/>
</node>
<node CREATED="1328999857990" MODIFIED="1328999857990" TEXT="chopped garlic">
<node CREATED="1328999857990" MODIFIED="1328999857990" TEXT="2-1/2 T"/>
</node>
<node CREATED="1328999857990" MODIFIED="1328999857990" TEXT="toasted coriander seeds">
<node CREATED="1328999857990" MODIFIED="1328999857990" TEXT="1t"/>
</node>
<node CREATED="1329005985815" ID="Freemind_Link_1162710987" MODIFIED="1329005987571" TEXT="also add">
<node CREATED="1329005987832" ID="Freemind_Link_396756872" MODIFIED="1329005991043" TEXT="p. 232 version"/>
<node CREATED="1329005991696" ID="Freemind_Link_1011986629" MODIFIED="1329006002812" TEXT="freshly grated nutmeg">
<node CREATED="1329006004392" ID="Freemind_Link_558027068" MODIFIED="1329006065314" TEXT="1 t"/>
</node>
<node CREATED="1329006009856" ID="Freemind_Link_653423353" MODIFIED="1329006084743" TEXT="roasted peanuts, roasted">
<node CREATED="1329006013688" ID="Freemind_Link_1090521647" MODIFIED="1329006063642" TEXT="2-1/2 T"/>
</node>
<node CREATED="1329006053942" ID="Freemind_Link_745588800" MODIFIED="1329006081485" TEXT="cumin seeds, roasted">
<node CREATED="1329006057622" ID="Freemind_Link_1737411262" MODIFIED="1329006060794" TEXT="1-1/2 t"/>
</node>
<node CREATED="1329006038132" ID="Freemind_Link_1755797968" MODIFIED="1329006042037" TEXT="pinch ground white pepper"/>
</node>
</node>
<node CREATED="1329001489440" FOLDED="true" ID="Freemind_Link_60001427" MODIFIED="1329001503631" TEXT="cooking red curry&#xa;p. 231">
<node CREATED="1329001476667" ID="Freemind_Link_116698654" MODIFIED="1329001476667" TEXT="Ingredients">
<node CREATED="1329001511438" MODIFIED="1329001511438" TEXT="cracked coconut cream">
<node CREATED="1329001511439" MODIFIED="1329001511439" TEXT="1/2c"/>
</node>
<node CREATED="1329001511439" MODIFIED="1329001511439" TEXT="palm sugar">
<node CREATED="1329001511439" MODIFIED="1329001511439" TEXT="1 rounded T"/>
</node>
<node CREATED="1329001511439" MODIFIED="1329001511439" TEXT="pinch of salt"/>
<node CREATED="1329001511439" MODIFIED="1329001511439" TEXT="light soy sauce">
<node CREATED="1329001511439" MODIFIED="1329001511439" TEXT="2t"/>
</node>
<node CREATED="1329001511439" MODIFIED="1329001511439" TEXT="tamarind water">
<node CREATED="1329001511439" MODIFIED="1329001511439" TEXT="2-1/2 T"/>
</node>
<node CREATED="1329001511439" MODIFIED="1329001511439" TEXT="coconut milk">
<node CREATED="1329001511439" MODIFIED="1329001511439" TEXT="2c"/>
</node>
<node CREATED="1329001511439" MODIFIED="1329001511439" TEXT="oyster mushrooms">
<node CREATED="1329001511439" MODIFIED="1329001511439" TEXT="7 oz"/>
<node CREATED="1329001511439" MODIFIED="1329001511439" TEXT="200 g"/>
</node>
<node CREATED="1329001511439" MODIFIED="1329001511439" TEXT="soft tofu">
<node CREATED="1329001511439" MODIFIED="1329001511439" TEXT="8 oz"/>
<node CREATED="1329001511439" MODIFIED="1329001511439" TEXT="225 g"/>
</node>
<node CREATED="1329001511439" ID="Freemind_Link_297450502" MODIFIED="1329001903836" TEXT="Siamese watercress&#xa;(ong choi)">
<node CREATED="1329001511439" MODIFIED="1329001511439" TEXT="cut into 1-1/4in (3-cm) lengths"/>
<node CREATED="1329001511439" MODIFIED="1329001511439" TEXT="4 oz"/>
<node CREATED="1329001511439" MODIFIED="1329001511439" TEXT="100 g"/>
</node>
<node CREATED="1329001511439" MODIFIED="1329001511439" TEXT="kaffir lime leaves">
<node CREATED="1329001511439" MODIFIED="1329001511439" TEXT="7"/>
</node>
<node CREATED="1329001511439" MODIFIED="1329001511439" TEXT="small kaffir limes">
<node CREATED="1329001511439" MODIFIED="1329001511439" TEXT="2"/>
<node CREATED="1329001511439" MODIFIED="1329001511439" TEXT="cut in half"/>
<node CREATED="1329001511439" MODIFIED="1329001511439" TEXT="deseeded"/>
</node>
</node>
<node CREATED="1329001515737" ID="Freemind_Link_1574742432" MODIFIED="1329001517517" TEXT="method">
<node CREATED="1329001729363" ID="Freemind_Link_638926395" MODIFIED="1329001730119" TEXT="1">
<node CREATED="1329001730363" ID="Freemind_Link_1849700888" MODIFIED="1329001732591" TEXT="make the paste"/>
</node>
<node CREATED="1329001733491" ID="Freemind_Link_438455734" MODIFIED="1329001733911" TEXT="2">
<node CREATED="1329001742636" ID="Freemind_Link_616712077" MODIFIED="1329001771995" TEXT="simmer 2-1/2 T curry paste &#xa;in cracked coconut cream">
<node CREATED="1329001764604" ID="Freemind_Link_932853958" MODIFIED="1329001767032" TEXT="until fragrant"/>
</node>
</node>
<node CREATED="1329001734996" ID="Freemind_Link_322849164" MODIFIED="1329001735167" TEXT="3">
<node CREATED="1329001778549" ID="Freemind_Link_106622593" MODIFIED="1329001797080" TEXT="season with">
<node CREATED="1329001779948" ID="Freemind_Link_436760192" MODIFIED="1329001785320" TEXT="palm sugar"/>
<node CREATED="1329001786276" ID="Freemind_Link_576291072" MODIFIED="1329001787152" TEXT="salt"/>
<node CREATED="1329001787597" ID="Freemind_Link_1674516457" MODIFIED="1329001789208" TEXT="soy sauce"/>
<node CREATED="1329001789541" ID="Freemind_Link_547823423" MODIFIED="1329001792104" TEXT="tamarind water"/>
</node>
</node>
<node CREATED="1329001735387" ID="Freemind_Link_572837640" MODIFIED="1329001735943" TEXT="4">
<node CREATED="1329001806389" ID="Freemind_Link_659657719" MODIFIED="1329001809033" TEXT="add coconut milk"/>
</node>
<node CREATED="1329001736700" ID="Freemind_Link_1945864418" MODIFIED="1329001736895" TEXT="5">
<node CREATED="1329001813005" ID="Freemind_Link_804912243" MODIFIED="1329001815345" TEXT="when boiling">
<node CREATED="1329001816654" ID="Freemind_Link_840213449" MODIFIED="1329001817265" TEXT="add">
<node CREATED="1329001820229" ID="Freemind_Link_115627030" MODIFIED="1329001822289" TEXT="mushrooms"/>
<node CREATED="1329001822877" ID="Freemind_Link_247521670" MODIFIED="1329001823985" TEXT="tofu"/>
<node CREATED="1329001824357" ID="Freemind_Link_724341221" MODIFIED="1329001826241" TEXT="watercress"/>
<node CREATED="1329001826885" ID="Freemind_Link_253250264" MODIFIED="1329001830321" TEXT="kaffir lime laves"/>
<node CREATED="1329001842334" ID="Freemind_Link_1381269068" MODIFIED="1329001846577" TEXT="kaffir lime juice"/>
</node>
<node CREATED="1329001855230" ID="Freemind_Link_1890512504" MODIFIED="1329001856162" TEXT="simmer">
<node CREATED="1329001856454" ID="Freemind_Link_1486177881" MODIFIED="1329001860764" TEXT="until vegetables are cooked"/>
</node>
</node>
</node>
<node CREATED="1329001810509" ID="Freemind_Link_1303648285" MODIFIED="1329001811041" TEXT="6">
<node CREATED="1329001868639" ID="Freemind_Link_1659207335" MODIFIED="1329001871122" TEXT="adjust seasoning"/>
<node CREATED="1329001871343" ID="Freemind_Link_393305125" MODIFIED="1329001872242" TEXT="serve"/>
</node>
</node>
<node CREATED="1329006262541" ID="Freemind_Link_1518309431" MODIFIED="1329006262541" TEXT=""/>
</node>
<node CREATED="1329006267829" ID="Freemind_Link_508247420" MODIFIED="1329006286055" TEXT="Next time copy this recipe&#xa;Red curry of beef with peanut&#xa;p. 232"/>
</node>
</node>
<node CREATED="1329003220542" ID="Freemind_Link_1372729657" MODIFIED="1329003221930" TEXT="about.com">
<node CREATED="1329003222358" ID="Freemind_Link_1093562082" MODIFIED="1329003224362" TEXT="mussaman">
<node CREATED="1329003228915" LINK="http://thaifood.about.com/od/thaicurrypasterecipes/r/massamanpaste.htm" MODIFIED="1329003228915" TEXT="thaifood.about.com &gt; Od &gt; Thaicurrypasterecipes &gt; R &gt; Massamanpaste"/>
<node CREATED="1329003247284" MODIFIED="1329003247284" TEXT="Ingredients:">
<node CREATED="1329003247285" MODIFIED="1329003247285" TEXT="&#x2022;        1/4 cup dry roasted peanuts, unsalted"/>
<node CREATED="1329003247285" MODIFIED="1329003247285" TEXT="&#x2022;        2 shallots, sliced"/>
<node CREATED="1329003247285" MODIFIED="1329003247285" TEXT="&#x2022;        5 cloves garlic, peeled"/>
<node CREATED="1329003247285" MODIFIED="1329003247285" TEXT="&#x2022;        1-2 red chilies, OR substitute 1/2 to 1 tsp. dried crushed chili"/>
<node CREATED="1329003247285" MODIFIED="1329003247285" TEXT="&#x2022;        1 thumb-size piece galangal (or ginger), thinly sliced"/>
<node CREATED="1329003247285" MODIFIED="1329003247285" TEXT="&#x2022;        1 stalk lemongrass, minced, OR 2-3 Tbsp. frozen or bottled prepared lemongrass"/>
<node CREATED="1329003247285" MODIFIED="1329003247285" TEXT="&#x2022;        1 tsp. ground coriander"/>
<node CREATED="1329003247285" MODIFIED="1329003247285" TEXT="&#x2022;        1/2 Tbsp. ground cumin"/>
<node CREATED="1329003247285" MODIFIED="1329003247285" TEXT="&#x2022;        1/2 tsp. whole cumin seeds"/>
<node CREATED="1329003247285" MODIFIED="1329003247285" TEXT="&#x2022;        1/8 tsp. nutmeg, preferably ground from whole nutmeg"/>
<node CREATED="1329003247285" MODIFIED="1329003247285" TEXT="&#x2022;        1/2 tsp. cinnamon"/>
<node CREATED="1329003247285" MODIFIED="1329003247285" TEXT="&#x2022;        1/8 tsp. ground cloves"/>
<node CREATED="1329003247285" MODIFIED="1329003247285" TEXT="&#x2022;        1/4 tsp. ground cardamon"/>
<node CREATED="1329003247285" MODIFIED="1329003247285" TEXT="&#x2022;        2 Tbsp. fish sauce"/>
<node CREATED="1329003247285" MODIFIED="1329003247285" TEXT="&#x2022;        1 tsp. shrimp paste"/>
<node CREATED="1329003247285" MODIFIED="1329003247285" TEXT="&#x2022;        1 tsp. palm sugar OR brown sugar"/>
<node CREATED="1329003247285" MODIFIED="1329003247285" TEXT="&#x2022;        1-3 Tbsp. coconut milk, depending on how thick or runny you prefer your paste (save remainder for cooking your curry)"/>
</node>
<node CREATED="1329003297888" ID="Freemind_Link_1659315662" MODIFIED="1329003297888" TEXT="Preparation:">
<node CREATED="1329003307016" ID="Freemind_Link_979343203" MODIFIED="1329003307017" TEXT="1.        ">
<node CREATED="1329003297888" ID="Freemind_Link_1996883315" MODIFIED="1329003307013" TEXT="Place all paste ingredients in a food processor (or blender) and process well. To make a sauce rather than a paste, add up to 1 can coconut milk."/>
</node>
<node CREATED="1329003313528" ID="Freemind_Link_813550369" MODIFIED="1329003313530" TEXT="2.        ">
<node CREATED="1329003297888" ID="Freemind_Link_1108792929" MODIFIED="1329003313526" TEXT="To use immediately, place sauce in a casserole/baking dish together with 1-2 bay leaves, plus your choice of chicken, beef, lamb, tofu/wheat gluten, plus vegetables. Add 2-3 whole bay leaves if you have them (this is a common ingredient in Massaman curries). Stir well to combine, and simmer in a wok OR cover and bake in the oven at 350 degrees until finished."/>
</node>
<node CREATED="1329003318911" ID="Freemind_Link_1035717151" MODIFIED="1329003318912" TEXT="3.        ">
<node CREATED="1329003297889" ID="Freemind_Link_5352325" MODIFIED="1329003318908" TEXT="Garnish your Massaman curry with whole roasted peanuts and fresh coriander. Lime wedges can also be served if your curry is on the salty side. For a complete recipe, see my Thai Massaman Curry Recipe."/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1291573205355" FOLDED="true" ID="Freemind_Link_1441787165" MODIFIED="1455252277595" TEXT="peanut sauce">
<node CREATED="1291573209323" ID="Freemind_Link_1754644513" MODIFIED="1291573210359" TEXT="dad"/>
<node CREATED="1291573277480" ID="Freemind_Link_1142235487" MODIFIED="1296009674434" TEXT="&#xa;After cooking vegetables, saute &#xa;then add &#xa;&#x9;broth/water, &#xa;&#x9;soy sauce, &#xa;&#x9;small amt of sugar, &#xa;cover till done (depending on vegetables, usually 4 min or so) &#xa;then add peanut butter until right consistency &#xa;&#x9;(not too thick, not to runny). &#xa;Can also add red pepper flakes for some &quot;heat&quot;.&#xa;"/>
<node CREATED="1296009705212" ID="Freemind_Link_696062130" MODIFIED="1296009708735" TEXT="garlic flakes"/>
</node>
<node CREATED="1472614510196" FOLDED="true" ID="ID_764900414" MODIFIED="1496691137724" TEXT="mouhamara v3.1">
<node CREATED="1472614515923" ID="ID_421235266" MODIFIED="1472614523454" TEXT="(Secret Recipe, do not share :) )&#xa;&#xa;Full Recipe&#xa;&#xa;3/4 cup&#x9;olive oil&#xa;1/2 cup&#x9;ACI pepper paste (spicy, pointy red peppers in jar)&#xa;1/2 cup&#x9;chopped walnuts&#xa;1/4 cup&#x9;bread crumbs &#xa;&#x9;&#x9;(with anise, this is about 1-1/3 Tucson breadsticks)&#xa;3 tbs&#x9;cumin &#xa;&#x9;&#x9;(this is 1 tsp extra, but easier to remember than 8 tsp)&#xa;3 tbs&#x9;pomegranate syrup &#xa;&#x9;&#x9;(again 1 tsp extra)&#xa;2 tbs&#x9;Alleppo red pepper &#xa;&#x9;&#x9;(or less if too spicy, really it&apos;s 4-8 tsp)&#xa;1/2 tsp&#x9;ground anise&#xa;&#x9;&#x9;(estimating 1 big pinch = 1/2 tsp)&#xa;&#xa;From: &#xa;&#xa;Quarter Recipe&#xa;&#xa;3&#x9;tbs &#x9;olive oil&#xa;2&#x9;tbs&#x9;ACI pepper paste&#xa;2&#x9;tbs&#x9;chopped walnuts&#xa;1&#x9;tbs&#x9;bread crumbs&#xa;                (have anise in them)&#xa;2+&#x9;tsp&#x9;cumin&#xa;2&#x9;tsp&#x9;pomegranate syrup&#xa;1-2&#x9;tsp &#x9;Alleppo red pepper&#xa;pinch anise (grind it)">
<node CREATED="1480465546130" FOLDED="true" ID="ID_1468296575" MODIFIED="1496285353388" TEXT="11/29/16">
<node CREATED="1480465558172" ID="ID_994324063" MODIFIED="1480465560061" TEXT="testing">
<node CREATED="1480465560062" ID="ID_94603746" MODIFIED="1480465567783" TEXT="4 tbs allepo pepper"/>
<node CREATED="1480478269528" ID="ID_857297167" MODIFIED="1480478271385" TEXT="result">
<node CREATED="1480478271784" ID="ID_469795221" MODIFIED="1480478277546" TEXT="more bitter than hot"/>
</node>
<node CREATED="1480478280101" ID="ID_1160219714" MODIFIED="1480478283877" TEXT="conclusion">
<node CREATED="1480478284150" ID="ID_1667506965" MODIFIED="1480478287336" TEXT="go back to 2 tbs"/>
</node>
</node>
</node>
</node>
<node CREATED="1492980147584" ID="ID_116586689" MODIFIED="1492980164298" TEXT="next time could try adding 1/2 tsp of lemon juice ">
<node CREATED="1492980180050" LINK="http://www.ottolenghi.co.uk/muhammara-shop" MODIFIED="1492980180050" TEXT="ottolenghi.co.uk &gt; Muhammara-shop"/>
</node>
</node>
<node CREATED="1472614510196" FOLDED="true" ID="ID_1244549176" MODIFIED="1491341959366" TEXT="mouhamara v3.1&#xa;BACKUP">
<node CREATED="1472614515923" ID="ID_682952194" MODIFIED="1472614523454" TEXT="(Secret Recipe, do not share :) )&#xa;&#xa;Full Recipe&#xa;&#xa;3/4 cup&#x9;olive oil&#xa;1/2 cup&#x9;ACI pepper paste (spicy, pointy red peppers in jar)&#xa;1/2 cup&#x9;chopped walnuts&#xa;1/4 cup&#x9;bread crumbs &#xa;&#x9;&#x9;(with anise, this is about 1-1/3 Tucson breadsticks)&#xa;3 tbs&#x9;cumin &#xa;&#x9;&#x9;(this is 1 tsp extra, but easier to remember than 8 tsp)&#xa;3 tbs&#x9;pomegranate syrup &#xa;&#x9;&#x9;(again 1 tsp extra)&#xa;2 tbs&#x9;Alleppo red pepper &#xa;&#x9;&#x9;(or less if too spicy, really it&apos;s 4-8 tsp)&#xa;1/2 tsp&#x9;ground anise&#xa;&#x9;&#x9;(estimating 1 big pinch = 1/2 tsp)&#xa;&#xa;From: &#xa;&#xa;Quarter Recipe&#xa;&#xa;3&#x9;tbs &#x9;olive oil&#xa;2&#x9;tbs&#x9;ACI pepper paste&#xa;2&#x9;tbs&#x9;chopped walnuts&#xa;1&#x9;tbs&#x9;bread crumbs&#xa;                (have anise in them)&#xa;2+&#x9;tsp&#x9;cumin&#xa;2&#x9;tsp&#x9;pomegranate syrup&#xa;1-2&#x9;tsp &#x9;Alleppo red pepper&#xa;pinch anise (grind it)">
<node CREATED="1480465546130" FOLDED="true" ID="ID_37453961" MODIFIED="1480478289309" TEXT="11/29/16">
<node CREATED="1480465558172" ID="ID_1271323015" MODIFIED="1480465560061" TEXT="testing">
<node CREATED="1480465560062" ID="ID_1589452199" MODIFIED="1480465567783" TEXT="4 tbs allepo pepper"/>
<node CREATED="1480478269528" ID="ID_1197089252" MODIFIED="1480478271385" TEXT="result">
<node CREATED="1480478271784" ID="ID_1630084659" MODIFIED="1480478277546" TEXT="more bitter than hot"/>
</node>
<node CREATED="1480478280101" ID="ID_1347996230" MODIFIED="1480478283877" TEXT="conclusion">
<node CREATED="1480478284150" ID="ID_1664240454" MODIFIED="1480478287336" TEXT="go back to 2 tbs"/>
</node>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1383616603681" ID="ID_706170110" MODIFIED="1383616607986" POSITION="right" TEXT="hors d&apos;oevres">
<node CREATED="1383616682542" ID="ID_947590423" MODIFIED="1383616686968" TEXT="bites">
<node CREATED="1383616691119" ID="ID_1010231109" MODIFIED="1383616692901" TEXT="">
<node CREATED="1383616686969" ID="ID_1683345620" MODIFIED="1383616689586" TEXT="edamame"/>
<node CREATED="1383616694496" ID="ID_1148785191" MODIFIED="1383616696778" TEXT="ponzu"/>
<node CREATED="1383616697590" ID="ID_1889764515" MODIFIED="1383616701458" TEXT="shaved pickled ginger"/>
</node>
</node>
</node>
<node CREATED="1337033753264" ID="ID_1472669380" MODIFIED="1337033755260" POSITION="right" TEXT="Starches">
<node CREATED="1313114575982" ID="Freemind_Link_1327880544" MODIFIED="1493759845693" TEXT="rice">
<node CREATED="1313114585508" LINK="http://www.chinatownconnection.com/cook-rice.htm" MODIFIED="1313114585508" TEXT="chinatownconnection.com &gt; Cook-rice"/>
<node CREATED="1313114595996" FOLDED="true" ID="Freemind_Link_1333524822" MODIFIED="1313114673564" TEXT="Steamed rice. -------------&#xa;2hrs">
<node CREATED="1313114595996" MODIFIED="1313114595996" TEXT="Soak a cup of rice in one and a fourth cups of water for an hour, then add a cup of milk, turn into a dish suitable for serving it from at table, and place in a steam-cooker or a covered steamer over a kettle of boiling water, and steam for an hour. It should be stirred with a fork occasionally, for the first ten or fifteen minutes."/>
</node>
<node CREATED="1313114609333" FOLDED="true" ID="Freemind_Link_1406329302" MODIFIED="1313114609333" TEXT="Boiled rice (Japanese method). ------------------------------">
<node CREATED="1313114609333" MODIFIED="1313114609333" TEXT="Thoroughly cleanse the rice by washing in several waters, and soak it overnight. In the morning, drain it, and put to cook in an equal quantity of boiling water, that is, a pint of water for a pint of rice. For cooking, a stewpan with tightly fitting cover should be used. Heat the water to boiling, then add the rice, and after stirring, put on the cover, which is not again to be removed during the boiling. At first, as the water boils, steam will puff out freely from under the cover, but when the water has nearly evaporated, which will be in eight to ten minutes, according to the age and quality of the rice, only a faint suggestion of steam will be observed, and the stewpan must then be removed from over the fire to some place on the range, where it will not burn, to swell and dry for fifteen or twenty minutes."/>
<node CREATED="1313114609333" MODIFIED="1313114609333" TEXT="Rice to be boiled in the ordinary manner requires two quarts of boiling water to one cupful of rice. It should be boiled rapidly until tender, then drained at once, and set in a moderate oven to become dry. Picking and lifting lightly occasionally with a fork will make it more flaky and dry. Care must be taken, however, not to mash the rice grains."/>
</node>
<node CREATED="1493759855992" FOLDED="true" ID="ID_1366752679" MODIFIED="1493759861993" TEXT="Rice Pilaf">
<node CREATED="1493759861163" LINK="http://www.foodnetwork.com/recipes/alton-brown/rice-pilaf-recipe2" MODIFIED="1493759861163" TEXT="foodnetwork.com &gt; Recipes &gt; Alton-brown &gt; Rice-pilaf-recipe2"/>
</node>
</node>
<node CREATED="1327900327579" FOLDED="true" ID="Freemind_Link_1997105272" MODIFIED="1337033781121" TEXT="30 ramen hacks">
<node CREATED="1327900333246" LINK="http://www.seriouseats.com/2011/03/ramen-hacks-30-easy-ways-to-upgrade-your-instant-noodles-japanese-what-to-do-with-ramen.html" MODIFIED="1327900333246" TEXT="seriouseats.com &gt; 2011 &gt; 03 &gt; Ramen-hacks-30-easy-ways-to-upgrade-your-instant-noodles-japanese-what-to-do-with-ramen"/>
</node>
</node>
<node CREATED="1337033725791" ID="ID_1706320251" MODIFIED="1337033727588" POSITION="right" TEXT="Soups">
<node CREATED="1293320966880" FOLDED="true" ID="Freemind_Link_505857037" MODIFIED="1296009659085" TEXT="squash soup">
<node CREATED="1296009660567" MODIFIED="1296009660567" TEXT="dad&apos;s"/>
<node CREATED="1298351698287" LINK="/Users/durantschoon/Documents/food/Butternut%20Squash%20and%20Leek%20Soup.doc" MODIFIED="1298351698287" TEXT="Butternut Squash and Leek Soup.doc"/>
</node>
</node>
<node CREATED="1337033716206" ID="ID_1590597053" MODIFIED="1337033717764" POSITION="right" TEXT="Desserts">
<node CREATED="1291879250936" ID="Freemind_Link_1047489741" MODIFIED="1291879250936" TEXT="peanut butter cookies">
<node CREATED="1291879266088" ID="Freemind_Link_316361436" MODIFIED="1291879285483" TEXT="Lisa McNamara">
<node CREATED="1291879287881" ID="Freemind_Link_111853295" MODIFIED="1291879289548" TEXT="on FB"/>
</node>
<node CREATED="1291879153232" FOLDED="true" ID="Freemind_Link_1465887340" MODIFIED="1362357805627" TEXT="recipe">
<node CREATED="1291879234725" MODIFIED="1291879234725" TEXT="Peanut Butter Sandwich Cookies"/>
<node CREATED="1291879234725" MODIFIED="1291879234725" TEXT="by Linda Weber"/>
<node CREATED="1291879234725" MODIFIED="1291879234725" TEXT="One Star Two Stars Three Stars Four Stars Five Stars read reviews (5)"/>
<node CREATED="1291879234725" MODIFIED="1291879234725" TEXT="This recipe can be mixed by hand or in an electric mixer. Use smooth peanut butter rather than chunky. I get good results with Skippy.Yields eighteen 2-1/2-inch sandwiches."/>
<node CREATED="1291879234725" MODIFIED="1291879234725" TEXT="ingredient discovery"/>
<node CREATED="1291879234725" MODIFIED="1291879234725" TEXT="Ingredients peanut butter cake flour baking soda more more"/>
<node CREATED="1291879234726" MODIFIED="1291879234726" TEXT="table salt all-purpose flour butter granulated sugar brown sugar pure vanilla extract semisweet chocolate peanuts semisweet chocolate chips"/>
<node CREATED="1291879234726" MODIFIED="1291879234726" TEXT="For the cookies:"/>
<node CREATED="1291879234726" MODIFIED="1291879234726" TEXT="6 oz. (1-1/3 cups) all-purpose flour"/>
<node CREATED="1291879234726" MODIFIED="1291879234726" TEXT="2 oz. (2/3 cup) cake flour"/>
<node CREATED="1291879234726" MODIFIED="1291879234726" TEXT="1/2 tsp. baking soda"/>
<node CREATED="1291879234726" MODIFIED="1291879234726" TEXT="1/4 tsp. salt"/>
<node CREATED="1291879234726" MODIFIED="1291879234726" TEXT="6 oz. (12 Tbs.) unsalted butter, completely softened at room temperature"/>
<node CREATED="1291879234726" MODIFIED="1291879234726" TEXT="3/4 cup smooth peanut butter"/>
<node CREATED="1291879234726" MODIFIED="1291879234726" TEXT="1/2 cup sugar"/>
<node CREATED="1291879234726" MODIFIED="1291879234726" TEXT="1/2 cup firmly packed light brown sugar"/>
<node CREATED="1291879234726" MODIFIED="1291879234726" TEXT="1 tsp. pure vanilla extract"/>
<node CREATED="1291879234726" MODIFIED="1291879234726" TEXT="1 large egg"/>
<node CREATED="1291879234726" MODIFIED="1291879234726" TEXT="For the filling:"/>
<node CREATED="1291879234726" MODIFIED="1291879234726" TEXT="1-1/2 cups confectioners&apos; sugar"/>
<node CREATED="1291879234726" MODIFIED="1291879234726" TEXT="3 oz. (6 Tbs.) unsalted butter, softened at room temperature"/>
<node CREATED="1291879234726" MODIFIED="1291879234726" TEXT="3/4 cup smooth peanut butter"/>
<node CREATED="1291879234726" MODIFIED="1291879234726" TEXT="3 Tbs. heavy cream"/>
<node CREATED="1291879234726" MODIFIED="1291879234726" TEXT="1/4 cup coarsely chopped roasted unsalted peanuts"/>
<node CREATED="1291879234726" MODIFIED="1291879234726" TEXT="1/4 cup coarsely chopped semisweet chocolate, or mini semisweet chocolate chips"/>
<node CREATED="1291879234726" MODIFIED="1291879234726" TEXT="To make the cookies"/>
<node CREATED="1291879234726" MODIFIED="1291879234726" TEXT="Heat the oven to 350&#xb0;F. Line two baking sheets with parchment. In a medium bowl, sift together the two flours, baking soda, and salt. In the bowl of an electric mixer, cream the butter, peanut butter, and sugars with the paddle attachment until light and fluffy. Add the vanilla and egg; continue creaming until smooth and fluffy, about 3 min. with an electric mixer (longer by hand). Stir in the flour mixture by hand just until it&apos;s incorporated; don&apos;t overmix or the cookies will be tough. Drop heaping tablespoonfuls of batter, spaced about 2 inches apart, onto the lined baking sheets. With floured fingers, flatten each dab of batter into a 2-inch round. Bake until the cookies are puffed and golden, 12 to 14 min., rotating the baking sheets if needed for even baking. Transfer the cookies to a rack to cool."/>
<node CREATED="1291879234727" MODIFIED="1291879234727" TEXT="Peanut Butter Sandwich Cookies Recipe Instead of cross-hatching with a fork, use your fingers to press and decorate."/>
<node CREATED="1291879234727" MODIFIED="1291879234727" TEXT="While the cookies cool, make the filling"/>
<node CREATED="1291879234727" MODIFIED="1291879234727" TEXT="In a small bowl, cream the confectioners&apos; sugar, butter, and peanut butter until smooth. Add the heavy cream; continue creaming until smooth and fluffy. Stir in the chopped peanuts and chocolate."/>
<node CREATED="1291879234727" MODIFIED="1291879234727" TEXT="To assemble"/>
<node CREATED="1291879234727" MODIFIED="1291879234727" TEXT="Transfer the cooled cookies to a work surface, flipping half of them over. With an offset spatula or a butter knife, spread a scant teaspoon of filling onto each turned-over cookie. Set another wafer on top of each filled cookie, pressing gently to spread the filling. Store sealed at room temperature or in the refrigerator."/>
<node CREATED="1291879234727" MODIFIED="1291879234727" TEXT="Peanut Butter Sandwich Cookies Recipe An offset spatula is handy for spreading the cookie filling easily and evenly. A blunt table knife works, too."/>
<node CREATED="1291879234727" MODIFIED="1291879234727" TEXT="nutrition information (per serving):"/>
<node CREATED="1291879234727" MODIFIED="1291879234727" TEXT="Size: per cookie; Calories (kcal): 390; Fat (g): 25; Fat Calories (kcal): 230; Saturated Fat (g): 11; Protein (g): 8; Monounsaturated Fat (g): 10; Carbohydrates (g): 36; Polyunsaturated Fat (g): 4; Sodium (mg): 290; Cholesterol (mg): 45; Fiber (g): 2;"/>
<node CREATED="1291879234727" MODIFIED="1291879234727" TEXT="photo: Anna Williams"/>
<node CREATED="1291879234727" MODIFIED="1291879234727" TEXT="From Fine Cooking 43, pp. 54-55"/>
<node CREATED="1291879234727" MODIFIED="1291879234727" TEXT="February 1, 2001"/>
<node CREATED="1291879234757" MODIFIED="1291879234757" TEXT="Links">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1291879234758" LINK="http://www.finecooking.com/recipes/peanut_butter_sandwich_cookies.aspx#reviews" MODIFIED="1291879234758" TEXT="read reviews"/>
<node CREATED="1291879234760" LINK="http://www.finecooking.com/item/5241/peanut-butter" MODIFIED="1291879234760" TEXT="peanut butter"/>
<node CREATED="1291879234760" LINK="http://www.finecooking.com/item/5129/cake-flour" MODIFIED="1291879234760" TEXT="cake flour"/>
<node CREATED="1291879234761" LINK="http://www.finecooking.com/item/5053/baking-soda" MODIFIED="1291879234761" TEXT="baking soda"/>
<node CREATED="1291879236422" LINK="http://www.finecooking.com/item/5902/table-salt" MODIFIED="1291879236422" TEXT="table salt"/>
<node CREATED="1291879236424" LINK="http://www.finecooking.com/item/5008/all-purpose-flour" MODIFIED="1291879236424" TEXT="all-purpose flour"/>
<node CREATED="1291879236425" LINK="http://www.finecooking.com/item/5124/butter" MODIFIED="1291879236425" TEXT="butter"/>
<node CREATED="1291879236426" LINK="http://www.finecooking.com/item/5365/granulated-sugar" MODIFIED="1291879236426" TEXT="granulated sugar"/>
<node CREATED="1291879236427" LINK="http://www.finecooking.com/item/5253/brown-sugar" MODIFIED="1291879236427" TEXT="brown sugar"/>
<node CREATED="1291879236428" LINK="http://www.finecooking.com/item/5652/pure-vanilla-extract" MODIFIED="1291879236428" TEXT="pure vanilla extract"/>
<node CREATED="1291879236429" LINK="http://www.finecooking.com/item/5729/semisweet-chocolate" MODIFIED="1291879236429" TEXT="semisweet chocolate"/>
<node CREATED="1291879236429" LINK="http://www.finecooking.com/item/5583/peanuts" MODIFIED="1291879236429" TEXT="peanuts"/>
<node CREATED="1291879236431" LINK="http://www.finecooking.com/item/5730/semisweet-chocolate-chips" MODIFIED="1291879236431" TEXT="semisweet chocolate chips"/>
</node>
</node>
</node>
<node CREATED="1292011003021" ID="Freemind_Link_600958837" MODIFIED="1292011007920" TEXT="pumpkin rice pudding">
<node CREATED="1292011051665" FOLDED="true" ID="Freemind_Link_1124737785" MODIFIED="1292011055724" TEXT="first message">
<node CREATED="1292011058289" ID="Freemind_Link_12830808" MODIFIED="1292011063126" TEXT="Ok here&apos;s the deal. I&apos;ve been making this for years now at Thanksgiving (sans pumpkin) and it&apos;s super simple. Here&apos;s the regular recipe I&apos;ve been using. You basically just mix all the following together and heat on low heat:&#xa;&#xa;1 cup Arborio rice (usually used for risotto)&#xa;6 cups whole milk&#xa;2/3 cup sugar&#xa;1/2 tsp. vanilla extract&#xa;1/4 tsp. salt&#xa;1 tablespoon butter&#xa;&#xa;That makes a decent sized batch, fills an 9x9 glass serving dish and can serve many people ... or one if you eat it over several days and don&apos;t mind binging. :) You could halve it for a more individual-friendly size if necessary.&#xa;&#xa;Now, the key is that you need to use *low* heat and *stir* a lot, like every 5 minutes minimum. If you really want to do it right, use a double boiler or a hacked together one (which is what I do). That ensures constant low-level even heat. If you have to cook directly on the stovetop, keep it super low and stir even more because the milk could separate if it gets too hot. Here&apos;s a web page that shows a hacked double boiler and gives the general idea although the my recipe is different (esp. the type of rice) than the one shown and comes out a bit different depending on how long you cook it:&#xa;&#xa;http://www.thehungrymouse.com/2008/11/18/decadent-sweet-cream-rice-pudding/&#xa;&#xa;There is an art to figuring out when to stop cooking it. It&apos;s going to thicken more than you realize once it cools, so you have to stop it while it&apos;s just starting to get thick. If you stop it too runny it&apos;ll be too runny once cold. If you stop it when it&apos;s as thick as you expect the final pudding to be, it&apos;ll be *really* firm once cold. (But&apos;ll still taste fine.) It usually takes me about 90 minutes to cook. This part only comes with just trying it and learning.&#xa;&#xa;Once done, put in the fridge overnight, or at least 4-5 hours. (Though I always scoop out a bowl of the hot stuff for me!)&#xa;&#xa;Ok, so this year for the pumpkin version (an idea that came to me independently but I quickly found it&apos;s very popular) I just googled around for various recipes and ended up adding the following, about halfway through cooking:&#xa;&#xa;1 cup canned pumpkin&#xa;1 teaspoon cinnamon&#xa;1/4 teaspoon nutmeg&#xa;1/4 teaspoon ginger&#xa;&#xa;It ended up quite good but the pumpkin flavor was a little more mild than I was hoping. I think next time I&apos;ll inch up to 1.5 cups pumpkin and more spices too and see how that goes. One recipe also called for adding it very late, like with only 10 minutes to go. That probably prevents the pumpkin flavor from cooking off too much, so I think I&apos;ll try that too. This part is really open to experimentation, so have fun!&#xa;"/>
</node>
<node CREATED="1292011008725" FOLDED="true" ID="Freemind_Link_1296410241" MODIFIED="1380230097639" TEXT="update">
<node CREATED="1292011010581" ID="Freemind_Link_960159789" MODIFIED="1292011017079" TEXT="  Jonathan Litt    December 10 at 9:08am  Reply   &#x2022; Report&#xa;Ok made another batch. (Ahhh, the joys of being off work. Last day unfortunately.) Used a whole &quot;small&quot; can (Campbell&apos;s soup size) of pumpkin, which was about 1 1/4 cups I think.&#xa;&#xa;http://i55.tinypic.com/2hqulo6.jpg&#xa;&#xa;Still need to experiment more with spice amounts since I&apos;d love for it to have more of a pumpkin pie &quot;finish&quot; in the taste. I might put more sugar in next time since I think the pumpkin kind of needs more sweetness to be balanced out vs. plain rice pudding. Cooked right on the gas this time, no double boiler. Just really low and lots of stirring and kept covered.&#xa;&#xa;Oooh yeah there&apos;s a place near DD that has pumpkin bread pudding that I hear is amazing. I haven&apos;t tried it yet.&#xa;&#xa;Yeah the alchemy of cooking is fascinating. I&apos;ve been reading a lot of Cook&apos;s Illustrated magazine which gets into that a lot. I also got a book called &quot;The Science Of Cooking&quot; which is really great. "/>
</node>
<node CREATED="1292011602019" FOLDED="true" ID="Freemind_Link_1851781186" MODIFIED="1292011605542" TEXT="durant: sweeteners">
<node CREATED="1292011606739" ID="Freemind_Link_927821493" MODIFIED="1292011613671" TEXT="  Durant Schoon    December 10 at 10:04am&#xa;&#xa;looks good! Also there are different kinds of sweeteners that might take it in interesting directions, I&apos;m thinking of maple syrup or brown sugar, molasses, hell dulce de leche! My dad makes cabbage sweetened with stevia which is normally too sweet for me, but absolutely perfect with cabbage for some reason. Could be fun and scientific to try. Too bad your little elves aren&apos;t old enough to help with the various batches. Soon enough :)&#xa;&#xa;nom, nom, nom "/>
</node>
</node>
<node CREATED="1312919770537" FOLDED="true" ID="Freemind_Link_1457550043" MODIFIED="1337033773159" TEXT="caramelized pears">
<node CREATED="1312919779419" LINK="http://lulumanhattan.blogspot.com/2004/10/caramelized-pears.html" MODIFIED="1312919779419" TEXT="lulumanhattan.blogspot.com &gt; 2004 &gt; 10 &gt; Caramelized-pears"/>
<node CREATED="1312919792658" MODIFIED="1312919792658" TEXT="August through October is the height of the pear season though you can pretty much find them all year round in Manhattan. At the peak of summer, I love biting into succulent Anjou pears that are bursting with tangy flavours. As it gets colder, I turn to fully ripe Bartletts which work really well in desserts."/>
<node CREATED="1312919792659" MODIFIED="1312919792659" TEXT="I recently came across a recipe for caramelized pears in Everyday Food magazine that is extremely simple and produces amazing results every time. Follow instructions and once the pears are cooked and the sauce is of your desired consistency, heap on a generous dollop of vanilla icecream and enjoy!"/>
<node CREATED="1312919792659" ID="Freemind_Link_1559858605" MODIFIED="1312919792659" TEXT="Recipe:">
<node CREATED="1312919792659" ID="Freemind_Link_814691985" MODIFIED="1312919878744" TEXT="Halve 4 red Bartlett pears lengthwise; &#xa;remove cores. &#xa;Place 1/3 cup granulated sugar on a plate. &#xa;Heat a large skillet over medium heat. &#xa;Press cut side of each halved pear in sugar; &#xa;place cut side down in skillet (fit will be snug). &#xa;Cook until beginning to brown. 7 to 8 minutes. &#xa;Add 1/2 cup water. Cover; &#xa;simmer until pears are tender, 5 to 10 minutes (depending on ripeness), adding more water if sugar begins to burn. &#xa;&#xa;Remove pears from skillet. &#xa;If liquid in pan is thin, simmer until thickened to a saucelike consistency; &#xa;if it is thick, add more water. &#xa;&#xa;Serve sauce over pears."/>
</node>
<node CREATED="1312919792660" ID="Freemind_Link_1057085316" MODIFIED="1312919792660" TEXT="Serves 4"/>
<node CREATED="1312919792660" MODIFIED="1312919792660" TEXT="Prep time: 20 minutes"/>
<node CREATED="1312919792660" MODIFIED="1312919792660" TEXT="Total time: 20 minutes"/>
</node>
<node CREATED="1362357155123" ID="ID_1774958430" MODIFIED="1362357162791" TEXT="microwave cookie?">
<node CREATED="1362357168339" ID="ID_3438723" LINK="http://prudentbaby.com/2012/04/entertaining-food/one-minuteone-cookie-how-to-make-a-single-microwave-chocolate-chip-cookie-in-a-minute/" MODIFIED="1362357168339" TEXT="prudentbaby.com &gt; 2012 &gt; 04 &gt; Entertaining-food &gt; One-minuteone-cookie-how-to-make-a-single-microwave-chocolate-chip-cookie-in-a-minute"/>
<node CREATED="1362357610087" ID="ID_90422441" MODIFIED="1362357614078" TEXT="SOFTEN one tablespoon of butter : microwave 6-8 seconds&#xa;Add 1.5 tsp sugar &amp; 1 tsp packed brown sugar; STIR&#xa;ADD drop of vanilla; STIR&#xa;ADD 1-2 tsp egg&#xa;ADD &#xa;&#x9;2 tablespoons of all purpose flour, &#xa;&#x9;a pinch of baking powder &#xa;&#x9;a pinch of salt&#xa;&#x9;chocolate chips; STIR&#xa;&#xa;MICROWAVE for 40 seconds. +5 if needed"/>
</node>
<node CREATED="1387861330357" ID="ID_834641907" MODIFIED="1387861345328" TEXT="Nutella 33 ways">
<node CREATED="1387861346014" LINK="http://www.buzzfeed.com/emofly/fancy-nutella-recipes" MODIFIED="1387861346014" TEXT="buzzfeed.com &gt; Emofly &gt; Fancy-nutella-recipes"/>
</node>
</node>
<node CREATED="1380230113435" ID="ID_1016464323" MODIFIED="1380230114300" POSITION="right" TEXT="Bread">
<node CREATED="1380230114595" FOLDED="true" ID="ID_873066572" MODIFIED="1381970938375" TEXT="No Knead Bread">
<node CREATED="1380230125008" ID="ID_1320438962" LINK="http://www.viand.net/doughs.html" MODIFIED="1380230125008" TEXT="viand.net &gt; Doughs"/>
<node CREATED="1380230150242" ID="ID_1860691107" MODIFIED="1380230199641" TEXT="10 minute bread (it&apos;s 10 minutes of work, including cleanup, over a 24 hour period)&#xa;&#xa;from Jim Lahey of Sullivan Street Bakery in New York.&#xa;&#xa;1. mix: 3 cups flour, 1/4 tsp yeast, 2 tsp salt, 1 5/8 cups water (that&apos;s 1.5 cups plus 2 tbsp). use your hands! (they&apos;re easier to clean. we discovered this after making the video.)&#xa;&#xa;2. cover bowl with plastic wrap (or a plate), let rest at room temperature at least 12 hours, but 18 or 24 is fine too.&#xa;&#xa;3. 1st nap: sprinkle LOTS of flour on a surface. remove the sticky dough from the bowl and dump it on the surface. sprinkle a lot of flour on the dough. fold it over on itself once or twice. cover with the plastic wrap and leave it alone for 15 minutes.&#xa;&#xa;4. 2nd nap: find a cotton towel that is not terrycloth (a kitchen towel, not a bath towel). sprinkle it with a lot of flour. shape the dough into a ball (or as close to a ball as you can get it, given that it may be very mushy). as you are tucking the dough into a ball, keep the smooth side up. put it in the towel. leave it alone for 1.5 hours.&#xa;&#xa;5. after 1.5 hours, turn the oven on to 450 degrees. Put a large pot for which you have a cover into the oven. (A 4 quart pot is good, but bigger is ok too. Cast iron, pyrex, stainless, or ceramic are all fine.) Go away for 30 minutes.&#xa;&#xa;6. carefully drop the dough into the pan. turning it upside down on the way from the towel to the pan. this way the &quot;seam&quot; from the bottom of the ball ends up on the top of the bread. put the lid on the pay. bake for 30 minutes. if it&apos;s starting to brown at this point it&apos;s probably done. turn the bread out of the pan and knock the bottom. if it sounds hollow it&apos;s done.&#xa;&#xa;7. if the bread smells yeasty or if it is not yet brown, put it back in the oven without the pot for another 10-20 minutes. how long is a matter of unpredictable oven temperature and your crust preferences. check it every 5 minutes until you get to know your oven and preferences. if you want a crisper crust, but it&apos;s already brown when you take it out of the pot, turn the oven off and put the bread back in for a few minutes.&#xa;"/>
</node>
</node>
<node CREATED="1364252062275" ID="ID_1494092935" MODIFIED="1365155792341" POSITION="right" TEXT="medicinal">
<node CREATED="1364326795110" ID="ID_1673028945" MODIFIED="1364326847855" TEXT="For the cough, &#xa;my grandmather&apos;s Onion Syrup &#xa;is the best for me: &#xa;(Cristina in Portugal)">
<node CREATED="1364326820733" ID="ID_1549063603" MODIFIED="1364326820734" TEXT="1) boil the onion peels for 10-15 min, "/>
<node CREATED="1364326816994" ID="ID_57744394" MODIFIED="1364326816995" TEXT="2) throw away the peels , "/>
<node CREATED="1364326809593" ID="ID_445128057" MODIFIED="1364326809593" TEXT="3) add some sugar (as much as you like) and let it boil again for 10- 15 min. Then take a spoon or two whenever you feel like."/>
</node>
</node>
<node CREATED="1364748224096" ID="ID_208479935" MODIFIED="1364748226862" POSITION="right" TEXT="interesting">
<node CREATED="1364748230007" ID="ID_55356281" MODIFIED="1381971009099" TEXT="&quot;scrambling&quot; an egg before hard boiling (video)">
<node CREATED="1364748246424" ID="ID_307836225" LINK="http://gizmodo.com/5992977/scrambling-eggs-inside-its-shells-to-make-scrambled-hard-boiled-eggs-looks-so-fun" MODIFIED="1364748246424" TEXT="gizmodo.com &gt; 5992977 &gt; Scrambling-eggs-inside-its-shells-to-make-scrambled-hard-boiled-eggs-looks-so-fun"/>
</node>
</node>
<node CREATED="1337033902433" HGAP="24" ID="ID_572390976" MODIFIED="1492388343893" POSITION="left" TEXT="ToDo" VSHIFT="-62">
<node CREATED="1332054866245" ID="Freemind_Link_1961790071" MODIFIED="1332054871289" TEXT="pinterest for recipies"/>
<node CREATED="1491210954896" ID="ID_688312731" MODIFIED="1492546647391" STYLE="fork" TEXT="try">
<cloud/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1491210961331" FOLDED="true" ID="ID_1951345823" MODIFIED="1492546625139" TEXT="Pretzel Hamburger Buns">
<node CREATED="1491210969209" ID="ID_1331840900" MODIFIED="1491210986880" TEXT="Prep Time: 2 hours  &#xa;Cook Time: 20 minutes  &#xa;Total Time: 2 hours, 20 minutes  &#xa;Yield: 8 Pretzel Hamburger Buns  &#xa;Serving Size: 1 Pretzel Hamburger Bun  "/>
<node CREATED="1491210997486" LINK="http://www.ladybehindthecurtain.com/pretzel-hamburger-buns/" MODIFIED="1491210997486" TEXT="ladybehindthecurtain.com &gt; Pretzel-hamburger-buns"/>
<node CREATED="1491211049037" FOLDED="true" ID="ID_1103795743" MODIFIED="1491211082673" TEXT="instructions">
<node CREATED="1491211036309" ID="ID_1461805012" MODIFIED="1491211079856" TEXT="Ingredients&#xa;&#xa;1-1/2 cup warm water (110 degrees)&#xa;2-1/4 teaspoons or 1/4 ounce active dry yeast&#xa;2 teaspoons granulated sugar&#xa;4-1/4 cups all-purpose or bread flour&#xa;2 teaspoons salt&#xa;4 tablespoons butter, melted&#xa;1/4 cup baking soda&#xa;1 egg, lightly beaten&#xa;salt for sprinkling (pretzel or kosher)&#xa;&#xa;Instructions&#xa;&#xa;In the bowl of your stand up mixer fitted with the dough hook, add the water, yeast and sugar.&#xa;Stir and let rest for 5 - 10 minutes or until foamy.&#xa;Add the flour, salt and melted butter.&#xa;Mix until well combined and forms a ball.&#xa;Transfer to a medium bowl sprayed with non stick cooking spray, cover with plastic wrap and allow to rise in a warm place for 1 hour or until doubled in size.&#xa;Line a cookie sheet with parchment paper and set aside.&#xa;Pull dough out of bowl onto a lightly floured surface and divide into 8 equal portions (about 4.2 ounces each).&#xa;Form each portion into and ball by first pinching the top to the bottom and then rolling in the palm of your hand.&#xa;Place on prepared cookie sheet seam down in a staggered position.&#xa;Cover with towel allow to rest in a warm place for 30 minutes or until they rise and double.&#xa;Preheat oven to 425 degrees and place oven rack in the middle position.&#xa;In a large saucepan, bring 2 quarts of water to a low boil.&#xa;Remove from heat and slowly add the baking soda.&#xa;Return to heat and continue with a steady boil.&#xa;Place 2 - 3 of the rolls at a time in the poaching liquid, seam side down.&#xa;Poach for 30 seconds and then carefully turn the roll over and poach for another 30 seconds.&#xa;Remove with a slotted spoon to the same prepared sheet pan, seam side down.&#xa;Repeat with the remaining rolls.&#xa;Using a pastry brush, brush each roll with the beaten egg, making sure to coat all sides completely and then sprinkle each roll with salt.&#xa;Using a sharp straight edged knife, cut a &quot;X&quot; shape in the top of each roll.&#xa;Bake for 15 to 20 minutes."/>
</node>
</node>
<node CREATED="1492292381072" FOLDED="true" ID="ID_1239018450" MODIFIED="1496702892222" TEXT="Easy 4 Ingredient Baked Oatmeal Cups">
<node CREATED="1492292385932" LINK="http://www.thereciperebel.com/easy-4-ingredient-baked-oatmeal-cups/" MODIFIED="1492292385932" TEXT="thereciperebel.com &gt; Easy-4-ingredient-baked-oatmeal-cups"/>
<node CREATED="1492292396435" ID="ID_1430788242" MODIFIED="1492292501348" TEXT="INGREDIENTS">
<node CREATED="1492292436222" ID="ID_1440108073" MODIFIED="1492292436222" TEXT="2&#xbd; cups large flake oats"/>
<node CREATED="1492292603364" ID="ID_751484615" MODIFIED="1492292607195" TEXT="1 large egg">
<node CREATED="1492292607197" ID="ID_443131074" MODIFIED="1492292607959" TEXT="or">
<node CREATED="1492292436226" ID="ID_835843357" MODIFIED="1492292436226" TEXT="&#xbc; cup EGG Creations Whole Eggs Original"/>
</node>
</node>
<node CREATED="1492292436227" ID="ID_170855184" MODIFIED="1492292436227" TEXT="1&#xbd; cups milk"/>
<node CREATED="1492292436228" MODIFIED="1492292436228" TEXT="&#xbd; cup maple syrup"/>
<node CREATED="1492292478138" ID="ID_1717897604" MODIFIED="1492292478138" TEXT="Optional mix ins:">
<node CREATED="1492292478142" MODIFIED="1492292478142" TEXT="finely chopped apples,"/>
<node CREATED="1492292478144" MODIFIED="1492292478144" TEXT="bananas,"/>
<node CREATED="1492292478145" MODIFIED="1492292478145" TEXT="cinnamon,"/>
<node CREATED="1492292478146" MODIFIED="1492292478146" TEXT="fresh or frozen berries,"/>
<node CREATED="1492292478146" MODIFIED="1492292478146" TEXT="peanut butter (warm it up a little before stirring in!),"/>
<node CREATED="1492292478147" MODIFIED="1492292478147" TEXT="mini chocolate chips,"/>
<node CREATED="1492292478147" MODIFIED="1492292478147" TEXT="nuts,"/>
<node CREATED="1492292478148" MODIFIED="1492292478148" TEXT="dried fruit"/>
</node>
<node CREATED="1492292501349" ID="ID_896128389" MODIFIED="1492292502623" TEXT="INSTRUCTIONS">
<node CREATED="1492292576052" ID="ID_851523101" MODIFIED="1492292576052" TEXT="1.">
<node CREATED="1492292576053" MODIFIED="1492292576053" TEXT="Preheat oven to 350 degrees F and line a 12 cup muffin pan with liners (I like to use the silicone ones!). Set aside."/>
</node>
<node CREATED="1492292576055" MODIFIED="1492292576055" TEXT="2.">
<node CREATED="1492292576055" ID="ID_121705424" MODIFIED="1492292630691" TEXT="In a medium bowl stir together oats, egg, milk and maple syrup until combined. Divide evenly between muffin cups (make sure you get equal liquid in each cup so none are dry)."/>
</node>
<node CREATED="1492292576057" MODIFIED="1492292576057" TEXT="3.">
<node CREATED="1492292576058" MODIFIED="1492292576058" TEXT="Stir in optional mix ins if desired (*NOTE: if you do this, you may want to leave a little extra space in your muffin cups, and therefore you may get 16 instead of 12): I did apple and cinnamon, banana and peanut butter, raspberries and chocolate chips."/>
</node>
<node CREATED="1492292576060" MODIFIED="1492292576060" TEXT="4.">
<node CREATED="1492292576060" MODIFIED="1492292576060" TEXT="Bake for about 25 minutes, until light golden brown on top and completely set. Let sit for 5-10 minutes before removing from liners."/>
</node>
<node CREATED="1492292576062" MODIFIED="1492292576062" TEXT="5.">
<node CREATED="1492292576063" MODIFIED="1492292576063" TEXT="Oatmeal Cups can be stored in the refrigerator for up to 1 week or frozen for up to 3 months. They reheat beautifully! Serve alone or with milk and additional maple syrup if desired."/>
</node>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1337033763680" ID="ID_1193146509" MODIFIED="1337033765317" POSITION="left" TEXT="Meat">
<node CREATED="1291573214948" FOLDED="true" ID="Freemind_Link_719305086" MODIFIED="1381971031639" TEXT="pork tenderloins">
<node CREATED="1291573222268" ID="Freemind_Link_1431982051" MODIFIED="1291573223207" TEXT="mom">
<node CREATED="1291573346127" ID="Freemind_Link_309873658" MODIFIED="1291573390245" TEXT="they&apos;ll last about a week in a refrigerator. &#xa;&#xa;I pound them out with a mallet (like a hammer with an uneven texture on the end) until they are about 1/4 inch thick.  &#xa;&#xa;then I dip them in beaten egg (or egg beaters) &#xa;&#xa;and then in seasoned bread crumbs. Here you buy them seasoned, but if not, add salt, pepper, herbs of choice. &#xa;&#xa;You can also add Parmesan cheese, but it sticks to the pan. &#xa;&#xa;I fry them with olive oil."/>
</node>
<node CREATED="1292055740029" ID="Freemind_Link_1653410317" MODIFIED="1292055757208" TEXT="like low/medium heat so as not to burn"/>
<node CREATED="1292055767316" ID="Freemind_Link_1614028880" MODIFIED="1292055777537" TEXT="2 min on first side, 1 min on other, or so"/>
</node>
<node CREATED="1295908523181" FOLDED="true" ID="Freemind_Link_540404769" MODIFIED="1381971029199" TEXT="Short Rib and Barley Stew">
<node CREATED="1295908524370" ID="Freemind_Link_796037083" MODIFIED="1295908530094" TEXT="substitute beef broth"/>
<node CREATED="1295908517175" LINK="http://www.seriouseats.com/recipes/2011/01/short-rib-and-barley-stew.html" MODIFIED="1295908517175" TEXT="seriouseats.com &gt; Recipes &gt; 2011 &gt; 01 &gt; Short-rib-and-barley-stew"/>
</node>
<node CREATED="1329197836551" FOLDED="true" ID="Freemind_Link_154467102" MODIFIED="1455252132241" TEXT="asian pork meat balls">
<node CREATED="1329197850395" LINK="http://www.theravenouscouple.com/2010/10/canh-kho-qua-bitter-melon-soup.html" MODIFIED="1329197850395" TEXT="theravenouscouple.com &gt; 2010 &gt; 10 &gt; Canh-kho-qua-bitter-melon-soup"/>
<node CREATED="1329197851311" ID="Freemind_Link_1634380554" MODIFIED="1329197859332" TEXT="use zucchini instead of bitter melon"/>
<node CREATED="1329197881905" ID="Freemind_Link_365359013" MODIFIED="1329197881905" TEXT="ingredients:">
<node CREATED="1329197881905" ID="Freemind_Link_802330315" MODIFIED="1329197881905" TEXT="Two medium length 8-12 inch bitter melons">
<node CREATED="1329197892497" ID="Freemind_Link_1650152392" MODIFIED="1329197898116" TEXT="ZUCCHINI"/>
</node>
<node CREATED="1329197881905" MODIFIED="1329197881905" TEXT="6 cups of water or chicken broth"/>
<node CREATED="1329197881905" MODIFIED="1329197881905" TEXT="1 green onion, thinly chopped"/>
<node CREATED="1329197881905" MODIFIED="1329197881905" TEXT="sprigs of cilantro, thinly chopped"/>
<node CREATED="1329197881905" MODIFIED="1329197881905" TEXT="1 peeled shallot"/>
<node CREATED="1329197881905" MODIFIED="1329197881905" TEXT="salt"/>
<node CREATED="1329197881905" MODIFIED="1329197881905" TEXT="Pork Stuffing"/>
<node CREATED="1329197881905" MODIFIED="1329197881905" TEXT="1/2 lb ground pork"/>
<node CREATED="1329197881905" MODIFIED="1329197881905" TEXT="1 tsp fish sauce"/>
<node CREATED="1329197881905" MODIFIED="1329197881905" TEXT="1 tsp ground pepper"/>
<node CREATED="1329197881905" MODIFIED="1329197881905" TEXT="1 tsp sugar"/>
<node CREATED="1329197881905" MODIFIED="1329197881905" TEXT="1/2 tsp salt"/>
<node CREATED="1329197881905" MODIFIED="1329197881905" TEXT="1 minced shallots or small onion, diced"/>
<node CREATED="1329197881905" MODIFIED="1329197881905" TEXT="1/2 cup bean thread noodles, soaked in warm water about 10 min, drain and cut in 2-3 inch length"/>
<node CREATED="1329197881905" MODIFIED="1329197881905" TEXT="1/4 cup wood ear mushrooms, soaked in warm water about 10 min, drain and cut thinly"/>
</node>
<node CREATED="1329197881905" ID="Freemind_Link_1824473587" MODIFIED="1329197881905" TEXT="directions:">
<node CREATED="1329197881905" MODIFIED="1329197881905" TEXT="In a large mixing bowl, mix together the pork, fish sauce, sugar, pepper, salt, shallots, wood ear mushrooms, bean thread noodles and mix well. Cover and set aside."/>
<node CREATED="1329197881905" MODIFIED="1329197881905" TEXT="Blanching is an optional technique to reduce a bit of the bitterness. Quickly blanch the entire bitter melon in boiling water for about 1 minute--the color will turn a dark green. Remove and allow to cool. When cool to the touch, slice melons cross wise into 1 to 1.5 inch pieces. Using a table spoon or knife, drive it between the green outer flesh and the center white pith. Hold the bitter melon in one hand and spoon/knife in other, and slowly core out the pith and seeds with a circular motion."/>
<node CREATED="1329197881906" MODIFIED="1329197881906" TEXT="Add the peeled shallot to the water/stock and bring to boil. In the meantime, stuff the bitter melons with the pork stuffing. When water/stock is boiling add the stuffed bitter melons and cover reducing heat to low, cook for about 15 minutes or until the center of the pork is done. Season with some salt to taste. Transfer to soup bowl and garnish with chopped green onion and cilantro. Serve with jasmine rice."/>
</node>
</node>
</node>
<node CREATED="1337033863320" ID="ID_1056529821" MODIFIED="1337033880510" POSITION="left" TEXT="Methodological">
<node CREATED="1295908497834" ID="Freemind_Link_306204186" MODIFIED="1381971016488" TEXT="crockpot">
<node CREATED="1295908555219" ID="Freemind_Link_955472777" MODIFIED="1295908555219" TEXT="Crockpot Pork with Cabbage">
<node CREATED="1295908555219" LINK="http://busycooks.about.com/od/porkentreerecipes/r/porkcabbage.htm" MODIFIED="1295908555219" TEXT="busycooks.about.com &gt; Od &gt; Porkentreerecipes &gt; R &gt; Porkcabbage"/>
<node CREATED="1295908573619" ID="Freemind_Link_799733870" MODIFIED="1295908577447" TEXT="try ginger and stevia"/>
</node>
<node CREATED="1295908555218" MODIFIED="1295908555218" TEXT="Crockpot Beef Stroganoff">
<node CREATED="1295908555219" LINK="http://busycooks.about.com/od/beefsteakrecipes/r/Crockpot-Beef-Stroganoff.htm" MODIFIED="1295908555219" TEXT="busycooks.about.com &gt; Od &gt; Beefsteakrecipes &gt; R &gt; Crockpot-Beef-Stroganoff"/>
</node>
<node CREATED="1295908555220" ID="Freemind_Link_1550655613" MODIFIED="1295908555220" TEXT="Three Bean Cassoulet">
<node CREATED="1295908555220" LINK="http://busycooks.about.com/od/meatlessentreerecipes/r/cpbeancassoulet.htm" MODIFIED="1295908555220" TEXT="busycooks.about.com &gt; Od &gt; Meatlessentreerecipes &gt; R &gt; Cpbeancassoulet"/>
<node CREATED="1295908562331" ID="Freemind_Link_481751247" MODIFIED="1295908568934" TEXT="use white beans not limas"/>
</node>
<node CREATED="1295908555221" MODIFIED="1295908555221" TEXT="Crockpot Lasagna">
<node CREATED="1295908555221" LINK="http://busycooks.about.com/od/groundbeefrecipes/r/cplasagna.htm" MODIFIED="1295908555221" TEXT="busycooks.about.com &gt; Od &gt; Groundbeefrecipes &gt; R &gt; Cplasagna"/>
</node>
</node>
</node>
<node CREATED="1379997782200" ID="ID_277704819" MODIFIED="1379997784760" POSITION="left" TEXT="Saveur">
<node CREATED="1379997785140" FOLDED="true" ID="ID_584339045" MODIFIED="1455252340437" TEXT="Parisian Gnocchi">
<node CREATED="1379997790345" ID="ID_385077556" LINK="http://www.seriouseats.com/2013/09/the-food-lab-parisian-gnocchi-how-to.html" MODIFIED="1379997790345" TEXT="seriouseats.com &gt; 2013 &gt; 09 &gt; The-food-lab-parisian-gnocchi-how-to"/>
</node>
</node>
<node CREATED="1388645882209" ID="ID_1810958790" MODIFIED="1492221872193" POSITION="left" TEXT="Reddit">
<node CREATED="1388645884292" FOLDED="true" ID="ID_1668858494" MODIFIED="1455252334095" TEXT="Simple yet impressive">
<node CREATED="1388645890303" LINK="http://www.reddit.com/r/AskReddit/comments/1u6kg2/what_are_some_meals_that_are_simple_to_make_but/cef4dt7" MODIFIED="1388645890303" TEXT="reddit.com &gt; R &gt; AskReddit &gt; Comments &gt; 1u6kg2 &gt; What are some meals that are simple to make but &gt; Cef4dt7"/>
<node CREATED="1388645900783" MODIFIED="1388645900783" TEXT="-Baked Gnocchi with Taleggio, Pancetta, and Sage"/>
<node CREATED="1388645900784" MODIFIED="1388645900784" TEXT="-Balsamic and Blue Cheese Steak Sandwich"/>
<node CREATED="1388645900784" MODIFIED="1388645900784" TEXT="-Parm Crusted Scalloped Potatoes"/>
<node CREATED="1388645900784" ID="ID_55973944" MODIFIED="1388645900784" TEXT="-Sloppy Buffalo Bettys"/>
<node CREATED="1388645900784" ID="ID_197329043" MODIFIED="1388645900784" TEXT="-Pork Stir Fry with Green Onion"/>
<node CREATED="1388645900784" MODIFIED="1388645900784" TEXT="-Roasted Smashed Potatoes"/>
<node CREATED="1388645900784" MODIFIED="1388645900784" TEXT="-Drunken Mexican Beans with Cilantro and Bacon"/>
<node CREATED="1388645900784" MODIFIED="1388645900784" TEXT="-Lamb Burger with Chunky Mint Tzatziki"/>
<node CREATED="1388645900784" MODIFIED="1388645900784" TEXT="-Juicy Mini Burgers with Special Sauce"/>
<node CREATED="1388645900784" MODIFIED="1388645900784" TEXT="-Crispy Baked Avocado Fries"/>
<node CREATED="1388645900784" ID="ID_1523900699" MODIFIED="1388645900784" TEXT="-Paseo Cuban Roast Pork Sandwiches"/>
<node CREATED="1388645900784" MODIFIED="1388645900784" TEXT="-Parm Baked Potato Halves"/>
<node CREATED="1388645900785" ID="ID_1924359733" MODIFIED="1388645900785" TEXT="-Spinach and Sausage Pasta"/>
<node CREATED="1388645906361" FOLDED="true" ID="ID_995214588" MODIFIED="1388645950726" TEXT="poultry">
<node CREATED="1388645900784" ID="ID_1198649660" MODIFIED="1388645900784" TEXT="-Chicken, Bacon and Artichoke Pasta with Creamy Garlic Sauce"/>
<node CREATED="1388645900784" ID="ID_741619173" MODIFIED="1388645900784" TEXT="-Slow Cooker Verde Chicken Tostadas"/>
<node CREATED="1388645900784" ID="ID_551042291" MODIFIED="1388645900784" TEXT="-Pesto Orichette with Chicken Sausage"/>
<node CREATED="1388645900784" ID="ID_1947416980" MODIFIED="1388645900784" TEXT="-Chicken Pot Pie Crumble"/>
<node CREATED="1388645900784" ID="ID_556940007" MODIFIED="1388645900784" TEXT="-Buffalo Chicken Meatloaf"/>
</node>
</node>
</node>
<node CREATED="1492221868187" ID="ID_844478776" MODIFIED="1492221870730" POSITION="left" TEXT="NYT">
<node CREATED="1492221878356" ID="ID_499983971" MODIFIED="1492222095676" TEXT="Cauliflower Gratin &#xa;With Goat Cheese Topping">
<node CREATED="1492221885410" LINK="https://cooking.nytimes.com/recipes/1012426-cauliflower-gratin-with-goat-cheese-topping?utm_medium=Social&amp;utm_source=Facebook_Paid&amp;utm_campaign=Recipes&amp;kwp_0=197020&amp;kwp_4=781739&amp;kwp_1=392440&amp;join_cooking_newsletter=true&amp;register=facebook&amp;register=facebook" MODIFIED="1492221885410" TEXT="https://cooking.nytimes.com/recipes/1012426-cauliflower-gratin-with-goat-cheese-topping?utm_medium=Social&amp;utm_source=Facebook_Paid&amp;utm_campaign=Recipes&amp;kwp_0=197020&amp;kwp_4=781739&amp;kwp_1=392440&amp;join_cooking_newsletter=true&amp;register=facebook&amp;register=facebook"/>
<node CREATED="1492221969287" FOLDED="true" ID="ID_368424411" MODIFIED="1492222074844" TEXT="INGREDIENTS">
<node CREATED="1492221969288" MODIFIED="1492221969288" TEXT="1 large or 2 smaller cauliflowers (about 2 pounds), broken into florets"/>
<node CREATED="1492221969289" ID="ID_1499034143" MODIFIED="1492221969289" TEXT="3 tablespoons extra virgin olive oil"/>
<node CREATED="1492221969289" ID="ID_1523739349" MODIFIED="1492221969289" TEXT="Salt"/>
<node CREATED="1492221969289" ID="ID_1025635203" MODIFIED="1492221969289" TEXT="freshly ground pepper"/>
<node CREATED="1492221969290" ID="ID_442526753" MODIFIED="1492221969290" TEXT="6 ounces fresh goat cheese"/>
<node CREATED="1492221969290" ID="ID_682119500" MODIFIED="1492221969290" TEXT="1 plump garlic clove, halved, green shoot removed"/>
<node CREATED="1492221969291" ID="ID_663414450" MODIFIED="1492221969291" TEXT="5 tablespoons low-fat milk"/>
<node CREATED="1492221969291" ID="ID_1298973290" MODIFIED="1492221969291" TEXT="1 teaspoon chopped fresh thyme leaves or 1/2 teaspoon dried thyme"/>
<node CREATED="1492221969291" ID="ID_1601936072" MODIFIED="1492221969291" TEXT="&#xbc; cup dry, fine breadcrumbs"/>
</node>
<node CREATED="1492222048092" FOLDED="true" ID="ID_683562085" MODIFIED="1492222075735" TEXT="PREPARATION">
<node CREATED="1492222048093" MODIFIED="1492222048093" TEXT="Preheat the oven to 450 degrees. Oil a two-quart gratin dish with olive oil."/>
<node CREATED="1492222048096" MODIFIED="1492222048096" TEXT="Place the cauliflower in a steaming basket above one inch of boiling water. Cover and steam for one minute. Lift the lid and allow steam to escape for 15 seconds, then cover again and steam for six to eight minutes, until the cauliflower is tender. Remove from the heat and refresh with cold water. Drain on paper towels, then transfer to the gratin dish."/>
<node CREATED="1492222048101" MODIFIED="1492222048101" TEXT="Season the cauliflower generously with salt and pepper, then toss with 2 tablespoons of the olive oil and half of the thyme. Spread in an even layer."/>
<node CREATED="1492222048104" MODIFIED="1492222048104" TEXT="Place the garlic in a mortar and pestle with a quarter-teaspoon salt, and mash to a paste. Combine with the goat cheese and milk in a food processor fitted with the steel blade, and blend until smooth. Add the remaining thyme and freshly ground pepper to taste, and pulse together. Spread this mixture over the cauliflower in an even layer."/>
<node CREATED="1492222048107" MODIFIED="1492222048107" TEXT="Just before baking, sprinkle on the breadcrumbs and drizzle on the remaining tablespoon of olive oil. Bake 15 to 20 minutes, until the top is lightly browned and the dish is sizzling. Serve at once."/>
</node>
</node>
</node>
</node>
</map>
