<map version="0.8.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1174340699550" ID="Freemind_Link_1224006761" MODIFIED="1174340703517" TEXT="Shipping">
<node CREATED="1174340704606" ID="_" MODIFIED="1174340706683" POSITION="right" TEXT="eBay">
<node CREATED="1174340882316" ID="Freemind_Link_273207673" MODIFIED="1174340884405" TEXT="usps">
<node CREATED="1174340708155" ID="Freemind_Link_1733259754" MODIFIED="1174340770688" TEXT="Cheapest is 7-day parcel post"/>
<node CREATED="1174340776865" ID="Freemind_Link_1595291630" MODIFIED="1174340787241" TEXT="use delivery confirmation $0.50"/>
<node CREATED="1174340871549" ID="Freemind_Link_1070087873" LINK="http://shop.usps.com/webapp/wcs/stores/servlet/ProductCategoryDisplay?catalogId=10152&amp;storeId=10001&amp;categoryId=11823&amp;langId=-1&amp;parent_category_rn=11820&amp;top_category=11820" MODIFIED="1174340879665" TEXT="mail scales for $35"/>
<node CREATED="1174341012264" ID="Freemind_Link_490374261" LINK="https://carrierpickup.usps.com/cgi-bin/WebObjects/CarrierPickup.woa" MODIFIED="1174341019254" TEXT="free carrier pickup"/>
<node CREATED="1174491691821" ID="Freemind_Link_912748843" MODIFIED="1174492134170" TEXT="computer can do parcel post with delivery confirmation"/>
<node CREATED="1175115798386" ID="Freemind_Link_422316212" MODIFIED="1175115812347" TEXT="Return Receipt must also be certified : total $4.64/letter"/>
</node>
<node CREATED="1174341022476" ID="Freemind_Link_395708956" MODIFIED="1174341026515" TEXT="fedex ground">
<node CREATED="1174341027392" ID="Freemind_Link_1079355645" MODIFIED="1174341039312" TEXT="cheaper, but you need phone #, not good for eBay"/>
</node>
</node>
</node>
</map>
