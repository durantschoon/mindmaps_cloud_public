<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1313633915022" ID="Freemind_Link_1492068296" LINK="MetaMap.mm" MODIFIED="1313633921576" TEXT="emacs.mm">
<node CREATED="1313634049305" ID="_" MODIFIED="1313634055397" POSITION="right" TEXT="EmacsWiki">
<node CREATED="1313634047701" LINK="http://www.emacswiki.org/emacs/SiteMap" MODIFIED="1313634047701" TEXT="emacswiki.org &gt; Emacs &gt; SiteMap"/>
<node CREATED="1313634074305" ID="Freemind_Link_1995202759" MODIFIED="1313634076053" TEXT="crovner">
<node CREATED="1313634077598" ID="ID_537465523" LINK="http://www.emacswiki.org/emacs/.emacs-ChristianRovner.el" MODIFIED="1313634077598" TEXT="emacswiki.org &gt; Emacs &gt; .emacs-ChristianRovner"/>
<node CREATED="1336177365780" ID="ID_1214520949" MODIFIED="1511225472352" TEXT="&quot;Now everything is on github.&quot; - cr"/>
</node>
<node CREATED="1511225424670" ID="ID_1991947982" MODIFIED="1511225427610" TEXT="danielmai">
<node CREATED="1511225431852" LINK="https://github.com/danielmai/.emacs.d/blob/master/config.org" MODIFIED="1511225431852" TEXT="https://github.com/danielmai/.emacs.d/blob/master/config.org"/>
</node>
</node>
<node CREATED="1497764155562" HGAP="22" ID="ID_1002892350" MODIFIED="1497764217149" POSITION="left" TEXT="See" VSHIFT="-63">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1497764152578" ID="ID_73017540" LINK="/Users/durantschoon/Documents/tech_computer/emacs_notes.org" MODIFIED="1511210016617" STYLE="bubble" TEXT="emacs_notes.org"/>
</node>
<node CREATED="1500267574999" ID="ID_272884937" MODIFIED="1500267579086" POSITION="left" TEXT="To Do">
<node CREATED="1500267580157" ID="ID_987127781" MODIFIED="1500267593473" TEXT="I should document my influcences">
<node CREATED="1500267594356" ID="ID_1128348117" MODIFIED="1500267597276" TEXT="Daniel Mai">
<node CREATED="1500267609838" ID="ID_112880125" MODIFIED="1500267612544" TEXT="config.org"/>
</node>
<node CREATED="1500267616454" ID="ID_42958040" MODIFIED="1500267636729" TEXT="incorporate next">
<node CREATED="1500267624308" ID="ID_1702931830" MODIFIED="1500267627671" TEXT="definitely"/>
<node CREATED="1500267620349" ID="ID_1528508129" MODIFIED="1500267623083" TEXT="maybe">
<node CREATED="1497757153506" ID="ID_1225577346" MODIFIED="1497757154491" TEXT="Lee Hinman">
<node CREATED="1500267677276" ID="ID_532183337" MODIFIED="1500267683404" TEXT="exploring on">
<node CREATED="1497757095889" ID="ID_411481208" MODIFIED="1500267675228" TEXT="6/17/17"/>
</node>
<node CREATED="1497756910823" ID="ID_330701343" MODIFIED="1497756922578" TEXT="dakrone">
<node CREATED="1497756932383" LINK="https://writequit.org/org/#6017d330-9337-4d97-82f2-2e605b7a262a" MODIFIED="1497756932383" TEXT="https://writequit.org/org/#6017d330-9337-4d97-82f2-2e605b7a262a"/>
<node CREATED="1497756941042" ID="ID_1307299486" MODIFIED="1497756963840" TEXT="My dotfiles setup uses a slightly different setup than most peoples&apos;.  Instead of a repo full of configuration files that people can put into place, my setup consists of a number of org-mode files that contain source code in them. Using Emacs they are then &quot;tangled&quot; to produce specific files.&#xa;"/>
<node CREATED="1497756963842" ID="ID_1135632141" MODIFIED="1497756963842" TEXT="For example, the zsh.org file can be tangled to produce the .zshenv, .zshrc and other ZSH-related configuration files."/>
<node CREATED="1497757086728" LINK="https://github.com/dakrone/eos/blob/master/eos.org" MODIFIED="1497757086728" TEXT="https://github.com/dakrone/eos/blob/master/eos.org"/>
</node>
<node CREATED="1497757185021" ID="ID_1296990307" LINK="https://writequit.org/" MODIFIED="1497757185021" TEXT="https://writequit.org/"/>
</node>
<node CREATED="1500267703190" ID="ID_173805789" MODIFIED="1500267704858" TEXT="youtube">
<node CREATED="1500267712042" ID="ID_151261842" MODIFIED="1500267712607" TEXT="Mike Zamansky">
<node CREATED="1500267721213" ID="ID_259155691" MODIFIED="1500267732510" TEXT="Using Emacs 33 - &#xa;projectile and dumb-jump">
<node CREATED="1500267726856" LINK="https://www.youtube.com/watch?v=wBfZzaff77g" MODIFIED="1500267726856" TEXT="https://www.youtube.com/watch?v=wBfZzaff77g"/>
<node CREATED="1500268074560" ID="ID_1667011449" MODIFIED="1500268075218" TEXT="counsel-projectile "/>
<node CREATED="1500268085748" ID="ID_1950158446" MODIFIED="1500268086571" TEXT="which-key package"/>
<node CREATED="1500268095491" ID="ID_1508432269" MODIFIED="1500268096698" TEXT="dumb-jump"/>
</node>
</node>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1500267567339" ID="ID_1225518531" MODIFIED="1500267572357" POSITION="left" TEXT="interesting"/>
</node>
</map>
