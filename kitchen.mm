<map version="0.8.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1174329428578" ID="Freemind_Link_237240452" MODIFIED="1174329432958" TEXT="kitchen">
<node CREATED="1174329437576" ID="_" MODIFIED="1174329445043" POSITION="right" TEXT="fridge">
<node CREATED="1174329446563" ID="Freemind_Link_1112972312" MODIFIED="1174329453593" TEXT="windex on outside"/>
<node CREATED="1174329460249" ID="Freemind_Link_693060905" MODIFIED="1174329467802" TEXT="baking soda on inside"/>
<node CREATED="1174329469507" ID="Freemind_Link_347192048" MODIFIED="1174329479882" TEXT="periodically vaccuum the motor for best performance"/>
<node CREATED="1174329480612" ID="Freemind_Link_1047338027" MODIFIED="1174329491761" TEXT="use a rubber mallet to break up the ice inside"/>
</node>
<node CREATED="1187476230042" ID="Freemind_Link_1571470554" MODIFIED="1187476240313" POSITION="left" TEXT="drinking glasses">
<node CREATED="1187476247870" ID="Freemind_Link_817458854" MODIFIED="1187476255347" TEXT="4&quot; juice glasses">
<node CREATED="1187476299625" ID="Freemind_Link_1754346953" MODIFIED="1187476305336" TEXT="LIB15239">
<node CREATED="1187476313253" ID="Freemind_Link_1968284188" MODIFIED="1187476316412" TEXT="$55.95">
<node CREATED="1187476317138" ID="Freemind_Link_1218025662" MODIFIED="1187476321191" TEXT="3x12"/>
</node>
</node>
</node>
<node CREATED="1187476256565" ID="Freemind_Link_1974654531" MODIFIED="1187476269555" TEXT="2&quot; small rock glasses">
<node CREATED="1187476299625" ID="Freemind_Link_1164195404" MODIFIED="1187476335069" TEXT="LIB15249">
<node CREATED="1187476313253" ID="Freemind_Link_1757196178" MODIFIED="1187476353107" TEXT="$35.95">
<node CREATED="1187476317138" ID="Freemind_Link_253902821" MODIFIED="1187476321191" TEXT="3x12"/>
</node>
</node>
</node>
<node CREATED="1187476270353" ID="Freemind_Link_1315798365" MODIFIED="1187476285434" TEXT="16 oz milkshake glasses">
<node CREATED="1187476299625" ID="Freemind_Link_269429796" MODIFIED="1187476343130" TEXT="LIB15256">
<node CREATED="1187476313253" ID="Freemind_Link_1554263246" MODIFIED="1187476365540" TEXT="$57.95">
<node CREATED="1187476317138" ID="Freemind_Link_1352344645" MODIFIED="1187476373669" TEXT="2x12"/>
</node>
</node>
</node>
<node CREATED="1187476375865" ID="Freemind_Link_1777536385" MODIFIED="1187476385043" TEXT="prices recorded 8/18/07">
<node CREATED="1187476241130" ID="Freemind_Link_1037645724" MODIFIED="1187476247348" TEXT="charlie&apos;s fixtures prices"/>
<node CREATED="1187476478662" ID="Freemind_Link_1719693559" MODIFIED="1187476483402" TEXT="spoke to Ginger"/>
</node>
<node CREATED="1187476464298" ID="Freemind_Link_79027032" MODIFIED="1187476467925" TEXT="brand Libbey"/>
</node>
<node CREATED="1275162979884" ID="Freemind_Link_378799871" MODIFIED="1275162990846" POSITION="right" TEXT="plastic scraper">
<node CREATED="1275163613873" ID="Freemind_Link_1412707629" LINK="http://www.progressiveintl.com/wheretobuy.asp" MODIFIED="1275163613873" TEXT="progressiveintl.com &gt; Wheretobuy"/>
</node>
</node>
</map>
