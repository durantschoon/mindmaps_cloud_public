<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1480374544671" ID="ID_905901901" LINK="MetaMap.mm" MODIFIED="1480374602704" STYLE="bubble" TEXT="GameTheory.mm">
<node CREATED="1480374596912" ID="ID_776074583" LINK="https://en.wikipedia.org/wiki/Game_theory" MODIFIED="1480374602704" POSITION="right" STYLE="bubble" TEXT="https://en.wikipedia.org/wiki/Game_theory">
<node CREATED="1480375590062" ID="ID_294153295" MODIFIED="1480375598687" STYLE="fork" TEXT="Chainstore paradox">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node CREATED="1480375489686" ID="ID_749830366" LINK="https://en.wikipedia.org/wiki/Chainstore_paradox" MODIFIED="1480375603183" STYLE="bubble" TEXT="https://en.wikipedia.org/wiki/Chainstore_paradox">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
<node CREATED="1480375504775" ID="ID_1603259425" MODIFIED="1480375513736" STYLE="fork" TEXT="Selten&apos;s levels of decision making">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node CREATED="1480375523272" ID="ID_657846874" MODIFIED="1480375523838" TEXT="The routine level">
<node CREATED="1480375529986" ID="ID_569311686" MODIFIED="1480375530633" TEXT="The individuals use their past experience of the results of decisions to guide their response to choices in the present. &quot;The underlying criteria of similarity between decision situations are crude and sometimes inadequate&quot;. (Selten)"/>
</node>
<node CREATED="1480375535738" ID="ID_988042554" MODIFIED="1480375536301" TEXT="The imagination level">
<node CREATED="1480375541673" ID="ID_591862615" MODIFIED="1480375542342" TEXT="The individual tries to visualize how the selection of different alternatives may influence the probable course of future events. This level employs the routine level within the procedural decisions. This method is similar to a computer simulation."/>
</node>
<node CREATED="1480375543191" ID="ID_713408954" MODIFIED="1480375552215" TEXT="The reasoning level">
<node CREATED="1480375557718" ID="ID_73817207" MODIFIED="1480375558683" TEXT="The individual makes a conscious effort to analyze the situation in a rational way, using both past experience and logical thinking. This mode of decision uses simplified models whose assumptions are products of imagination, and is the only method of reasoning permitted and expected by game theory."/>
</node>
</node>
<node CREATED="1480375800066" ID="ID_1942259332" MODIFIED="1480375802028" TEXT="Unexpected hanging paradox">
<node CREATED="1480375790595" ID="ID_1145187588" LINK="https://en.wikipedia.org/wiki/Unexpected_hanging_paradox" MODIFIED="1480375808045" STYLE="bubble" TEXT="https://en.wikipedia.org/wiki/Unexpected_hanging_paradox">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
</node>
</node>
<node CREATED="1480374613435" ID="ID_948290176" MODIFIED="1480374626553" POSITION="right" STYLE="fork" TEXT="KQML">
<node CREATED="1480374618081" ID="ID_1575346957" LINK="https://www.csee.umbc.edu/csee/research/kqml/kqml-applications.html" MODIFIED="1480374633570" STYLE="bubble" TEXT="https://www.csee.umbc.edu/csee/research/kqml/kqml-applications.html"/>
</node>
<node CREATED="1480374646501" ID="ID_1188847357" LINK="https://en.wikipedia.org/wiki/Algorithmic_mechanism_design" MODIFIED="1480374646501" POSITION="right" TEXT="https://en.wikipedia.org/wiki/Algorithmic_mechanism_design"/>
<node CREATED="1480374705288" ID="ID_496087964" MODIFIED="1480374729898" POSITION="right" STYLE="fork" TEXT="CS &amp; Operations research">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node CREATED="1480374694743" ID="ID_323163108" LINK="https://en.wikipedia.org/wiki/Approximation_algorithm" MODIFIED="1480374734367" STYLE="bubble" TEXT="https://en.wikipedia.org/wiki/Approximation_algorithm">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
</node>
</map>
