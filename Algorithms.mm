<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1374184647212" ID="ID_1721947892" LINK="../../Documents/mindmaps/MetaMap.mm" MODIFIED="1374184656982" TEXT="Algorithms">
<node CREATED="1375600003157" ID="ID_835705796" MODIFIED="1375600009500" POSITION="right" TEXT="learning them">
<node CREATED="1375600005825" ID="ID_506789670" LINK="http://www.quora.com/Learning-Algorithms/What-are-the-most-learner-friendly-resources-for-learning-about-algorithms" MODIFIED="1375600005825" TEXT="quora.com &gt; Learning-Algorithms &gt; What-are-the-most-learner-friendly-resources-for-learning-about-algorithms"/>
</node>
<node CREATED="1350157563050" ID="ID_873259826" MODIFIED="1374347525697" POSITION="right" TEXT="top ten algorithms&#xa;of 20th century">
<node CREATED="1350157568128" ID="ID_640069961" LINK="http://www.cs.auckland.ac.nz/~cristian/topten.pdf" MODIFIED="1350157568128" TEXT="cs.auckland.ac.nz &gt; Cristian &gt; Topten"/>
<node CREATED="1374184669855" ID="ID_1797118241" MODIFIED="1374184669855" TEXT="">
<node CREATED="1374184677936" ID="ID_1721413737" MODIFIED="1374184677936" TEXT="1946:">
<node CREATED="1374184687581" ID="ID_552861354" MODIFIED="1374184687581" TEXT="Monte Carlo method"/>
</node>
<node CREATED="1374184693539" ID="ID_1899639135" MODIFIED="1374184693539" TEXT="1947:">
<node CREATED="1374184705873" ID="ID_299527510" MODIFIED="1374184705873" TEXT="simplex method for linear programming."/>
</node>
<node CREATED="1374184714039" ID="ID_999678687" MODIFIED="1374184714039" TEXT="1950:">
<node CREATED="1374184721390" ID="ID_308907764" MODIFIED="1374184728079" TEXT="Krylov subspace iteration methods"/>
</node>
<node CREATED="1374184874486" ID="ID_196895468" MODIFIED="1374184874486" TEXT="1951:">
<node CREATED="1374184881709" ID="ID_1740343260" MODIFIED="1374184888995" TEXT="decompositional approach to matrix computations"/>
</node>
<node CREATED="1374184900521" ID="ID_1711623227" MODIFIED="1374184900521" TEXT="1957:">
<node CREATED="1374184906870" ID="ID_1052114169" MODIFIED="1374184906870" TEXT="Fortran optimizing compiler"/>
</node>
<node CREATED="1374184918820" ID="ID_34786653" MODIFIED="1374184918820" TEXT="1959&#x2013;61:">
<node CREATED="1374184924587" ID="ID_240113978" MODIFIED="1374184931133" TEXT="QR algorithm"/>
</node>
<node CREATED="1374184939055" ID="ID_993674916" MODIFIED="1374184939055" TEXT="1962:">
<node CREATED="1374184953607" ID="ID_1822325498" MODIFIED="1374184956671" TEXT="Quicksort">
<node CREATED="1374184948462" ID="ID_1252888011" MODIFIED="1374184953607" TEXT="Tony Hoare of Elliott Brothers, Ltd., London, presents "/>
</node>
</node>
<node CREATED="1374184988773" ID="ID_1476608524" MODIFIED="1374184992263" TEXT="1965: ">
<node CREATED="1374184998587" ID="ID_1566900193" MODIFIED="1378772192621" TEXT="fast Fourier transform"/>
</node>
<node CREATED="1374185004449" ID="ID_688912454" MODIFIED="1374185004449" TEXT="1977:">
<node CREATED="1374185014031" ID="ID_728135854" MODIFIED="1374185014031" TEXT="integer relation detection algorithm.">
<node CREATED="1374186113565" ID="ID_16223938" MODIFIED="1374186114321" TEXT="PSLQ algorithm"/>
</node>
</node>
<node CREATED="1374185023949" ID="ID_737500311" MODIFIED="1374185023949" TEXT="1987:">
<node CREATED="1374185030108" ID="ID_1387977" MODIFIED="1374185030108" TEXT="fast multipole algorithm."/>
</node>
</node>
</node>
<node CREATED="1382045481027" ID="ID_1682285943" MODIFIED="1382045488129" POSITION="left" TEXT="The Stony Brook Algorithm Repository">
<node CREATED="1382045487080" ID="ID_397916449" LINK="http://www.cs.sunysb.edu/~algorith/major_section/1.1.shtml" MODIFIED="1382045487080" TEXT="cs.sunysb.edu &gt; Algorith &gt; Major section &gt; 1.1"/>
</node>
<node CREATED="1374347872063" ID="ID_1582005454" MODIFIED="1374347873562" POSITION="left" TEXT="sampling">
<node CREATED="1374347875638" ID="ID_1942725883" MODIFIED="1374347876354" TEXT="Probability: sampling a random combination">
<node CREATED="1374347882087" ID="ID_790899026" LINK="https://en.wikipedia.org/wiki/Combination#Probability:_sampling_a_random_combination" MODIFIED="1374347882087" TEXT="https://en.wikipedia.org/wiki/Combination#Probability:_sampling_a_random_combination"/>
<node CREATED="1374351735108" ID="ID_1200703248" MODIFIED="1374351735961" TEXT="One way to select a k-combination efficiently from a population of size n is to iterate across each element of the population, and at each step pick that element with a dynamically changing probability of"/>
<node CREATED="1374351749320" ID="ID_1010987659" MODIFIED="1374351750669" TEXT="over">
<node CREATED="1374351739802" ID="ID_600332251" MODIFIED="1374351748877" TEXT="k - # samples chosen"/>
<node CREATED="1374351739802" ID="ID_819129440" MODIFIED="1374351770049" TEXT="n - # samples visited"/>
</node>
<node CREATED="1374351775667" ID="ID_1444287749" MODIFIED="1374351789725" TEXT="obv break when numerator or denominator = 0"/>
</node>
</node>
</node>
</map>
