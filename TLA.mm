<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1493824745058" ID="ID_370810519" LINK="../../Documents/mindmaps/MetaMap.mm" MODIFIED="1493824771285" TEXT="TLA.mm">
<node CREATED="1493858061900" ID="ID_560402307" MODIFIED="1493858063244" POSITION="right" TEXT="todo">
<node CREATED="1501375473153" ID="ID_277471419" MODIFIED="1501375477013" TEXT="study notes">
<node CREATED="1509313548021" ID="ID_1330330628" LINK="https://github.com/pmer/tlaplus-study-notes" MODIFIED="1509313548021" TEXT="https://github.com/pmer/tlaplus-study-notes"/>
<node CREATED="1501375489034" ID="ID_1704385408" MODIFIED="1501375572397" TEXT="current">
<node CREATED="1501375572682" ID="ID_1720949720" MODIFIED="1501375574914" TEXT="vscode">
<node CREATED="1501375519623" ID="ID_288684300" MODIFIED="1501375537965" TEXT="Add PlusCal code formatting"/>
<node CREATED="1501375538759" ID="ID_1003305129" MODIFIED="1501375543232" TEXT="Add font ligatures"/>
<node CREATED="1501375644921" ID="ID_409147383" MODIFIED="1501375645645" TEXT="vscode language server">
<node CREATED="1501375648897" ID="ID_1800530324" MODIFIED="1501375651852" TEXT="TLA+"/>
</node>
</node>
</node>
<node CREATED="1504185890783" ID="ID_465979320" MODIFIED="1504185892326" TEXT="next">
<node CREATED="1504185899132" LINK="https://devblogs.nvidia.com/parallelforall/thinking-parallel-part-iii-tree-construction-gpu/" MODIFIED="1504185899132" TEXT="https://devblogs.nvidia.com/parallelforall/thinking-parallel-part-iii-tree-construction-gpu/"/>
</node>
</node>
<node CREATED="1493858063785" ID="ID_1534455729" MODIFIED="1501375557837" TEXT="reading">
<node CREATED="1493858066269" ID="ID_194539943" MODIFIED="1493858071616" TEXT="finish/skim">
<node CREATED="1493858071951" ID="ID_777079061" MODIFIED="1493858073784" TEXT="math.pdf"/>
<node CREATED="1494614207278" ID="ID_1601901971" MODIFIED="1494614209899" TEXT="proof.pdf"/>
</node>
<node CREATED="1496607505998" ID="ID_543551403" MODIFIED="1496607507109" TEXT="TLA+ in Practice and Theory"/>
<node CREATED="1493858081699" ID="ID_73948766" MODIFIED="1493858085471" TEXT="LearnTLA.com"/>
</node>
<node CREATED="1497668083680" ID="ID_6407834" MODIFIED="1497668092211" TEXT="learn a little vscode">
<node CREATED="1497668359917" ID="ID_369178909" LINK="https://github.com/Microsoft/vscode-tips-and-tricks" MODIFIED="1497668365285" TEXT="tips and tricks"/>
</node>
</node>
<node CREATED="1493825213243" ID="ID_42971713" MODIFIED="1493825260733" POSITION="right" TEXT="eSpark Learning">
<node CREATED="1493825223545" ID="ID_710438553" MODIFIED="1493825224848" TEXT="Hillel Wayne">
<node CREATED="1493825252963" ID="ID_1729441686" MODIFIED="1493825255262" TEXT="LearnTLA">
<node CREATED="1493825257441" LINK="https://learntla.com/introduction/" MODIFIED="1493825257441" TEXT="https://learntla.com/introduction/"/>
<node CREATED="1493827144086" ID="ID_694980841" MODIFIED="1493827145724" TEXT="Resources">
<node CREATED="1493827216723" ID="ID_490333032" MODIFIED="1493827223010" TEXT="from" VSHIFT="-9">
<node CREATED="1493827217782" LINK="https://learntla.com/introduction/about-this-guide/" MODIFIED="1493827217782" TEXT="https://learntla.com/introduction/about-this-guide/"/>
</node>
<node CREATED="1493827153564" FOLDED="true" ID="ID_1942734354" MODIFIED="1509583805020" TEXT="TLA+ Toolbox">
<node CREATED="1493827159844" ID="ID_1230309455" LINK="http://lamport.azurewebsites.net/tla/toolbox.html#downloading" MODIFIED="1493827159844" TEXT="lamport.azurewebsites.net &gt; Tla &gt; Toolbox"/>
</node>
<node CREATED="1493827166711" FOLDED="true" ID="ID_15936388" MODIFIED="1509583806006" TEXT="The PlusCal Manual">
<node CREATED="1493827176726" ID="ID_1204568485" LINK="https://research.microsoft.com/en-us/um/people/lamport/tla/pluscal.html" MODIFIED="1493827176726" TEXT="https://research.microsoft.com/en-us/um/people/lamport/tla/pluscal.html"/>
</node>
<node CREATED="1493827184049" ID="ID_1523371761" MODIFIED="1493830309133" TEXT="The TLA+ Cheat Sheet">
<node CREATED="1493827190858" ID="ID_262439639" LINK="http://lamport.azurewebsites.net/tla/summary-standalone.pdf" MODIFIED="1493827190858" TEXT="lamport.azurewebsites.net &gt; Tla &gt; Summary-standalone"/>
</node>
<node CREATED="1493827198149" FOLDED="true" ID="ID_208194787" MODIFIED="1509583808176" TEXT="Specifying Systems">
<node CREATED="1493827204140" ID="ID_1062754399" LINK="https://research.microsoft.com/en-us/um/people/lamport/tla/book.html" MODIFIED="1493827204140" TEXT="https://research.microsoft.com/en-us/um/people/lamport/tla/book.html"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1496281371758" ID="ID_334832564" MODIFIED="1496281395161" POSITION="right" TEXT="Dr. TLA+ Series - learn an algorithm and protocol, study a specification">
<node CREATED="1496281359743" ID="ID_1977220773" LINK="https://github.com/tlaplus/DrTLAPlus" MODIFIED="1496281359743" TEXT="https://github.com/tlaplus/DrTLAPlus"/>
</node>
<node CREATED="1496281552057" ID="ID_28529676" MODIFIED="1496281554907" POSITION="right" TEXT="Ron Pressler">
<node CREATED="1496281555454" LINK="https://pron.github.io/posts/correctness-and-complexity" MODIFIED="1496281555454" TEXT="https://pron.github.io/posts/correctness-and-complexity"/>
<node CREATED="1496281635229" ID="ID_736222775" LINK="https://pron.github.io/" MODIFIED="1496281635229" TEXT="https://pron.github.io/"/>
<node CREATED="1509026990506" ID="ID_341007100" LINK="https://www.youtube.com/watch?v=15uy9Ga-14I" MODIFIED="1509027003291" TEXT="Ron Pressler - The Practice and Theory of TLA+"/>
<node CREATED="1509263654966" ID="ID_1867109953" MODIFIED="1509263660336" TEXT="4 part series">
<node CREATED="1509254864222" ID="ID_128005446" LINK="https://pron.github.io/posts/tlaplus_part1" MODIFIED="1509254864222" TEXT="https://pron.github.io/posts/tlaplus_part1">
<node CREATED="1509254856659" FOLDED="true" ID="ID_1249198934" MODIFIED="1509255403437" TEXT="I would like to describe an approach to program analysis which has been popular in programming language theory circles in the last few decades, to which TLA+ stands in stark contrast.">
<node CREATED="1509254884989" ID="ID_813572635" MODIFIED="1509254886344" TEXT="The first is the (partial) identification of computation with functionsPrior to Turing, mathematicians were interested in the question of which functions are calculable &#x2014; most famously Kurt G&#xf6;del and Alonzo Church. The strong influence of Church&#x2019;s work on programming language theory is therefore an underlying cause for this view of algorithms to this day. While some mathematicians like &#xc9;mil Borel and L. E. J. Brouwer spoke of construction of mathematical objects as a sequence (Borel even described a &#x201c;computable real number&#x201d; as a sequence in 1912), Turing was the first to give a precise definition of computation not by the functions it can compute but as the process that yields it, writing: &#x201c;The real question at issue is &#x2018;What are the possible processes which can be carried out in computing&#x2026;?&#x2019;&#x201d;, on which Robin Gandy remarks (The confluence of ideas in 1936, 1988) that &#x201c;[t]his is significantly different from the question &#x2018;What is a computable function?&#x2019; which other authors asked. Turing, so to speak, has pointed himself in the true direction.&#x201d; ; the second is the attempt to unify constructive mathematics &#x2014; a rethinking of mathematical practices originating in part from a strict philosophical view of mathematics &#x2014; with programming"/>
<node CREATED="1509255401284" ID="ID_1946458312" MODIFIED="1509255402238" TEXT="..."/>
<node CREATED="1509255397054" ID="ID_403126904" MODIFIED="1509255398959" TEXT="The aesthetic choice in the design of TLA+ is diametrically opposite: to reason precisely &#x2014; i.e. mathematically, for math is the language of precision &#x2014; about programs, we shouldn&#x2019;t use a formalism designed to express exotic math in a programming language, but quite the opposite, use ordinary math to express and reason about our programs, preferably in a uniform way for all kinds of algorithms; in other words, describe programs in ordinary math."/>
</node>
</node>
<node CREATED="1512191616822" ID="ID_1719878676" MODIFIED="1512191616822" TEXT="">
<node CREATED="1509263678384" ID="ID_1916976776" LINK="https://pron.github.io/posts/tlaplus_part2#logical-formulas-and-expressions" MODIFIED="1512191621047" TEXT="tlaplus_part2#logical-formulas-and-expressions">
<node CREATED="1509264509070" ID="ID_797071987" MODIFIED="1509264510713" TEXT="typo">
<node CREATED="1509264511095" ID="ID_1104808708" MODIFIED="1509264515876" TEXT="&quot;may&quot; -&gt; &quot;many&quot;"/>
<node CREATED="1509264521345" ID="ID_471074836" MODIFIED="1509264522271" TEXT="When proving a theorem, we often need to prove may intermediate steps"/>
</node>
</node>
<node CREATED="1509264637209" ID="ID_593970537" MODIFIED="1512191621049" TEXT="should reread">
<node CREATED="1509264644192" ID="ID_1364833649" MODIFIED="1509264645653" TEXT="First Order Logic and Other Orders"/>
</node>
<node CREATED="1509264907149" ID="ID_1928124457" MODIFIED="1512191621051" TEXT="typo">
<node CREATED="1509264919331" ID="ID_1430576599" MODIFIED="1509264932227" TEXT="&quot;Qunatifiers&quot; -&gt; &quot;Quantifiers&quot;"/>
</node>
</node>
<node CREATED="1512191629652" ID="ID_1949018189" LINK="https://pron.github.io/posts/tlaplus_part2#set-fundamentals" MODIFIED="1512193266746" TEXT="https://pron.github.io/posts/tlaplus_part2#set-fundamentals">
<node CREATED="1512191638805" ID="ID_475080032" MODIFIED="1512191651772" TEXT="typo?">
<node CREATED="1512191690257" ID="ID_664903610" MODIFIED="1512191699162" TEXT="M o n o i d ( M , _ &#x22c5; _ ) &#x225c; &#x2227; S e m i g r o u p ( M , &#x22c5; ) &#x2227; &#x2203; i d &#x2208; M : &#x2200; a &#x2208; M : &#x2227; i d &#x22c5; a = a &#x2227; a &#x22c5; i d = a Identity element "/>
<node CREATED="1512191707664" ID="ID_1829522760" MODIFIED="1512191740904" TEXT="either I don&apos;t understand or the and is unneeded:">
<node CREATED="1512191740905" ID="ID_205383285" MODIFIED="1512191742052" TEXT="&#x2227; i d &#x22c5; a"/>
</node>
<node CREATED="1512191867938" ID="ID_524810760" MODIFIED="1512191886059" TEXT="yes, it looks like it&apos;s left over from the Group definition  "/>
<node CREATED="1512191963615" ID="ID_1547971371" MODIFIED="1512191969925" TEXT="maybe this is intensional"/>
<node CREATED="1512191976010" ID="ID_70260513" MODIFIED="1512191979243" TEXT="is it valid?"/>
</node>
</node>
<node CREATED="1512194826494" ID="ID_38500936" LINK="https://pron.github.io/posts/tlaplus_part2#functions" MODIFIED="1512195448791" TEXT="https://pron.github.io/posts/tlaplus_part2#functions">
<node CREATED="1512194872948" ID="ID_888955687" MODIFIED="1512194881284" TEXT="should Injection be Bijection here?">
<node CREATED="1512194882433" ID="ID_267613306" MODIFIED="1512194883245" TEXT="&#x2200; f : F n ( f ) &#x2227; I n j e c t i o n ( f ) &#x21d2; &#x2227; I n v e r s e ( f ) &#x2219; f = I d e n t i t y ( D O M A I N f ) &#x2227; f &#x2219; I n v e r s e ( f ) = I d e n t i t y ( I m a g e ( f ) ) "/>
</node>
</node>
<node CREATED="1512196309890" ID="ID_960533826" LINK="https://pron.github.io/posts/tlaplus_part2#operators-vs-values" MODIFIED="1512198136823" TEXT="https://pron.github.io/posts/tlaplus_part2#operators-vs-values">
<node CREATED="1512196313213" ID="ID_703488793" MODIFIED="1512196314804" TEXT="typo">
<node CREATED="1512196328731" ID="ID_1161515203" MODIFIED="1512196329514" TEXT="a collection, a class that is cannot be constructed">
<node CREATED="1512196331300" ID="ID_1858317927" MODIFIED="1512196338787" TEXT="remove &quot;is&quot;">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node CREATED="1512198134699" ID="ID_1565173014" LINK="https://pron.github.io/posts/tlaplus_part2#data-refinement-and-inductive-data-types" MODIFIED="1512200372239" TEXT="https://pron.github.io/posts/tlaplus_part2#data-refinement-and-inductive-data-types">
<node CREATED="1512198139491" ID="ID_1377353472" MODIFIED="1512198141341" TEXT="typo">
<node CREATED="1512198216843" ID="ID_1581179332" MODIFIED="1512198217906" TEXT="when we define algorithms over linked list to show">
<node CREATED="1512198218727" ID="ID_693424654" MODIFIED="1512198222522" TEXT="should it be">
<node CREATED="1512198225594" ID="ID_44880994" MODIFIED="1512198228896" TEXT="over linked lists"/>
</node>
</node>
</node>
</node>
<node CREATED="1512200361214" ID="ID_330289527" MODIFIED="1512200366162" TEXT="Ask about the typos on reddit?">
<node CREATED="1512200367494" LINK="https://www.reddit.com/r/tlaplus/comments/6emjp1/tla_in_practice_and_theory_part_2_the_in_tla/" MODIFIED="1512200367494" TEXT="https://www.reddit.com/r/tlaplus/comments/6emjp1/tla_in_practice_and_theory_part_2_the_in_tla/"/>
</node>
<node CREATED="1512200402675" ID="ID_1519050546" LINK="https://pron.github.io/posts/tlaplus_part3" MODIFIED="1513228141212" TEXT="https://pron.github.io/posts/tlaplus_part3">
<node CREATED="1513228143145" ID="ID_154546612" MODIFIED="1513230625197" TEXT="typo">
<node CREATED="1513228154417" ID="ID_83337126" MODIFIED="1513228155107" TEXT="If a behavior  &#x3c3; &#x3c3; does not satisfy  P P, i.e.  &#x3c3; &#x2209; P &#x3c3;&#x2209;P, then property is violated by some finite prefix of the behavior.">
<node CREATED="1513228185214" ID="ID_605192557" MODIFIED="1513228186661" TEXT="?">
<node CREATED="1513228162734" ID="ID_1211122892" MODIFIED="1513228183978" TEXT="property P is violated"/>
<node CREATED="1513228170834" ID="ID_946071876" MODIFIED="1513228175016" TEXT="the property is violated"/>
</node>
</node>
</node>
</node>
<node CREATED="1513459428132" ID="ID_144181738" LINK="https://pron.github.io/posts/tlaplus_part3#machine-closure-and-fairness" MODIFIED="1513459428132" TEXT="https://pron.github.io/posts/tlaplus_part3#machine-closure-and-fairness">
<node CREATED="1513459430885" ID="ID_1348646513" MODIFIED="1513459434728" TEXT="correct?">
<node CREATED="1513459573331" ID="ID_1892345093" MODIFIED="1513459576790" TEXT="&quot;so this specification is not equivalent to any machine machine-closed specification involving  m m and no other variables.&quot;"/>
</node>
</node>
<node CREATED="1513534382754" ID="ID_119615075" LINK="https://pron.github.io/posts/tlaplus_part3#level-of-detail" MODIFIED="1513534384677" TEXT="https://pron.github.io/posts/tlaplus_part3#level-of-detail">
<node CREATED="1513534394365" ID="ID_917017511" MODIFIED="1513534397226" TEXT="&quot;Lemport makes this argument in his paper&quot;"/>
</node>
<node CREATED="1513536309486" ID="ID_953342894" LINK="https://pron.github.io/posts/tlaplus_part3#more-complex-examples" MODIFIED="1513536309486" TEXT="https://pron.github.io/posts/tlaplus_part3#more-complex-examples">
<node CREATED="1513536293414" ID="ID_1242956090" MODIFIED="1513536315630" TEXT="dead link">
<node CREATED="1513536328182" ID="ID_1999981302" MODIFIED="1513536350860" TEXT="For a step-by-step explanation of &#xa;how the specification is written, s&#xa;ee this explanation by Lamport">
<node CREATED="1513536338244" LINK="https://www.youtube.com/watch?v=4nhFqf_46ZQ&amp;feature=youtu.be&amp;t=32m35s" MODIFIED="1513536338244" TEXT="https://www.youtube.com/watch?v=4nhFqf_46ZQ&amp;feature=youtu.be&amp;t=32m35s"/>
</node>
</node>
</node>
<node CREATED="1513538384044" ID="ID_1570697006" LINK="https://pron.github.io/posts/tlaplus_part3#higher-order-computation" MODIFIED="1513538384044" TEXT="https://pron.github.io/posts/tlaplus_part3#higher-order-computation">
<node CREATED="1513538396945" ID="ID_1901595307" MODIFIED="1513538397450" TEXT="&#x201c;Higher-order&#x201d; progeams are first-order"/>
</node>
<node CREATED="1509263675290" ID="ID_305038960" MODIFIED="1513460961648" TEXT="stopped here">
<node CREATED="1512200400506" ID="ID_1943570126" MODIFIED="1512200402100" TEXT="NEXT">
<node CREATED="1513549173179" ID="ID_99693819" LINK="https://pron.github.io/posts/tlaplus_part4" MODIFIED="1513549173179" TEXT="https://pron.github.io/posts/tlaplus_part4"/>
</node>
</node>
</node>
</node>
<node CREATED="1513231462739" ID="ID_176486483" MODIFIED="1513231477042" POSITION="right" TEXT="fairness">
<node CREATED="1513231465476" LINK="http://www.ccs.neu.edu/home/wahl/Publications/fairness.pdf" MODIFIED="1513231465476" TEXT="ccs.neu.edu &gt; Home &gt; Wahl &gt; Publications &gt; Fairness"/>
<node CREATED="1513231980911" ID="ID_1711312060" MODIFIED="1513231983051" TEXT="check out later?">
<node CREATED="1513231988213" LINK="http://www.cs.cmu.edu/~emc/15414-s14/lecture/ModelChecking.pdf" MODIFIED="1513231988213" TEXT="cs.cmu.edu &gt; Emc &gt; 15414-s14 &gt; Lecture &gt; ModelChecking"/>
</node>
</node>
<node CREATED="1509583814256" ID="ID_1197885112" MODIFIED="1509583821183" POSITION="right" TEXT="Leslie Lamport&apos;s MOOC">
<node CREATED="1509583826735" ID="ID_747130557" LINK="http://lamport.azurewebsites.net/video/videos.html" MODIFIED="1513231476055" TEXT="lamport.azurewebsites.net &gt; Video &gt; Videos">
<node CREATED="1509583848014" ID="ID_95801878" MODIFIED="1509583850486" TEXT="1">
<node CREATED="1509583855216" ID="ID_389445315" MODIFIED="1509583856083" TEXT="Introduction to TLA+"/>
</node>
<node CREATED="1509583857140" ID="ID_467543011" MODIFIED="1509583857410" TEXT="2">
<node CREATED="1509584163011" ID="ID_1457724080" MODIFIED="1509584164315" TEXT="State Machines in TLA+ "/>
</node>
<node CREATED="1509583858120" ID="ID_1208373829" MODIFIED="1509583858507" TEXT="3">
<node CREATED="1509584172115" ID="ID_295685030" MODIFIED="1509584172837" TEXT="Resources and Tools"/>
</node>
<node CREATED="1509583859101" ID="ID_531525144" MODIFIED="1509583859429" TEXT="4">
<node CREATED="1509584178596" ID="ID_1338943299" MODIFIED="1509584179354" TEXT="Die Hard"/>
</node>
<node CREATED="1509583860202" ID="ID_1253693684" MODIFIED="1509583860818" TEXT="5">
<node CREATED="1509584181650" ID="ID_2169353" MODIFIED="1509584197930" TEXT="Transaction Commit"/>
</node>
<node CREATED="1509583861311" ID="ID_1495848558" MODIFIED="1509583861849" TEXT="6">
<node CREATED="1509584183727" ID="ID_1178915714" MODIFIED="1509584205364" TEXT="Two-Phase Commit"/>
</node>
<node CREATED="1509583868834" ID="ID_416453740" MODIFIED="1509583869283" TEXT="7">
<node CREATED="1509584185265" ID="ID_1698382152" MODIFIED="1509584212761" TEXT="Paxos Commit"/>
</node>
<node CREATED="1509583869801" ID="ID_806355515" MODIFIED="1509583870233" TEXT="8">
<node CREATED="1509584186991" ID="ID_1412356557" MODIFIED="1509584218158" TEXT="Paxos Commit"/>
</node>
</node>
</node>
<node CREATED="1493824810078" ID="ID_1908960526" MODIFIED="1493824857224" POSITION="right" TEXT="Reflections of an EECS &#xa;professor at SUNY Buffalo">
<node CREATED="1493824831144" ID="ID_705932301" MODIFIED="1493824831847" TEXT="My experience with using TLA+ in distributed systems class">
<node CREATED="1493825541683" ID="ID_1188760409" MODIFIED="1493825545830" TEXT="automated testing">
<node CREATED="1493825549186" ID="ID_489731650" MODIFIED="1493825550446" TEXT="(Next time around, I will also work on devising automated ways to grade the TLA+ projects. My TA had do manual testing of the projects, not a pleasant task for a class of 60 students.)"/>
</node>
<node CREATED="1493825009510" ID="ID_1154359769" MODIFIED="1493825012898" TEXT="declarative style">
<node CREATED="1493824921284" ID="ID_105607607" MODIFIED="1493825042889" TEXT="My code worked, but it used procedures, and it was very procedural/operational rather than declarative--the way Math should be"/>
<node CREATED="1493825038911" ID="ID_761316665" MODIFIED="1493825039476" TEXT="My code become more declarative than operational. And, for message passing, I used Lamport&apos;s trick of using record typed messages which are written just once to a shared message board; it was the acceptors responsibility to react to them (by just reading, not consuming them). My new refactored code translated to ~150 LOC TLA+, whereas my first version had translated to ~800 LOC TLA. And the new version model checked in a minute for 3 acceptors, leading to a couple of magnitudes of order improvement in efficiency. "/>
</node>
<node CREATED="1493825014013" ID="ID_1144925416" MODIFIED="1493825018430" TEXT="labels in PlusCal">
<node CREATED="1493825019332" ID="ID_173785099" MODIFIED="1493825020017" TEXT="The labels in PlusCal are not just aesthetic. They determine the granularity of atomic execution of blocks of code, so it is critical to place the labels right. If you fail to do this right, you will run into deadlocks due to co-dependent await conditions in different blocks of code. Inserting a label reduces the granularity of block of code from atomic to break at the label"/>
</node>
</node>
<node CREATED="1493824840106" LINK="http://muratbuffalo.blogspot.com/2015/01/my-experience-with-using-tla-in.html" MODIFIED="1493824840106" TEXT="muratbuffalo.blogspot.com &gt; 2015 &gt; 01 &gt; My-experience-with-using-tla-in"/>
</node>
<node CREATED="1493857754102" ID="ID_617464843" MODIFIED="1493857756153" POSITION="left" TEXT="Project">
<node CREATED="1493857756559" ID="ID_519199413" MODIFIED="1493857757943" TEXT="backend">
<node CREATED="1493857759245" ID="ID_938753594" MODIFIED="1493857769696" TEXT="tlaplus"/>
<node CREATED="1493857770838" ID="ID_381063861" MODIFIED="1493857775548" TEXT="theorem proving as a service"/>
<node CREATED="1493858278245" ID="ID_381779601" MODIFIED="1493858283254" TEXT="docker"/>
</node>
<node CREATED="1493857776563" ID="ID_1830013543" MODIFIED="1493857778447" TEXT="front end">
<node CREATED="1496603046575" ID="ID_300018795" MODIFIED="1496603048904" TEXT="Yes, now">
<node CREATED="1496603051446" ID="ID_1035466558" MODIFIED="1496603053238" TEXT="vscode"/>
</node>
<node CREATED="1496603037049" ID="ID_1935487489" MODIFIED="1496603043928" TEXT="maybe later">
<node CREATED="1493857851721" ID="ID_1296206232" MODIFIED="1493857854317" TEXT="D3">
<node CREATED="1493857778904" ID="ID_85107828" MODIFIED="1493857787977" TEXT="d3.express">
<node CREATED="1493857789219" ID="ID_594685682" MODIFIED="1493857790179" TEXT="VR?"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1493863601197" ID="ID_1520523671" MODIFIED="1493863603015" POSITION="right" TEXT="Lamport">
<node CREATED="1493863603498" ID="ID_771804411" MODIFIED="1493863606786" TEXT="How to write proofs">
<node CREATED="1493863610133" ID="ID_915147609" LINK="http://lamport.azurewebsites.net/pubs/lamport-how-to-write.pdf" MODIFIED="1493863610133" TEXT="lamport.azurewebsites.net &gt; Pubs &gt; Lamport-how-to-write"/>
</node>
</node>
<node CREATED="1495074927600" ID="ID_1232274424" MODIFIED="1495074930003" POSITION="left" TEXT="GitHub">
<node CREATED="1495074930531" LINK="https://github.com/durantschoon/tla_plus" MODIFIED="1495074930531" TEXT="https://github.com/durantschoon/tla_plus"/>
<node CREATED="1495074950099" LINK="https://github.com/danielmai/tlaplus-study-notes" MODIFIED="1495074950099" TEXT="https://github.com/danielmai/tlaplus-study-notes"/>
</node>
<node CREATED="1495989974339" ID="ID_37748862" MODIFIED="1495989981451" POSITION="left" TEXT="Articles/Blogs">
<node CREATED="1495989988580" ID="ID_1207590815" MODIFIED="1495989989310" TEXT="TLA+ in Practice and Theory">
<node CREATED="1496607517835" ID="ID_995270477" MODIFIED="1496607521289" TEXT="Ron Pressler"/>
<node CREATED="1496607474895" ID="ID_519999299" MODIFIED="1496607480767" TEXT="Part">
<node CREATED="1496607476541" ID="ID_1312969032" MODIFIED="1496607479296" TEXT="1">
<node CREATED="1495989994319" ID="ID_185946392" LINK="https://pron.github.io/posts/tlaplus_part1" MODIFIED="1495989994319" TEXT="https://pron.github.io/posts/tlaplus_part1"/>
</node>
<node CREATED="1496607483289" ID="ID_39769170" MODIFIED="1496607483641" TEXT="2">
<node CREATED="1496607488501" LINK="https://pron.github.io/posts/tlaplus_part2" MODIFIED="1496607488501" TEXT="https://pron.github.io/posts/tlaplus_part2"/>
</node>
</node>
</node>
</node>
</node>
</map>
