<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1495176316347" ID="ID_1676982511" LINK="../../Documents/mindmaps/MetaMap.mm" MODIFIED="1495176337485" TEXT="Haskell.mm">
<node CREATED="1495176378134" ID="ID_1706040098" LINK="https://learnxinyminutes.com/docs/haskell/" MODIFIED="1495176874607" POSITION="right" TEXT="https://learnxinyminutes.com/docs/haskell/"/>
<node CREATED="1495180570575" ID="ID_1161768373" MODIFIED="1495180573911" POSITION="right" TEXT="">
<node CREATED="1495176359230" ID="ID_157347935" LINK="http://learnyouahaskell.com/" MODIFIED="1495180575695" TEXT="learnyouahaskell.com"/>
<node CREATED="1495180572678" ID="ID_1776748911" LINK="http://book.realworldhaskell.org/" MODIFIED="1495180572678" TEXT="book.realworldhaskell.org"/>
</node>
<node CREATED="1495176345948" ID="ID_1504500109" LINK="https://medium.com/@sjsyrek/some-notes-on-haskell-pedagogy-de43281b1a5c" MODIFIED="1495176345948" POSITION="right" TEXT="https://medium.com/@sjsyrek/some-notes-on-haskell-pedagogy-de43281b1a5c"/>
<node CREATED="1495179746391" ID="ID_556455762" MODIFIED="1495179752857" POSITION="left" TEXT="Folds">
<node CREATED="1511225894469" HGAP="21" ID="ID_1832959213" MODIFIED="1511225900928" TEXT="C Olah" VSHIFT="-20">
<node CREATED="1511225898199" LINK="http://colah.github.io/posts/2015-02-DataList-Illustrated/" MODIFIED="1511225898199" TEXT="colah.github.io &gt; Posts &gt; 2015-02-DataList-Illustrated"/>
</node>
<node CREATED="1495179321967" ID="ID_451044841" LINK="https://en.wikipedia.org/wiki/Fold_" MODIFIED="1495179321967" TEXT="https://en.wikipedia.org/wiki/Fold_(higher-order_function)"/>
<node CREATED="1495179755595" ID="ID_265544067" LINK="https://www.quora.com/When-is-it-good-to-use-foldr-in-Haskell" MODIFIED="1495179755595" TEXT="https://www.quora.com/When-is-it-good-to-use-foldr-in-Haskell">
<node CREATED="1495179819761" ID="ID_73312922" MODIFIED="1495179827049" TEXT="foldl&apos; vs foldl"/>
</node>
<node CREATED="1495179911662" ID="ID_595995220" LINK="https://wiki.haskell.org/Fold" MODIFIED="1495179911662" TEXT="https://wiki.haskell.org/Fold"/>
</node>
</node>
</map>
