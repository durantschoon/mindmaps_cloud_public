<map version="0.8.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1173739013995" ID="Freemind_Link_1478397438" MODIFIED="1173739502918" TEXT="Technology Transfer">
<node COLOR="#669900" CREATED="1173739039979" ID="Freemind_Link_1982829364" MODIFIED="1173739053734" POSITION="right" TEXT="Independent Variables">
<node CREATED="1173739071049" ID="Freemind_Link_1472058384" MODIFIED="1173806695073" TEXT="community wants it"/>
<node CREATED="1173739352937" ID="Freemind_Link_1305714715" MODIFIED="1173739363278" TEXT="motivation">
<icon BUILTIN="messagebox_warning"/>
<node CREATED="1173739378820" ID="Freemind_Link_1123080095" MODIFIED="1173806721471" TEXT="Durant contends that motivation is irrelevant. It cannot be measured and what we&apos;d want to measure  is something else. Maybe &quot;track-record&quot;">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1173739264173" ID="Freemind_Link_539497982" MODIFIED="1173806737822" TEXT="non-intrinsic reasons why an insider would take a technology (eg. increase in status)"/>
<node CREATED="1173739179478" ID="Freemind_Link_933616626" MODIFIED="1173739188316" TEXT="full disclosure"/>
<node CREATED="1173739079316" ID="Freemind_Link_1369787661" MODIFIED="1173739144528" TEXT="monoculture (continuous variable)"/>
<node CREATED="1173739145587" ID="Freemind_Link_409886929" MODIFIED="1173739150158" TEXT="dependency"/>
</node>
<node COLOR="#669900" CREATED="1173739030850" ID="_" MODIFIED="1173739037985" POSITION="left" TEXT="Dependent Variables">
<node CREATED="1173739216307" ID="Freemind_Link_1582376924" MODIFIED="1173739218718" TEXT="morality"/>
<node CREATED="1173739224285" ID="Freemind_Link_535921577" MODIFIED="1173739227215" TEXT="good for everyone"/>
</node>
</node>
</map>
