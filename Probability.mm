<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1480635380688" ID="ID_1726806697" LINK="MetaMap.mm" MODIFIED="1480635397038" TEXT="Probability.mm">
<node CREATED="1480635399362" ID="ID_1296409957" LINK="https://en.wikipedia.org/wiki/Graphical_model" MODIFIED="1480635635535" POSITION="right" STYLE="bubble" TEXT="https://en.wikipedia.org/wiki/Graphical_model">
<node CREATED="1480635412109" LINK="https://en.wikipedia.org/wiki/Structural_equation_modeling" MODIFIED="1480635635535" TEXT="https://en.wikipedia.org/wiki/Structural_equation_modeling"/>
<node CREATED="1480635419033" LINK="https://en.wikipedia.org/wiki/Restricted_Boltzmann_machine" MODIFIED="1480635635536" TEXT="https://en.wikipedia.org/wiki/Restricted_Boltzmann_machine"/>
</node>
<node CREATED="1480635534902" ID="ID_1419655977" LINK="https://en.wikipedia.org/wiki/Judea_Pearl" MODIFIED="1480635637788" POSITION="right" STYLE="bubble" TEXT="https://en.wikipedia.org/wiki/Judea_Pearl">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node CREATED="1480635545246" LINK="https://en.wikipedia.org/wiki/Bayesian_network" MODIFIED="1480635637784" TEXT="https://en.wikipedia.org/wiki/Bayesian_network"/>
<node CREATED="1480635550646" LINK="https://en.wikipedia.org/wiki/Belief_propagation" MODIFIED="1480635637785" TEXT="https://en.wikipedia.org/wiki/Belief_propagation"/>
</node>
<node CREATED="1480635565239" ID="ID_644453259" LINK="https://en.wikipedia.org/wiki/Markov_random_field" MODIFIED="1480635638261" POSITION="right" STYLE="bubble" TEXT="https://en.wikipedia.org/wiki/Markov_random_field">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
<node CREATED="1511393907562" ID="ID_1285875298" MODIFIED="1511393914109" POSITION="left" TEXT="Add probability to TLA+?">
<node CREATED="1511393914957" LINK="http://probcomp.csail.mit.edu/papers/lua-thesis-2016.pdf" MODIFIED="1511393914957" TEXT="probcomp.csail.mit.edu &gt; Papers &gt; Lua-thesis-2016"/>
</node>
</node>
</map>
