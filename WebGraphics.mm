<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1496185954406" ID="ID_828887314" LINK="../../Documents/mindmaps/MetaMap.mm" MODIFIED="1496185974781" TEXT="WebGraphics.mm">
<node CREATED="1496185988427" ID="ID_37988478" MODIFIED="1496185991403" POSITION="right" TEXT="regl">
<node CREATED="1496186009848" ID="ID_64944023" LINK="http://regl.party/" MODIFIED="1496186011880" TEXT="regl.party"/>
<node CREATED="1496186003769" ID="ID_1860725095" LINK="https://github.com/regl-project/regl" MODIFIED="1496186003769" TEXT="https://github.com/regl-project/regl"/>
<node CREATED="1496185981073" ID="ID_1776672660" MODIFIED="1496186012969" TEXT="Beautifully Animate Points with WebGL and regl">
<node CREATED="1496185985690" LINK="http://peterbeshai.com/beautifully-animate-points-with-webgl-and-regl.html" MODIFIED="1496185985690" TEXT="peterbeshai.com &gt; Beautifully-animate-points-with-webgl-and-regl"/>
</node>
</node>
<node CREATED="1496186117671" ID="ID_467294995" MODIFIED="1496186137663" POSITION="right" TEXT="glslify">
<node CREATED="1496186195826" ID="ID_14865389" MODIFIED="1496186197160" TEXT="A node.js-style module system for GLSL!"/>
<node CREATED="1496186121138" LINK="https://github.com/stackgl/glslify" MODIFIED="1496186121138" TEXT="https://github.com/stackgl/glslify"/>
<node CREATED="1496186153982" ID="ID_1725699592" MODIFIED="1496186154590" TEXT="GLSL">
<node CREATED="1496186161273" ID="ID_1214485187" MODIFIED="1496186161743" TEXT="GLSL (GLslang) is a short term for the official OpenGL Shading Language. GLSL is a C/C++ similar high level programming language for several parts of the graphic card. With GLSL you can code (right up to) short programs, called shaders, which are executed on the GPU."/>
<node CREATED="1496186182930" LINK="https://en.wikipedia.org/wiki/OpenGL_Shading_Language" MODIFIED="1496186182930" TEXT="https://en.wikipedia.org/wiki/OpenGL_Shading_Language"/>
</node>
</node>
</node>
</map>
