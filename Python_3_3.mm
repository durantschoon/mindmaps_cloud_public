<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1378077881411" ID="ID_1177809933" LINK="../../Documents/mindmaps/MetaMap.mm" MODIFIED="1378078449558" TEXT="Python Language&#xa;Version 3.3">
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node CREATED="1378079757635" HGAP="32" ID="ID_1637077322" MODIFIED="1378079784782" POSITION="left" TEXT="compare to" VSHIFT="-219">
<node CREATED="1378079760801" ID="ID_921158681" LINK="http://freemind.sourceforge.net/wiki/extensions/freemind/flashwindow.php?startCollapsedToLevel=4&amp;initLoadFile=/wiki/images/1/1a/Python_WebLinks.mm&amp;mm_title=Python%202.5%20Computer%20Language%20-%20Contents" MODIFIED="1378079760801" TEXT="freemind.sourceforge.net &gt; Wiki &gt; Extensions &gt; Freemind &gt; Flashwindow.php?startCollapsedToLevel=4&amp;initLoadFile= &gt; Wiki &gt; Images &gt; 1 &gt; 1a &gt; Python WebLinks.mm&amp;mm title=Python 2"/>
</node>
<node BACKGROUND_COLOR="#ffff66" CREATED="1378078311501" ID="ID_597249686" MODIFIED="1378078449557" POSITION="left" STYLE="bubble" TEXT="Sources">
<cloud/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378078037637" ID="ID_1438412411" LINK="http://python.org/download/releases/3.3.2/" MODIFIED="1378079345864" STYLE="bubble" TEXT="python.org &gt; Download &gt; Releases &gt; 3.3.2">
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378080965488" ID="ID_501000438" LINK="http://docs.python.org/3/index.html" MODIFIED="1378080968351" STYLE="bubble" TEXT="docs.python.org &gt; 3 &gt; Index">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1379802330874" ID="ID_358959351" MODIFIED="1379802344241" STYLE="bubble" TEXT="scoping">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1379802340597" ID="ID_396447389" LINK="http://en.wikipedia.org/wiki/Scope_" MODIFIED="1379802347781" STYLE="bubble" TEXT="en.wikipedia.org &gt; Wiki &gt; Scope (programming)#Python">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1379802373074" ID="ID_178752876" MODIFIED="1379802382860" STYLE="bubble" TEXT="LEGB rule (Local, Enclosing, Global, Built-in)">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1379802446826" FOLDED="true" ID="ID_1789742511" MODIFIED="1379802495473" STYLE="bubble" TEXT="global, nonlocal">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1379802437826" ID="ID_1937743839" MODIFIED="1379802459523" STYLE="bubble" TEXT="Both these rules can be overridden with a global or nonlocal (in Python 3) declaration prior to use, which allows accessing global variables even if there is an intervening nonlocal variable, and assigning to global or nonlocal variables.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
</node>
</node>
</node>
<node BACKGROUND_COLOR="#ffff66" CREATED="1378078311501" ID="ID_1330663188" MODIFIED="1378078957959" POSITION="left" STYLE="bubble" TEXT="Built-in Data Types&#xa;">
<cloud/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378079008619" ID="ID_1067052396" MODIFIED="1378079350482" STYLE="bubble" TEXT="Sequence Type">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378079019860" ID="ID_731063620" MODIFIED="1378079096508" TEXT="Immutable">
<node BACKGROUND_COLOR="#ccccff" CREATED="1378079024284" ID="ID_195864611" LINK="../../Public/duro_public_mindmaps/Python_3_3_strings.mm" MODIFIED="1378080082602" TEXT="Strings"/>
</node>
</node>
</node>
</node>
</map>
