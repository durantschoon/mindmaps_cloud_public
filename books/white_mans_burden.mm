<map version="0.8.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1173931666432" ID="Freemind_Link_1824326967" MODIFIED="1173931775399" TEXT="&lt;html&gt;&lt;img src=&quot;file:/Users/durantschoon/Documents/mindmaps/images/The White Man&apos;s Burden.gif&quot;&gt;">
<node COLOR="#669900" CREATED="1173931929248" ID="Freemind_Link_1802241139" MODIFIED="1173931933237" POSITION="right" TEXT="Parts">
<node COLOR="#999999" CREATED="1173931796060" ID="_" MODIFIED="1173932139088" TEXT="Chapter 1">
<arrowlink DESTINATION="_" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Freemind_Arrow_Link_765746535" STARTARROW="None" STARTINCLINATION="0;0;"/>
<arrowlink DESTINATION="_" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Freemind_Arrow_Link_1932621164" STARTARROW="None" STARTINCLINATION="0;0;"/>
<font NAME="SansSerif" SIZE="10"/>
<node CREATED="1173932100595" ID="Freemind_Link_1855110082" MODIFIED="1173932105784" TEXT="Planners vs. Searchers"/>
</node>
<node COLOR="#999999" CREATED="1173932108792" ID="Freemind_Link_105659567" MODIFIED="1173932323866" TEXT="Part I">
<edge COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="10"/>
<node CREATED="1173932196345" ID="Freemind_Link_1389368294" MODIFIED="1173932502631" TEXT="WHY PLANNERS CANNOT BRING PROSPERITY">
<node COLOR="#999999" CREATED="1173932112441" ID="Freemind_Link_1962022344" MODIFIED="1173932150200" TEXT="Chapter 2">
<edge COLOR="#808080" WIDTH="thin"/>
<arrowlink DESTINATION="Freemind_Link_1962022344" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Freemind_Arrow_Link_1003814399" STARTARROW="None" STARTINCLINATION="0;0;"/>
<font NAME="SansSerif" SIZE="10"/>
<node CREATED="1173932117030" ID="Freemind_Link_497691956" MODIFIED="1173932130520" TEXT="The Legend of the Big Push"/>
</node>
<node COLOR="#999999" CREATED="1173932155428" ID="Freemind_Link_827242646" MODIFIED="1173932178721" TEXT="Chapter 3">
<edge COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="10"/>
<node CREATED="1173932247721" FOLDED="true" ID="Freemind_Link_1385639589" MODIFIED="1173932259996" TEXT="You Can&apos;t Plan a Market">
<node CREATED="1181066835870" ID="Freemind_Link_378841035" MODIFIED="1181067839733" TEXT="p.60 Paradox - Free markets work, but free market reforms don&apos;t."/>
<node CREATED="1181066866814" ID="Freemind_Link_1614111975" MODIFIED="1181067854060" TEXT="p. 62 Don&apos;t start from scratch, start from what you already have."/>
<node CREATED="1181066972732" ID="Freemind_Link_1154646956" MODIFIED="1181067883801" TEXT="p. 75 Any voluntary exchange makes both parties better off, &#xa;although not necessarily to the same degree."/>
<node CREATED="1181067590794" ID="Freemind_Link_492973654" MODIFIED="1181067605718" TEXT="Better Business Bureau and warranties are more availble in rich countries"/>
<node CREATED="1181067775921" ID="Freemind_Link_1374506136" MODIFIED="1181067806654" TEXT="p.83 Examples of ethnic specializations leading to domination of economic sectors"/>
<node CREATED="1181151853226" ID="Freemind_Link_1058931479" MODIFIED="1181152128450" TEXT="p.93 Tragedy of the Commons&#xa;Analysis: Commons regimes work until the benefit of defecting from the community becomes sufficiently large."/>
</node>
</node>
<node COLOR="#999999" CREATED="1173932159896" ID="Freemind_Link_633000268" MODIFIED="1173932177656" TEXT="Chapter 4">
<edge COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="10"/>
<node CREATED="1173932261795" ID="Freemind_Link_1551643674" MODIFIED="1173932267036" TEXT="Planners and Gangsters"/>
</node>
</node>
</node>
<node COLOR="#999999" CREATED="1173932108792" ID="Freemind_Link_1642328766" MODIFIED="1173932327776" TEXT="Part II">
<edge COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="10"/>
<node CREATED="1173932196345" ID="Freemind_Link_128528162" MODIFIED="1173932315204" TEXT="ACTING OUT THE BURDEN">
<arrowlink DESTINATION="Freemind_Link_128528162" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Freemind_Arrow_Link_345619734" STARTARROW="None" STARTINCLINATION="0;0;"/>
<node COLOR="#999999" CREATED="1173932112441" ID="Freemind_Link_1422401781" MODIFIED="1173932336374" TEXT="Chapter 5">
<edge COLOR="#808080" WIDTH="thin"/>
<arrowlink DESTINATION="Freemind_Link_1422401781" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Freemind_Arrow_Link_471203409" STARTARROW="None" STARTINCLINATION="0;0;"/>
<font NAME="SansSerif" SIZE="10"/>
<node CREATED="1173932117030" FOLDED="true" ID="Freemind_Link_1435655533" MODIFIED="1173932402144" TEXT="The Rich Have Markets, The Poor have Bureaucrats">
<arrowlink DESTINATION="Freemind_Link_1435655533" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Freemind_Arrow_Link_1177741157" STARTARROW="None" STARTINCLINATION="0;0;"/>
<node CREATED="1181153403471" ID="Freemind_Link_217476077" MODIFIED="1181153495051" TEXT="p.116&#xa;Impartial courts/police:&#xa; 1) enforce contracts&#xa; 2) protect private property&#xa; 3) provide security against predators&#xa; 4) punish law breakers "/>
<node CREATED="1181784356841" ID="Freemind_Link_520384651" MODIFIED="1181784374383" TEXT="p.117&#xa;in a democracy everyone watches the watchers"/>
<node CREATED="1181784387986" ID="Freemind_Link_1188045377" MODIFIED="1181784413697" TEXT="p.119&#xa;democracy includes individual rights and freedom&#xa;to dissent as well as majority rule"/>
<node CREATED="1181784437508" ID="Freemind_Link_1567491742" MODIFIED="1181784458905" TEXT="p.120&#xa;the paradox of democracy: that the majority can vote out democracy"/>
<node CREATED="1181784462984" ID="Freemind_Link_1421117796" MODIFIED="1181784485619" TEXT="p.120&#xa;intimidation of voters is common in many poor countries"/>
<node CREATED="1181784501972" ID="Freemind_Link_1667433590" MODIFIED="1181784527861" TEXT="p.120&#xa;Harvard study: executive powers higher with ethnic heterogeneity"/>
<node CREATED="1181784582073" ID="Freemind_Link_1323754234" MODIFIED="1181784892392" TEXT="p.121&#xa;Daren Acemoglu (MIT) compares oligarchy vs democracy w.r.t. economic growth&#xa;Oligarchies protect elite interests which can securely invest in business w/o the &#xa;&quot;democratic threat to property rights&quot;. However, democracies are better for fostering&#xa;Searchers. Acemoglu contrasts Carribean oligarchs with New England democracy in&#xa;18th and 19th centuries (oligarchs stayed in sugar which eventually failed, New&#xa;Englanders were open to new tech from the industrial revolution)."/>
<node CREATED="1181785615557" ID="Freemind_Link_277111178" MODIFIED="1181785804771" TEXT="p.122&#xa;Acemoglu (MIT) &amp; Robinson (Harvard) &#xa;Economic Origins of Democracy and Dictatorship">
<node CREATED="1181785813081" LINK="http://tinyurl.com/2toh37" MODIFIED="1181785813081" TEXT="tinyurl.com &gt; 2toh37"/>
</node>
<node CREATED="1181784956560" ID="Freemind_Link_1284841400" MODIFIED="1181785014498" TEXT="p.124-125&#xa;natural resources determine size of middle class&#xa;Profitable sugar lead to elite control, while wheat of small time farmers lead to a more equitable society. "/>
<node CREATED="1181786204180" ID="Freemind_Link_923095588" MODIFIED="1181786251429" TEXT="p.128&#xa;differing ethnic groups with differing interests can cause the breakdown of democracy and lead to reduced services"/>
<node CREATED="1181786270963" ID="Freemind_Link_632484710" MODIFIED="1181786363115" TEXT="p.129&#xa;Vulnerabilities of democracy:&#xa;&#x9;* elite manipulation ofpolitics&#xa;&#x9;* weak social norms&#xa;&#x9;* landed wealth&#xa;&#x9;* national resources&#xa;&#x9;* high inequality&#xa;&#x9;* corruption&#xa;&#x9;* ethnic nationalism and hatreds"/>
<node CREATED="1181788215658" ID="Freemind_Link_1963414325" MODIFIED="1181788218405" TEXT="Poverty Reduction Strategy Paper">
<node CREATED="1181788224793" LINK="http://en.wikipedia.org/wiki/Poverty_Reduction_Strategy_Paper" MODIFIED="1181788224793" TEXT="en.wikipedia.org &gt; Wiki &gt; Poverty Reduction Strategy Paper"/>
<node CREATED="1181788227502" ID="Freemind_Link_149412731" MODIFIED="1181788260413" TEXT="part of: structural adjustments of world bank and imf"/>
</node>
<node CREATED="1182021874869" ID="Freemind_Link_934771758" MODIFIED="1182021909444" TEXT="p.171&#xa;multiple principles weaken accountability">
<node CREATED="1182021884675" LINK="http://en.wikipedia.org/wiki/Principal-agent_problem" MODIFIED="1182021884675" TEXT="en.wikipedia.org &gt; Wiki &gt; Principal-agent problem"/>
</node>
<node CREATED="1182022400135" ID="Freemind_Link_1988154456" MODIFIED="1182022450034" TEXT="p.179&#xa;Factors of success for SEARCHERS&#xa; * measurable goals&#xa; * link from efforts to results&#xa; * fewer objectives&#xa; * particular solvable problems"/>
<node CREATED="1182022457621" ID="Freemind_Link_599661204" MODIFIED="1182022486860" TEXT="p.180&#xa;Testability &#xa; * moduiar/independent&#xa; * visible&#xa; * individual accountability"/>
<node CREATED="1182044968433" ID="Freemind_Link_1595681227" MODIFIED="1182045003177" TEXT="p.191 Critique of aid agencies&#xa;Many agencies trying to do everything does not work"/>
</node>
</node>
<node COLOR="#999999" CREATED="1173932155428" ID="Freemind_Link_1243497782" MODIFIED="1173932343950" TEXT="Chapter 6">
<edge COLOR="#808080" WIDTH="thin"/>
<arrowlink DESTINATION="Freemind_Link_1243497782" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Freemind_Arrow_Link_520027230" STARTARROW="None" STARTINCLINATION="0;0;"/>
<font NAME="SansSerif" SIZE="10"/>
<node CREATED="1173932247721" ID="Freemind_Link_786926271" MODIFIED="1173932824213" TEXT="Bailing Out the Poor">
<node CREATED="1182045562272" ID="Freemind_Link_1382702874" MODIFIED="1182045595698" TEXT="p.227&#xa;? Debt cancellation as a moral hazard&#xa;=&gt; grants are better than loans"/>
</node>
</node>
<node COLOR="#999999" CREATED="1173932159896" ID="Freemind_Link_1692531271" MODIFIED="1173932347989" TEXT="Chapter 7">
<edge COLOR="#808080" WIDTH="thin"/>
<arrowlink DESTINATION="Freemind_Link_1692531271" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Freemind_Arrow_Link_377243717" STARTARROW="None" STARTINCLINATION="0;0;"/>
<font NAME="SansSerif" SIZE="10"/>
<node CREATED="1173932261795" FOLDED="true" ID="Freemind_Link_907946625" MODIFIED="1173932481208" TEXT="The Healers: Triumph and Tragedy">
<arrowlink DESTINATION="Freemind_Link_907946625" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Freemind_Arrow_Link_571937331" STARTARROW="None" STARTINCLINATION="0;0;"/>
<node CREATED="1182045614392" ID="Freemind_Link_1637128055" MODIFIED="1182046146571" TEXT="p.241&#xa;Success: polio and river blindness&#xa;Post 1985: region program to eliminate polo in Latin America now successful&#xa;Program started in 1974 to eliminate river blindness in Western Africa now successful&#xa;&#xa;In 1996, 7 southern African countries administer routine vaccination for measles&#xa;In 2000, they all had all eliminated measles&#xa;Egypt: oral rehydration therapy from 1982 to 1989 cut deaths from diarrhea by 82% that period&#xa;Leading preventable cause of blindness, trachoma, cut by 90% in children &lt; 10 in Morroco since 1997&#xa;by promoting surgery, antibiotics, face washing, and environmental cleanliness&#xa;Sri Lanka maternal mortality &#xa;China tuburculosis"/>
<node CREATED="1182046150942" ID="Freemind_Link_1756287907" MODIFIED="1182046164002" TEXT="p.243&#xa;Massive AIDS failure"/>
<node CREATED="1182046194484" ID="Freemind_Link_781956797" MODIFIED="1182046215479" TEXT="p.256&#xa;&quot;New money&quot; still needs to be prioritized"/>
<node CREATED="1182046217113" ID="Freemind_Link_948925918" MODIFIED="1182046260439" TEXT="p.257&#xa;Educating prostitutes saves lives 50 to 1000x per $&#xa;But social stigma (here) against"/>
<node CREATED="1182046277523" ID="Freemind_Link_876357639" MODIFIED="1182046328238" TEXT="p.258&#xa;The question is not:&#xa;&quot;Do they deserve to die?&quot;&#xa;but rather&#xa;&quot;Do we deserve to decide who dies?&quot;"/>
</node>
</node>
</node>
</node>
<node COLOR="#999999" CREATED="1173932108792" ID="Freemind_Link_928151175" MODIFIED="1173932570882" TEXT="Part III">
<edge COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="10"/>
<node CREATED="1173932196345" ID="Freemind_Link_1593567980" MODIFIED="1173933303905" TEXT="THE WHITE MAN&apos;S ARMY">
<arrowlink DESTINATION="Freemind_Link_1593567980" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Freemind_Arrow_Link_230524651" STARTARROW="None" STARTINCLINATION="0;0;"/>
<node COLOR="#999999" CREATED="1173932112441" ID="Freemind_Link_62932230" MODIFIED="1173932674949" TEXT="Chapter 8">
<edge COLOR="#808080" WIDTH="thin"/>
<arrowlink DESTINATION="Freemind_Link_62932230" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Freemind_Arrow_Link_291719937" STARTARROW="None" STARTINCLINATION="0;0;"/>
<font NAME="SansSerif" SIZE="10"/>
<node CREATED="1173932117030" FOLDED="true" ID="Freemind_Link_1516454196" MODIFIED="1173932802340" TEXT="From Colonialism to Postmodern Imperialism">
<arrowlink DESTINATION="Freemind_Link_1516454196" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Freemind_Arrow_Link_668961613" STARTARROW="None" STARTINCLINATION="0;0;"/>
<node CREATED="1182046781987" MODIFIED="1182046781987" TEXT="p.275 Europeans have increased amount of despotism in Africa through destability"/>
<node CREATED="1182046781988" ID="Freemind_Link_458874756" MODIFIED="1182046798757" TEXT="p.278 Seierra Leone - Country for freed slaves. &#xa;Examples of British charities raising money to help them &#xa;(well, baptize them, really) in the 1800&apos;s."/>
<node CREATED="1182046781991" MODIFIED="1182046781991" TEXT="p.281forced to grow cotton instead of high yield crops, also taxation backed by force"/>
<node CREATED="1182046781992" MODIFIED="1182046781992" TEXT="p.282 British farming doesn&apos;t work everywhere"/>
<node CREATED="1182046781992" MODIFIED="1182046781992" TEXT="p.286 History of colonial Congo &quot;Abused the Most and Longest&quot;. &quot;In 1482 ...&quot;"/>
<node CREATED="1182046781994" MODIFIED="1182046781994" TEXT="p.306 Patrick Awuah - Microsoftie Ghana"/>
</node>
</node>
<node COLOR="#999999" CREATED="1173932155428" ID="Freemind_Link_1719585496" MODIFIED="1173932736082" TEXT="Chapter 9">
<edge COLOR="#808080" WIDTH="thin"/>
<arrowlink DESTINATION="Freemind_Link_1719585496" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Freemind_Arrow_Link_295053533" STARTARROW="None" STARTINCLINATION="0;0;"/>
<arrowlink DESTINATION="Freemind_Link_1719585496" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Freemind_Arrow_Link_1979485286" STARTARROW="None" STARTINCLINATION="0;0;"/>
<font NAME="SansSerif" SIZE="10"/>
<node CREATED="1173932247721" ID="Freemind_Link_1586949061" MODIFIED="1173932818834" TEXT="Invading the Poor">
<arrowlink DESTINATION="Freemind_Link_1586949061" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Freemind_Arrow_Link_1986416322" STARTARROW="None" STARTINCLINATION="0;0;"/>
</node>
</node>
</node>
</node>
<node COLOR="#999999" CREATED="1173932108792" ID="Freemind_Link_322849224" MODIFIED="1173932646847" TEXT="Part IV">
<edge COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="10"/>
<node CREATED="1173932196345" ID="Freemind_Link_1110596051" MODIFIED="1173933318821" TEXT="THE FUTURE">
<arrowlink DESTINATION="Freemind_Link_1110596051" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Freemind_Arrow_Link_961280002" STARTARROW="None" STARTINCLINATION="0;0;"/>
<node COLOR="#999999" CREATED="1173932112441" ID="Freemind_Link_60583853" MODIFIED="1173932771439" TEXT="Chapter 10">
<edge COLOR="#808080" WIDTH="thin"/>
<arrowlink DESTINATION="Freemind_Link_60583853" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Freemind_Arrow_Link_1312197565" STARTARROW="None" STARTINCLINATION="0;0;"/>
<arrowlink DESTINATION="Freemind_Link_60583853" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Freemind_Arrow_Link_1452205522" STARTARROW="None" STARTINCLINATION="0;0;"/>
<font NAME="SansSerif" SIZE="10"/>
<node CREATED="1173932117030" ID="Freemind_Link_1964911716" MODIFIED="1173932845428" TEXT="Homegrown Development">
<arrowlink DESTINATION="Freemind_Link_1964911716" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Freemind_Arrow_Link_1425449118" STARTARROW="None" STARTINCLINATION="0;0;"/>
<arrowlink DESTINATION="Freemind_Link_1964911716" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Freemind_Arrow_Link_1083309464" STARTARROW="None" STARTINCLINATION="0;0;"/>
</node>
</node>
<node COLOR="#999999" CREATED="1173932155428" ID="Freemind_Link_1384875699" MODIFIED="1173932777057" TEXT="Chapter 11">
<edge COLOR="#808080" WIDTH="thin"/>
<arrowlink DESTINATION="Freemind_Link_1384875699" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Freemind_Arrow_Link_64155074" STARTARROW="None" STARTINCLINATION="0;0;"/>
<arrowlink DESTINATION="Freemind_Link_1384875699" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Freemind_Arrow_Link_26929383" STARTARROW="None" STARTINCLINATION="0;0;"/>
<font NAME="SansSerif" SIZE="10"/>
<node CREATED="1173932247721" ID="Freemind_Link_199197750" MODIFIED="1173932857636" TEXT="The Future of Western Assistance">
<arrowlink DESTINATION="Freemind_Link_199197750" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Freemind_Arrow_Link_650461124" STARTARROW="None" STARTINCLINATION="0;0;"/>
</node>
</node>
</node>
</node>
</node>
<node COLOR="#669900" CREATED="1173931807108" ID="Freemind_Link_863822196" MODIFIED="1173931861639" POSITION="left" TEXT="Arguments">
<node CREATED="1181067940365" ID="Freemind_Link_106780791" MODIFIED="1181069215990" TEXT="p. 76 Financial markets are:&#xa;1) A great source of free market efficiency&#xa;2) Create opportunities for everyone to get rich by borrowing and investing"/>
<node CREATED="1181069218486" ID="Freemind_Link_1385627605" MODIFIED="1181069333173" TEXT="p.86&#xa;Understanding financial markets requires understanding beyond individual choices. One needs to understand the bottom-up search for:&#xa; 1) social norms&#xa; 2) producer and consumer networks&#xa; 3) kin relationships that facilitate exchange"/>
<node CREATED="1181068587778" ID="Freemind_Link_1782338912" MODIFIED="1181068811122" TEXT="p. 86&#xa;The gains from trade through personalized exchange are much less than through the impersonal &#xa;exchange made possible by formal institutions. &#xa;[This is due to the exclusionary aspects of ignoring people outside of the (ethnic/group) network]"/>
<node CREATED="1181153082564" ID="Freemind_Link_1638336330" MODIFIED="1181153265365" TEXT="p.101&#xa;A reform where the gradual introduction of formal rules reinforced the existing [social, commercial, political] networks would work better than one that tried to replace them. A plausible story for the evolution of institutions in the West is that informal relationships and norms in the networks gradually hardenened into formal rules (which are still supported by informal relationships and norms)."/>
</node>
<node COLOR="#669900" CREATED="1173931821271" ID="Freemind_Link_160135127" MODIFIED="1173931860910" POSITION="left" TEXT="Evidence"/>
<node COLOR="#669900" CREATED="1173931825721" ID="Freemind_Link_295579883" MODIFIED="1173931860509" POSITION="left" TEXT="Anecdotes">
<node CREATED="1181067224210" ID="Freemind_Link_238478955" MODIFIED="1181067361902" TEXT="p78. The &quot;Hold-up&quot; Problem:&#xa;This is the &quot;hold-up&quot; problem--often at a point in a transaction, one party has a stranglehold &#xa;on the other and can extort additional payment. A contemporary of Julius Caesar&apos;s, Crassus, &#xa;made a fortune in early Rome witha a private fire company that would negotiate a price for &#xa;extinguisihng a fire as it was raging. "/>
<node CREATED="1181068278896" ID="Freemind_Link_1062978464" MODIFIED="1181068340454" TEXT="p. 82 Age groups in West Africa&#xa;men of the same age build a reputation, casting out those that are disreputable&#xa;this age group is a kind of brand to the rest of the community"/>
<node CREATED="1181151034233" ID="Freemind_Link_1001993853" MODIFIED="1181151233432" TEXT="p. 90&#xa;Property Rights: man from Isla Trinitaria, Ecuador&#xa;cut back even on food and clothing to save enough to build up a small shellfish business. But he lost it all when the mayor seized the land.&#xa;(from Voices of the Poor, vol. 3, p. 75)"/>
<node CREATED="1181152339925" ID="Freemind_Link_169453978" MODIFIED="1181152765624" TEXT="p.96 Problems with Private Property and Local Tradition&#xa;Example of a family over-selling property with customs according to local tradition, but combined with new private property laws, caused problems for everyone. Ocholla Ogweng of Kanyamkago got a loan of thirty thousand Kenyan shillings from Barclay&apos;s Bank in 1979...&#xa;RESULT: imposing land titling on on complex social customs can make land less secure."/>
<node CREATED="1182045235230" FOLDED="true" ID="Freemind_Link_939463068" MODIFIED="1182045489283" TEXT="p.193-194 Canadian International-Aid Agency/World Bank in Lesotho (the Thaba Tseka Region)&#xa;Scientific range management of live stock grazing did not work because of local laws about public&#xa; lands and the intended farmers were migrants who had no interest in farming.">
<node CREATED="1182045387047" LINK="http://en.wikipedia.org/wiki/Lesotho" MODIFIED="1182045387047" TEXT="en.wikipedia.org &gt; Wiki &gt; Lesotho"/>
</node>
</node>
<node COLOR="#669900" CREATED="1173931831898" ID="Freemind_Link_1805182556" MODIFIED="1173931860079" POSITION="left" TEXT="Concepts">
<node CREATED="1181067514117" ID="Freemind_Link_1639600644" MODIFIED="1181068203915" TEXT="p. 79 Social Capital&#xa;trust, how much people follow rules w/o coercion.&#xa;"/>
<node CREATED="1181068151543" ID="Freemind_Link_1437263972" MODIFIED="1181068187729" TEXT="p. 84 Multilateral punishment strategy&#xa;prevents agents from cheating a network of traders"/>
<node CREATED="1181069824813" ID="Freemind_Link_59691720" MODIFIED="1181070266517" TEXT="&lt;html&gt;&#xa;p. 87 Showdown at Predator&apos;s Pass&lt;br&gt;&#xa;Game Theory of Threat and Self-Protection&lt;br&gt;&#xa;&#xa;&lt;table style=&quot;width: 100%; text-align: left;&quot; border=&quot;1&quot; cellpadding=&quot;2&quot;&#xa;cellspacing=&quot;2&quot;&gt;&#xa;&lt;tbody&gt;&#xa;&lt;tr&gt;&#xa;&lt;td style=&quot;vertical-align: top;&quot;&gt;&lt;br&gt;&#xa;&lt;/td&gt;&#xa;&lt;td style=&quot;vertical-align: top;&quot;&gt;&lt;span style=&quot;font-weight: bold;&quot;&gt;B&lt;/span&gt;&#xa;buys guns&lt;/td&gt;&#xa;&lt;td style=&quot;vertical-align: top;&quot;&gt;&lt;span style=&quot;font-weight: bold;&quot;&gt;B&lt;/span&gt;&#xa;does not buys guns&lt;/td&gt;&#xa;&lt;/tr&gt;&#xa;&lt;tr&gt;&#xa;&lt;td style=&quot;vertical-align: top;&quot;&gt;&lt;span style=&quot;font-weight: bold;&quot;&gt;A&lt;/span&gt;&#xa;buys guns&lt;br&gt;&#xa;&lt;/td&gt;&#xa;&lt;td style=&quot;vertical-align: top;&quot;&gt;Equilibrium/Standoff, money&#xa;spent on guns, &lt;br&gt;&#xa;no production&lt;br&gt;&#xa;&lt;/td&gt;&#xa;&lt;td style=&quot;vertical-align: top;&quot;&gt;&lt;span style=&quot;font-weight: bold;&quot;&gt;A&lt;/span&gt;&#xa;takes over &lt;span style=&quot;font-weight: bold;&quot;&gt;B&lt;/span&gt;&apos;s resources by&#xa;force&lt;br&gt;&#xa;&lt;/td&gt;&#xa;&lt;/tr&gt;&#xa;&lt;tr&gt;&#xa;&lt;td style=&quot;vertical-align: top;&quot;&gt;&lt;span style=&quot;font-weight: bold;&quot;&gt;A&lt;/span&gt;&#xa;does not buy guns&lt;span style=&quot;font-weight: bold;&quot;&gt;&lt;/span&gt;&lt;br&gt;&#xa;&lt;/td&gt;&#xa;&lt;td style=&quot;vertical-align: top;&quot;&gt;&lt;span style=&quot;font-weight: bold;&quot;&gt;B&lt;/span&gt;&lt;span&#xa;style=&quot;font-weight: bold;&quot;&gt;&lt;/span&gt; takes over &lt;span&#xa;style=&quot;font-weight: bold;&quot;&gt;&lt;/span&gt;&lt;span style=&quot;font-weight: bold;&quot;&gt;A&lt;/span&gt;&apos;s&#xa;resources by force&lt;/td&gt;&#xa;&lt;td style=&quot;vertical-align: top;&quot;&gt;Equilibrium, money spent on&#xa;production&lt;br&gt;&#xa;&lt;/td&gt;&#xa;&lt;/tr&gt;&#xa;&lt;/tbody&gt;&#xa;&lt;/table&gt;"/>
</node>
<node COLOR="#669900" CREATED="1173931835237" ID="Freemind_Link_1472044623" MODIFIED="1173931859599" POSITION="left" TEXT="Quotes">
<node CREATED="1181066921058" ID="Freemind_Link_826332349" MODIFIED="1181066929855" TEXT="Sachs: &quot;You can&apos;t cross a chasm in two leaps&quot;"/>
<node CREATED="1181067647308" ID="Freemind_Link_1610492659" MODIFIED="1181067684017" TEXT="Marcel Fapfchamps:&#xa;Africa has a flea-market economy rather than a free-market economy."/>
</node>
<node COLOR="#669900" CREATED="1173931845053" ID="Freemind_Link_151198434" MODIFIED="1173931858820" POSITION="left" TEXT="To Research">
<node CREATED="1181065572104" ID="Freemind_Link_1112878689" MODIFIED="1181065575914" TEXT="Edmund Burke"/>
<node CREATED="1181065746778" ID="Freemind_Link_814311781" MODIFIED="1181065750174" TEXT="Lindblom">
<node CREATED="1181065751534" ID="Freemind_Link_1944782764" MODIFIED="1181065756492" TEXT="disjointed equilibrium"/>
</node>
<node CREATED="1181066664709" ID="Freemind_Link_1119929150" MODIFIED="1181066667902" TEXT="Robert Owens"/>
<node CREATED="1181066725725" ID="Freemind_Link_205107766" MODIFIED="1181066730671" TEXT="Walter Rostow">
<node CREATED="1181066733381" ID="Freemind_Link_202955908" MODIFIED="1181066764173" TEXT="Stages of Economic Growth : A Non-communist manifesto (cold war influence)"/>
</node>
<node CREATED="1181149325562" ID="Freemind_Link_810961649" MODIFIED="1181149343270" TEXT="Evolution of benevolent(?)/accountable gov&apos;t in the west"/>
</node>
<node CREATED="1179877810616" ID="Freemind_Link_51412134" MODIFIED="1179895914128" POSITION="left" TEXT="paragraph summary">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1181064568453" ID="Freemind_Link_486286793" MODIFIED="1181065223410" TEXT="In the past 50 years, the West has spent $2.3 trillion, yet has not succeeded in poverty elimination or economic develoment in the Rest. He blames the organization, procedures and sometimes aims of aid organizations as well as war and disease. Citing the historical context of colonialism, Easterly supports his critique with many examples of interference by Western outsiders. He distinguishes two approaches of those in the West who are trying to help: Planners, who have unrealistic, utopian vision of how to fix everything and Searchers, those close to the problems who help find incremental solutions to one problem at a time. "/>
</node>
<node CREATED="1179877771170" ID="Freemind_Link_1047429140" MODIFIED="1179895911718" POSITION="right" TEXT="Context">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1181064469012" ID="Freemind_Link_457751395" MODIFIED="1181064519336" TEXT="Hundreds of years of colonialism"/>
</node>
<node CREATED="1179877774729" ID="Freemind_Link_43449071" MODIFIED="1179895911777" POSITION="right" TEXT="Core Argument / Trajectory">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1181065229312" ID="Freemind_Link_1849760785" MODIFIED="1181065234426" TEXT="The Problems">
<node CREATED="1181065235502" ID="Freemind_Link_1752019173" MODIFIED="1181065240220" TEXT="Aid Organizations">
<node CREATED="1181065272261" ID="Freemind_Link_1277683671" MODIFIED="1181065404343" TEXT="Accountability problems:&#xa;These organizations are currently accountable to rich country funders&#xa;and not to poor people themselves."/>
<node CREATED="1181065610164" ID="Freemind_Link_305755874" MODIFIED="1181065710908" TEXT="Many groups all claim to be accountable for solving many problems.&#xa;When no one can solve the problem, they all avoid responsibility.&#xa;Easterly advocates groups attempt to solve single problems in specific areas&#xa;and be held accountable if they do not succeed (do not get future funding)."/>
</node>
<node CREATED="1181065241518" ID="Freemind_Link_1891787902" MODIFIED="1181065247239" TEXT="Aid Program Procedures">
<node CREATED="1181065590590" ID="Freemind_Link_1682138039" MODIFIED="1181065595939" TEXT="No Feedback"/>
</node>
<node CREATED="1181065253096" ID="Freemind_Link_453200520" MODIFIED="1181065258275" TEXT="Aid Program Aims">
<node CREATED="1181065264822" ID="Freemind_Link_844649458" MODIFIED="1181065270272" TEXT="Supporting ruthless dictators"/>
</node>
</node>
</node>
<node CREATED="1179877921270" ID="Freemind_Link_375248242" MODIFIED="1179895911774" POSITION="right" TEXT="Concepts">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1181064530182" ID="Freemind_Link_22138452" MODIFIED="1181064567453" TEXT="Planners vs Searchers">
<node CREATED="1181065521498" ID="Freemind_Link_1539206000" MODIFIED="1181065548532" TEXT="Karl Popper distinguishes &quot;utopian social engineering&quot; from &quot;piecemeal democratic reform&quot;"/>
</node>
</node>
<node CREATED="1179877782459" ID="Freemind_Link_1230702378" MODIFIED="1179895911772" POSITION="right" TEXT="Evidence Used">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1181788135000" ID="Freemind_Link_560594097" MODIFIED="1181788139049" TEXT="sources">
<node CREATED="1181788101395" ID="Freemind_Link_62495855" MODIFIED="1181788104878" TEXT="annecdotes"/>
<node CREATED="1181788105628" ID="Freemind_Link_160538158" MODIFIED="1181788111037" TEXT="data from world bank"/>
<node CREATED="1181788112775" ID="Freemind_Link_1272808558" MODIFIED="1181788128296" TEXT="books by researchers (eg. Acemoglu &amp; Robinson)"/>
</node>
</node>
<node CREATED="1179877787568" ID="Freemind_Link_205578298" MODIFIED="1179895911771" POSITION="right" TEXT="Innovations">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1179877791788" ID="Freemind_Link_1344084508" MODIFIED="1179895911756" POSITION="right" TEXT="Implications / Significance / Stakes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</map>
