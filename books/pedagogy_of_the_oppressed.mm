<map version="0.8.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1179877555080" ID="Freemind_Link_194965986" MODIFIED="1201142363820" TEXT="Pedagogy of the Oppressed&#xa;by Paolo Freire">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1179877771170" ID="Freemind_Link_1047429140" MODIFIED="1179895911718" POSITION="right" TEXT="Context">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1179877774729" ID="Freemind_Link_43449071" MODIFIED="1179895911777" POSITION="right" TEXT="Core Argument / Trajectory">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1179877921270" ID="Freemind_Link_375248242" MODIFIED="1179895911774" POSITION="right" TEXT="Concepts">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1179877782459" ID="Freemind_Link_1230702378" MODIFIED="1179895911772" POSITION="right" TEXT="Evidence Used">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1179877787568" ID="Freemind_Link_205578298" MODIFIED="1179895911771" POSITION="right" TEXT="Innovations">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1179877791788" ID="Freemind_Link_1344084508" MODIFIED="1179895911756" POSITION="right" TEXT="Implications / Significance / Stakes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1179877810616" ID="Freemind_Link_51412134" MODIFIED="1183048672834" POSITION="left" TEXT="Paragraph Summary of Book">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1201142392209" ID="_" MODIFIED="1201142395551" POSITION="left" TEXT="Ch 1">
<node CREATED="1201142438937" ID="Freemind_Link_548411359" MODIFIED="1201143145579" TEXT="The oppressors and the oppressed see themselves as on opposite poles. Their relationship dehumanizes both of them. The oppressors and the oppressed must not be mislead by &quot;false generosity&quot; on the part of the oppressors. This aid is but a minor salve, not salvation. It is the oppressed who can humanize both of them through liberation, which is for both to become more fully human. Both must do this together. This relationship exists because both allow it and this relationship can be broken by both working together through a dialogue, becoming peers. The oppressed have much to overcome, such as fear of freedom, which is one form of the internalization of their oppression. To transcend it is to become a new person. "/>
</node>
<node CREATED="1201581053335" ID="Freemind_Link_1687378458" MODIFIED="1201581054755" POSITION="left" TEXT="Ch 2">
<node CREATED="1201581133344" ID="Freemind_Link_1581177308" MODIFIED="1201581133344" TEXT="The banking model of teaching describes filling an empty student with information, like depositing money. This type of teaching is oppressive, while &quot;Problem Posing&quot; education is not. Problem posing involves students taking active participation in answering questions. it examines the interplay of permanence and change."/>
</node>
<node CREATED="1201581147709" ID="Freemind_Link_983990376" MODIFIED="1201581149358" POSITION="left" TEXT="Ch 3">
<node CREATED="1201581425689" MODIFIED="1201581425689" TEXT="Generative Themes arise from meaningful issues to students. The &quot;teacher&quot; must approach the student with love, humility and faith and be willing to discover with the student what is important. This dialogical style of education is liberation, &quot;education is the practice of freedom.&quot; To discover the meaningful issues, start with local, then regional then national circles of investigation. Start with the familiar before moving to the unfamiliar. Being too explicit will border on dogmatism. Being enigmatic will keep ideas out of reach. The process of coding involves abstraction. Decoding returns to the concrete. Going through this with each student, everyone discovers and benefits from the personal decoding of the world (often culture is a fruitful topic). This peer to peer learning, reflection and action, is humanizing."/>
</node>
<node CREATED="1201142396221" ID="Freemind_Link_1335339843" MODIFIED="1201142397650" POSITION="left" TEXT="Ch 4">
<node CREATED="1201143213905" ID="Freemind_Link_1244790067" MODIFIED="1201144385820" TEXT="The antidialogical is part of  oppression, the cultural invasion of one group by another. The dialogical is part of liberation, part of the process of cultural synthesis, the respectful, self-introspective unifying experience of two groups. The antidialogical uses conquest, division and rule, manipulation. The dialogical uses cooperation and organization. The liberation from oppression, which is humanization, involves praxis, the cycle of theory and practice. Though liberation must come from the oppressed, sometimes the oppressed have come from the ranks of the oppressors. Revolutionary leaders are the oppressed who, through self-realization and dialogue, choose to aid in the liberation of the oppressed, are faced with requirements, challenges and temptations. The requirements are that they must work with and listen to, be in dialogue with, the oppressed. The challenges, also discussed in chapter 1, include helping to overcome the internalization of oppression of others. The temptation is to use propoganda and slogans, maybe even sectarianism or to fall back on models of the oppressors. Of course, the revoultionary must not do this. Another temptation is the messianic impulse to save. But the oppressed must save themselves, through revolutionary praxis, through self-reflection and dialogue. The salvific urge is actually one of conquest, of cultural invasion. The humanization process can only happen together, perhaps catalyzed by a revolutionary leader who has the humility and respect for the other oppressed. To collectively transform, all must participate in the process of self-actualizing cultural synthesis."/>
</node>
</node>
</map>
