<map version="0.8.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1179877555080" ID="Freemind_Link_194965986" MODIFIED="1179878083365" TEXT="&lt;html&gt;&#xa;&lt;b&gt;Why They &lt;/b&gt;Don&apos;t &lt;b&gt;Hate Us&lt;/b&gt; &lt;br&gt;&#xa;&lt;i&gt;Lifting the Veil on the Axis of Evil&lt;/i&gt;">
<node CREATED="1179877669317" ID="_" MODIFIED="1179877710201" POSITION="left" TEXT="&lt;html&gt;&lt;img src=&quot;images/why_they_dont_hate_us.jpg&quot;&gt;"/>
<node CREATED="1179877771170" ID="Freemind_Link_1047429140" MODIFIED="1179895956782" POSITION="right" TEXT="Context">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1179877842881" ID="Freemind_Link_1962081818" MODIFIED="1179877846240" TEXT="Us vs Them">
<node CREATED="1179877846671" ID="Freemind_Link_1525308275" MODIFIED="1179877869367" TEXT="&quot;They are us&quot;"/>
</node>
</node>
<node CREATED="1179877774729" ID="Freemind_Link_43449071" MODIFIED="1179895958464" POSITION="right" TEXT="Core Argument / Trajectory">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1179877878336" ID="Freemind_Link_533869922" MODIFIED="1179877880856" TEXT="Re-history"/>
<node CREATED="1179877881374" ID="Freemind_Link_1160751887" MODIFIED="1179877888119" TEXT="Islam in globalization"/>
<node CREATED="1179877897573" ID="Freemind_Link_887757933" MODIFIED="1179877903852" TEXT="resistance movements"/>
<node CREATED="1179877904230" ID="Freemind_Link_613711108" MODIFIED="1179877917533" TEXT="Culture Jamming as a solution"/>
</node>
<node CREATED="1179877921270" ID="Freemind_Link_375248242" MODIFIED="1179895959531" POSITION="right" TEXT="Concepts">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1179877924119" ID="Freemind_Link_249715286" MODIFIED="1179877927390" TEXT="Culture Jamming">
<node CREATED="1179877931008" ID="Freemind_Link_937849841" MODIFIED="1179877950886" TEXT="like musical jamming, ie collaboration (not signal jamming)"/>
</node>
</node>
<node CREATED="1179877782459" ID="Freemind_Link_1230702378" MODIFIED="1179895960434" POSITION="right" TEXT="Evidence Used">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1179877963533" ID="Freemind_Link_418753818" MODIFIED="1179877977365" TEXT="Cultural impact of globalization in the Arab World"/>
</node>
<node CREATED="1179877787568" ID="Freemind_Link_205578298" MODIFIED="1179895961522" POSITION="right" TEXT="Innovations">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1179877982691" ID="Freemind_Link_1905409478" MODIFIED="1179877996583" TEXT="Culture Jamming like a fusion event (music jamming)"/>
<node CREATED="1179877996951" ID="Freemind_Link_1078607878" MODIFIED="1179878001949" TEXT="Axis is Empathy"/>
<node CREATED="1179878002324" ID="Freemind_Link_96243806" MODIFIED="1179878028468" TEXT="Examines relevance of alter globalization to the Arab World"/>
</node>
<node CREATED="1179877791788" ID="Freemind_Link_1344084508" MODIFIED="1179895962901" POSITION="right" TEXT="Implications / Significance / Stakes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1179877810616" ID="Freemind_Link_51412134" MODIFIED="1179895964622" POSITION="left" TEXT="paragraph summary">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1179878155213" ID="Freemind_Link_830050083" MODIFIED="1179879225688" TEXT="Dr. Mark Levine begins by reviewing the history of the Middle East and challenges Western intellectuals who emphasize the differences between America and Islamic countries. Moving from colonialism to consumerism, he charts the cultural mashups that define globalism in the Middle East region. Terror and conflict are manifestations of the larger struggle of &quot;human nationalism&quot; versus &quot;inhuman globalization&quot;, but Levine points out the progress of the global peace and justice movements could unite the people of both regions. He views open dialogue and collaborative &quot;culture jamming&quot; as the most promising starting point for reconciliation. "/>
</node>
</node>
</map>
