<map version="0.8.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1179877555080" ID="Freemind_Link_194965986" MODIFIED="1179895918717" TEXT="BOOK TITLE">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1179877771170" ID="Freemind_Link_1047429140" MODIFIED="1179895911718" POSITION="right" TEXT="Context">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1179877774729" ID="Freemind_Link_43449071" MODIFIED="1179895911777" POSITION="right" TEXT="Core Argument / Trajectory">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1179877921270" ID="Freemind_Link_375248242" MODIFIED="1179895911774" POSITION="right" TEXT="Concepts">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1179877782459" ID="Freemind_Link_1230702378" MODIFIED="1179895911772" POSITION="right" TEXT="Evidence Used">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1179877787568" ID="Freemind_Link_205578298" MODIFIED="1179895911771" POSITION="right" TEXT="Innovations">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1179877791788" ID="Freemind_Link_1344084508" MODIFIED="1179895911756" POSITION="right" TEXT="Implications / Significance / Stakes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1179877810616" ID="Freemind_Link_51412134" MODIFIED="1183048672834" POSITION="left" TEXT="Paragraph Summary of Book">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</map>
