<map version="0.8.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1179877555080" ID="Freemind_Link_194965986" MODIFIED="1200943789341" TEXT="&lt;html&gt;&#xa;&lt;b&gt;How to Change The World&lt;/b&gt;&lt;br&gt;&#xa;Social Entrepreneurs and the Power of New Ideas&lt;br&gt;&#xa;&lt;i&gt;by Daniel Bornstein&lt;/i&gt;&#xa;">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1179877771170" ID="Freemind_Link_1047429140" MODIFIED="1179895911718" POSITION="right" TEXT="Context">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1200943914582" ID="Freemind_Link_188486262" MODIFIED="1200944037378" TEXT="Social development programs created by entrepreneurs.&#xa;Case studies in multiple countries of individuals selected by the Ashoka Foundation."/>
</node>
<node CREATED="1179877774729" ID="Freemind_Link_43449071" MODIFIED="1179895911777" POSITION="right" TEXT="Core Argument / Trajectory">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1200944040132" ID="Freemind_Link_1811128724" MODIFIED="1200944111303" TEXT="Social development can be enhanced in capitalist societies through social entrepreneurship.&#xa;There is a coordination problem of philanthropic/government funder finding and aiding &#xa;social entrepreneurs at the right stage of their projects."/>
</node>
<node CREATED="1179877921270" ID="Freemind_Link_375248242" MODIFIED="1179895911774" POSITION="right" TEXT="Concepts">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1200944149329" ID="Freemind_Link_1366920541" MODIFIED="1200944215796" TEXT="Social Entrepreneurship: vision and leadership of a project with social values as goals"/>
</node>
<node CREATED="1179877782459" ID="Freemind_Link_1230702378" MODIFIED="1179895911772" POSITION="right" TEXT="Evidence Used">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1200944122278" ID="Freemind_Link_1633125008" MODIFIED="1200944145162" TEXT="Case studies from many countries, such as Brazil, Poland, South Africa."/>
</node>
<node CREATED="1179877787568" ID="Freemind_Link_205578298" MODIFIED="1179895911771" POSITION="right" TEXT="Innovations">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1200944230258" ID="Freemind_Link_1769934507" MODIFIED="1200944341458" TEXT="Analysis of a successful organization: Ashoka Foundation&#xa;Analysis of individual projects: &#xa;&#x9;Rural Electrification in Brazil&#xa;&#x9;AIDS care in South Africa&#xa;Synthesis of common attributes, strategies and political opportunities of successful projects"/>
</node>
<node CREATED="1179877791788" ID="Freemind_Link_1344084508" MODIFIED="1179895911756" POSITION="right" TEXT="Implications / Significance / Stakes">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1200944610914" MODIFIED="1200944610914" TEXT="Strengths of social entrepreneurship:">
<node CREATED="1200944610914" MODIFIED="1200944610914" TEXT="ability to operate at low cost due to lack of resources"/>
<node CREATED="1200944610914" MODIFIED="1200944610914" TEXT="creative solutions not found elsewhere"/>
<node CREATED="1200944610915" MODIFIED="1200944610915" TEXT="grass roots volunteerism motivated by social goals"/>
</node>
<node CREATED="1200944610915" MODIFIED="1200944610915" TEXT="Weaknesses of social entrepreneurship">
<node CREATED="1200944610915" MODIFIED="1200944610915" TEXT="lack of metrics in field / can be difficult to identify Ashoka Fellows"/>
<node CREATED="1200944610916" MODIFIED="1200944610916" TEXT="funders have not (in the past) funded based on results"/>
<node CREATED="1200944610916" MODIFIED="1200944610916" TEXT="critical gap between funders and entrepreneurs can be left unfound"/>
</node>
<node CREATED="1200944801522" MODIFIED="1200944801522" TEXT="Significance">
<node CREATED="1200944801522" MODIFIED="1200944801522" TEXT="Information for funders to evaluate candidates"/>
<node CREATED="1200944801523" MODIFIED="1200944801523" TEXT="Best practices, inspiration, warnings for social entrepreneurs"/>
</node>
<node CREATED="1200944801523" MODIFIED="1200944801523" TEXT="Stakes">
<node CREATED="1200944801523" MODIFIED="1200944801523" TEXT="wasted money that could have been spent on more effective projects"/>
<node CREATED="1200944801524" MODIFIED="1200944801524" TEXT="suffering which can be avoided with access to projects"/>
<node CREATED="1200944801524" MODIFIED="1200944801524" TEXT="ideological issue of capitalism, socialism, communism"/>
</node>
</node>
<node CREATED="1179877810616" ID="Freemind_Link_51412134" MODIFIED="1183048672834" POSITION="left" TEXT="Paragraph Summary of Book">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1200943723350" MODIFIED="1200943723350" TEXT="Social entrepreneurship is analyzed through the efforts of Bill Drayton and the Ashoka Foundation, which he created in 1981. Ashoka&apos;s approach is to identify social entrepreneurs and fund them at a critical stage of their projects. Ashoka is now in 46 countries and has supported 1,400 fellows (as of the book&apos;s writing in 2004). Bornstein examines case studies of social entrepreneurs from multiple continents and identifies common threads. Bornstein depicts social entrepreneurs as obsessed, driven by their ideas and devoted to scaling them up. He defines the commonalities of successful social entrepreneurs as self-correcting, sharing credit, disrupting existing social structures, crossing disciplinary boundaries and working quietly, for a long time. Bornstein is pro-market and uses a framework of capital allocation to discuss efficiency considerations for funding social entrepreneurs, meaning that money should follow results."/>
</node>
<node CREATED="1200688538472" ID="Freemind_Link_424891512" MODIFIED="1200868125280" POSITION="left" TEXT="resource guide" VSHIFT="34">
<node CREATED="1200688554495" FOLDED="true" ID="Freemind_Link_991319450" MODIFIED="1200688568265" TEXT="Resources for People Seeking Jobs and Vounteer Opportunities">
<node CREATED="1200689904480" ID="Freemind_Link_108427081" MODIFIED="1200689904480" TEXT="The Chronicle of Philanthropy, www.philanthropy.com"/>
<node CREATED="1200689904481" MODIFIED="1200689904481" TEXT="Community Service.org, www.communityservice.org"/>
<node CREATED="1200689904482" MODIFIED="1200689904482" TEXT="Idealist.org, www.idealist.org"/>
<node CREATED="1200689904482" MODIFIED="1200689904482" TEXT="Non Profit Times, www.nptimes.com"/>
<node CREATED="1200689904483" MODIFIED="1200689904483" TEXT="Nonprofit Career Network, www.nonprofitcareer.com"/>
<node CREATED="1200689904485" MODIFIED="1200689904485" TEXT="Opportunity Nocs, www.opportunitynocs.org"/>
<node CREATED="1200689904486" ID="Freemind_Link_648411150" MODIFIED="1200689960311" TEXT="Volunteer Match, www.volunteermatch.org (also Volunteer Match Corporate)"/>
<node CREATED="1200689904487" MODIFIED="1200689904487" TEXT="Wet Feet Guides, www.wetfeet.com"/>
<node CREATED="1200689904489" ID="Freemind_Link_762839546" MODIFIED="1200689946840" TEXT="Yahoo!, www.yahoo.com (See categories: Community Service and Voluntarism, Philanthropy, Nonprofit Resources, Grant-Making Foundations)"/>
<node CREATED="1200689904490" MODIFIED="1200689904490" TEXT="Youth Service America, www.ysa.org"/>
</node>
<node CREATED="1200691375538" FOLDED="true" ID="Freemind_Link_1569864245" MODIFIED="1200691375538" TEXT="Organizations that Identify or Support Social Entrepreneurs">
<node CREATED="1200691375539" MODIFIED="1200691375539" TEXT="Ashoka: Innovators for the Public, www.ashoka.org"/>
<node CREATED="1200691375539" MODIFIED="1200691375539" TEXT="Avina Foundation, www.avina.net"/>
<node CREATED="1200691375539" MODIFIED="1200691375539" TEXT="Changemakers Journal and Resources, www.changemakers.net (focuses on innovative strategies and resources for social entrepreneurs)"/>
<node CREATED="1200691375541" MODIFIED="1200691375541" TEXT="Draper Richards Foundation. www.draperrichards.org"/>
<node CREATED="1200691375541" MODIFIED="1200691375541" TEXT="Echoing Green roundation, www.ecnoinggreen.org"/>
<node CREATED="1200691375542" MODIFIED="1200691375542" TEXT="Ewing Marion Kauffman Foundation, www.emkf.org"/>
<node CREATED="1200691375545" MODIFIED="1200691375545" TEXT="The Flatiron Foundation, www.flatironpartners.com/foundation.html"/>
<node CREATED="1200691375548" MODIFIED="1200691375548" TEXT="New Profit, Inc., www.newprofitinc.org"/>
<node CREATED="1200691375568" MODIFIED="1200691375568" TEXT="New Schools Venture Fund, www.newschools.org (makes investments in &quot;education entrepreneurs&quot;)"/>
<node CREATED="1200691375570" MODIFIED="1200691375570" TEXT="Peninsula Community Foundation Center for Venture Philanthropy, www.pcf.org/venture.philanthropy"/>
<node CREATED="1200691375590" MODIFIED="1200691375590" TEXT="Robin Hood Foundation, www.robinhood.org"/>
<node CREATED="1200691375591" MODIFIED="1200691375591" TEXT="Schwab Foundation for Social Entrepreneurs, www.schwabfound.org"/>
<node CREATED="1200691375592" MODIFIED="1200691375592" TEXT="The Skoll Toundation, www.skollfoundation.org"/>
<node CREATED="1200691375593" MODIFIED="1200691375593" TEXT="Social Venture Partners, www.svpseattle.org"/>
<node CREATED="1200691375593" MODIFIED="1200691375593" TEXT="Tides Center, www.tidescenter.org (provides a fiscal home and infrastructure support for people starting social-purpose projects who have not established a aonprofit organization)"/>
<node CREATED="1200691375596" MODIFIED="1200691375596" TEXT="Venture Philanthropy Partners, www.venturephilanthropypartners.org"/>
<node CREATED="1200691375597" MODIFIED="1200691375597" TEXT="Youth Venture, www.youthventure.org"/>
</node>
<node CREATED="1200691985237" FOLDED="true" ID="Freemind_Link_358380554" MODIFIED="1200691985237" TEXT="Management and Funding Resources for Citizen Organizations">
<node CREATED="1200691985238" MODIFIED="1200691985238" TEXT="BoardnetUSA, www.boardnetUSA.org"/>
<node CREATED="1200691985241" MODIFIED="1200691985241" TEXT="Board Source, www.boardsource.org (practical information on building nonprofit boards)"/>
<node CREATED="1200691985242" MODIFIED="1200691985242" TEXT="The Bridgespan Group, www.bridgespangroup.org"/>
<node CREATED="1200691985242" MODIFIED="1200691985242" TEXT="Califomia Management Assistance Partnership, Nonprofit Genie, www.genie.org"/>
<node CREATED="1200691985243" MODIFIED="1200691985243" TEXT="Center for Excellence in Nonprofits, www.cen.org."/>
<node CREATED="1200691985244" MODIFIED="1200691985244" TEXT="Community Wealth, www.communitywealth.com"/>
<node CREATED="1200691985245" ID="Freemind_Link_49642016" MODIFIED="1200693141575" TEXT="Global Social Venture Competition, a partnership among the Haas School of Business at U.C. Berkeley, Columbia Business School, London Business School and the Goldman Sachs Foundation, www.socialvc.net&#xa;"/>
<node CREATED="1200691985247" ID="Freemind_Link_1610710496" MODIFIED="1200691985247" TEXT="The Grantsmanship Center, www.tgci.com"/>
<node CREATED="1200691985247" MODIFIED="1200691985247" TEXT="Institute for Social Entrepreneurs, www.socialent.org"/>
<node CREATED="1200691985249" MODIFIED="1200691985249" TEXT="Internet Nonprofit Center, www.nonprofits.org"/>
<node CREATED="1200691985250" MODIFIED="1200691985250" TEXT="Dorothy A. Johnson Center for Philanthropy and Nonprofit Leadership, www.nonprofitbasics.org"/>
<node CREATED="1200691985267" MODIFIED="1200691985267" TEXT="Leader to Leader Insritute, www.leadertoleader.org"/>
<node CREATED="1200691985268" MODIFIED="1200691985268" TEXT="Mission Movers, www.missionmovers.org"/>
<node CREATED="1200691985268" MODIFIED="1200691985268" TEXT="Narional Center&apos;for Social Entrepreneurs, www.socialentrepreneurs.org"/>
<node CREATED="1200691985269" MODIFIED="1200691985269" TEXT="Origo Social Enterprise Partners, www.origoinc.com"/>
<node CREATED="1200691985271" MODIFIED="1200691985271" TEXT="The Roberts Enterprise Development Fund, www.redf.org"/>
<node CREATED="1200692943067" ID="Freemind_Link_1221889903" MODIFIED="1200692949395" TEXT="SocialEdge, www.socialedge.org"/>
<node CREATED="1200691985271" ID="Freemind_Link_1389858526" MODIFIED="1200692943050" TEXT="Social Enterprise Alliance, www.se-alliance.org"/>
<node CREATED="1200691985273" MODIFIED="1200691985273" TEXT="Social Enterprise Magazine, www.socialenterprisemagazine.org"/>
<node CREATED="1200691985279" MODIFIED="1200691985279" TEXT="Techsoup, www.techsoup.org (technology assistance for nonprofits)"/>
<node CREATED="1200691985280" ID="Freemind_Link_99628835" MODIFIED="1200692989203" TEXT="Yale School of Management, Goldman Sachs Foundation Partnership on Nonprofit Ventures, www.ventures.yale.edu "/>
</node>
<node CREATED="1200691615437" FOLDED="true" ID="Freemind_Link_157514976" MODIFIED="1200691615437" TEXT="Citizen Sector Networks">
<node CREATED="1200691615438" MODIFIED="1200691615438" TEXT="Civicus, www.civicus.org"/>
<node CREATED="1200691615438" MODIFIED="1200691615438" TEXT="Community Action Network (CAN) (England), www.can-online.org.uk"/>
<node CREATED="1200691615439" MODIFIED="1200691615439" TEXT="Independent Sector, www.independentsector.org"/>
<node CREATED="1200691615440" MODIFIED="1200691615440" TEXT="One World, www.oneworld.net"/>
<node CREATED="1200691615440" MODIFIED="1200691615440" TEXT="Social Entrepreneurial Organizations (England), www.seo-online.org.uk"/>
</node>
<node CREATED="1200691909202" FOLDED="true" ID="Freemind_Link_940231495" MODIFIED="1200691909202" TEXT="Academic-based Resources">
<node CREATED="1200691909204" MODIFIED="1200691909204" TEXT="Canadian Center for Social Entrepreneurship, www.bus.ualberta.ca/ccse"/>
<node CREATED="1200691909205" MODIFIED="1200691909205" TEXT="Center for the Advancement of Social Entrepreneurship, Fuqua School of Business, Duke University, www.fuqua.duke.edu"/>
<node CREATED="1200691909206" MODIFIED="1200691909206" TEXT="Columbia Business School, Social Enterprise Program, www.gsb.columbia.edu/social enterprise, and Research Initiative on Social Entrepreneurship, www.riseproject.org"/>
<node CREATED="1200691909208" MODIFIED="1200691909208" TEXT="Harvard Business School, Initiative on Social Enterprise, www.hbs.edu/socialenterprise"/>
<node CREATED="1200691909209" MODIFIED="1200691909209" TEXT="Harvard Kennedy School of Government, Hauser Center for Nonprofit"/>
<node CREATED="1200691909210" MODIFIED="1200691909210" TEXT="Organizations, www.ksghauser.harvard.edu"/>
<node CREATED="1200691909211" ID="Freemind_Link_562365808" MODIFIED="1200693015476" TEXT="Johns Hopkins University Institute for Policy Studies, Center for Civil Society Studies, www.jhu.edu/~ccss"/>
<node CREATED="1200691909212" MODIFIED="1200691909212" TEXT="Skoll Center for Social Entrepreneurship, Said Business School at Oxford University"/>
<node CREATED="1200691909212" MODIFIED="1200691909212" TEXT="Stanford Business School, Center for Social Innovation, www.gsb.stanford.edu/csi"/>
</node>
<node CREATED="1200692319915" FOLDED="true" ID="Freemind_Link_1957845351" MODIFIED="1200692319915" TEXT="Resources for Funders">
<node CREATED="1200692319916" MODIFIED="1200692319916" TEXT="Aspen Institute Nonprofit Sector and Philanthropy Program, www.aspeninst.org"/>
<node CREATED="1200692319917" MODIFIED="1200692319917" TEXT="Center for Effective Philanthropy, www.effectivephilanthropy.com"/>
<node CREATED="1200692319918" MODIFIED="1200692319918" TEXT="Charity Navigator, www.charitynavigator.org"/>
<node CREATED="1200692319919" ID="Freemind_Link_362809752" MODIFIED="1200692319919" TEXT="CharityVillage.Com, www.charityvillage.com (general resource for the Canadian nonprofit sector)"/>
<node CREATED="1200692319921" MODIFIED="1200692319921" TEXT="Giving Global, www.givingglobal.org"/>
<node CREATED="1200692319921" MODIFIED="1200692319921" TEXT="Global Giving, www.globalgiving.com"/>
<node CREATED="1200692319922" MODIFIED="1200692319922" TEXT="Grantmakers for Effective Organizations, www.geofunders.org"/>
<node CREATED="1200692319923" MODIFIED="1200692319923" TEXT="Grantmakers Without Borders, www.internationaldonors.org"/>
<node CREATED="1200692319924" MODIFIED="1200692319924" TEXT="GrantSmart.Org, www.grantsmart.org"/>
<node CREATED="1200692319926" MODIFIED="1200692319926" TEXT="The Philanthropic Initiative, www.tpi.org"/>
<node CREATED="1200692319926" MODIFIED="1200692319926" TEXT="Venture Philanthropy Guide, www.VenturePhilanthropyGuide.org (venture philanthropy landscape in the United States and Canada)"/>
</node>
<node CREATED="1200692676667" FOLDED="true" ID="Freemind_Link_1893392344" MODIFIED="1200692676667" TEXT="Resources for Businesspeople">
<node CREATED="1200692676667" MODIFIED="1200692676667" TEXT="Aspen Institute, Initiative for Social Innovation Through Business, www.aspeninstitute.org"/>
<node CREATED="1200692676668" MODIFIED="1200692676668" TEXT="Business for Social Responsibility, www.bsr.org (helps companies succeed in business while adhering to ethical standards)"/>
<node CREATED="1200692676669" MODIFIED="1200692676669" TEXT="GIobal Business Network, www.gbn.org (seeks to advance innovation in business and society)"/>
<node CREATED="1200692676670" MODIFIED="1200692676670" TEXT="Investors Circle, a national intermediary supporring individuals and institutions investing in social responsible and sustainable businesses, www.investorscircle.net"/>
<node CREATED="1200692676673" MODIFIED="1200692676673" TEXT="Net Impact, www.net-impact.org (originally Students for Responsible Business; now a network with 80 chapters and 8,500 members"/>
<node CREATED="1200692676674" MODIFIED="1200692676674" TEXT="Social Venture Network, www.svn.org (businesspeople, investors, and social sector groups promoting socially responsible business practices)"/>
</node>
</node>
<node CREATED="1200695342165" FOLDED="true" ID="Freemind_Link_630747556" MODIFIED="1200695344565" POSITION="right" TEXT="Chapters">
<node CREATED="1200846207326" ID="Freemind_Link_1838465632" MODIFIED="1200846207892" TEXT="1">
<node CREATED="1200695355726" FOLDED="true" ID="Freemind_Link_1004801635" MODIFIED="1200695360229" TEXT="Ch 1&#xa;Restless People">
<node CREATED="1200695366419" MODIFIED="1200695366419" TEXT="Summary 11/12/07"/>
<node CREATED="1200695366420" MODIFIED="1200695366420" TEXT="Bornstein begins by describing the type of person that he calls a social entrepreneur, &quot;people who solve social problems on a large scale.&quot; [Bornstein, 2004, pg 1] . These people are all over the world and don&apos;t just work toward changing situations, they work toward changing entire systems of interactions. He notes the recent increase in number of Non-Governmental Organizations, as part of what he calls the citizen&apos;s sector [ibid. pg 3, sometimes called &quot;civil society&quot; in other works - Durant]. Bornstein lists 6 attributes that make many of these organizations new [ibid. pg 6]. He describes why these organizations are forming and sums them up as due to people having more &quot;freedom, time, wealth, health, exposure, social mobility and confidence to address social problems in bold new ways.&quot; He cites increased global awareness due to modern communications as well as awareness of our shared environment (global warming). All of the following are increasing, he says, number of NGO&apos;s, problems (AIDS, environment), corporatization, awareness, government failures (failure to innovate). Social entrepreneurial initiatives are nimble and more capable than government organizations at adapting to slow problems with cumulative effects on society [ibid, Jessica T. Matthews quoted from Foreign Affairs]."/>
</node>
</node>
<node CREATED="1200846208801" ID="Freemind_Link_1303940020" MODIFIED="1200846209351" TEXT="2">
<node CREATED="1200695370427" FOLDED="true" ID="Freemind_Link_1578993550" MODIFIED="1200695379543" TEXT="Ch 2 &#xa;From Little Acorns Do Great Trees Grow">
<node CREATED="1200695385773" MODIFIED="1200695385773" TEXT="Summary 11/12/07"/>
<node CREATED="1200695385773" MODIFIED="1200695385773" TEXT="Bornstein identifies the Akosha Foundation as one of the earliest trackers of global social entrepreneurs. Started by American Bill Dayton, an assistant administrator of the EPA, in 1978, Ashoka is in 46 countries and has assisted 1,400 social entrepreneurs with $40 million in funding [2004]. In the earliest days, funding came from Dayton and his friends, a relatively small $50,000. Dayton sought candidates who might be capable of enacting large change in many different countries. Some countries were too restrictive (China, Soviet Union) to foster these types of individuals, others too expensive (US). By 1981 Ashoka had hundreds of leads from interviewing referral after referral in many countries. Their first fellow was Gloria de Souza, an educator in India who had created a new educational system that engaged students in creative thinking instead of rote memorization. Besides being innovative, they chose her because she was an excellent salesperson and highly commited. Hindered by the inertia in the educational system of the time, eventually Ashoka recognized her and paid her four years stipend ($10,000US) to work full time on spreading her ideas. And her determination paid off. Her system spread to become the national standard of all of India by the late 1980&apos;s."/>
</node>
</node>
<node CREATED="1200846209891" ID="Freemind_Link_1763729949" MODIFIED="1200846210354" TEXT="3">
<node CREATED="1200695397060" FOLDED="true" ID="Freemind_Link_284726458" MODIFIED="1200695400716" TEXT="Ch 3&#xa;The Light in My Head Went On&#xa;F&#xe1;bio Rosa, Brazil: Rural Electrification">
<node CREATED="1200695408968" MODIFIED="1200695408968" TEXT="Summary 11/12/07"/>
<node CREATED="1200695408969" MODIFIED="1200695408969" TEXT="The story of F&#xe1;bio Rosa is another story of determination, this time with repeated failures and repeated successes. It is a story of both great skill and great chance. Rosa&apos;s parents grew up on the grasslands called the pampas and Rosa idealized that life. Rosa was invited by a college friend to visit him in a rural part of Brazil. Rosa didn&apos;t know it, but his friend&apos;s father had been elected mayor. After a long dinner conversation with the father about improving life for the villagers, the father appointed Rosa as secretary of agriculture. The politicians in the area spoke of building roads, but Rosa asked the people what they wanted and it turned out they didn&apos;t mention roads, they mentioned education for their children so they could escape poverty and keep their farms. To do this they needed to boost their farm incomes."/>
<node CREATED="1200695408979" MODIFIED="1200695408979" TEXT="Most of the wealth in the area was from rice crops, but the water supply was controlled and sold for three times the world average; the farmers barely making making on their rice. Rosa had read about the Louisiana in the 1940&apos;s and how rice was irrigated with artesian wells. He also had been watching TV and saw Ennio Amaral who was advocating a cheaper, simpler system of electricity (using one wire, monophase, instead of three, triple phase). With electricity, Rosa realized, the rice farmers could pump well water for their crops. The story that follows is one of combating the electric company, government change and unforeseen problems. Rosa has political skill, persuading the national government to pressure the electric company. He had to fight to change the standard to allow this new electrical system to be recognized and legal (with his early successes can some popular support). He had technical issues to consider, like well water maybe being too low for monophase pumps, but fortunately the water pressure pushed up the water high enough after all. His solutions allowed new farming methods, like irrigating the entire land (now possible with pumps) instead of rotating land to avoid weeds. Rosa had business skill. He provided all necessary financial research data to convince investors to make small loans to the farmers of this trial area. Rosa had public relations skills, ensuring that journalists and politicians heard about this project."/>
<node CREATED="1200695408988" MODIFIED="1200695408988" TEXT="He faced setbacks, but Rosa kept on with helping villagers. After the initial electrification project, he had another insight in the form of combining systems. Many grasslands are over grazed, but a French system (by Andre Voisin) had been tried in Brazil, but didn&apos;t work. It involved electric fences to keep cattle in areas to graze one part of the land at a time. This preserved the grasslands. Rosa investigated why the system didn&apos;t work in Brazil and realized that local plants would grow high and lower the voltages, so cows wouldn&apos;t care. He created a new system where this wouldn&apos;t happen. Realizing that solar would help, but is too expensive, he pioneered a cheaper electric fence, so that the entire system was affordable. Soon, this market solution was making people money selling organic meat and milk to the booming European organic food market. By studying the local conditions and being good at adapting solutions, Rosa was able to bring prosperity to small cattle ranchers."/>
</node>
</node>
<node CREATED="1200846219309" ID="Freemind_Link_1577738361" MODIFIED="1200846219869" TEXT="4">
<node CREATED="1200845896049" FOLDED="true" ID="Freemind_Link_238450371" MODIFIED="1200845941613" TEXT="Ch 4&#xa;The Fixed Determination of an Indomitable Will&#xa;Florence Nightingale, England: Nursing">
<node CREATED="1200847101771" ID="Freemind_Link_366085594" MODIFIED="1200847106872" TEXT="Florence Nightengale">
<node CREATED="1200847129222" LINK="http://en.wikipedia.org/wiki/Florence_Nightengale" MODIFIED="1200847129222" TEXT="en.wikipedia.org &gt; Wiki &gt; Florence Nightengale"/>
<node CREATED="1200847143562" ID="Freemind_Link_252096251" MODIFIED="1200847147919" TEXT="p. 40">
<node CREATED="1200847149001" ID="Freemind_Link_1505714909" MODIFIED="1200849188777" TEXT="Florence Nightengale, a young woman of priviledge in England in the 1840&apos;s was forbidden by her father to become a nurse. It was odd for a woman of high social standing to want to work at all, much less at a time when nurses had a reputation as being ignorant, brutal and having bad habits."/>
</node>
<node CREATED="1200847337772" ID="Freemind_Link_1177297750" MODIFIED="1200847340142" TEXT="p. 41">
<node CREATED="1200847340664" ID="Freemind_Link_1324185777" MODIFIED="1200847361896" TEXT="Eventually there was a nursing school for women of good standing in Germany"/>
<node CREATED="1200847362471" ID="Freemind_Link_992238600" MODIFIED="1200847368704" TEXT="She chose nursing over marriage"/>
<node CREATED="1200847375821" ID="Freemind_Link_1439849738" MODIFIED="1200847377809" TEXT="age 33">
<node CREATED="1200847378532" ID="Freemind_Link_233214721" MODIFIED="1200847397477" TEXT="administrator of a women&apos;s care facility"/>
</node>
<node CREATED="1200847398657" ID="Freemind_Link_154735374" MODIFIED="1200847401954" TEXT="age 34">
<node CREATED="1200847402806" ID="Freemind_Link_183250636" MODIFIED="1200847409003" TEXT="Crimean War">
<node CREATED="1200847410864" ID="Freemind_Link_701053014" MODIFIED="1200847418463" TEXT="Went to help English soldiers"/>
<node CREATED="1200847419523" ID="Freemind_Link_1344172604" MODIFIED="1200847435824" TEXT="deathrate dropped from 43% to 2% (of injured?)"/>
<node COLOR="#cc0000" CREATED="1200847472862" ID="Freemind_Link_1288937635" MODIFIED="1200849301899" TEXT="kept records!"/>
<node CREATED="1200847492151" ID="Freemind_Link_1156439126" MODIFIED="1200849253207" TEXT="she came with 30,000 English pounds of private funding">
<node COLOR="#3300cc" CREATED="1200849255156" ID="Freemind_Link_99515780" MODIFIED="1200849266263" TEXT="money/privelege">
<edge COLOR="#808080" WIDTH="thin"/>
</node>
</node>
<node CREATED="1200847436454" ID="Freemind_Link_1894992017" MODIFIED="1200847452173" TEXT="clean clothes, towels, sanitation"/>
<node CREATED="1200847453188" ID="Freemind_Link_1223435115" MODIFIED="1200847462002" TEXT="38 nurses showed up, rejected by army at first"/>
<node CREATED="1200849366171" ID="Freemind_Link_267462045" MODIFIED="1200849370730" TEXT="army opposed her at first"/>
</node>
</node>
</node>
<node CREATED="1200847531550" ID="Freemind_Link_920564968" MODIFIED="1200847533011" TEXT="p. 43">
<node CREATED="1200849323234" ID="Freemind_Link_276906480" MODIFIED="1200849323260" TEXT="new crusdade">
<node CREATED="1200847533523" ID="Freemind_Link_715048269" MODIFIED="1200849323228" TEXT="improve the health of the British Army">
<node CREATED="1200847550275" ID="Freemind_Link_1667110003" MODIFIED="1200847555781" TEXT="use of statistics"/>
</node>
</node>
<node CREATED="1200847557112" ID="Freemind_Link_1223645366" MODIFIED="1200847567357" TEXT="she became bedridden with Crimean Fever">
<node COLOR="#3300cc" CREATED="1200849211227" ID="Freemind_Link_1592864033" MODIFIED="1200849240703" TEXT="personal cost">
<arrowlink DESTINATION="Freemind_Link_1592864033" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Freemind_Arrow_Link_1786112161" STARTARROW="None" STARTINCLINATION="0;0;"/>
</node>
</node>
</node>
<node CREATED="1200847574051" ID="Freemind_Link_384113949" MODIFIED="1200847575863" TEXT="p. 45">
<node CREATED="1200847576675" ID="Freemind_Link_655580236" MODIFIED="1200847601406" TEXT="she was an administrator statistician and lobbyist"/>
</node>
<node CREATED="1200847602845" ID="Freemind_Link_1962247103" MODIFIED="1200847608144" TEXT="p. 46">
<node CREATED="1200847629776" ID="Freemind_Link_446665373" MODIFIED="1200847634448" TEXT="Changing a System">
<node CREATED="1200847634976" ID="Freemind_Link_1790670349" MODIFIED="1200847639558" TEXT="Change">
<node CREATED="1200847640187" ID="Freemind_Link_1526492970" MODIFIED="1200847643496" TEXT="attitudes"/>
<node CREATED="1200847644036" ID="Freemind_Link_807918230" MODIFIED="1200847646251" TEXT="expectations"/>
<node CREATED="1200847646930" ID="Freemind_Link_230766640" MODIFIED="1200847649514" TEXT="behaviors"/>
</node>
<node CREATED="1200847652592" ID="Freemind_Link_251844530" MODIFIED="1200847655139" TEXT="Overcome">
<node CREATED="1200847655694" ID="Freemind_Link_790405811" MODIFIED="1200847659499" TEXT="disbelief"/>
<node CREATED="1200847660107" ID="Freemind_Link_1330141993" MODIFIED="1200847662666" TEXT="prejudice"/>
<node CREATED="1200847663235" ID="Freemind_Link_8093731" MODIFIED="1200847664295" TEXT="fear"/>
</node>
<node CREATED="1200847671137" ID="Freemind_Link_1377182334" MODIFIED="1200847690875" TEXT="Defenders of status quo often impervious to common sense"/>
<node CREATED="1200847707902" ID="Freemind_Link_985935982" MODIFIED="1200847711775" TEXT="The Prince">
<node CREATED="1200847712373" ID="Freemind_Link_1966284804" MODIFIED="1200847730456" TEXT="Niccol&#xf2; Machiavelli"/>
<node CREATED="1200847731039" ID="Freemind_Link_451500253" MODIFIED="1200847774113" TEXT="&quot;[T]here is nothing more diffucult to carry out, nor more doubtful of success, nor more dangerous to handle, than to initiate a new order of things. For the reformer has enemies in all those who profit by the old order, and onlly lukewarm defenders in all those who would profit by the new order.&quot;"/>
</node>
<node CREATED="1200847804544" ID="Freemind_Link_992792319" MODIFIED="1200847820588" TEXT="requires">
<node CREATED="1200847821319" ID="Freemind_Link_119515477" MODIFIED="1200847828219" TEXT="concentrated focus"/>
<node CREATED="1200847835193" ID="Freemind_Link_135273761" MODIFIED="1200847841085" TEXT="practical creativity"/>
<node CREATED="1200847842142" ID="Freemind_Link_812502749" MODIFIED="1200847850681" TEXT="long-term source of energy"/>
</node>
<node CREATED="1200847857494" ID="Freemind_Link_1631946268" MODIFIED="1200847861698" TEXT="social entrepreneurs">
<node CREATED="1200847862249" ID="Freemind_Link_929379439" MODIFIED="1200847867076" TEXT="quality of motivation"/>
<node CREATED="1200847867704" ID="Freemind_Link_329220632" MODIFIED="1200847873918" TEXT="inexplicable obessions"/>
<node CREATED="1200847875254" ID="Freemind_Link_582906346" MODIFIED="1200847880330" TEXT="action and growth orientation"/>
<node CREATED="1200847880939" ID="Freemind_Link_1155369002" MODIFIED="1200847891836" TEXT="unwavering belief in the rightness of their ideas"/>
</node>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1200846220574" ID="Freemind_Link_548446118" MODIFIED="1200846221043" TEXT="5">
<node CREATED="1200845944118" FOLDED="true" ID="Freemind_Link_523709464" MODIFIED="1200845966939" TEXT="Ch 5&#xa;A Very Significan Force&#xa;Bill Drayton, United States: The Bubble">
<node CREATED="1200847932474" ID="Freemind_Link_539051083" MODIFIED="1200847937526" TEXT="Bill Drayton">
<node CREATED="1200847938131" ID="Freemind_Link_956926642" MODIFIED="1200847942309" TEXT="parents lived their dreams">
<node CREATED="1200849901902" ID="Freemind_Link_1085199017" MODIFIED="1200849908575" TEXT="English Aristocratic family"/>
<node CREATED="1200849909268" ID="Freemind_Link_971647411" MODIFIED="1200849919818" TEXT="father dropped out of Harvard to become an explorer"/>
<node CREATED="1200849971510" ID="Freemind_Link_1995717847" MODIFIED="1200849971527" TEXT="mother">
<node CREATED="1200849973652" ID="Freemind_Link_1106234908" MODIFIED="1200849979858" TEXT="middle class family in australia"/>
<node CREATED="1200849980597" ID="Freemind_Link_588825093" MODIFIED="1200849982968" TEXT="came to nyc"/>
<node CREATED="1200849941090" ID="Freemind_Link_1569903357" MODIFIED="1200849971490" TEXT="gifted cellist"/>
</node>
<node CREATED="1200847942921" ID="Freemind_Link_1840565107" MODIFIED="1200847947066" TEXT="role models?"/>
</node>
<node CREATED="1200848007479" ID="Freemind_Link_1575058215" MODIFIED="1200848014312" TEXT="Philips Academy"/>
<node CREATED="1200849821485" ID="Freemind_Link_740410760" MODIFIED="1200849825494" TEXT="Harvard"/>
<node CREATED="1200849831121" ID="Freemind_Link_1590167893" MODIFIED="1200849834442" TEXT="Oxford">
<node CREATED="1200849834920" ID="Freemind_Link_708805047" MODIFIED="1200849866012" TEXT="economic, public finance, history"/>
</node>
<node CREATED="1200849871341" ID="Freemind_Link_1358546511" MODIFIED="1200849880178" TEXT="Yale Law School"/>
<node CREATED="1200847950955" ID="Freemind_Link_1618163780" MODIFIED="1200847962518" TEXT="active at age 14 (1957)">
<node CREATED="1200847963126" ID="Freemind_Link_1219377689" MODIFIED="1200847977205" TEXT="protested Woolworth&apos;s discriminatory practices">
<node CREATED="1200848152117" ID="Freemind_Link_1651162374" MODIFIED="1200848159314" TEXT="racial issues"/>
</node>
</node>
<node CREATED="1200848193348" ID="Freemind_Link_1311704876" MODIFIED="1200848198237" TEXT="p. 48">
<node CREATED="1200848173371" ID="Freemind_Link_1687254500" MODIFIED="1200848177688" TEXT="admired Ghandi">
<node CREATED="1200848178545" ID="Freemind_Link_1957054326" MODIFIED="1200848187299" TEXT="particularly his practices">
<node CREATED="1200848188069" ID="Freemind_Link_1567890083" MODIFIED="1200848192313" TEXT="how-to"/>
</node>
<node CREATED="1200848407856" ID="Freemind_Link_98168596" MODIFIED="1200848431048" TEXT="engaged in details of">
<node CREATED="1200848411925" ID="Freemind_Link_324706294" MODIFIED="1200848415826" TEXT="politics"/>
<node CREATED="1200848416998" ID="Freemind_Link_449637349" MODIFIED="1200848420642" TEXT="administration"/>
<node CREATED="1200848421311" ID="Freemind_Link_1039352060" MODIFIED="1200848425381" TEXT="implementation"/>
</node>
<node CREATED="1200848221646" ID="Freemind_Link_148342490" MODIFIED="1200848232499" TEXT="new ethics based on">
<node CREATED="1200848233059" ID="Freemind_Link_910193189" MODIFIED="1200848241884" TEXT="empathy"/>
<node CREATED="1200848234812" ID="Freemind_Link_1847461250" MODIFIED="1200848238956" TEXT="not rules"/>
</node>
</node>
</node>
<node CREATED="1200848271469" ID="Freemind_Link_1315628042" MODIFIED="1200848275015" TEXT="p. 49">
<node CREATED="1200848275521" ID="Freemind_Link_812359694" MODIFIED="1200848281525" TEXT="Ghandi&apos;s Salt March">
<node CREATED="1200848317622" ID="Freemind_Link_770842808" MODIFIED="1200848334720" TEXT="1930 protest march 241 miles to pick salt (illegal)"/>
<node CREATED="1200848343834" ID="Freemind_Link_1184899007" MODIFIED="1200848350904" TEXT="illegal for the press to cover it"/>
</node>
</node>
<node CREATED="1200849444926" ID="Freemind_Link_1914292080" MODIFIED="1200849447186" TEXT="p. 50">
<node CREATED="1200849447768" ID="Freemind_Link_1574903311" MODIFIED="1200849453795" TEXT="King Ashoka">
<node CREATED="1200849498994" ID="Freemind_Link_1430654264" LINK="http://en.wikipedia.org/wiki/Ashoka_the_Great" MODIFIED="1200849498994" TEXT="en.wikipedia.org &gt; Wiki &gt; Ashoka the Great"/>
<node CREATED="1200849454494" ID="Freemind_Link_362660743" MODIFIED="1200849460344" TEXT="269 to 232 BCE"/>
</node>
</node>
<node CREATED="1200849560325" ID="Freemind_Link_1426798056" MODIFIED="1200849562022" TEXT="p. 51">
<node CREATED="1200849648100" ID="Freemind_Link_226437149" MODIFIED="1200849652917" TEXT="Vinoba Bhave">
<node CREATED="1200849658707" LINK="http://en.wikipedia.org/wiki/Vinoba_Bhave" MODIFIED="1200849658707" TEXT="en.wikipedia.org &gt; Wiki &gt; Vinoba Bhave"/>
<node CREATED="1200849661263" ID="Freemind_Link_501468083" MODIFIED="1200849666138" TEXT="land grands to untouchables">
<node CREATED="1200849666850" ID="Freemind_Link_1254808423" MODIFIED="1200849687506" TEXT="bhoodan">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1200849672562" ID="Freemind_Link_1358005948" MODIFIED="1200849676729" TEXT="land gift"/>
</node>
<node CREATED="1200849679187" ID="Freemind_Link_472672077" MODIFIED="1200849689014" TEXT="gramdan">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1200849681865" ID="Freemind_Link_1886378056" MODIFIED="1200849683928" TEXT="village gift"/>
</node>
<node CREATED="1200849699420" ID="Freemind_Link_1680286270" MODIFIED="1200849714528" TEXT="1951 aged 55 w/malaria walked 10 to 12 miles/day"/>
</node>
</node>
</node>
<node CREATED="1200849753735" ID="Freemind_Link_1685361647" MODIFIED="1200849755704" TEXT="p. 52">
<node CREATED="1200849756190" ID="Freemind_Link_989531368" MODIFIED="1200849771405" TEXT="David C. McClelland">
<node CREATED="1200849772129" ID="Freemind_Link_1410085560" MODIFIED="1200849777629" TEXT="The Achieving Society">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1200849778157" ID="Freemind_Link_1184840950" MODIFIED="1200849789746" TEXT="need for">
<node CREATED="1200849790300" ID="Freemind_Link_1394668802" MODIFIED="1200849792306" TEXT="power"/>
<node CREATED="1200849792912" ID="Freemind_Link_200581871" MODIFIED="1200849796484" TEXT="affiliation"/>
<node CREATED="1200849797033" ID="Freemind_Link_1916927412" MODIFIED="1200849802534" TEXT="achievement"/>
</node>
</node>
</node>
</node>
<node CREATED="1200850262527" ID="Freemind_Link_956839470" MODIFIED="1200850265317" TEXT="p. 53">
<node CREATED="1200850265838" ID="Freemind_Link_25074651" MODIFIED="1200850267809" TEXT="1970&apos;s">
<node CREATED="1200850278177" ID="Freemind_Link_1762075720" MODIFIED="1200850278177" TEXT="McKinsey &amp; Company">
<node CREATED="1200850283706" LINK="http://en.wikipedia.org/wiki/Mckinsey" MODIFIED="1200850283706" TEXT="en.wikipedia.org &gt; Wiki &gt; Mckinsey"/>
</node>
</node>
</node>
<node CREATED="1200850363034" ID="Freemind_Link_221993392" MODIFIED="1200850365828" TEXT="p. 54">
<node CREATED="1200850366563" ID="Freemind_Link_972571625" MODIFIED="1200850368556" TEXT="the bubble">
<node CREATED="1200850369040" ID="Freemind_Link_1246967234" MODIFIED="1200850372572" TEXT="emissions trading"/>
</node>
</node>
<node CREATED="1200850377949" ID="Freemind_Link_1361932097" MODIFIED="1200850387886" TEXT="p. 56 - 57">
<node CREATED="1200850388454" ID="Freemind_Link_1508415744" MODIFIED="1200850407741" TEXT="Reagan admin takedown of the EPA"/>
</node>
</node>
</node>
</node>
<node CREATED="1200846221545" ID="Freemind_Link_1428118290" MODIFIED="1200846222228" TEXT="6">
<node CREATED="1200845967752" FOLDED="true" ID="Freemind_Link_1875515767" MODIFIED="1200845979939" TEXT="Ch 6&#xa;Why Was I Never Told about This?">
<node CREATED="1200851033057" ID="Freemind_Link_1337229823" MODIFIED="1200851048754" TEXT="1980&apos;s Drayton 1/2 at EPA 1/2 at McKinsey">
<node CREATED="1200851049960" ID="Freemind_Link_1473495240" MODIFIED="1200851056130" TEXT="developing Ashoka Foundation"/>
</node>
<node CREATED="1200851080131" ID="Freemind_Link_715649155" MODIFIED="1200851083031" TEXT="3x5 cards"/>
<node CREATED="1200851128937" ID="Freemind_Link_1565793541" MODIFIED="1200851130608" TEXT="1984">
<node CREATED="1200851131119" ID="Freemind_Link_734149765" MODIFIED="1200851152010" TEXT="MacArthur Fellowship">
<node CREATED="1200851152548" ID="Freemind_Link_164263059" MODIFIED="1200851155479" TEXT="$200,000"/>
<node CREATED="1200851223329" ID="Freemind_Link_448153338" MODIFIED="1200851227938" TEXT="helped him start Ashoka"/>
</node>
</node>
<node CREATED="1200851230436" ID="Freemind_Link_245145489" MODIFIED="1200851234286" TEXT="looking for people">
<node CREATED="1200851239773" ID="Freemind_Link_1992698419" MODIFIED="1200851246683" TEXT="nominator network"/>
</node>
<node CREATED="1200851102266" ID="Freemind_Link_1166268999" MODIFIED="1200851105165" TEXT="Brazil added"/>
<node CREATED="1200851106203" ID="Freemind_Link_799716099" MODIFIED="1200851434027" TEXT="seeing the sum of socially minded activity in Brazil makes a new compelling view"/>
</node>
</node>
<node CREATED="1200846222727" ID="Freemind_Link_1971477874" MODIFIED="1200846223296" TEXT="7">
<node CREATED="1200845980541" FOLDED="true" ID="Freemind_Link_1539989510" MODIFIED="1200846006377" TEXT="Ch 7&#xa;Ten--Nine--Eight--Childline!&#xa;Jeroo Billimoria, India: Child Protection">
<node CREATED="1200851494508" ID="Freemind_Link_92575739" MODIFIED="1200851500476" TEXT="enabling factors of success">
<node CREATED="1200851500986" ID="Freemind_Link_639523898" MODIFIED="1200851502872" TEXT="1">
<node CREATED="1200851503323" ID="Freemind_Link_803426435" MODIFIED="1200851508864" TEXT="Public telephones"/>
</node>
<node CREATED="1200851510033" ID="Freemind_Link_1710967506" MODIFIED="1200851510288" TEXT="2">
<node CREATED="1200851511182" ID="Freemind_Link_619647329" MODIFIED="1200851527391" TEXT="Government newly willing to work with citizen groups"/>
</node>
<node CREATED="1200851528647" ID="Freemind_Link_356049805" MODIFIED="1200851529558" TEXT="3">
<node CREATED="1200851530004" ID="Freemind_Link_1733846505" MODIFIED="1200851558385" TEXT="success in garnering corporate sponsorship">
<node CREATED="1200851558868" ID="Freemind_Link_37173303" MODIFIED="1200851572867" TEXT="expertise and support"/>
<node CREATED="1200851561906" ID="Freemind_Link_299805045" MODIFIED="1200851568297" TEXT="not just money!"/>
</node>
</node>
<node CREATED="1200851760189" ID="Freemind_Link_1117064416" MODIFIED="1200851760939" TEXT="4">
<node CREATED="1200851761398" ID="Freemind_Link_339797240" MODIFIED="1200851768319" TEXT="recognition">
<node CREATED="1200851768795" ID="Freemind_Link_1850469211" MODIFIED="1200851769727" TEXT="local"/>
<node CREATED="1200851770227" ID="Freemind_Link_1075702296" MODIFIED="1200851771628" TEXT="national"/>
<node CREATED="1200851772146" ID="Freemind_Link_1518087528" MODIFIED="1200851773796" TEXT="international"/>
</node>
</node>
</node>
<node CREATED="1200851657850" ID="Freemind_Link_1544625296" MODIFIED="1200851660262" TEXT="strategy">
<node CREATED="1200851660804" ID="Freemind_Link_821181338" MODIFIED="1200851665407" TEXT="find supporters in government"/>
<node CREATED="1200851665920" ID="Freemind_Link_1538940106" MODIFIED="1200851671091" TEXT="still need wealthy individuals, though"/>
</node>
<node CREATED="1200851673857" ID="Freemind_Link_361077595" MODIFIED="1200851676274" TEXT="scale up">
<node CREATED="1200851886082" ID="Freemind_Link_1989357935" MODIFIED="1200851894562" TEXT="more sophisticated stategies for training">
<node CREATED="1200851902107" ID="Freemind_Link_1250396318" MODIFIED="1200851904617" TEXT="public health"/>
<node CREATED="1200851908934" ID="Freemind_Link_1592069444" MODIFIED="1200851910257" TEXT="police"/>
<node CREATED="1200851910803" ID="Freemind_Link_970794720" MODIFIED="1200851915245" TEXT="dept of telecom"/>
</node>
<node CREATED="1200851951766" ID="Freemind_Link_1559288641" MODIFIED="1200851956941" TEXT="data analysis">
<node CREATED="1200851963919" ID="Freemind_Link_1294955619" MODIFIED="1200851965909" TEXT="p. 86"/>
</node>
<node CREATED="1200852010924" ID="Freemind_Link_720011010" MODIFIED="1200852016513" TEXT="durant&apos;s question">
<node CREATED="1200851998058" ID="Freemind_Link_1249809420" MODIFIED="1200852008728" TEXT="do demands on the organizers impede scale up?"/>
</node>
</node>
<node CREATED="1200851807504" ID="Freemind_Link_1395212798" MODIFIED="1200851808929" TEXT="lesson">
<node CREATED="1200851809480" ID="Freemind_Link_1135961769" MODIFIED="1200851813506" TEXT="learning to let go">
<node CREATED="1200851969197" ID="Freemind_Link_1459893878" MODIFIED="1200851970851" TEXT="p. 89"/>
</node>
</node>
</node>
</node>
<node CREATED="1200846223854" ID="Freemind_Link_92845581" MODIFIED="1200846224745" TEXT="8">
<node CREATED="1200846007301" FOLDED="true" ID="Freemind_Link_1730081125" MODIFIED="1200846024421" TEXT="Ch 8&#xa;The Role of the Social Entrepreneur">
<node CREATED="1200855887934" ID="Freemind_Link_611504517" MODIFIED="1200855889184" TEXT="ideas">
<node CREATED="1200855866772" ID="Freemind_Link_519411042" MODIFIED="1200855869903" TEXT="ideas compete for">
<node CREATED="1200855870609" ID="Freemind_Link_1398050317" MODIFIED="1200855872538" TEXT="attention"/>
<node CREATED="1200855872998" ID="Freemind_Link_1154880655" MODIFIED="1200855877398" TEXT="legitimacy"/>
</node>
<node CREATED="1200855894321" ID="Freemind_Link_592241583" MODIFIED="1200855896462" TEXT="Victor Hugo">
<node CREATED="1200855898381" ID="Freemind_Link_233034739" MODIFIED="1200855943156" TEXT="&quot;There is one thing stronger than all the armies in the world, &#xa;and that is an idea whose time has come.&quot;"/>
</node>
<node CREATED="1200855959354" ID="Freemind_Link_1389212521" MODIFIED="1200855968901" TEXT="need&#xa;(like theatrical plays)">
<node CREATED="1200855969491" ID="Freemind_Link_1610196953" MODIFIED="1200855974049" TEXT="producer"/>
<node CREATED="1200855974967" ID="Freemind_Link_542822311" MODIFIED="1200855976249" TEXT="promoter"/>
</node>
</node>
<node CREATED="1200855984815" ID="Freemind_Link_668238210" MODIFIED="1200855986446" TEXT="book">
<node CREATED="1200856044573" LINK="http://books.google.com/books?id=1kgoTN2Jp_cC&amp;q=leading+change+the+argument+for+values&amp;dq=leading+change+the+argument+for+values&amp;ei=3JuTR9jYLYiA6wKeiZmlBw&amp;pgis=1" MODIFIED="1200856044573" TEXT="books.google.com &gt; Books ? ..."/>
<node CREATED="1200856053239" MODIFIED="1200856053239" TEXT="Leading Change: The Argument for Values-Based Leadership&#xd; By James O&apos;Toole"/>
</node>
<node CREATED="1200856069910" ID="Freemind_Link_1364763504" MODIFIED="1200856071680" TEXT="change">
<node CREATED="1200856072200" ID="Freemind_Link_337308546" MODIFIED="1200856079438" TEXT="resistance to change">
<node CREATED="1200856080526" ID="Freemind_Link_737880145" MODIFIED="1200856083723" TEXT="comes from">
<node CREATED="1200856084394" ID="Freemind_Link_178422097" MODIFIED="1200856094046" TEXT="desire not to have the will of others forced on you"/>
</node>
</node>
</node>
<node CREATED="1200856100340" ID="Freemind_Link_1350096774" MODIFIED="1200856101919" TEXT="example">
<node CREATED="1200856102431" ID="Freemind_Link_1739355470" MODIFIED="1200856105430" TEXT="Penny Post">
<node CREATED="1200856105877" ID="Freemind_Link_843127199" MODIFIED="1200856112129" TEXT="Rowland Hill">
<node CREATED="1200856115296" ID="Freemind_Link_1373804956" MODIFIED="1200856116985" TEXT="resistance">
<node CREATED="1200856117450" ID="Freemind_Link_922913031" MODIFIED="1200856119796" TEXT="postal workers"/>
</node>
<node CREATED="1200856121634" ID="Freemind_Link_857125489" MODIFIED="1200856123113" TEXT="support">
<node CREATED="1200856123574" ID="Freemind_Link_1293651063" MODIFIED="1200856126904" TEXT="newspapers"/>
</node>
</node>
<node CREATED="1200856128372" ID="Freemind_Link_747110375" MODIFIED="1200856135282" TEXT="took years to demostrate, finally win over"/>
</node>
</node>
<node CREATED="1200856143898" ID="Freemind_Link_1937257362" MODIFIED="1200856146388" TEXT="good marketting">
<node CREATED="1200856154994" ID="Freemind_Link_1033583966" MODIFIED="1200856169252" TEXT="anyone who can stop your project by saying &quot;no&quot; needs to say &quot;yes&quot;"/>
</node>
<node CREATED="1200856181657" ID="Freemind_Link_1760180869" MODIFIED="1200856191665" TEXT="evidence is not enough (marketting is important)">
<node CREATED="1200858280558" ID="Freemind_Link_69656220" MODIFIED="1200858288798" TEXT="Quakers freeing slaves">
<node CREATED="1200858292935" ID="Freemind_Link_263445835" MODIFIED="1200858320708" TEXT="1743 John Woolman walking campaign across NJ"/>
</node>
<node CREATED="1200858348309" ID="Freemind_Link_256533859" MODIFIED="1200858351972" TEXT="Unification of Europe">
<node CREATED="1200858352434" ID="Freemind_Link_885401362" MODIFIED="1200858358087" TEXT="Jean Monnet">
<node CREATED="1200858358796" ID="Freemind_Link_167197869" MODIFIED="1200858362096" TEXT="internationalist"/>
</node>
</node>
<node CREATED="1200856200181" ID="Freemind_Link_465273447" MODIFIED="1200856208969" TEXT="Anesthesia">
<node CREATED="1200856209528" ID="Freemind_Link_389143295" MODIFIED="1200856211459" TEXT="publicity"/>
<node CREATED="1200858168615" ID="Freemind_Link_1869278729" MODIFIED="1200858266285" TEXT="Ellison C. Pierce activated by death of friend&apos;s daughter 18 yr old daughter died from wrong dosage"/>
<node CREATED="1200858150624" ID="Freemind_Link_1452270566" MODIFIED="1200858164835" TEXT="standardized equipment reduced mistakes"/>
</node>
<node CREATED="1200856213148" ID="Freemind_Link_1731941315" MODIFIED="1200858036918" TEXT="High blood pressure&#xa;&quot;The Silent Killer&quot;">
<node CREATED="1200856222094" ID="Freemind_Link_245303501" MODIFIED="1200856229354" TEXT="successful after support">
<node CREATED="1200858017239" ID="Freemind_Link_1558070900" MODIFIED="1200858022636" TEXT="by Mary Laskers">
<node CREATED="1200858100464" ID="Freemind_Link_1190296577" MODIFIED="1200858142268" TEXT="she also supported the US gov&apos;t&apos;s &quot;War on Cancer&quot;,&#xa;generally regarded as &quot;having been misconceived&quot;"/>
</node>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1200846225277" ID="Freemind_Link_1131713036" MODIFIED="1200846225747" TEXT="9">
<node CREATED="1200846025077" FOLDED="true" ID="Freemind_Link_897182748" MODIFIED="1200846057683" TEXT="Ch 9&#xa;&quot;What Sort of Mother Are You?&quot;&#xa;Erzs&#xe9;bet Szekeres, Hungary: Assisted Living for the Disabled">
<node CREATED="1200858372831" ID="Freemind_Link_1748682847" MODIFIED="1200858385989" TEXT="Alliance Industrial Union"/>
<node CREATED="1200858386467" ID="Freemind_Link_1195816404" MODIFIED="1200858399289" TEXT="principles">
<node CREATED="1200858399753" ID="Freemind_Link_165807084" MODIFIED="1200858400325" TEXT="1">
<node CREATED="1200858400963" ID="Freemind_Link_1085681956" MODIFIED="1200858403213" TEXT="trust"/>
</node>
<node CREATED="1200858405092" ID="Freemind_Link_930378574" MODIFIED="1200858405533" TEXT="2">
<node CREATED="1200858407761" ID="Freemind_Link_1695759347" MODIFIED="1200858415570" TEXT="socialization"/>
</node>
<node CREATED="1200858406092" ID="Freemind_Link_58754287" MODIFIED="1200858406507" TEXT="3">
<node CREATED="1200858417258" ID="Freemind_Link_815160600" MODIFIED="1200858425197" TEXT="skills"/>
</node>
</node>
<node CREATED="1200858971844" ID="Freemind_Link_1435715657" MODIFIED="1200858990909" TEXT="treating people with dignity and giving them meaningful jobs works!"/>
<node CREATED="1200858994119" ID="Freemind_Link_1621409064" MODIFIED="1200859015392" TEXT="must understand that they are people and see the world as everyone else">
<node CREATED="1200859015820" ID="Freemind_Link_949075163" MODIFIED="1200859026313" TEXT="empty coffee cup example">
<node CREATED="1200859028377" ID="Freemind_Link_1632556214" MODIFIED="1200859037659" TEXT="facilitator thought it was a game"/>
<node CREATED="1200859038264" ID="Freemind_Link_1224281078" MODIFIED="1200859042124" TEXT="passed it back and forth"/>
<node CREATED="1200859042627" ID="Freemind_Link_1666328301" MODIFIED="1200859051131" TEXT="patient became angry/violent"/>
<node CREATED="1200859051680" ID="Freemind_Link_1711147343" MODIFIED="1200859057063" TEXT="was restrained and drugged"/>
<node CREATED="1200859059298" ID="Freemind_Link_651068406" MODIFIED="1200859065972" TEXT="empty cup probably meant he was thirsty"/>
</node>
</node>
<node CREATED="1200858428125" ID="Freemind_Link_770560588" MODIFIED="1200858430135" TEXT="strategy">
<node CREATED="1200858430655" ID="Freemind_Link_1052756355" MODIFIED="1200858437340" TEXT="workers are bored">
<node CREATED="1200858437797" ID="Freemind_Link_1295462332" MODIFIED="1200858442018" TEXT="change work either">
<node CREATED="1200858442540" ID="Freemind_Link_345330606" MODIFIED="1200858443881" TEXT="easier"/>
<node CREATED="1200858444441" ID="Freemind_Link_458234376" MODIFIED="1200858445801" TEXT="harder"/>
</node>
</node>
</node>
<node CREATED="1200858450099" ID="Freemind_Link_592305696" MODIFIED="1200858455988" TEXT="factors of success">
<node CREATED="1200858456478" ID="Freemind_Link_422774244" MODIFIED="1200858476072" TEXT="Hungary entering EU, needed to comply with standards of institutions"/>
</node>
</node>
</node>
<node CREATED="1200846226204" ID="Freemind_Link_1088382094" MODIFIED="1200846227093" TEXT="10">
<node CREATED="1200846058557" FOLDED="true" ID="Freemind_Link_409153625" MODIFIED="1200846086601" TEXT="Ch 10&#xa;Are They Posssessed, Really Possessed, by an Idea?">
<node CREATED="1200858641894" ID="Freemind_Link_1959960851" MODIFIED="1200858649714" TEXT="qualities of a social entrepreneur">
<node CREATED="1200858698165" ID="Freemind_Link_831166162" MODIFIED="1200858698726" TEXT="1">
<node CREATED="1200858664135" ID="Freemind_Link_1953185069" MODIFIED="1200858671035" TEXT="obsessed with how-to problems"/>
</node>
<node CREATED="1200858699413" ID="Freemind_Link_973901948" MODIFIED="1200858699878" TEXT="2">
<node CREATED="1200858707632" ID="Freemind_Link_1714144072" MODIFIED="1200858709903" TEXT="realistic"/>
</node>
<node CREATED="1200858700274" ID="Freemind_Link_1053146215" MODIFIED="1200858700705" TEXT="3">
<node CREATED="1200858711321" ID="Freemind_Link_751363241" MODIFIED="1200858744832" TEXT="driven by inherent value of the idea"/>
</node>
<node CREATED="1200858701056" ID="Freemind_Link_1771124407" MODIFIED="1200858701585" TEXT="4">
<node CREATED="1200858722068" ID="Freemind_Link_1441451938" MODIFIED="1200858726478" TEXT="ethical fiber / trust"/>
</node>
</node>
<node CREATED="1200858652559" ID="Freemind_Link_1123080378" MODIFIED="1200858657082" TEXT="goal">
<node CREATED="1200858657607" ID="Freemind_Link_618932210" MODIFIED="1200858658828" TEXT="setting"/>
<node CREATED="1200858659306" ID="Freemind_Link_785112525" MODIFIED="1200858660557" TEXT="solving"/>
</node>
<node CREATED="1200858677082" ID="Freemind_Link_1942475749" MODIFIED="1200858680752" TEXT="takes years to know the result">
<node CREATED="1200858681160" ID="Freemind_Link_1765864581" MODIFIED="1200858683990" TEXT="Grameen Bank"/>
</node>
</node>
</node>
<node CREATED="1200846227589" ID="Freemind_Link_981780927" MODIFIED="1200846228366" TEXT="11">
<node CREATED="1200846090914" FOLDED="true" ID="Freemind_Link_941795687" MODIFIED="1200846120101" TEXT="Ch 11&#xa;If the World Is to Be Put in Order&#xa;Vera Cordeiro, Brazil: Reformiong Healthcare">
<node CREATED="1200858755419" ID="Freemind_Link_1804655813" MODIFIED="1200858789929" TEXT="Renascer">
<node CREATED="1200858802854" ID="Freemind_Link_3040375" MODIFIED="1200858805775" TEXT="&quot;Rebirth&quot;"/>
</node>
<node CREATED="1200858843162" ID="Freemind_Link_1824488759" MODIFIED="1200858847242" TEXT="problem to solve">
<node CREATED="1200858847720" ID="Freemind_Link_23230362" MODIFIED="1200858869076" TEXT="children treated, but because they are poor return to the hosptial in a couple months"/>
<node CREATED="1200858891548" ID="Freemind_Link_1263050564" MODIFIED="1200858905516" TEXT="wasted effort in that system"/>
</node>
<node CREATED="1200858870895" ID="Freemind_Link_115727117" MODIFIED="1200858874024" TEXT="her solution">
<node CREATED="1200858874692" ID="Freemind_Link_1019658766" MODIFIED="1200858882422" TEXT="follow up care"/>
</node>
<node CREATED="1200858914061" ID="Freemind_Link_325806775" MODIFIED="1200858927908" TEXT="success">
<node CREATED="1200859081561" ID="Freemind_Link_799296361" MODIFIED="1200859084321" TEXT="step by step"/>
<node CREATED="1200859084901" ID="Freemind_Link_1087996157" MODIFIED="1200859088551" TEXT="shoe string budget">
<node CREATED="1200859139216" ID="Freemind_Link_757011276" MODIFIED="1200859142934" TEXT="so many volunteers"/>
</node>
<node CREATED="1200859089296" ID="Freemind_Link_1916730315" MODIFIED="1200859091560" TEXT="slowly grew"/>
<node CREATED="1200859147622" ID="Freemind_Link_131621128" MODIFIED="1200859150762" TEXT="it was working!"/>
<node CREATED="1200859092458" ID="Freemind_Link_1281071133" MODIFIED="1200859116913" TEXT="Cordeiro lavished thanks on everyone, did not seek limelight herself"/>
<node CREATED="1200859118910" ID="Freemind_Link_1430158909" MODIFIED="1200859125179" TEXT="local, then national, then international">
<node CREATED="1200859333567" ID="Freemind_Link_880981498" MODIFIED="1200859349134" TEXT="2001 Cordeiro was one of Brazils 10 women of the year"/>
</node>
</node>
<node CREATED="1200858929066" ID="Freemind_Link_1762539714" MODIFIED="1200859236564" TEXT="ashoka brought in McKinsey to consult">
<node CREATED="1200859237075" ID="Freemind_Link_125927912" MODIFIED="1200859246044" TEXT="McKinsey provides some expertise"/>
<node CREATED="1200859246503" ID="Freemind_Link_831988461" MODIFIED="1200859279765" TEXT="learns how social service groups operate and &#xa;succeed with so many constraints, so few resources"/>
<node CREATED="1200859305925" ID="Freemind_Link_1436448227" MODIFIED="1200859314494" TEXT="balancing financial and human considerations?"/>
</node>
</node>
</node>
<node CREATED="1200846228846" ID="Freemind_Link_1445843552" MODIFIED="1200846229407" TEXT="12">
<node CREATED="1200846144114" FOLDED="true" ID="Freemind_Link_1180146195" MODIFIED="1200846156423" TEXT="Ch 12&#xa;In Search of Social Excellence">
<node CREATED="1200864379573" ID="Freemind_Link_1611993793" MODIFIED="1200864385711" TEXT="some patterns observed">
<node CREATED="1200864386188" ID="Freemind_Link_1214168875" MODIFIED="1200864397058" TEXT="children in charge">
<node CREATED="1200864437146" ID="Freemind_Link_1880721217" MODIFIED="1200864439815" TEXT="problem solving"/>
<node CREATED="1200864440384" ID="Freemind_Link_628070813" MODIFIED="1200864444074" TEXT="decision making"/>
<node CREATED="1200864397670" ID="Freemind_Link_128623916" MODIFIED="1200864401291" TEXT="Bengladesh">
<node CREATED="1200864402705" ID="Freemind_Link_488727026" MODIFIED="1200864407346" TEXT="Ibrahim Soghan"/>
<node CREATED="1200864407862" ID="Freemind_Link_1816567114" MODIFIED="1200864432093" TEXT="10 child learning groups">
<node CREATED="1200864422009" ID="Freemind_Link_65164295" MODIFIED="1200864426320" TEXT="advanced childe teaches others"/>
</node>
</node>
</node>
<node CREATED="1200864453550" ID="Freemind_Link_1441408263" MODIFIED="1200864457620" TEXT="barefoot professionals">
<node CREATED="1200864458088" ID="Freemind_Link_1501866878" MODIFIED="1200864464266" TEXT="train">
<node CREATED="1200864464766" ID="Freemind_Link_1033936332" MODIFIED="1200864466568" TEXT="family"/>
<node CREATED="1200864467086" ID="Freemind_Link_294641014" MODIFIED="1200864469406" TEXT="neighborhood"/>
</node>
</node>
<node CREATED="1200864476843" ID="Freemind_Link_1392798149" MODIFIED="1200864482692" TEXT="legal framework for environmental reform">
<node CREATED="1200864487530" ID="Freemind_Link_1288881438" MODIFIED="1200864499851" TEXT="ownership -&gt; resource use rights"/>
</node>
<node CREATED="1200864509943" ID="Freemind_Link_1102956496" MODIFIED="1200864514254" TEXT="small producers capture profits">
<node CREATED="1200864518041" ID="Freemind_Link_1403191095" MODIFIED="1200864520621" TEXT="Grameen Bank"/>
<node CREATED="1200864514631" ID="Freemind_Link_48492134" MODIFIED="1200864516902" TEXT="SEWA"/>
<node CREATED="1200864523249" ID="Freemind_Link_702090711" MODIFIED="1200864528709" TEXT="Fair Trade"/>
<node CREATED="1200864532635" ID="Freemind_Link_709697685" MODIFIED="1200864544954" TEXT="APAEB">
<node CREATED="1200864549931" ID="Freemind_Link_1247886717" MODIFIED="1200864559688" TEXT="sisal farmers organized mills"/>
<node CREATED="1200864560148" ID="Freemind_Link_44203536" MODIFIED="1200864574488" TEXT="now multi million $ rug/rope business"/>
<node CREATED="1200864577583" ID="Freemind_Link_1053000929" MODIFIED="1200864583042" TEXT="Ismael Ferreira"/>
<node CREATED="1200864586800" ID="Freemind_Link_1783372228" MODIFIED="1200864590260" TEXT="Bahai Brazil"/>
</node>
</node>
<node CREATED="1200864616052" ID="Freemind_Link_246742535" MODIFIED="1200865835082" TEXT="Economic &amp; Environmental">
<node CREATED="1200864621272" ID="Freemind_Link_374820259" MODIFIED="1200864639688" TEXT="Two sides of the same coin - Fabio Rosa">
<node CREATED="1200864640255" ID="Freemind_Link_1217265698" MODIFIED="1200864659960" TEXT="Cordeira -&gt; health / social"/>
</node>
<node CREATED="1200864667917" ID="Freemind_Link_1221042739" MODIFIED="1200864670976" TEXT="Poland">
<node CREATED="1200864671664" ID="Freemind_Link_471234628" MODIFIED="1200864674925" TEXT="ECEAT">
<node CREATED="1200864706856" ID="Freemind_Link_965656307" MODIFIED="1200864714744" TEXT="family farming"/>
<node CREATED="1200864715283" ID="Freemind_Link_1341582291" MODIFIED="1200864722771" TEXT="open to tourism now"/>
<node CREATED="1200864723360" ID="Freemind_Link_1644572340" MODIFIED="1200864732009" TEXT="meets needs of city folk who want to get away"/>
</node>
</node>
</node>
<node CREATED="1200865837552" ID="Freemind_Link_475468293" MODIFIED="1200865847858" TEXT="Unleashing Resources in Community">
<node CREATED="1200865848378" ID="Freemind_Link_1792712794" MODIFIED="1200865850081" TEXT="CDI">
<node CREATED="1200865853256" ID="Freemind_Link_1045156858" MODIFIED="1200865860168" TEXT="information technology in slums"/>
<node CREATED="1200865901798" ID="Freemind_Link_1860777830" MODIFIED="1200865911109" TEXT="crossing the digital divide in Brazil"/>
<node CREATED="1200865861374" ID="Freemind_Link_199537564" MODIFIED="1200865866372" TEXT="Rodrigo Gaggio"/>
</node>
</node>
<node CREATED="1200865931762" ID="Freemind_Link_734113355" MODIFIED="1200865957711" TEXT="Linking Citizen, Government and Business Sectors&#xa;for comprehensive solutions">
<node CREATED="1200865972591" ID="Freemind_Link_1850330486" MODIFIED="1200865975901" TEXT="South Africa">
<node CREATED="1200865976445" ID="Freemind_Link_1853071121" MODIFIED="1200865986385" TEXT="Beulah Thumbadoo"/>
<node CREATED="1200865987371" ID="Freemind_Link_663869250" MODIFIED="1200865999173" TEXT="Easy Reading for Adults"/>
<node CREATED="1200865999718" ID="Freemind_Link_676940157" MODIFIED="1200866004126" TEXT="Everyone&apos;s Reading in Africa"/>
<node CREATED="1200866012059" ID="Freemind_Link_952818148" MODIFIED="1200866019589" TEXT="easy reading in indigenous languages"/>
</node>
<node CREATED="1200866059324" ID="Freemind_Link_1377849469" MODIFIED="1200866060256" TEXT="NY">
<node CREATED="1200866061193" ID="Freemind_Link_372415772" MODIFIED="1200866064556" TEXT="IAVI">
<node CREATED="1200866065198" ID="Freemind_Link_264857170" MODIFIED="1200866074675" TEXT="International Aides Vaccine Initiative"/>
<node CREATED="1200866106621" ID="Freemind_Link_1981799515" MODIFIED="1200866112233" TEXT="bringing together">
<node CREATED="1200866112812" ID="Freemind_Link_51201028" MODIFIED="1200866122448" TEXT="big pharma"/>
<node CREATED="1200866123001" ID="Freemind_Link_1502705853" MODIFIED="1200866127636" TEXT="biotech companies"/>
<node CREATED="1200866128300" ID="Freemind_Link_1233521643" MODIFIED="1200866134943" TEXT="academic labs"/>
<node CREATED="1200866135522" ID="Freemind_Link_264779789" MODIFIED="1200866139613" TEXT="governments"/>
<node CREATED="1200866140195" ID="Freemind_Link_1194599816" MODIFIED="1200866144038" TEXT="multilateral agencies"/>
<node CREATED="1200866144619" ID="Freemind_Link_1180135377" MODIFIED="1200866156069" TEXT="scientific research bodies"/>
<node CREATED="1200866156722" ID="Freemind_Link_1215794601" MODIFIED="1200866160385" TEXT="media organizations"/>
<node CREATED="1200866160904" ID="Freemind_Link_1809231855" MODIFIED="1200866166613" TEXT="NGO&apos;s"/>
</node>
<node CREATED="1200866082996" ID="Freemind_Link_1218812125" MODIFIED="1200866086607" TEXT="Foundations">
<node CREATED="1200866087231" ID="Freemind_Link_586780145" MODIFIED="1200866088806" TEXT="Gates"/>
<node CREATED="1200866089386" ID="Freemind_Link_940276587" MODIFIED="1200866091656" TEXT="Rockefeller"/>
</node>
</node>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1200846230111" ID="Freemind_Link_783770134" MODIFIED="1200846231062" TEXT="13">
<node CREATED="1200846157044" FOLDED="true" ID="Freemind_Link_1768432024" MODIFIED="1200846181872" TEXT="Ch 13&#xa;The Talent Is Out There&#xa;J. B. Scramm, United States: College Access">
<node CREATED="1200866243020" ID="Freemind_Link_392219283" MODIFIED="1200866247958" TEXT="College Summit">
<node CREATED="1200866453928" ID="Freemind_Link_971344996" MODIFIED="1200866458583" TEXT="J. B. Schramm">
<node CREATED="1200866462791" ID="Freemind_Link_1666943976" MODIFIED="1200866471323" TEXT="Harvard Divinity School"/>
<node CREATED="1200866474301" ID="Freemind_Link_1667812521" MODIFIED="1200866495839" TEXT="Noticed university wanted high performers from low income schools"/>
<node CREATED="1200866496415" ID="Freemind_Link_1250160559" MODIFIED="1200866507842" TEXT="those students also wanted to go to universities"/>
<node CREATED="1200866575910" ID="Freemind_Link_704583343" MODIFIED="1200866582250" TEXT="&quot;college market gap&quot;"/>
<node CREATED="1200866595455" ID="Freemind_Link_524589959" MODIFIED="1200866604694" TEXT="try to find students who were &quot;better than their numbers suggested&quot;"/>
<node CREATED="1200866510681" ID="Freemind_Link_174693993" MODIFIED="1200866530405" TEXT="measurement tools (grades) were not true indicators of students&apos; potential"/>
<node CREATED="1200866530973" ID="Freemind_Link_626932017" MODIFIED="1200866560377" TEXT="solution : teach students to write essays that demonstrate what they&apos;ve got"/>
</node>
<node CREATED="1200866276199" ID="Freemind_Link_1551158820" MODIFIED="1200866283169" TEXT="addresses a market failure: coordination"/>
<node CREATED="1200866286977" ID="Freemind_Link_871841350" MODIFIED="1200866294326" TEXT="success assisted by">
<node CREATED="1200866294781" ID="Freemind_Link_1185473746" MODIFIED="1200866302783" TEXT="funders"/>
</node>
<node CREATED="1200866304321" ID="Freemind_Link_1956428952" MODIFIED="1200866306752" TEXT="chronology">
<node CREATED="1200866308130" ID="Freemind_Link_654594436" MODIFIED="1200866311740" TEXT="1">
<node CREATED="1200866325185" ID="Freemind_Link_741731653" MODIFIED="1200866332956" TEXT="shoestring budget + volunteers"/>
</node>
<node CREATED="1200866312279" ID="Freemind_Link_1988389577" MODIFIED="1200866313000" TEXT="2">
<node CREATED="1200866334162" ID="Freemind_Link_1232941649" MODIFIED="1200866343166" TEXT="some success -&gt; small growth"/>
</node>
<node CREATED="1200866313422" ID="Freemind_Link_1466476458" MODIFIED="1200866314139" TEXT="3">
<node CREATED="1200866344751" ID="Freemind_Link_1426723373" MODIFIED="1200866356374" TEXT="bring in expertise">
<node CREATED="1200866356855" ID="Freemind_Link_1444835475" MODIFIED="1200866363635" TEXT="hard choices">
<node CREATED="1200866364180" ID="Freemind_Link_1983812137" MODIFIED="1200866376109" TEXT="drop states that aren&apos;t showing results"/>
</node>
</node>
</node>
<node CREATED="1200866314359" ID="Freemind_Link_78999001" MODIFIED="1200866315039" TEXT="4">
<node CREATED="1200866382419" ID="Freemind_Link_817559988" MODIFIED="1200866384878" TEXT="recognition">
<node CREATED="1200866390296" ID="Freemind_Link_1600698087" MODIFIED="1200866395065" TEXT="Chicago School System"/>
</node>
</node>
<node CREATED="1200866315306" ID="Freemind_Link_1171943851" MODIFIED="1200866316639" TEXT="5">
<node CREATED="1200866399133" ID="Freemind_Link_1396710233" MODIFIED="1200866406161" TEXT="rave reviews">
<node CREATED="1200866406786" ID="Freemind_Link_1957891101" MODIFIED="1200866409280" TEXT="returning customers"/>
<node CREATED="1200866409847" ID="Freemind_Link_973358057" MODIFIED="1200866415281" TEXT="bigger donors"/>
<node CREATED="1200866419838" ID="Freemind_Link_1856318321" MODIFIED="1200866429266" TEXT="attracts more talent (speakers)"/>
</node>
</node>
</node>
</node>
<node CREATED="1200866248567" ID="Freemind_Link_1199044559" MODIFIED="1200866250310" TEXT="Firms">
<node CREATED="1200866250799" ID="Freemind_Link_1752643857" MODIFIED="1200866272633" TEXT="arise when: one-on-one transactions don&apos;t coordinate well enough"/>
</node>
</node>
</node>
<node CREATED="1200846259870" ID="Freemind_Link_723391617" MODIFIED="1200846261095" TEXT="14">
<node CREATED="1200846261842" FOLDED="true" ID="Freemind_Link_1809889470" MODIFIED="1200846279971" TEXT="Ch 14&#xa;New Opportunities, New Challenges&#xa;">
<node CREATED="1200867587392" ID="Freemind_Link_571546236" MODIFIED="1200867591732" TEXT="Ashoka meets Avina">
<node CREATED="1200867802719" ID="Freemind_Link_91884165" MODIFIED="1200867805831" TEXT="Avina foundation">
<node CREATED="1200867806328" ID="Freemind_Link_517746845" MODIFIED="1200867822494" TEXT="promote sustainable development"/>
<node CREATED="1200867823067" ID="Freemind_Link_183798701" MODIFIED="1200867826451" TEXT="esp in Latin America"/>
<node CREATED="1200867832237" ID="Freemind_Link_471529855" MODIFIED="1200867839156" TEXT="strategy is to invest in leaders"/>
</node>
<node CREATED="1200868154854" ID="Freemind_Link_1584388983" MODIFIED="1200868159829" TEXT="Ashoka&apos;s reputation">
<node CREATED="1200868160337" ID="Freemind_Link_1762438507" MODIFIED="1200868171169" TEXT="indigenous development"/>
<node CREATED="1200868172277" ID="Freemind_Link_2509209" MODIFIED="1200868175387" TEXT="investing in leaders"/>
</node>
</node>
<node CREATED="1200868181055" ID="Freemind_Link_1347621412" MODIFIED="1200868187438" TEXT="Africa had different problems">
<node CREATED="1200868192592" ID="Freemind_Link_1695253218" MODIFIED="1200868214683" TEXT="They underestimated extent of problems &#xa;and didn&apos;t raise enough money at first"/>
</node>
<node CREATED="1200868248818" ID="Freemind_Link_1935199050" MODIFIED="1200868252154" TEXT="race issue">
<node CREATED="1200868252669" ID="Freemind_Link_1309302175" MODIFIED="1200868361892" TEXT="first leaders found often upper and middle classes">
<node CREATED="1200868362763" ID="Freemind_Link_1005100270" MODIFIED="1200868362763" TEXT="have  resources, information and connections"/>
<node CREATED="1200868367721" ID="Freemind_Link_1848397665" MODIFIED="1200868372708" TEXT="were white"/>
</node>
<node CREATED="1200868269708" ID="Freemind_Link_1306292740" MODIFIED="1200868290256" TEXT="took a while to find non-white fellows"/>
</node>
<node CREATED="1200868223183" ID="Freemind_Link_910846604" MODIFIED="1200868224952" TEXT="2 dangers">
<node CREATED="1200868225437" ID="Freemind_Link_1061830810" MODIFIED="1200868232930" TEXT="lose focus for being"/>
<node CREATED="1200868233479" ID="Freemind_Link_958225824" MODIFIED="1200868239318" TEXT="lose global unity"/>
</node>
</node>
</node>
<node CREATED="1200846282307" ID="Freemind_Link_1473211659" MODIFIED="1200846283487" TEXT="15">
<node CREATED="1200846284028" FOLDED="true" ID="Freemind_Link_337926683" MODIFIED="1200846316632" TEXT="Ch 15&#xa;Something Needed to Be Done&#xa;Veronica Khosa, South Africa: Care for AIDS Patients">
<node CREATED="1200868620883" ID="Freemind_Link_1186415566" MODIFIED="1200868622805" TEXT="Tateni">
<node CREATED="1200868623268" ID="Freemind_Link_216267397" MODIFIED="1200868625214" TEXT="1">
<node CREATED="1200868625953" ID="Freemind_Link_1390004412" MODIFIED="1200868631173" TEXT="complement formal health care"/>
</node>
<node CREATED="1200868632270" ID="Freemind_Link_560356517" MODIFIED="1200868633021" TEXT="2">
<node CREATED="1200868633459" ID="Freemind_Link_1327606983" MODIFIED="1200868646379" TEXT="seek partnerships with community organizations"/>
</node>
<node CREATED="1200868647675" ID="Freemind_Link_301750934" MODIFIED="1200868648112" TEXT="3">
<node CREATED="1200868648616" ID="Freemind_Link_1911444982" MODIFIED="1200868661326" TEXT="enhance home care skills of family members"/>
</node>
<node CREATED="1200868662302" ID="Freemind_Link_1951800870" MODIFIED="1200868662993" TEXT="4">
<node CREATED="1200868663390" ID="Freemind_Link_1006336853" MODIFIED="1200868677158" TEXT="involve community in major decisions of Tateni">
<node CREATED="1200868848187" ID="Freemind_Link_1373064911" MODIFIED="1200868858755" TEXT="recognition that AIDS care is stigmitized"/>
<node CREATED="1200868859383" ID="Freemind_Link_935670053" MODIFIED="1200868882518" TEXT="if they offere general care, this problem goes away"/>
</node>
</node>
</node>
<node CREATED="1200868686894" ID="Freemind_Link_1953338381" MODIFIED="1200868695303" TEXT="Khosa had to scavenge for resources">
<node CREATED="1200868695772" ID="Freemind_Link_1667889390" MODIFIED="1200868713880" TEXT="she did not have elite wealthy contacts"/>
</node>
<node CREATED="1200868818247" ID="Freemind_Link_948269414" MODIFIED="1200868835172" TEXT="initially had problems with securing what they had"/>
<node CREATED="1200868836011" ID="Freemind_Link_1916573082" MODIFIED="1200868841513" TEXT="eventually moved into a safer place"/>
</node>
</node>
<node CREATED="1200846318603" ID="Freemind_Link_1572762472" MODIFIED="1200846320277" TEXT="16">
<node CREATED="1200846321075" FOLDED="true" ID="Freemind_Link_310828911" MODIFIED="1200846335922" TEXT="Ch 16&#xa;Four Practices of Innovative Organizations">
<node CREATED="1200868896773" ID="Freemind_Link_773449561" MODIFIED="1200868901313" TEXT="1">
<node CREATED="1200868902270" ID="Freemind_Link_355475179" MODIFIED="1200868910836" TEXT="Listening to Clients">
<node CREATED="1200868911437" ID="Freemind_Link_751064738" MODIFIED="1200868914974" TEXT="find matches">
<node CREATED="1200869007159" ID="Freemind_Link_1135160650" MODIFIED="1200869017582" TEXT="Poland&#xa;&quot;Sharing the things we have&quot;">
<node CREATED="1200868915517" ID="Freemind_Link_656181177" MODIFIED="1200868925258" TEXT="potatoes for trips to the city">
<node CREATED="1200868925773" ID="Freemind_Link_1744671627" MODIFIED="1200868930653" TEXT="farmers donate potatoes"/>
<node CREATED="1200868931961" ID="Freemind_Link_1162163528" MODIFIED="1200868945390" TEXT="city folk offer rides and tours of city to farmers&apos; children"/>
</node>
<node CREATED="1200868952545" ID="Freemind_Link_775873568" MODIFIED="1200868959947" TEXT="computer training for ramps">
<node CREATED="1200868960433" ID="Freemind_Link_1375013482" MODIFIED="1200868973241" TEXT="disabled in city are trained in computer skills"/>
<node CREATED="1200868974129" ID="Freemind_Link_154389622" MODIFIED="1200868985226" TEXT="trade:"/>
<node CREATED="1200868985818" ID="Freemind_Link_257114823" MODIFIED="1200869000823" TEXT="build ramps and visit country for computer lessons"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1200869035251" ID="Freemind_Link_1662850935" MODIFIED="1200869036020" TEXT="2">
<node CREATED="1200869036516" ID="Freemind_Link_892723387" MODIFIED="1200869041272" TEXT="Attention to the Exceptional">
<node CREATED="1200869206311" ID="Freemind_Link_765385526" MODIFIED="1200869213504" TEXT="Barka Foundation for Promotion of Mutual Help">
<node CREATED="1200869430845" ID="Freemind_Link_303571026" LINK="http://www.barka.org.pl/index_eng.htm" MODIFIED="1200869430845" TEXT="barka.org.pl &gt; Index eng"/>
<node CREATED="1200869197712" ID="Freemind_Link_1814051113" MODIFIED="1200869200773" TEXT="Western Poland"/>
<node CREATED="1200869190568" ID="Freemind_Link_1385815297" MODIFIED="1200869197184" TEXT="Thomaz Sadowski">
<node CREATED="1200869931545" ID="Freemind_Link_1842340427" MODIFIED="1200869938515" TEXT="purchased abandoned school house">
<node CREATED="1200869943781" ID="Freemind_Link_31792317" MODIFIED="1200869947991" TEXT="homeless"/>
<node CREATED="1200869948520" ID="Freemind_Link_479137675" MODIFIED="1200869951980" TEXT="former prison inmates"/>
</node>
</node>
<node CREATED="1200869828768" ID="Freemind_Link_938427949" MODIFIED="1200869834036" TEXT="No">
<node CREATED="1200869834603" ID="Freemind_Link_379494872" MODIFIED="1200869839304" TEXT="driking / drugs">
<node CREATED="1200869898005" ID="Freemind_Link_1478192387" MODIFIED="1200869904665" TEXT="so as not to tempt the others"/>
</node>
<node CREATED="1200869839863" ID="Freemind_Link_1535920911" MODIFIED="1200869841154" TEXT="violence"/>
</node>
<node CREATED="1200869878792" ID="Freemind_Link_1191748270" MODIFIED="1200869895128" TEXT="if kicked out, you may apply to re-join one year later"/>
<node CREATED="1200869909691" ID="Freemind_Link_1084380842" MODIFIED="1200869925230" TEXT="2003 was 20 locations and counting"/>
</node>
</node>
</node>
<node CREATED="1200870062857" ID="Freemind_Link_83417856" MODIFIED="1200870064247" TEXT="3">
<node CREATED="1200870064805" ID="Freemind_Link_464938897" MODIFIED="1200870066586" TEXT="Realism">
<node CREATED="1200870069174" ID="Freemind_Link_511479048" MODIFIED="1200870087092" TEXT="realistic about human behavior">
<node CREATED="1200870087503" ID="Freemind_Link_1241745576" MODIFIED="1200870093424" TEXT="AIDS Brazil">
<node CREATED="1200870093903" ID="Freemind_Link_1660732593" MODIFIED="1200870170911" TEXT="1st self respect from knowledge about">
<node CREATED="1200870101491" ID="Freemind_Link_1227087106" MODIFIED="1200870141874" TEXT="rights"/>
<node CREATED="1200870142413" ID="Freemind_Link_1728043868" MODIFIED="1200870176344" TEXT="bodies"/>
<node CREATED="1200870144623" ID="Freemind_Link_874494608" MODIFIED="1200870146894" TEXT="blackness"/>
<node CREATED="1200870147520" ID="Freemind_Link_1188130570" MODIFIED="1200870154220" TEXT="situation as a woman"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1200870276234" ID="Freemind_Link_22959533" MODIFIED="1200870277614" TEXT="4">
<node CREATED="1200870278164" ID="Freemind_Link_1519522486" MODIFIED="1200870283739" TEXT="Human Qualities">
<node CREATED="1200870284370" ID="Freemind_Link_1024332327" MODIFIED="1200870288768" TEXT="intrapreneur">
<node CREATED="1200870289290" ID="Freemind_Link_1598511599" MODIFIED="1200870294020" TEXT="empathy"/>
<node CREATED="1200870294537" ID="Freemind_Link_1160645370" MODIFIED="1200870301596" TEXT="flexible thinking"/>
<node CREATED="1200870302225" ID="Freemind_Link_1614974318" MODIFIED="1200870308455" TEXT="strong &quot;inner core&quot;"/>
<node CREATED="1200870308938" ID="Freemind_Link_30016065" MODIFIED="1200870312353" TEXT="ethical"/>
<node CREATED="1200870320901" ID="Freemind_Link_551335396" MODIFIED="1200870324280" TEXT="innovators for the public"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1200846337809" ID="Freemind_Link_443391207" MODIFIED="1200846339210" TEXT="17">
<node CREATED="1200846339832" FOLDED="true" ID="Freemind_Link_1559595285" MODIFIED="1200846372763" TEXT="Ch 17&#xa;This Country Has to Change&#xa;Javed Abidi, India: Disability Rights">
<node CREATED="1200870333486" ID="Freemind_Link_1818570878" MODIFIED="1200870344574" TEXT="NCPEDP"/>
<node CREATED="1200870371976" ID="Freemind_Link_1010896722" MODIFIED="1200870374725" TEXT="breakthrough">
<node CREATED="1200870375344" ID="Freemind_Link_1246247175" MODIFIED="1200870384632" TEXT="journalist">
<node CREATED="1200870386051" ID="Freemind_Link_778416474" MODIFIED="1200870404205" TEXT="1st told newspapers to give him a deadline and he would worry about getting the story"/>
<node CREATED="1200870404945" ID="Freemind_Link_48007409" MODIFIED="1200870412243" TEXT="no one offered him the chance"/>
<node CREATED="1200870412702" ID="Freemind_Link_934924319" MODIFIED="1200870445554" TEXT="when they were desperate for a story once, he managed to get it"/>
<node CREATED="1200870446113" ID="Freemind_Link_815890047" MODIFIED="1200870450144" TEXT="they never doubted him again"/>
</node>
</node>
<node CREATED="1200870463878" ID="Freemind_Link_1567962481" MODIFIED="1200870472996" TEXT="recognizes necessity for political will"/>
<node CREATED="1200870473439" ID="Freemind_Link_24776903" MODIFIED="1200870479325" TEXT="Pressure">
<node CREATED="1200870479814" ID="Freemind_Link_938113803" MODIFIED="1200870480883" TEXT="rallies"/>
<node CREATED="1200870481415" ID="Freemind_Link_154710452" MODIFIED="1200870483513" TEXT="strikes"/>
<node CREATED="1200870483986" ID="Freemind_Link_1551259007" MODIFIED="1200870489511" TEXT="lawsuits"/>
<node CREATED="1200870490129" ID="Freemind_Link_1869295234" MODIFIED="1200870495831" TEXT="publicity (celebrities)">
<node CREATED="1200870506445" ID="Freemind_Link_1613744161" MODIFIED="1200870513834" TEXT="Steven Hawking"/>
</node>
<node CREATED="1200870498338" ID="Freemind_Link_1118738152" MODIFIED="1200870505877" TEXT="ridicule"/>
</node>
</node>
</node>
<node CREATED="1200846417705" ID="Freemind_Link_1038684644" MODIFIED="1200846418729" TEXT="18">
<node CREATED="1200846419339" FOLDED="true" ID="Freemind_Link_1096867576" MODIFIED="1200846435908" TEXT="Ch 18&#xa;Six Qualities of Successful Social Entrepreneurs">
<node CREATED="1200870908136" ID="Freemind_Link_569433826" MODIFIED="1200870910991" TEXT="success">
<node CREATED="1200870911435" ID="Freemind_Link_1705035916" MODIFIED="1200870930431" TEXT="not as expected">
<node CREATED="1200870930909" ID="Freemind_Link_880368529" MODIFIED="1200870933519" TEXT="confident"/>
<node CREATED="1200870933991" ID="Freemind_Link_237704074" MODIFIED="1200870936519" TEXT="persistent"/>
<node CREATED="1200870937005" ID="Freemind_Link_1736269249" MODIFIED="1200870939908" TEXT="knowledgeable"/>
</node>
<node CREATED="1200870941977" ID="Freemind_Link_214262339" MODIFIED="1200870943197" TEXT="rather">
<node CREATED="1200870944024" ID="Freemind_Link_70704363" MODIFIED="1200870949244" TEXT="motivated">
<node CREATED="1200870949703" ID="Freemind_Link_712874089" MODIFIED="1200870956723" TEXT="more">
<node CREATED="1200870957936" ID="Freemind_Link_1772845192" MODIFIED="1200870957936" TEXT="systematic">
<node CREATED="1200870977925" ID="Freemind_Link_1236969506" MODIFIED="1200870983411" TEXT="in searching for opportunities"/>
</node>
<node CREATED="1200870958599" ID="Freemind_Link_843015599" MODIFIED="1200870972678" TEXT="anticipatory of obstacles"/>
<node CREATED="1200870985953" ID="Freemind_Link_15412727" MODIFIED="1200870995401" TEXT="monitoring of results"/>
<node CREATED="1200870997191" ID="Freemind_Link_159747644" MODIFIED="1200871000140" TEXT="planning ahead"/>
</node>
</node>
<node CREATED="1200871007107" ID="Freemind_Link_726565001" MODIFIED="1200871013321" TEXT="quality and efficiency minded"/>
<node CREATED="1200871013814" ID="Freemind_Link_1725386584" MODIFIED="1200871031373" TEXT="committed to people they employ/parnter with"/>
<node CREATED="1200871037077" ID="Freemind_Link_752265422" MODIFIED="1200871047355" TEXT="long term over short term gain"/>
<node CREATED="1200871095140" ID="Freemind_Link_884316741" MODIFIED="1200871097402" TEXT="willingness to">
<node CREATED="1200871058770" ID="Freemind_Link_1613251045" MODIFIED="1200871470363" TEXT="self correct">
<node CREATED="1200871065499" ID="Freemind_Link_1674096394" MODIFIED="1200871089374" TEXT="even and especially if changing course will be hard"/>
</node>
<node CREATED="1200871097891" ID="Freemind_Link_1420947380" MODIFIED="1200871102090" TEXT="share credit"/>
<node CREATED="1200871102639" ID="Freemind_Link_363484852" MODIFIED="1200871112132" TEXT="break forces of established structures"/>
<node CREATED="1200871113636" ID="Freemind_Link_1910597725" MODIFIED="1200871126425" TEXT="cross disciplinary boundaries">
<node CREATED="1200871127120" ID="Freemind_Link_1761821802" MODIFIED="1200871134300" TEXT="&quot;creative combining&quot;"/>
<node CREATED="1200871153642" ID="Freemind_Link_1302514288" MODIFIED="1200871173409" TEXT="people have &quot;whole&quot; needs (not just narrow ones first imagined)"/>
</node>
<node CREATED="1200871141228" ID="Freemind_Link_1935355644" MODIFIED="1200871152045" TEXT="work quietly"/>
</node>
<node CREATED="1200871181666" ID="Freemind_Link_1897953900" MODIFIED="1200871186024" TEXT="strong ethical impetus">
<node CREATED="1200871201459" ID="Freemind_Link_138497590" MODIFIED="1200871213215" TEXT="sometimes">
<node CREATED="1200871186523" ID="Freemind_Link_1234130992" MODIFIED="1200871201009" TEXT="person with strong values while growing up (role model)"/>
<node CREATED="1200871215025" ID="Freemind_Link_1350759285" MODIFIED="1200871222976" TEXT="pain had branded them and they feel compelled"/>
</node>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1200846437230" ID="Freemind_Link_149600476" MODIFIED="1200846439041" TEXT="19">
<node CREATED="1200846439646" FOLDED="true" ID="Freemind_Link_1943218185" MODIFIED="1200871360533" TEXT="Ch 19&#xa;Morality Must March with Capacity&#xa;James P. Grant, United States: The Child Survival Revolution">
<node CREATED="1200871238257" ID="Freemind_Link_127834818" MODIFIED="1200871352186" TEXT="UNICEF 1980&apos;s - 1990&apos;s">
<node CREATED="1200871503228" ID="Freemind_Link_691429353" MODIFIED="1200871510120" TEXT="ORT oral rehydration therapy">
<node CREATED="1200871710928" ID="Freemind_Link_1901285673" MODIFIED="1200871715557" TEXT="10 cents to save a life">
<node CREATED="1200871721455" ID="Freemind_Link_1807987067" MODIFIED="1200871737931" TEXT="glucose to hold liquids"/>
<node CREATED="1200871738483" ID="Freemind_Link_1747825499" MODIFIED="1200871742499" TEXT="add sugar to salt water"/>
</node>
</node>
<node CREATED="1200871515765" ID="Freemind_Link_1560770118" MODIFIED="1200871522431" TEXT="ORS oral rehydration salts"/>
</node>
<node CREATED="1200871369590" ID="Freemind_Link_32215391" MODIFIED="1200871372918" TEXT="James P. Grant">
<node CREATED="1200871531921" ID="Freemind_Link_1679772752" MODIFIED="1200871548269" TEXT="born 1922 in Peking to missionary parents"/>
<node CREATED="1200871755285" ID="Freemind_Link_1431404737" MODIFIED="1200871774961" TEXT="inspired by lecture">
<node CREATED="1200871775486" ID="Freemind_Link_985025057" MODIFIED="1200871781119" TEXT="&quot;Why the other half dies&quot;">
<node CREATED="1200871782166" ID="Freemind_Link_1818664522" MODIFIED="1200871786196" TEXT="John Rohdes"/>
</node>
</node>
<node CREATED="1200871373409" ID="Freemind_Link_255525635" MODIFIED="1200871377168" TEXT="self effacing"/>
<node CREATED="1200871377675" ID="Freemind_Link_471765067" MODIFIED="1200871383317" TEXT="pure ethics"/>
<node CREATED="1200871386363" ID="Freemind_Link_434424171" MODIFIED="1200871396316" TEXT="enabled entrepreneurs within the organization to thrive">
<node CREATED="1200871638189" ID="Freemind_Link_573898490" MODIFIED="1200871647770" TEXT="improved greeting card business"/>
</node>
<node CREATED="1200871401160" ID="Freemind_Link_1086842957" MODIFIED="1200871409018" TEXT="grew the budget of UNICEF">
<node CREATED="1200871579308" ID="Freemind_Link_1014608943" MODIFIED="1200871592524" TEXT="during his leadership $313 million to $1 billion"/>
</node>
<node CREATED="1200871602810" ID="Freemind_Link_1968433728" MODIFIED="1200871614634" TEXT="brought groups together" VSHIFT="28">
<node CREATED="1200871622134" ID="Freemind_Link_1577333783" MODIFIED="1200871626376" TEXT="red cross"/>
<node CREATED="1200871626910" ID="Freemind_Link_239406021" MODIFIED="1200871629453" TEXT="rotary club"/>
<node CREATED="1200871630031" ID="Freemind_Link_1039669506" MODIFIED="1200871632982" TEXT="catholic church"/>
<node CREATED="1200871659752" ID="Freemind_Link_1884567391" MODIFIED="1200871668214" TEXT="WHO + UNICEF + Rockefeller"/>
</node>
<node CREATED="1200871669511" ID="Freemind_Link_679303470" MODIFIED="1200871677472" TEXT="access to heads of state">
<node CREATED="1200871678086" ID="Freemind_Link_570369672" MODIFIED="1200871680048" TEXT="persistence">
<node CREATED="1200871693552" ID="Freemind_Link_611335614" MODIFIED="1200871705824" TEXT="every time he saw them, he&apos;d take out the ORS"/>
</node>
<node CREATED="1200871680549" ID="Freemind_Link_1379704596" MODIFIED="1200871683398" TEXT="competition">
<node CREATED="1200871683895" ID="Freemind_Link_671958335" MODIFIED="1200871691144" TEXT="&quot;You know, Columbia is doing it&quot;"/>
</node>
<node CREATED="1200871919445" ID="Freemind_Link_847006110" MODIFIED="1200871931063" TEXT="reiterated their promises in letters when leaving their countries"/>
</node>
</node>
</node>
</node>
<node CREATED="1200846551056" ID="Freemind_Link_1593799386" MODIFIED="1200846551967" TEXT="20">
<node CREATED="1200846552477" FOLDED="true" ID="Freemind_Link_732430614" MODIFIED="1200846562402" TEXT="Ch 20&#xa;Blueprint Copying">
<node CREATED="1200873365629" ID="Freemind_Link_1378847059" MODIFIED="1200873404721" TEXT="&lt;html&gt;&#xa;&#xa;&lt;table style=&quot;width: 100%; text-align: left;&quot; border=&quot;0&quot; cellpadding=&quot;2&quot;&#xa;cellspacing=&quot;2&quot;&gt;&#xa;&lt;tbody&gt;&#xa;&lt;tr&gt;&#xa;&lt;td style=&quot;vertical-align: top;&quot;&gt;&lt;br&gt;&#xa;&lt;/td&gt;&#xa;&lt;td&#xa;style=&quot;vertical-align: top; text-align: center; font-weight: bold;&quot;&gt;countries&lt;br&gt;&#xa;&lt;/td&gt;&#xa;&lt;td&#xa;style=&quot;vertical-align: top; text-align: center; font-weight: bold;&quot;&gt;fellows&lt;br&gt;&#xa;&lt;/td&gt;&#xa;&lt;/tr&gt;&#xa;&lt;tr&gt;&#xa;&lt;td style=&quot;vertical-align: top; font-weight: bold;&quot;&gt;1990&lt;br&gt;&#xa;&lt;/td&gt;&#xa;&lt;td style=&quot;vertical-align: top; text-align: center;&quot;&gt;8&lt;br&gt;&#xa;&lt;/td&gt;&#xa;&lt;td style=&quot;vertical-align: top; text-align: center;&quot;&gt;200&lt;br&gt;&#xa;&lt;/td&gt;&#xa;&lt;/tr&gt;&#xa;&lt;tr&gt;&#xa;&lt;td style=&quot;vertical-align: top; font-weight: bold;&quot;&gt;2003&lt;br&gt;&#xa;&lt;/td&gt;&#xa;&lt;td style=&quot;vertical-align: top; text-align: center;&quot;&gt;46&lt;br&gt;&#xa;&lt;/td&gt;&#xa;&lt;td style=&quot;vertical-align: top; text-align: center;&quot;&gt;1,400&lt;br&gt;&#xa;&lt;/td&gt;&#xa;&lt;/tr&gt;&#xa;&lt;/tbody&gt;&#xa;&lt;/table&gt;"/>
<node CREATED="1200873434658" LINK="http://www.changemakers.net/" MODIFIED="1200873434658" TEXT="changemakers.net"/>
<node CREATED="1200873548004" ID="Freemind_Link_946548486" MODIFIED="1200873551542" TEXT="Jared Diamond">
<node CREATED="1200873552141" ID="Freemind_Link_860106428" MODIFIED="1200873556370" TEXT="Guns, Germs and Steel">
<node CREATED="1200873556860" ID="Freemind_Link_1034279158" MODIFIED="1200873561230" TEXT="slower">
<node CREATED="1200873561749" ID="Freemind_Link_499159466" MODIFIED="1200873563928" TEXT="idea diffusion"/>
</node>
<node CREATED="1200873565080" ID="Freemind_Link_485810679" MODIFIED="1200873566677" TEXT="faster">
<node CREATED="1200873567186" ID="Freemind_Link_255735240" MODIFIED="1200873571677" TEXT="blueprint copying"/>
</node>
</node>
</node>
<node CREATED="1200873577762" ID="Freemind_Link_492897772" MODIFIED="1200873584669" TEXT="greatest challenge of these projects">
<node CREATED="1200873585134" ID="Freemind_Link_1711110111" MODIFIED="1200873590366" TEXT="long term financial stability"/>
</node>
<node CREATED="1200873895872" ID="Freemind_Link_259789086" MODIFIED="1200873899458" TEXT="citizen base initiative">
<node CREATED="1200873900180" ID="Freemind_Link_919866031" MODIFIED="1200873900907" TEXT="1">
<node CREATED="1200873901408" ID="Freemind_Link_1404717442" MODIFIED="1200873915850" TEXT="identify fellows who had build stabel, decnetralized bases"/>
</node>
<node CREATED="1200873917533" ID="Freemind_Link_36403733" MODIFIED="1200873918143" TEXT="2">
<node CREATED="1200873918650" ID="Freemind_Link_1671628125" MODIFIED="1200873941506" TEXT="analyze their strategies">
<node CREATED="1200873942124" ID="Freemind_Link_1951720429" MODIFIED="1200873947869" TEXT="citizen base competitions"/>
</node>
</node>
</node>
<node CREATED="1200873954971" ID="Freemind_Link_1077076544" MODIFIED="1200873965501" TEXT="2002 Leslie Crutchfield HBS">
<node CREATED="1200873966028" ID="Freemind_Link_925677781" MODIFIED="1200873978601" TEXT="Ashoka&apos;s Accelerator for Social Entrepreneurship">
<node CREATED="1200873979266" ID="Freemind_Link_14672470" MODIFIED="1200873983045" TEXT="matches fellows with firms"/>
</node>
<node CREATED="1200874609784" ID="Freemind_Link_1578326391" MODIFIED="1200874619546" TEXT="Open Sourcing Social Solutions"/>
</node>
<node CREATED="1200876707789" ID="Freemind_Link_253252065" MODIFIED="1200876714539" TEXT="scientific study?"/>
<node CREATED="1200876715208" ID="Freemind_Link_545728284" MODIFIED="1200876720282" TEXT="capability to spread?">
<node CREATED="1200876724809" ID="Freemind_Link_1381640023" MODIFIED="1200876726669" TEXT="simpler"/>
<node CREATED="1200876727281" ID="Freemind_Link_1916174216" MODIFIED="1200876729772" TEXT="cheaper"/>
<node CREATED="1200876730608" ID="Freemind_Link_1700381388" MODIFIED="1200876732790" TEXT="less partisan"/>
<node CREATED="1200876735584" ID="Freemind_Link_647862561" MODIFIED="1200876740880" TEXT="more generally acceptable"/>
</node>
<node CREATED="1200877048805" ID="Freemind_Link_1402501808" MODIFIED="1200877062217" TEXT="Michael Porter&apos;s Diamond of Competitive Advantage"/>
<node CREATED="1200877105937" ID="Freemind_Link_689410805" MODIFIED="1200877108027" TEXT="strategy">
<node CREATED="1200877108826" ID="Freemind_Link_1788291367" MODIFIED="1200877114358" TEXT="build wealth"/>
<node CREATED="1200877116493" ID="Freemind_Link_311114509" MODIFIED="1200877121395" TEXT="repair the earth"/>
<node CREATED="1200877122201" ID="Freemind_Link_345334867" MODIFIED="1200877126894" TEXT="adress major social problems"/>
</node>
</node>
</node>
<node CREATED="1200846779760" ID="Freemind_Link_1707553751" MODIFIED="1200846781241" TEXT="21">
<node CREATED="1200846781899" FOLDED="true" ID="Freemind_Link_1199811597" MODIFIED="1200846807721" TEXT="Ch 21&#xa;Conclusion&#xa;The Emergence of the Citizen Sector">
<node CREATED="1200877916011" ID="Freemind_Link_1303241630" MODIFIED="1200877925555" TEXT="World Economic Forum"/>
<node CREATED="1200877958653" ID="Freemind_Link_1389779933" MODIFIED="1200877977748" TEXT="critiques medieval china">
<node CREATED="1200877985315" ID="Freemind_Link_975333019" MODIFIED="1200877994963" TEXT="inventions were kept by state, not made widely available"/>
</node>
<node CREATED="1200878008488" ID="Freemind_Link_339969551" MODIFIED="1200878012595" TEXT="shift in social arena">
<node CREATED="1200878013179" ID="Freemind_Link_828812324" MODIFIED="1200878014918" TEXT="from">
<node CREATED="1200878015464" ID="Freemind_Link_342002169" MODIFIED="1200878021185" TEXT="church"/>
<node CREATED="1200878021735" ID="Freemind_Link_1809672605" MODIFIED="1200878024703" TEXT="wealthy patrons"/>
</node>
<node CREATED="1200878025715" ID="Freemind_Link_1979167076" MODIFIED="1200878026894" TEXT="to">
<node CREATED="1200878027586" ID="Freemind_Link_1159020867" MODIFIED="1200878046838" TEXT="taxing wealthy"/>
<node CREATED="1200878040332" ID="Freemind_Link_575945773" MODIFIED="1200878043119" TEXT="state provisions"/>
</node>
</node>
<node CREATED="1200878053789" ID="Freemind_Link_1147685271" MODIFIED="1200878063757" TEXT="governments often resist change"/>
<node CREATED="1200878087605" ID="Freemind_Link_259576277" MODIFIED="1200878090355" TEXT="citizen sector">
<node CREATED="1200878107128" ID="Freemind_Link_128767533" MODIFIED="1200878111557" TEXT="market economics of social ideas"/>
</node>
<node CREATED="1200878134626" ID="Freemind_Link_14405697" MODIFIED="1200878136513" TEXT="problems today">
<node CREATED="1200878137312" ID="Freemind_Link_1552784006" MODIFIED="1200878142057" TEXT="money is not going to best use"/>
<node CREATED="1200878147517" ID="Freemind_Link_1308162530" MODIFIED="1200878174620" TEXT="groups can form and get recognition through wealth and social connections, not merit "/>
</node>
<node CREATED="1200878191575" ID="Freemind_Link_154842620" MODIFIED="1200878199086" TEXT="most philanthropic groups are old">
<node CREATED="1200878200002" ID="Freemind_Link_1638111542" MODIFIED="1200878215321" TEXT="vs. most companies are new (more competition, more dynamic change/improvement)"/>
</node>
<node CREATED="1200878217097" ID="Freemind_Link_677171923" MODIFIED="1200878235466" TEXT="donors don&apos;t compare results"/>
<node CREATED="1200878236193" ID="Freemind_Link_446692380" MODIFIED="1200878264375" TEXT="Government funding is like an IPO for a social &quot;venture&quot;"/>
<node CREATED="1200878271152" ID="Freemind_Link_101802649" MODIFIED="1200878275985" TEXT="metrics and benchmarking">
<node CREATED="1200878276533" ID="Freemind_Link_509499168" MODIFIED="1200878288036" TEXT="each organization makes their own"/>
<node CREATED="1200878288472" ID="Freemind_Link_779693869" MODIFIED="1200878295185" TEXT="ie. there is no standardization"/>
</node>
<node CREATED="1200878304380" ID="Freemind_Link_1768494989" MODIFIED="1200878307450" TEXT="p. 274">
<node CREATED="1200878307946" ID="Freemind_Link_1103772136" MODIFIED="1200878322719" TEXT="capital allocation problem">
<node CREATED="1200878323416" ID="Freemind_Link_707728027" MODIFIED="1200878331692" TEXT="give to the people">
<node CREATED="1200878332525" ID="Freemind_Link_1090109810" MODIFIED="1200878333703" TEXT="tools"/>
<node CREATED="1200878334334" ID="Freemind_Link_1628356805" MODIFIED="1200878336712" TEXT="information"/>
<node CREATED="1200878337019" ID="Freemind_Link_1585515264" MODIFIED="1200878339990" TEXT="incentives"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1200878404535" ID="Freemind_Link_318305771" MODIFIED="1200878409011" TEXT="Epilogue">
<node CREATED="1200878410080" ID="Freemind_Link_889565570" MODIFIED="1200878431676" TEXT="People who solve problems must first arrive at the belief that they can solve problems"/>
</node>
</node>
</node>
</map>
