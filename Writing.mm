<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1376082608689" ID="ID_316635167" LINK="../../Documents/mindmaps/MetaMap.mm" MODIFIED="1376082619900" TEXT="Writing">
<node CREATED="1376082628870" ID="ID_1631859213" MODIFIED="1376082628870" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div class="question">
      Could you explain your story breaking process?
    </div>
  </body>
</html></richcontent>
<node CREATED="1376082621933" ID="ID_1400327824" LINK="http://danharmon.tumblr.com/post/57779240046/could-you-explain-your-story-breaking-process" MODIFIED="1513982525315" TEXT="danharmon.tumblr.com &gt; Post &gt; 57779240046 &gt; Could-you-explain-your-story-breaking-process">
<node CREATED="1376082646837" ID="ID_777444290" MODIFIED="1376082652403" TEXT="from Chris Horvath on FB"/>
<node CREATED="1513982670515" ID="ID_175518059" LINK="https://en.wikipedia.org/wiki/Dan_Harmon" MODIFIED="1513982670515" TEXT="https://en.wikipedia.org/wiki/Dan_Harmon"/>
<node CREATED="1513982673575" ID="ID_88074594" MODIFIED="1513982680629" TEXT="Now known for Rick &amp; Morty"/>
<node CREATED="1513982692907" ID="ID_1735643564" MODIFIED="1513982718564" TEXT="This mindmap was last saved ">
<node CREATED="1513982700569" ID="ID_1408662844" MODIFIED="1513982705229" TEXT="8/9/13"/>
</node>
</node>
<node CREATED="1376082640412" ID="ID_1939553115" MODIFIED="1376082642265" TEXT="Start with random IDEAS.  Ideas can be anything - Poop is an idea, America, pickles, the number six, a raccoon, anything.  Some ideas will reveal related ideas, i.e. you may think, upon thinking about raccoons, that you have more than one thought about raccoons.  Clouds of related ideas that your mind recognizes as related in any way are potential story AREAS.  Look for areas that make you laugh and cry.  Draw a circle to symbolize your area, because your story will take the &#x201c;reader&#x201d; through related ideas in a path around a central idea.  You don&#x2019;t have to know what the central idea is.  It&#x2019;s probably dumb.  For God&#x2019;s sake, you&#x2019;re writing about raccoons.  Divide your circle into a top half and bottom half and ask yourself what those halves might be.  Like, your raccoon area might become divided into &#x201c;positive thoughts about raccoons&#x201d; and &#x201c;negative thoughts about raccoons.&#x201d;  If the division doesn&#x2019;t feel charged for you, pick something else, like male raccoon thoughts and female raccoon thoughts, or biological raccoon thoughts and storybook raccoon thoughts.  At some point, you will divide your area into two parts that create a personal &#x201c;charge&#x201d; for you, like a battery.  &#x201d;Ooo, I like the idea that there&#x2019;s a difference between biological raccoons and storybook raccoons, that tingled when I drew that line, I want to know more.&#x201d; &lt;&#x2014; that&#x2019;s my impression of you nailing it.  Divide the divided circle down the middle and pick another charged dichotomy for left and right.  For instance, biological/storybook raccoon area could get divided into dishonest/honest.    Now you have four quadrants to your circle, going clockwise: biological dishonest raccoon, storybook dishonest raccoon, storybook honest raccoon, biological honest raccoon.  Any point at which you stop feeling charged, go back a step or start over.  Maybe you had to get this far to realize you don&#x2019;t give a shit about raccoons.  Please note that at this point, people around you will start to express confusion and frustration, because they thought the idea was fine already.  Depending on your mood and standing, these people are called hacks, traitors, parasites, scabs or successful colleagues.  When you find an area that yields four charged quadrants, experiment with protagonists.  Easy answer first, maybe I&#x2019;m a raccoon.  So once upon a time there was a dishonest biological raccoon that became a storybook raccoon, which lead to him becoming honest before finally going back to being biological again.  Cool?  If not, go back or start over.  Again, please note that many people will not want you to go back or start over.  These people will one day drown in their own blood while you point and laugh with God.  Or maybe they&#x2019;re good people and you just have Asperger&#x2019;s.  Then you keep dividing the pie, adding &#x201c;curvature&#x201d; to the protagonist&#x2019;s path with the 8 point story structure you can find me blathering about elsewhere online.  Create more characters as needed, give them their own stories as needed.  Repeat every day until rich people give you money to do it for them.  Buy a house, become one of them and hire poor people to do it for you.  Somewhere in there try to get a dog and a funny girlfriend or it&#x2019;s all pretty pointless.  Speaking of which, I just realized I&#x2019;m the only one at the office. Thank you for this question."/>
</node>
</node>
</map>
