<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node COLOR="#000000" CREATED="1175625229187" ID="Freemind_Link_1907311943" MODIFIED="1182545216089">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="../../Documents/mindmaps/images/ruby.gif" />
  </body>
</html>
</richcontent>
<font NAME="SansSerif" SIZE="20"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties"/>
<node COLOR="#0033ff" CREATED="1339125664460" ID="ID_1915752020" MODIFIED="1339125665992" POSITION="left" TEXT="Todo">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1182963213023" ID="ID_1292313168" LINK="http://www.google.com/search?hl=en&amp;safe=off&amp;pwst=1&amp;sa=X&amp;oi=spell&amp;resnum=0&amp;ct=result&amp;cd=1&amp;q=ruby+metaprogramming&amp;spell=1" MODIFIED="1339125568661" TEXT="metaprogramming">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1182963926423" ID="ID_1614548532" MODIFIED="1339125568661" TEXT="Read Why&apos;s Poignant Guide">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1175625254928" ID="Freemind_Link_1308778502" MODIFIED="1339125568661" POSITION="left" TEXT="documentation">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1180436116657" ID="Freemind_Link_34006850" LINK="http://www.ruby-doc.org/" MODIFIED="1339125568661" TEXT="ruby-doc.org">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1180730958955" ID="Freemind_Link_1425078482" LINK="http://www.rubygarden.org/Ruby/page/show/HomePage" MODIFIED="1339125568661" TEXT="rubygarden.org &gt; Ruby &gt; Page &gt; Show &gt; HomePage">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1180742028655" ID="Freemind_Link_492859000" MODIFIED="1339125568661" POSITION="left" TEXT="reference">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1182452395840" ID="Freemind_Link_1495678032" MODIFIED="1339125568661" TEXT="good Regexp examples">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1182452406841" ID="Freemind_Link_1347565492" LINK="http://www.rubycentral.com/book/language.html" MODIFIED="1182452406842" TEXT="rubycentral.com &gt; Book &gt; Language">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1182453074444" ID="Freemind_Link_1674573476" MODIFIED="1339125568661" TEXT="classes and modules">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1185392509233" ID="Freemind_Link_557909070" LINK="http://www.ruby-doc.org/docs/ProgrammingRuby/html/builtins.html" MODIFIED="1185392509234" TEXT="ruby-doc.org &gt; Docs &gt; ProgrammingRuby &gt; Html &gt; Builtins">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1185392722693" ID="Freemind_Link_382722373" LINK="http://www.ruby-doc.org/core/" MODIFIED="1185392722694" TEXT="ruby-doc.org &gt; Core">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1182454236804" ID="Freemind_Link_86894430" LINK="http://www.zenspider.com/Languages/Ruby/QuickRef.html" MODIFIED="1339125568662" TEXT="QuickRef">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1182545202770" ID="Freemind_Link_420816083" MODIFIED="1339125568662" TEXT="rubyTk">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1182545207284" ID="Freemind_Link_1541060327" LINK="http://www.approximity.com/ruby/rubytk.html" MODIFIED="1182960532607" TEXT="Rubytk">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1182795738872" ID="Freemind_Link_239402946" LINK="http://incrtcl.sourceforge.net/blt/" MODIFIED="1182960524706" TEXT="Blt">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1182795755880" ID="Freemind_Link_1593551420" LINK="http://incrtcl.sourceforge.net/iwidgets/iwidgets.html" MODIFIED="1182960540326" TEXT="Iwidgets">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1182796729337" ID="Freemind_Link_686233904" LINK="http://www.tkzinc.org" MODIFIED="1182796729338" TEXT="tkzinc">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1182796948769" ID="Freemind_Link_72450464" LINK="http://svn.ruby-lang.org/cgi-bin/viewvc.cgi/trunk/ext/tk/lib/tkextlib/SUPPORT_STATUS?view=markup&amp;pathrev=9343" MODIFIED="1182796973042" TEXT="svn.ruby-lang.org &gt; ... &gt; Tkextlib &gt; SUPPORT STATUS ">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1182797648832" FOLDED="true" ID="Freemind_Link_1556752769" MODIFIED="1339125568662" TEXT="graphics">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1182797653746" ID="Freemind_Link_1004769792" LINK="http://raa.ruby-lang.org/cat.rhtml?category_major=Library" MODIFIED="1182797653747" TEXT="raa.ruby-lang.org &gt; Cat ? ...">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1182800227080" ID="Freemind_Link_223599649" MODIFIED="1182800232833" TEXT="graphviz">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1182800234071" ID="Freemind_Link_1660680716" LINK="http://raa.ruby-lang.org/project/graphviz_r/" MODIFIED="1182960509473" TEXT="Graphviz"/>
<node COLOR="#111111" CREATED="1182797552925" ID="Freemind_Link_1249609439" LINK="http://www.graphviz.org/cgi-bin/man?tcldot" MODIFIED="1182960493657" TEXT="graphviz.org">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1339125732959" ID="ID_43781543" MODIFIED="1339125734181" POSITION="right" TEXT="rails">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1339125754051" ID="ID_1751065077" LINK="http://railscasts.com/" MODIFIED="1339125754052" TEXT="railscasts.com">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1339125789581" ID="ID_1697770057" MODIFIED="1339125791187" TEXT="testing">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1339125807943" ID="ID_81217158" MODIFIED="1339125807943">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <span>Cyclomatic complexity</span>
  </body>
</html>
</richcontent>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1339125816452" ID="ID_553853335" MODIFIED="1339125816452">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <span>Saikuro</span>
  </body>
</html>
</richcontent>
</node>
<node COLOR="#111111" CREATED="1339125818505" ID="ID_456706694" MODIFIED="1339125819382" TEXT="flog">
<node COLOR="#111111" CREATED="1339125848131" ID="ID_559596251" LINK="http://ruby.sadi.st/Flog.html" MODIFIED="1339125848131" TEXT="ruby.sadi.st &gt; Flog"/>
</node>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1180146160361" ID="Freemind_Link_1307856753" MODIFIED="1339125568664" POSITION="right" TEXT="find gems">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1180146246575" ID="Freemind_Link_1830502825" LINK="http://rubyforge.org/" MODIFIED="1339125568664" TEXT="rubyforge.org">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1339125579074" FOLDED="true" ID="ID_1564413097" MODIFIED="1339125777289" POSITION="right" TEXT="old">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1182960848690" FOLDED="true" ID="Freemind_Link_1763916966" MODIFIED="1339125684214" TEXT="Todo" VSHIFT="-120">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1184609370520" HGAP="24" ID="Freemind_Link_1767796210" MODIFIED="1339125684214" TEXT="Learn Next" VSHIFT="-65">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1184609387225" ID="Freemind_Link_1423235796" MODIFIED="1339125684214" TEXT="Authentication">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1185860002134" ID="Freemind_Link_168070649" MODIFIED="1185860004673" TEXT="using AAA"/>
</node>
<node COLOR="#111111" CREATED="1184609391696" ID="Freemind_Link_575105571" MODIFIED="1339125684214" TEXT="uploading pictures">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1185249813032" ID="Freemind_Link_380449260" LINK="http://agilewebdevelopment.com/plugins/fleximage" MODIFIED="1185249813044" TEXT="agilewebdevelopment.com &gt; Plugins &gt; Fleximage"/>
</node>
<node COLOR="#111111" CREATED="1182876254836" ID="Freemind_Link_1084028964" MODIFIED="1339125684214" TEXT="rank items">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1185860118164" ID="Freemind_Link_752743434" LINK="http://demo.script.aculo.us/ajax/sortable_elements" MODIFIED="1185860118166" TEXT="demo.script.aculo.us &gt; Ajax &gt; Sortable elements"/>
</node>
<node COLOR="#111111" CREATED="1185902999107" ID="Freemind_Link_413938818" MODIFIED="1339125684214" TEXT="niftyCube">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1185903015289" ID="Freemind_Link_1341951674" LINK="http://www.html.it/articoli/niftycube/index.html" MODIFIED="1185903015291" TEXT="html.it &gt; Articoli &gt; Niftycube &gt; Index"/>
</node>
<node COLOR="#111111" CREATED="1185859947040" ID="Freemind_Link_170401031" MODIFIED="1339125684214" TEXT="rails pdf">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1185859978319" ID="Freemind_Link_1817083852" LINK="/Users/durantschoon/Documents/pdf/agilerails.pdf" MODIFIED="1185859978319" TEXT="agilerails.pdf"/>
</node>
<node COLOR="#111111" CREATED="1185526642462" ID="Freemind_Link_520636779" MODIFIED="1339125684214" TEXT="read about restful rails">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1185526660234" ID="Freemind_Link_1482627700" LINK="/Users/durantschoon/Documents/pdf/RestfulRails.pdf" MODIFIED="1185526660235" TEXT="RestfulRails.pdf"/>
</node>
<node COLOR="#111111" CREATED="1182876942691" ID="Freemind_Link_1876986208" MODIFIED="1339125684215" TEXT="svn w/rails">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1185312769348" ID="Freemind_Link_1923374059" MODIFIED="1185312774128" TEXT="online book">
<node COLOR="#111111" CREATED="1185308470591" ID="Freemind_Link_1332433194" LINK="http://svnbook.red-bean.com/" MODIFIED="1185308470592" TEXT="svnbook.red-bean.com"/>
<node COLOR="#111111" CREATED="1185312765650" ID="Freemind_Link_708720504" LINK="http://svnbook.red-bean.com/nightly/en/index.html" MODIFIED="1185312765652" TEXT="svnbook.red-bean.com &gt; Nightly &gt; En &gt; Index"/>
<node COLOR="#111111" CREATED="1185312785085" ID="Freemind_Link_408399083" MODIFIED="1185312787553" TEXT="current">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1185316192283" ID="Freemind_Link_1465164597" LINK="http://svnbook.red-bean.com/nightly/en/svn.basic.vsn-models.html" MODIFIED="1185316192286" TEXT="svnbook.red-bean.com &gt; Nightly &gt; En &gt; Svn.basic.vsn-models"/>
</node>
</node>
<node COLOR="#111111" CREATED="1182876948040" ID="Freemind_Link_938188242" LINK="http://wiki.rubyonrails.com/rails/pages/How+To+Use+Subversion+with+a+Rails+Project" MODIFIED="1185308479605" TEXT="How+To+Use+Subversion+with+a+Rails+Project">
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#111111" CREATED="1185308508466" FOLDED="true" ID="Freemind_Link_1854587850" MODIFIED="1185308514105" TEXT="svnX">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1185308517065" ID="Freemind_Link_818638611" LINK="http://www.lachoseinteractive.net/en/community/subversion/svnx/download" MODIFIED="1185308517070" TEXT="lachoseinteractive.net &gt; En &gt; Community &gt; Subversion &gt; Svnx &gt; Download"/>
</node>
<node COLOR="#111111" CREATED="1185229494279" ID="Freemind_Link_1835414756" LINK="http://piston.rubyforge.org/" MODIFIED="1185229494281" TEXT="piston.rubyforge.org"/>
</node>
<node COLOR="#111111" CREATED="1184797980920" ID="Freemind_Link_304191582" MODIFIED="1339125684215" TEXT="CMS">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1184797984843" ID="Freemind_Link_1651071023" MODIFIED="1185306017141" TEXT="comotose">
<icon BUILTIN="messagebox_warning"/>
<node COLOR="#111111" CREATED="1184402870488" ID="Freemind_Link_316040219" LINK="http://comatose.rubyforge.org/" MODIFIED="1184402870525" TEXT="comatose.rubyforge.org"/>
<node COLOR="#111111" CREATED="1184797988258" ID="Freemind_Link_714435450" LINK="http://comatose.rubyforge.org/getting-started-guide" MODIFIED="1184797988265" TEXT="comatose.rubyforge.org &gt; Getting-started-guide"/>
</node>
</node>
<node COLOR="#111111" CREATED="1185860512435" ID="Freemind_Link_1358544952" MODIFIED="1339125684215" TEXT="ACL">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1185860517530" ID="Freemind_Link_160283868" LINK="http://agilewebdevelopment.com/plugins/acl_system" MODIFIED="1185860517533" TEXT="agilewebdevelopment.com &gt; Plugins &gt; Acl system"/>
</node>
</node>
<node COLOR="#990000" CREATED="1184560629970" ID="Freemind_Link_1544767349" MODIFIED="1339125684217" TEXT="Rails tutorials">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1184563464634" ID="Freemind_Link_391475441" LINK="http://wiki.rubyonrails.org/rails/pages/TutorialStepTwo" MODIFIED="1339125684217" TEXT="wiki.rubyonrails.org &gt; Rails &gt; Pages &gt; TutorialStepTwo">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1185769184936" ID="Freemind_Link_1923988221" LINK="http://www.samspublishing.com/articles/article.asp?p=26943&amp;rl=1" MODIFIED="1339125684217" TEXT="samspublishing.com &gt; Articles &gt; Article ? ...">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1182960855603" ID="Freemind_Link_458239510" MODIFIED="1339125684217" TEXT="Read Programming Ruby">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1182960876098" ID="Freemind_Link_336514570" MODIFIED="1339125684217" TEXT="Read all classes">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1182960886983" ID="Freemind_Link_560505490" LINK="http://www.rubycentral.com/book/builtins.html" MODIFIED="1182960886986" TEXT="rubycentral.com &gt; Book &gt; Builtins"/>
<node COLOR="#111111" CREATED="1182960956403" ID="Freemind_Link_1068285904" MODIFIED="1182960962081" TEXT="esp class Module"/>
</node>
</node>
<node COLOR="#990000" CREATED="1183532520676" ID="Freemind_Link_1085562656" LINK="http://manuals.rubyonrails.com/" MODIFIED="1339125684218" TEXT="manuals.rubyonrails.com">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="bookmark"/>
</node>
<node COLOR="#990000" CREATED="1182960868195" ID="Freemind_Link_1968411181" MODIFIED="1339125684218" TEXT="Read HowTo&apos;s for Rails">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1182963213023" ID="Freemind_Link_13515604" LINK="http://www.google.com/search?hl=en&amp;safe=off&amp;pwst=1&amp;sa=X&amp;oi=spell&amp;resnum=0&amp;ct=result&amp;cd=1&amp;q=ruby+metaprogramming&amp;spell=1" MODIFIED="1339125684218" TEXT="metaprogramming">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1182963926423" ID="Freemind_Link_1339968138" MODIFIED="1339125684218" TEXT="Read Why&apos;s Poignant Guide">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1175626620839" FOLDED="true" ID="Freemind_Link_1074815581" LINK="http://www.rubyonrails.org/" MODIFIED="1339125730199" TEXT="rails">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1182876217048" ID="Freemind_Link_1625179148" MODIFIED="1339125730199" TEXT="capabilities">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1182885436203" HGAP="25" ID="Freemind_Link_1573216241" MODIFIED="1339125730199" TEXT="CMS?" VSHIFT="-63">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1184871126296" ID="Freemind_Link_1503576906" MODIFIED="1184871128929" TEXT="comotose">
<node COLOR="#111111" CREATED="1184402870488" ID="Freemind_Link_900603683" LINK="http://comatose.rubyforge.org/" MODIFIED="1184402870525" TEXT="comatose.rubyforge.org"/>
<node COLOR="#111111" CREATED="1184871133789" ID="Freemind_Link_659175529" LINK="http://groups.google.com/group/comatose-plugin" MODIFIED="1184871133791" TEXT="groups.google.com &gt; Group &gt; Comatose-plugin"/>
</node>
<node COLOR="#111111" CREATED="1184872503192" ID="Freemind_Link_1933410180" MODIFIED="1184872504990" TEXT="radiant">
<node COLOR="#111111" CREATED="1184872506093" ID="Freemind_Link_683509746" LINK="http://radiantcms.org/" MODIFIED="1184872506094" TEXT="radiantcms.org"/>
</node>
<node COLOR="#111111" CREATED="1184402376855" ID="Freemind_Link_92094603" LINK="http://wiki.rubyonrails.com/rails/pages/Ruby+on+Rails+based+CMS" MODIFIED="1184402376988" TEXT="wiki.rubyonrails.com &gt; Rails &gt; Pages &gt; Ruby+on+Rails+based+CMS"/>
<node COLOR="#111111" CREATED="1184403685769" ID="Freemind_Link_1628325474" LINK="http://www.opensourcecms.com/" MODIFIED="1184403685770" TEXT="opensourcecms.com"/>
</node>
<node COLOR="#111111" CREATED="1182876242217" ID="Freemind_Link_60967604" MODIFIED="1339125730200" TEXT="authentication">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1184637078734" ID="Freemind_Link_139163716" LINK="http://agilewebdevelopment.com/plugins/restful_authentication" MODIFIED="1184637078737" TEXT="agilewebdevelopment.com &gt; Plugins &gt; Restful authentication"/>
<node COLOR="#111111" CREATED="1184617367047" ID="Freemind_Link_1740415096" LINK="http://wiki.rubyonrails.org/rails/pages/Acts_as_authenticated" MODIFIED="1184617367049" TEXT="wiki.rubyonrails.org &gt; Rails &gt; Pages &gt; Acts as authenticated"/>
<node COLOR="#111111" CREATED="1184637116207" ID="Freemind_Link_114237045" LINK="http://technoweenie.stikipad.com/plugins/show/Acts+as+Authenticated" MODIFIED="1184637116213" TEXT="technoweenie.stikipad.com &gt; Plugins &gt; Show &gt; Acts+as+Authenticated"/>
<node COLOR="#111111" CREATED="1185860501222" ID="Freemind_Link_68189197" LINK="http://agilewebdevelopment.com/plugins/acl_system" MODIFIED="1185860501225" TEXT="agilewebdevelopment.com &gt; Plugins &gt; Acl system"/>
</node>
<node COLOR="#111111" CREATED="1182876246299" ID="Freemind_Link_843873982" MODIFIED="1339125730200" TEXT="submit items">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1182883472112" ID="Freemind_Link_166245470" MODIFIED="1339125730200" TEXT="search">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1182960910191" FOLDED="true" ID="Freemind_Link_398957141" MODIFIED="1182960912391" TEXT="ferret">
<node COLOR="#111111" CREATED="1182883475611" ID="Freemind_Link_1573381532" LINK="http://wiki.rubyonrails.com/rails/pages/HowToIntegrateFerretWithRails" MODIFIED="1182883475613" TEXT="wiki.rubyonrails.com &gt; Rails &gt; Pages &gt; HowToIntegrateFerretWithRails"/>
</node>
<node COLOR="#111111" CREATED="1182883810378" FOLDED="true" ID="Freemind_Link_1947030201" MODIFIED="1182883810379" TEXT="find_by_sql">
<node COLOR="#111111" CREATED="1182883832259" ID="Freemind_Link_701744072" LINK="http://ar.rubyonrails.com/classes/ActiveRecord/Base.html#M000345" MODIFIED="1182883832261" TEXT="ar.rubyonrails.com &gt; Classes &gt; ActiveRecord &gt; Base"/>
</node>
<node COLOR="#111111" CREATED="1182884055236" FOLDED="true" ID="Freemind_Link_446735549" MODIFIED="1182884057125" TEXT="pagination">
<node COLOR="#111111" CREATED="1182884067227" ID="Freemind_Link_875811673" LINK="http://wiki.rubyonrails.com/rails/pages/HowtoPagination" MODIFIED="1182884067233" TEXT="wiki.rubyonrails.com &gt; Rails &gt; Pages &gt; HowtoPagination"/>
<node COLOR="#111111" CREATED="1182884058189" ID="Freemind_Link_715634762" LINK="http://www.nullislove.com/2007/05/24/pagination-in-rails/" MODIFIED="1182884058190" TEXT="nullislove.com &gt; 2007 &gt; 05 &gt; 24 &gt; Pagination-in-rails"/>
<node COLOR="#111111" CREATED="1182884152860" ID="Freemind_Link_1290632484" LINK="http://www.railsonwave.com/railsonwave/2007/1/24/howto-paginate-an-array" MODIFIED="1182884162634" TEXT="Howto-paginate-an-array"/>
</node>
</node>
<node COLOR="#111111" CREATED="1182876254836" ID="Freemind_Link_1102667194" MODIFIED="1339125730200" TEXT="rank items">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1185860118164" ID="Freemind_Link_79471202" LINK="http://demo.script.aculo.us/ajax/sortable_elements" MODIFIED="1185860118166" TEXT="demo.script.aculo.us &gt; Ajax &gt; Sortable elements"/>
</node>
<node COLOR="#111111" CREATED="1182876264437" ID="Freemind_Link_362847321" MODIFIED="1339125730200" TEXT="group items">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1184402245225" ID="Freemind_Link_1529919965" MODIFIED="1339125730201" TEXT="pictures?">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1182876274871" ID="Freemind_Link_748807291" MODIFIED="1339125730201" TEXT="tag items">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1186023900275" ID="Freemind_Link_880097441" LINK="http://agilewebdevelopment.com/plugins/acts_as_taggable_on_steroids" MODIFIED="1186023900277" TEXT="agilewebdevelopment.com &gt; Plugins &gt; Acts as taggable on steroids"/>
<node COLOR="#111111" CREATED="1182883308403" ID="Freemind_Link_69260362" LINK="http://wiki.rubyonrails.com/rails/pages/HowToImplementTags" MODIFIED="1182883308404" TEXT="wiki.rubyonrails.com &gt; Rails &gt; Pages &gt; HowToImplementTags"/>
<node COLOR="#111111" CREATED="1186023694079" ID="Freemind_Link_1763514434" LINK="http://wiki.rubyonrails.org/rails/pages/Acts+As+Taggable+Plugin" MODIFIED="1186023694082" TEXT="wiki.rubyonrails.org &gt; Rails &gt; Pages &gt; Acts+As+Taggable+Plugin"/>
<node COLOR="#111111" CREATED="1186023739343" ID="Freemind_Link_1080143050" LINK="http://www.juixe.com/techknow/index.php/2006/07/15/acts-as-taggable-tag-cloud/" MODIFIED="1186023754396" TEXT="Acts-as-taggable-tag-cloud"/>
</node>
</node>
<node COLOR="#990000" CREATED="1182876539462" ID="Freemind_Link_434515897" MODIFIED="1339125730202" TEXT="admin" VSHIFT="-87">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1182876942691" ID="Freemind_Link_1856370120" MODIFIED="1339125730202" TEXT="svn w/rails">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1182876948040" ID="Freemind_Link_109427658" LINK="http://wiki.rubyonrails.com/rails/pages/How+To+Use+Subversion+with+a+Rails+Project" MODIFIED="1182876956177" TEXT="How+To+Use+Subversion+with+a+Rails+Project"/>
<node COLOR="#111111" CREATED="1185229494279" ID="Freemind_Link_935047188" LINK="http://piston.rubyforge.org/" MODIFIED="1185229494281" TEXT="piston.rubyforge.org"/>
</node>
<node COLOR="#111111" CREATED="1182876975154" FOLDED="true" ID="Freemind_Link_1562865211" MODIFIED="1339125730203" TEXT="testing">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1182876977533" ID="Freemind_Link_171541443" LINK="http://manuals.rubyonrails.com/read/book/5" MODIFIED="1182876977535" TEXT="manuals.rubyonrails.com &gt; Read &gt; Book &gt; 5"/>
</node>
<node COLOR="#111111" CREATED="1182883279170" ID="Freemind_Link_1700851673" MODIFIED="1339125730203" TEXT="security">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1184617457410" ID="Freemind_Link_556469561" LINK="http://wiki.rubyonrails.com/rails/pages/Authentication" MODIFIED="1184617457414" TEXT="wiki.rubyonrails.com &gt; Rails &gt; Pages &gt; Authentication"/>
<node COLOR="#111111" CREATED="1184617377003" ID="Freemind_Link_615345888" LINK="http://wiki.rubyonrails.org/rails/pages/Acts_as_authenticated" MODIFIED="1184617377005" TEXT="wiki.rubyonrails.org &gt; Rails &gt; Pages &gt; Acts as authenticated"/>
<node COLOR="#111111" CREATED="1182883287241" ID="Freemind_Link_620944522" LINK="http://www.rorsecurity.info/" MODIFIED="1182883287247" TEXT="rorsecurity.info"/>
</node>
<node COLOR="#111111" CREATED="1182876496896" ID="Freemind_Link_186452768" MODIFIED="1339125730203" TEXT="logging">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1182876569053" ID="Freemind_Link_574114170" MODIFIED="1339125730203" TEXT="testing">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1182876731023" FOLDED="true" ID="Freemind_Link_1150301198" MODIFIED="1339125730203" TEXT="stylesheets">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1182876735014" ID="Freemind_Link_894471356" LINK="http://wiki.rubyonrails.com/rails/pages/HowtoCorrectlyUseStylesheetsInYourTemplates" MODIFIED="1182876751456" TEXT="HowtoCorrectlyUseStylesheetsInYourTemplates"/>
</node>
<node COLOR="#111111" CREATED="1182876783003" FOLDED="true" ID="Freemind_Link_1343871929" MODIFIED="1339125730203" TEXT="xml">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1182876785356" ID="Freemind_Link_1677756793" LINK="http://wiki.rubyonrails.com/rails/pages/HowtoGenerateXml" MODIFIED="1182876785358" TEXT="wiki.rubyonrails.com &gt; Rails &gt; Pages &gt; HowtoGenerateXml"/>
</node>
<node COLOR="#111111" CREATED="1182876840321" FOLDED="true" ID="Freemind_Link_1035304348" MODIFIED="1339125730203" TEXT="charts">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1182876850837" ID="Freemind_Link_93853380" LINK="http://wiki.rubyonrails.com/rails/pages/HowtoGenerateJFreeCharts" MODIFIED="1182876850838" TEXT="wiki.rubyonrails.com &gt; Rails &gt; Pages &gt; HowtoGenerateJFreeCharts"/>
</node>
</node>
<node COLOR="#990000" CREATED="1186003451840" ID="Freemind_Link_1353554035" MODIFIED="1339125730204" TEXT="graphics">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1186003459530" ID="Freemind_Link_319971959" LINK="http://nubyonrails.com/pages/css_graphs" MODIFIED="1339125730204" TEXT="nubyonrails.com &gt; Pages &gt; Css graphs">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1186003852973" ID="Freemind_Link_793573217" MODIFIED="1339125730204" TEXT="other">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1186003854699" ID="Freemind_Link_1730499112" MODIFIED="1339125730204" TEXT="lighttpd has been installed">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1186003864888" ID="Freemind_Link_1333189743" MODIFIED="1186003869288" TEXT="but isn&apos;t in use yet"/>
</node>
<node COLOR="#111111" CREATED="1186070618332" ID="Freemind_Link_1926759410" MODIFIED="1339125730204" TEXT="verify">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1186070816255" ID="Freemind_Link_110011089" LINK="http://www.alexyoung.org/articles/show/4/rails_quality_control_tip_use_verify_in_controllers" MODIFIED="1186070816257" TEXT="alexyoung.org &gt; Articles &gt; Show &gt; 4 &gt; Rails quality control tip use verify in controllers"/>
<node COLOR="#111111" CREATED="1186070763556" ID="Freemind_Link_1901450812" LINK="http://rails.rubyonrails.org/classes/ActionController/Verification/ClassMethods.html" MODIFIED="1186070763560" TEXT="rails.rubyonrails.org &gt; Classes &gt; ActionController &gt; Verification &gt; ClassMethods"/>
</node>
</node>
<node COLOR="#990000" CREATED="1183608853103" ID="Freemind_Link_1137131168" MODIFIED="1339125730205" TEXT="with script.aculo.us">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1183608870554" ID="Freemind_Link_54793193" LINK="http://wiki.script.aculo.us/scriptaculous/show/IntegrationWithRubyOnRails" MODIFIED="1339125730205" TEXT="wiki.script.aculo.us &gt; Scriptaculous &gt; Show &gt; IntegrationWithRubyOnRails">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1184888288297" ID="Freemind_Link_1330791115" LINK="http://api.rubyonrails.org/classes/ActionView/Helpers/ScriptaculousHelper.html" MODIFIED="1339125730205" TEXT="api.rubyonrails.org &gt; Classes &gt; ActionView &gt; Helpers &gt; ScriptaculousHelper">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1175626677554" ID="Freemind_Link_1140279944" MODIFIED="1339125730205" TEXT="setup">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1175626679917" ID="Freemind_Link_1500206193" LINK="http://hivelogic.com/narrative/articles/ruby-rails-mongrel-mysql-osx" MODIFIED="1339125730205" TEXT="os x">
<font NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#111111" CREATED="1176431283977" ID="Freemind_Link_894397087" LINK="http://locomotive.raaum.org/" MODIFIED="1339125730205" TEXT="locamotive">
<font NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node COLOR="#990000" CREATED="1186170772739" ID="Freemind_Link_833184304" MODIFIED="1339125730205" TEXT="manual">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1186170775157" ID="Freemind_Link_1704633159" LINK="http://www.railsmanual.org/" MODIFIED="1339125730205" TEXT="railsmanual.org">
<font NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="bookmark"/>
</node>
</node>
<node COLOR="#990000" CREATED="1176091185111" ID="Freemind_Link_49797845" MODIFIED="1339125730206" TEXT="tutorials">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1176419615437" ID="Freemind_Link_307530192" LINK="http://developer.apple.com/tools/rubyonrails.html" MODIFIED="1339125730206" TEXT="apple">
<font NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#111111" CREATED="1176091201896" ID="Freemind_Link_222534586" LINK="http://www.onlamp.com/pub/a/onlamp/2006/12/14/revisiting-ruby-on-rails-revisited.html" MODIFIED="1339125730206" TEXT="oreilly">
<arrowlink DESTINATION="Freemind_Link_222534586" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Arrow_ID_1588113768" STARTARROW="None" STARTINCLINATION="0;0;"/>
<font NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#111111" CREATED="1180226331310" ID="Freemind_Link_1798677513" MODIFIED="1181595056231" TEXT="questions">
<node COLOR="#111111" CREATED="1180226334947" ID="Freemind_Link_829358942" MODIFIED="1181595056235" TEXT="How do I change the text box size for longer recipes?"/>
</node>
<node COLOR="#111111" CREATED="1180226453477" ID="Freemind_Link_1215826591" LINK="http://www.onlamp.com/pub/a/onlamp/2007/01/05/revisiting-ruby-on-rails-revisited-2.html" MODIFIED="1181595056238" TEXT="part 2">
<arrowlink DESTINATION="Freemind_Link_1215826591" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Arrow_ID_546702925" STARTARROW="None" STARTINCLINATION="0;0;"/>
</node>
</node>
<node COLOR="#111111" CREATED="1180284844153" ID="Freemind_Link_324556335" LINK="http://media.rubyonrails.org/video/rails_take2_with_sound.mov" MODIFIED="1339125730206" TEXT="media.rubyonrails.org &gt; Video &gt; Rails take2 with sound">
<font NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#111111" CREATED="1180284864190" ID="Freemind_Link_1072100071" MODIFIED="1181595056257" TEXT="make a blog in 15 minutes"/>
</node>
<node COLOR="#111111" CREATED="1182965008282" ID="Freemind_Link_1349605274" LINK="http://sl33p3r.free.fr/tutorials/rails/wiki/wiki-en.html" MODIFIED="1339125730206" TEXT="sl33p3r.free.fr &gt; Tutorials &gt; Rails &gt; Wiki &gt; Wiki-en">
<font NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#111111" CREATED="1184608515836" ID="Freemind_Link_430134630" LINK="http://sonjayatandon.com/05-2006/how-to-build-a-secured-web-application-with-ruby-on-rails/" MODIFIED="1339125730206" TEXT="How-to-build-a-secured-web-application-with-ruby-on-rails">
<font NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#111111" CREATED="1185252731408" ID="Freemind_Link_470021743" MODIFIED="1339125730206" TEXT="REST?">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1185252734320" ID="Freemind_Link_1988407685" LINK="http://www.xml.com/pub/a/2006/04/19/rest-on-rails.html" MODIFIED="1185252734321" TEXT="xml.com &gt; Pub &gt; A &gt; 2006 &gt; 04 &gt; 19 &gt; Rest-on-rails"/>
</node>
<node COLOR="#111111" CREATED="1184566100167" ID="Freemind_Link_1253718430" LINK="http://wiki.rubyonrails.org/rails/pages/GettingStartedWithRails" MODIFIED="1339125730206" TEXT="wiki.rubyonrails.org &gt; Rails &gt; Pages &gt; GettingStartedWithRails">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1184566131498" ID="Freemind_Link_250800733" MODIFIED="1184566137436" TEXT="see 7. Tutorials"/>
</node>
<node COLOR="#111111" CREATED="1184563589167" ID="Freemind_Link_1023124516" LINK="http://wiki.rubyonrails.org/rails/pages/Tutorial" MODIFIED="1339125730206" TEXT="wiki.rubyonrails.org &gt; Rails &gt; Pages &gt; Tutorial">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1180741998314" ID="Freemind_Link_14975764" LINK="http://www.rubyist.net/~slagell/ruby/" MODIFIED="1339125730207" TEXT="rubyist.net &gt; Slagell &gt; Ruby">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1180232442424" ID="Freemind_Link_945549839" LINK="http://wiki.rubyonrails.org/rails/pages/ActionMailer" MODIFIED="1339125730207" TEXT="wiki.rubyonrails.org &gt; Rails &gt; Pages &gt; ActionMailer">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1180233413128" ID="Freemind_Link_991158802" LINK="http://wiki.rubyonrails.com/rails/pages/Howtos" MODIFIED="1339125730207" TEXT="wiki.rubyonrails.com &gt; Rails &gt; Pages &gt; Howtos">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1180378259398" ID="Freemind_Link_1434070044" LINK="http://www.railsforum.com/viewforum.php?id=20" MODIFIED="1339125730207" TEXT="railsforum.com &gt; Viewforum ? ...">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1184560636999" ID="Freemind_Link_1754413641" LINK="http://manuals.rubyonrails.com/read/chapter/56" MODIFIED="1339125730207" TEXT="manuals.rubyonrails.com &gt; Read &gt; Chapter &gt; 56">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1184566243967" ID="Freemind_Link_1277856934" MODIFIED="1184566248774" TEXT="manuals seem f&apos;ed up"/>
<node COLOR="#111111" CREATED="1184566314884" ID="Freemind_Link_1607829946" MODIFIED="1184566343698" TEXT="this might be a continuation of howto&apos;s&#xa;and therefore requires running the others first"/>
</node>
<node COLOR="#111111" CREATED="1184617070669" ID="Freemind_Link_664048750" LINK="http://www.softwaredeveloper.com/features/74-ruby-on-rails-resources-tutorials-050207/" MODIFIED="1339125730207" TEXT="softwaredeveloper.com &gt; Features &gt; 74-ruby-on-rails-resources-tutorials-050207">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1185855216950" ID="Freemind_Link_305514962" LINK="http://www.digitalmediaminute.com/article/1816/top-ruby-on-rails-tutorials" MODIFIED="1339125730207" TEXT="digitalmediaminute.com &gt; Article &gt; 1816 &gt; Top-ruby-on-rails-tutorials">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1185855244737" ID="Freemind_Link_149285008" LINK="http://www.rubyinside.com/19-rails-tricks-most-rails-coders-dont-know-131.html" MODIFIED="1339125730207" TEXT="rubyinside.com &gt; 19-rails-tricks-most-rails-coders-dont-know-131">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1176435333298" ID="Freemind_Link_1737475840" MODIFIED="1339125730208" TEXT="docs">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1176435336802" ID="Freemind_Link_417135501" LINK="http://api.rubyonrails.org/" MODIFIED="1339125730208" TEXT="api">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1182903646810" ID="Freemind_Link_871464963" LINK="http://wiki.rubyonrails.org/rails" MODIFIED="1339125730208" TEXT="wiki.rubyonrails.org &gt; Rails">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1183583733560" ID="Freemind_Link_1653238722" LINK="http://blogs.tech-recipes.com/johnny/rails-quick-reference/" MODIFIED="1339125730208" TEXT="blogs.tech-recipes.com &gt; Johnny &gt; Rails-quick-reference">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1180730780611" ID="Freemind_Link_405332328" MODIFIED="1339125730209" TEXT="book">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1180730769716" ID="Freemind_Link_21419403" LINK="http://www.pragmaticprogrammer.com/titles/rails/index.html" MODIFIED="1339125730209" TEXT="pragmaticprogrammer.com &gt; Titles &gt; Rails &gt; Index">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1182181592441" ID="Freemind_Link_122303777" MODIFIED="1339125730209" TEXT="web">
<edge STYLE="bezier" WIDTH="thin"/>
<arrowlink DESTINATION="Freemind_Link_122303777" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Arrow_ID_4751134" STARTARROW="None" STARTINCLINATION="0;0;"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1182181595313" ID="Freemind_Link_1927963544" LINK="http://del.icio.us/popular/rubyonrails" MODIFIED="1339125730209" TEXT="del.icio.us &gt; Popular &gt; Rubyonrails">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1182181495156" ID="Freemind_Link_1734480326" LINK="http://betterexplained.com/articles/starting-ruby-on-rails-what-i-wish-i-knew/" MODIFIED="1339125730209" TEXT="betterexplained.com &gt; Articles &gt; Starting-ruby-on-rails-what-i-wish-i-knew">
<font NAME="SansSerif" SIZE="12"/>
<icon BUILTIN="bookmark"/>
</node>
<node COLOR="#111111" CREATED="1182888691409" ID="Freemind_Link_974696241" LINK="http://www.blazingrails.com/article/ruby_on_rails_best_support_sites.html" MODIFIED="1339125730209" TEXT="blazingrails.com &gt; Article &gt; Ruby on rails best support sites">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1184566776924" ID="Freemind_Link_1034733212" MODIFIED="1339125730209" TEXT="plugins">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1184566780312" ID="Freemind_Link_100638989" LINK="http://www.activescaffold.com/" MODIFIED="1339125730209" TEXT="activescaffold.com">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1184721587560" ID="Freemind_Link_583571800" LINK="http://nubyonrails.com/articles/the-complete-guide-to-rails-plugins-part-i" MODIFIED="1339125730209" TEXT="nubyonrails.com &gt; Articles &gt; The-complete-guide-to-rails-plugins-part-i">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1184797764191" ID="Freemind_Link_1452276201" LINK="http://www.liquidmarkup.org/" MODIFIED="1339125730209" TEXT="liquidmarkup.org">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node COLOR="#00b439" CREATED="1185769246424" ID="Freemind_Link_1770958976" MODIFIED="1339125594849" TEXT="tutorials">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1185769250470" ID="Freemind_Link_1680837388" LINK="http://www.samspublishing.com/articles/article.asp?p=26943&amp;rl=1" MODIFIED="1339125594849" TEXT="samspublishing.com &gt; Articles &gt; Article ? ...">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="desktop_new"/>
</node>
<node COLOR="#990000" CREATED="1185769269592" ID="Freemind_Link_1902949235" LINK="http://www.rubyist.net/~slagell/ruby/" MODIFIED="1339125594849" TEXT="rubyist.net &gt; Slagell &gt; Ruby">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1180731630418" FOLDED="true" ID="Freemind_Link_1753107952" MODIFIED="1339125590491" TEXT="book">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1180730757844" ID="Freemind_Link_1924184229" LINK="http://www.pragmaticprogrammer.com/titles/ruby/index.html" MODIFIED="1339125590491" TEXT="pragmaticprogrammer.com &gt; Titles &gt; Ruby &gt; Index">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1182187638637" ID="Freemind_Link_385494402" MODIFIED="1339125776376" TEXT="continuations">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1182187647939" ID="Freemind_Link_1495794407" LINK="http://blade.nagaokaut.ac.jp/~sinara/ruby/callcc-lib/" MODIFIED="1339125776376" TEXT="blade.nagaokaut.ac.jp &gt; Sinara &gt; Ruby &gt; Callcc-lib">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
</node>
</node>
</map>
