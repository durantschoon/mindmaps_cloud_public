<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1471920353868" ID="ID_1834545077" LINK="../../Documents/mindmaps/MetaMap.mm" MODIFIED="1471920736930" TEXT="MachineLearning.mm">
<node CREATED="1474833434175" HGAP="24" ID="ID_543608071" MODIFIED="1477635287483" POSITION="right" TEXT="Online learning" VSHIFT="-37">
<node CREATED="1474833453031" LINK="http://www.bayareadlschool.org/" MODIFIED="1474833453031" TEXT="bayareadlschool.org"/>
<node CREATED="1477892157870" ID="ID_1928661056" MODIFIED="1477892162029" TEXT="Tutorial: Unsupervised Feature Learning and Deep Learning">
<node CREATED="1477892150249" ID="ID_779104094" LINK="http://ufldl.stanford.edu/wiki/index.php/UFLDL_Tutorial" MODIFIED="1477892150249" TEXT="ufldl.stanford.edu &gt; Wiki &gt; Index.php &gt; UFLDL Tutorial"/>
</node>
<node CREATED="1498630729801" ID="ID_1145962429" MODIFIED="1498630731247" TEXT="articles">
<node CREATED="1498630735129" ID="ID_261260688" MODIFIED="1498630763007" TEXT="Introducing Deep Learning &#xa;and Neural Networks&#x200a;&#x2014;&#x200a;&#xa;Deep Learning for Rookies (1)">
<node CREATED="1498630753064" LINK="https://medium.com/towards-data-science/introducing-deep-learning-and-neural-networks-deep-learning-for-rookies-1-bd68f9cf5883" MODIFIED="1498630753064" TEXT="https://medium.com/towards-data-science/introducing-deep-learning-and-neural-networks-deep-learning-for-rookies-1-bd68f9cf5883"/>
</node>
</node>
</node>
<node CREATED="1497313709797" ID="ID_620831678" MODIFIED="1497313711718" POSITION="right" TEXT="Articles">
<node CREATED="1497313712239" ID="ID_372802748" MODIFIED="1497313716915" TEXT="Casual Intro">
<node CREATED="1497313717400" ID="ID_1425882236" MODIFIED="1497313719153" TEXT="with examples"/>
<node CREATED="1497313720370" LINK="https://medium.com/towards-data-science/a-casual-intro-to-machine-learning-f43ccf316245" MODIFIED="1497313720370" TEXT="https://medium.com/towards-data-science/a-casual-intro-to-machine-learning-f43ccf316245"/>
</node>
</node>
<node CREATED="1493662302192" ID="ID_1554830698" MODIFIED="1493662305121" POSITION="right" TEXT="cheetsheets">
<node CREATED="1493662356804" FOLDED="true" ID="ID_1928531049" MODIFIED="1493662384103" TEXT="ML in emoji">
<node CREATED="1493662360887" ID="ID_845653318" MODIFIED="1493662380920">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="../../Documents/mindmaps/images/ML_in_emoji.png" />
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1493662408802" ID="ID_283072225" MODIFIED="1493662409772" TEXT="A visual introduction to machine learning">
<node CREATED="1493662415785" LINK="http://www.r2d3.us/visual-intro-to-machine-learning-part-1/" MODIFIED="1493662415785" TEXT="r2d3.us &gt; Visual-intro-to-machine-learning-part-1"/>
</node>
</node>
<node CREATED="1492828847990" ID="ID_1535937047" MODIFIED="1492828858084" POSITION="right" TEXT="Terminology">
<node CREATED="1492828860153" FOLDED="true" ID="ID_1273682888" MODIFIED="1492829024323" TEXT="&quot;natural gradient&quot;">
<node CREATED="1492828865247" ID="ID_1985599573" MODIFIED="1492828884542" TEXT="I saw a better explanation (I think) somewhere else..."/>
<node CREATED="1492828877919" LINK="http://kvfrans.com/a-intuitive-explanation-of-natural-gradient-descent/" MODIFIED="1492828877919" TEXT="kvfrans.com &gt; A-intuitive-explanation-of-natural-gradient-descent"/>
<node CREATED="1492829022289" LINK="https://arxiv.org/pdf/1301.3584v7.pdf" MODIFIED="1492829022289" TEXT="https://arxiv.org/pdf/1301.3584v7.pdf"/>
</node>
<node CREATED="1492828933295" FOLDED="true" ID="ID_1441675442" MODIFIED="1492828987492" TEXT="KL Divergence">
<node CREATED="1492828942881" ID="ID_59772246" MODIFIED="1492828943449" TEXT="Kullback&#x2013;Leibler divergence"/>
<node CREATED="1492828948383" LINK="https://en.wikipedia.org/wiki/Kullback%E2%80%93Leibler_divergence" MODIFIED="1492828948383" TEXT="https://en.wikipedia.org/wiki/Kullback%E2%80%93Leibler_divergence"/>
<node CREATED="1492828970148" ID="ID_457734527" MODIFIED="1492828970832" TEXT="In probability theory and information theory, the Kullback&#x2013;Leibler divergence,[1][2] also called information divergence, information gain, relative entropy, KLIC, or KL divergence, is a measure (but not a metric) of the non-symmetric difference between two probability distributions P and Q. The Kullback&#x2013;Leibler divergence was originally introduced by Solomon Kullback and Richard Leibler in 1951 as the directed divergence between two distributions; Kullback himself preferred the name discrimination information.[3] It is discussed in Kullback&apos;s historic text, Information Theory and Statistics.[2]"/>
</node>
</node>
<node CREATED="1474906159185" ID="ID_1960949856" MODIFIED="1474906162761" POSITION="right" TEXT="Neural Networks">
<node CREATED="1493872347227" ID="ID_287463608" MODIFIED="1493872349678" TEXT="video">
<node CREATED="1493872367557" ID="ID_1931950256" MODIFIED="1497593994352" TEXT="How Deep Neural Networks Work">
<node CREATED="1493872431457" ID="ID_1464172523" LINK="https://www.youtube.com/watch?v=ILsA4nyG7I0" MODIFIED="1493872431457" TEXT="https://www.youtube.com/watch?v=ILsA4nyG7I0"/>
<node CREATED="1497593978504" ID="ID_1535442672" MODIFIED="1497593979505" TEXT="Brandon Rohrer"/>
</node>
<node CREATED="1497593995159" ID="ID_1682795292" MODIFIED="1497594006891" TEXT="Google&apos;s AI Chief Geoffrey Hinton &#xa;How Neural Networks Really Work">
<node CREATED="1497594012825" LINK="https://www.youtube.com/watch?v=EInQoVLg_UY" MODIFIED="1497594012825" TEXT="https://www.youtube.com/watch?v=EInQoVLg_UY"/>
</node>
</node>
<node CREATED="1474906185029" FOLDED="true" ID="ID_1905363263" MODIFIED="1493872346723" TEXT="A Beginner&apos;s Guide To Understanding &#xa;Convolutional Neural Networks">
<node CREATED="1474906171351" ID="ID_1596055149" LINK="https://adeshpande3.github.io/adeshpande3.github.io/A-Beginner%27s-Guide-To-Understanding-Convolutional-Neural-Networks/" MODIFIED="1474906171351" TEXT="https://adeshpande3.github.io/adeshpande3.github.io/A-Beginner%27s-Guide-To-Understanding-Convolutional-Neural-Networks/"/>
</node>
<node CREATED="1497024832998" ID="ID_365194402" LINK="http://www.asimovinstitute.org/neural-network-zoo/" MODIFIED="1497024839645" TEXT="asimovinstitute.org &gt; Neural-network-zoo"/>
<node CREATED="1471920795421" ID="ID_399210055" MODIFIED="1471920841676" TEXT="Neural Networks and Deep Learning&#xa;By Michael Nielsen / Jan 2016">
<node CREATED="1471920750455" FOLDED="true" ID="ID_1345339278" MODIFIED="1474906269399" TEXT="CHAPTER 2 - How the backpropagation algorithm works">
<node CREATED="1471920750455" LINK="http://neuralnetworksanddeeplearning.com/chap2.html" MODIFIED="1471920750455" TEXT="neuralnetworksanddeeplearning.com &gt; Chap2"/>
</node>
<node CREATED="1471920723519" FOLDED="true" ID="ID_1085598251" MODIFIED="1474906270961" TEXT="CHAPTER 4 - A visual proof that neural nets &#xa;can compute any function">
<node CREATED="1471920723520" ID="ID_1585748877" LINK="http://neuralnetworksanddeeplearning.com/chap4.html" MODIFIED="1471920723520" TEXT="neuralnetworksanddeeplearning.com &gt; Chap4"/>
</node>
</node>
<node CREATED="1471920732454" ID="ID_534686567" MODIFIED="1482992247404" TEXT="backpropagation is just the chain rule:&#xa;(Stanford)">
<node CREATED="1471920732455" ID="ID_1388358428" LINK="http://cs231n.github.io/optimization-2/" MODIFIED="1471920732455" TEXT="cs231n.github.io &gt; Optimization-2"/>
</node>
<node CREATED="1482991694674" ID="ID_1795534758" MODIFIED="1482991694674" TEXT="NNs standford slides">
<node CREATED="1482991708692" LINK="http://cs231n.stanford.edu/slides/winter1516_lecture4.pdf" MODIFIED="1482991708692" TEXT="cs231n.stanford.edu &gt; Slides &gt; Winter1516 lecture4"/>
</node>
<node CREATED="1484618920698" ID="ID_823651272" MODIFIED="1484618922590" TEXT="rotations">
<node CREATED="1484617249907" ID="ID_527847984" MODIFIED="1484618925090" TEXT="Harmonic Networks&#xa;(accountng for rotations)">
<node CREATED="1484617271038" ID="ID_50049901" LINK="https://www.youtube.com/watch?v=rAbhypxs1qQ" MODIFIED="1484617271038" TEXT="https://www.youtube.com/watch?v=rAbhypxs1qQ"/>
<node CREATED="1484618945740" ID="ID_1606684318" MODIFIED="1484618946534" TEXT=" Rotated MNIST"/>
</node>
<node CREATED="1484618778282" ID="ID_1328217625" MODIFIED="1484618927519" TEXT="Group Equivariant Convolutional Networks">
<node CREATED="1484618784491" ID="ID_1267089284" LINK="http://jmlr.org/proceedings/papers/v48/cohenc16.pdf" MODIFIED="1484618784491" TEXT="jmlr.org &gt; Proceedings &gt; Papers &gt; V48 &gt; Cohenc16"/>
<node CREATED="1484618797482" ID="ID_1115230394" MODIFIED="1484618801525" TEXT="&quot;exploits symmetry&quot;">
<node CREATED="1484618825372" ID="ID_1085595934" MODIFIED="1484618829985" TEXT="symmetry groups">
<node CREATED="1484618941371" ID="ID_1439650822" MODIFIED="1484618942686" TEXT=" Rotated MNIST"/>
</node>
</node>
<node CREATED="1484618786807" ID="ID_145049190" MODIFIED="1484618789350" TEXT="Taco Cohen">
<node CREATED="1484618843004" ID="ID_932578632" MODIFIED="1484618847375" TEXT="interned at OpenAI"/>
</node>
</node>
</node>
<node CREATED="1484617429075" ID="ID_1491838833" MODIFIED="1484617442240" TEXT="Image Synthesis From Text With Deep Learning&#xa;(producing high resolution images)"/>
<node CREATED="1493663538196" ID="ID_1819876880" MODIFIED="1493663552645" TEXT="Geometry for 3D reconstruction from 2D images">
<node CREATED="1493663553186" ID="ID_1181962047" LINK="http://alexgkendall.com/computer_vision/have_we_forgotten_about_geometry_in_computer_vision/" MODIFIED="1493663553186" TEXT="alexgkendall.com &gt; Computer vision &gt; Have we forgotten about geometry in computer vision"/>
</node>
<node CREATED="1504449423851" ID="ID_536264828" MODIFIED="1504449428258" TEXT="colah">
<node CREATED="1504449451712" ID="ID_1891293974" MODIFIED="1504449453931" TEXT="">
<node CREATED="1504449444691" ID="ID_327559122" MODIFIED="1504449445457" TEXT="Neural Networks, Manifolds, and Topology"/>
<node CREATED="1504449450151" ID="ID_1145601390" MODIFIED="1504449450642" TEXT="Posted on April 6, 2014"/>
<node CREATED="1504449464946" ID="ID_1538822370" LINK="http://colah.github.io/posts/2014-03-NN-Manifolds-Topology/" MODIFIED="1504449464946" TEXT="colah.github.io &gt; Posts &gt; 2014-03-NN-Manifolds-Topology"/>
<node CREATED="1504449469901" ID="ID_1357378171" MODIFIED="1504449471447" TEXT="comments">
<node CREATED="1504449510892" ID="ID_658145569" MODIFIED="1504449513693" TEXT="book">
<node CREATED="1504449514166" ID="ID_1122822252" MODIFIED="1504449515206" TEXT="An Introduction to Algebraic Geometry and Statistical Learning Theory"/>
<node CREATED="1504449520654" ID="ID_1908676769" LINK="http://watanabe-www.math.dis.titech.ac.jp/users/swatanab/algestle.pdf" MODIFIED="1504449520654" TEXT="watanabe-www.math.dis.titech.ac.jp &gt; Users &gt; Swatanab &gt; Algestle"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1495842514993" ID="ID_1741912675" MODIFIED="1495842522489" POSITION="right" TEXT="Generative Adversarial Networks">
<node CREATED="1495842512264" ID="ID_1836119568" MODIFIED="1495842525544" TEXT="GANs">
<node CREATED="1495842536468" ID="ID_1964330503" LINK="https://en.wikipedia.org/wiki/Generative_adversarial_networks" MODIFIED="1495842536468" TEXT="https://en.wikipedia.org/wiki/Generative_adversarial_networks"/>
<node CREATED="1495842570469" ID="ID_581732376" MODIFIED="1495842583402" TEXT="Deep adversarial learning is finally ready and will radically change the game">
<node CREATED="1495842596995" LINK="https://medium.com/intuitionmachine/deep-adversarial-learning-is-finally-ready-and-will-radically-change-the-game-f0cfda7b91d3" MODIFIED="1495842596995" TEXT="https://medium.com/intuitionmachine/deep-adversarial-learning-is-finally-ready-and-will-radically-change-the-game-f0cfda7b91d3"/>
<node CREATED="1495842548400" ID="ID_795785329" MODIFIED="1495842549239" TEXT="Wasserstein GAN">
<node CREATED="1495842607352" ID="ID_1226938184" LINK="https://arxiv.org/pdf/1701.07875.pdf" MODIFIED="1495842607352" TEXT="https://arxiv.org/pdf/1701.07875.pdf"/>
</node>
<node CREATED="1495842557271" ID="ID_174565593" MODIFIED="1495842557791" TEXT="Improved Training of Wasserstein GANs">
<node CREATED="1495842613801" LINK="https://arxiv.org/pdf/1704.00028.pdf" MODIFIED="1495842613801" TEXT="https://arxiv.org/pdf/1704.00028.pdf"/>
</node>
</node>
</node>
</node>
<node CREATED="1493944732663" HGAP="25" ID="ID_1840923977" MODIFIED="1493944752294" POSITION="left" TEXT="TensorFlow" VSHIFT="-156">
<node CREATED="1493944735961" ID="ID_363331034" MODIFIED="1493944738758" TEXT="courses">
<node CREATED="1473312258846" HGAP="52" ID="ID_767241475" MODIFIED="1493944749554" TEXT="TensorFlow Udacity" VSHIFT="3">
<node CREATED="1473561455940" ID="ID_1279094455" MODIFIED="1473561459120" TEXT="NotMNST">
<node CREATED="1473561454830" ID="ID_1174580131" LINK="https://classroom.udacity.com/courses/ud730/lessons/6370362152/concepts/63703142310923#" MODIFIED="1473561454830" TEXT="https://classroom.udacity.com/courses/ud730/lessons/6370362152/concepts/63703142310923#"/>
</node>
<node CREATED="1481412237258" ID="ID_550872618" MODIFIED="1481412238252" TEXT="Notes">
<node CREATED="1481412256101" ID="ID_1307269501" MODIFIED="1481412257551" TEXT="L1">
<node CREATED="1481412258019" ID="ID_820082413" MODIFIED="1481412260737" TEXT="SoftMax">
<node CREATED="1481412261602" ID="ID_867745406" MODIFIED="1481412269719" TEXT="scaling inputs">
<node CREATED="1481412269931" ID="ID_1237697223" MODIFIED="1481412274844" TEXT="if multiplying by 10">
<node CREATED="1481412275666" ID="ID_1793964025" MODIFIED="1481412289529" TEXT="output values go toward 0 or 1"/>
</node>
<node CREATED="1481412292003" ID="ID_1236080259" MODIFIED="1481412294782" TEXT="if dividing by 10">
<node CREATED="1481412295025" ID="ID_876480816" MODIFIED="1481412330255" TEXT="output vaules go toward the uniform distribution  ">
<node CREATED="1481413204812" LINK="https://classroom.udacity.com/courses/ud730/lessons/6370362152/concepts/63975713190923#" MODIFIED="1481413204812" TEXT="https://classroom.udacity.com/courses/ud730/lessons/6370362152/concepts/63975713190923#"/>
</node>
</node>
</node>
</node>
<node CREATED="1481413207699" ID="ID_1286190994" MODIFIED="1481413211256" TEXT="OneHot Encoding">
<node CREATED="1481413290916" ID="ID_1449971969" MODIFIED="1481413301913" TEXT="vector with as many values as classes"/>
<node CREATED="1481413303491" ID="ID_1349112961" MODIFIED="1481413311304" TEXT="only one has 1.0 the rest 0.0"/>
</node>
<node CREATED="1481413285798" ID="ID_530323309" MODIFIED="1481413288478" TEXT="Cross Entropy">
<node CREATED="1481413289809" ID="ID_1438628431" LINK="https://classroom.udacity.com/courses/ud730/lessons/6370362152/concepts/63798118260923" MODIFIED="1481413289809" TEXT="https://classroom.udacity.com/courses/ud730/lessons/6370362152/concepts/63798118260923"/>
<node CREATED="1481413338732" ID="ID_898615006" MODIFIED="1481413348381" TEXT="Measures a distance between vectors D(S,L)">
<node CREATED="1481413351211" ID="ID_1049366153" MODIFIED="1481413352642" TEXT="NOTE">
<node CREATED="1481413352950" ID="ID_1045331336" MODIFIED="1481413362050" TEXT="D(S,L) != D(L,S)"/>
</node>
</node>
<node CREATED="1481413552705" ID="ID_1845975" MODIFIED="1481413585533" TEXT="Multiinomial Logistic Classification  "/>
</node>
<node CREATED="1481413718880" ID="ID_986678769" MODIFIED="1481413725907" TEXT="Numerical Stability">
<node CREATED="1481413726383" ID="ID_1788744218" MODIFIED="1481413739262" TEXT="problems arise when adding very small values to very big values"/>
</node>
<node CREATED="1481413782329" ID="ID_1977092131" MODIFIED="1481413794113" TEXT="Normalized Inputs And Initial Weights">
<node CREATED="1481413794463" ID="ID_1317900366" MODIFIED="1481413796917" TEXT="Keep">
<node CREATED="1481413797307" ID="ID_99947326" MODIFIED="1481413801896" TEXT="0 mean"/>
<node CREATED="1481413802354" ID="ID_190405166" MODIFIED="1481413808394" TEXT="Equalized Variance"/>
</node>
<node CREATED="1481413855396" ID="ID_359761488" MODIFIED="1481413876268" TEXT="Choose your initial randomized weights with a Gaussian distribution  ">
<node CREATED="1481413888140" ID="ID_431700380" MODIFIED="1481413889188" TEXT="eg.">
<node CREATED="1481413889673" ID="ID_772333615" MODIFIED="1481413899592" TEXT="(pixels-128)/128"/>
</node>
</node>
</node>
</node>
<node CREATED="1481413387083" ID="ID_1795856369" MODIFIED="1481413402279" TEXT="better to take notes in a Jupyter Notebook?"/>
<node CREATED="1481413403576" ID="ID_1500285746" MODIFIED="1481413406866" TEXT="with screenshots"/>
</node>
</node>
</node>
<node CREATED="1493944759527" FOLDED="true" ID="ID_1667485793" MODIFIED="1493944773239" TEXT="Kadenze: &#xa;Creative Applications of &#xa;Deep Learning with TensorFlow">
<node CREATED="1493944753995" ID="ID_401456468" LINK="https://www.class-central.com/mooc/6679/kadenze-creative-applications-of-deep-learning-with-tensorflow" MODIFIED="1493944762920" TEXT="https://www.class-central.com/mooc/6679/kadenze-creative-applications-of-deep-learning-with-tensorflow"/>
</node>
</node>
<node CREATED="1477634510121" ID="ID_1916009598" MODIFIED="1477635273592" POSITION="right" STYLE="bubble" TEXT="Reinforcement Learning">
<node CREATED="1477634515654" ID="ID_1263456093" MODIFIED="1477635273593" TEXT="Deep Q-Learning">
<node CREATED="1477634529255" ID="ID_56752110" MODIFIED="1477635273593" TEXT="Guest Post (Part I): Demystifying Deep Reinforcement Learning">
<node CREATED="1477634538688" LINK="https://www.nervanasys.com/demystifying-deep-reinforcement-learning/" MODIFIED="1477635273593" TEXT="https://www.nervanasys.com/demystifying-deep-reinforcement-learning/"/>
</node>
</node>
<node CREATED="1477634652334" LINK="http://videolectures.net/rldm2015_silver_reinforcement_learning/" MODIFIED="1477635273593" TEXT="videolectures.net &gt; Rldm2015 silver reinforcement learning"/>
<node CREATED="1482992124303" ID="ID_663027916" MODIFIED="1482992133167" STYLE="fork" TEXT="RL2: Fast Reinforcement Learning via Slow Reinforcement Learning">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node CREATED="1482992155668" ID="ID_1766050028" LINK="https://arxiv.org/abs/1611.02779" MODIFIED="1482992155668" TEXT="https://arxiv.org/abs/1611.02779"/>
<node CREATED="1482992166673" FOLDED="true" ID="ID_1076723767" MODIFIED="1482992172629" TEXT="from:">
<node CREATED="1482992166674" ID="ID_365432603" LINK="https://medium.com/initialized-capital/the-public-policy-implications-of-artificial-intelligence-1df075c49755#.9kj2ac67a" MODIFIED="1482992166674" TEXT="https://medium.com/initialized-capital/the-public-policy-implications-of-artificial-intelligence-1df075c49755#.9kj2ac67a"/>
</node>
</node>
</node>
<node CREATED="1477634908718" ID="ID_1456007648" MODIFIED="1477634919369" POSITION="right" TEXT="Bayesian Interpretation of Regularization">
<node CREATED="1477634929293" ID="ID_1160964555" LINK="http://videolectures.net/deeplearning2016_precup_machine_learning/" MODIFIED="1477634929293" TEXT="videolectures.net &gt; Deeplearning2016 precup machine learning"/>
</node>
<node CREATED="1482125701448" ID="ID_1761248693" MODIFIED="1482125716656" POSITION="left" TEXT="Coursera">
<node CREATED="1482125681633" ID="ID_898901322" LINK="ProbabalisticGraphicalModels.mm" MODIFIED="1482125713837" STYLE="bubble" TEXT="ProbabalisticGraphicalModels.mm">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
<node CREATED="1482992716116" ID="ID_1111238783" MODIFIED="1482992719557" POSITION="left" TEXT="Deep Learning">
<node CREATED="1482992721654" ID="ID_1747161896" MODIFIED="1482992724052" TEXT="AWESOME">
<node CREATED="1482992724692" ID="ID_410736813" MODIFIED="1482992724692" TEXT="Reference: a-guide-to-deep-learning">
<node CREATED="1482992732034" LINK="http://yerevann.com/a-guide-to-deep-learning/" MODIFIED="1482992732034" TEXT="yerevann.com &gt; A-guide-to-deep-learning"/>
</node>
</node>
<node CREATED="1482989356608" ID="ID_424644442" MODIFIED="1482992739339" TEXT="NVidia">
<node CREATED="1482989362516" ID="ID_330094152" LINK="https://nvidia.qwiklab.com/tags/Deep%20Learning" MODIFIED="1482989362516" TEXT="https://nvidia.qwiklab.com/tags/Deep%20Learning"/>
<node CREATED="1482989384386" ID="ID_1874391142" LINK="https://www.nvidia.com/object/deep-learning-institute.html" MODIFIED="1482992744453" TEXT="https://www.nvidia.com/object/deep-learning-institute.html"/>
</node>
<node CREATED="1497588379053" ID="ID_331105438" MODIFIED="1497588384160" TEXT="Ladder Networks">
<node CREATED="1497588395339" LINK="https://medium.com/intuitionmachine/the-strange-loop-in-deep-learning-38aa7caf6d7d" MODIFIED="1497588395339" TEXT="https://medium.com/intuitionmachine/the-strange-loop-in-deep-learning-38aa7caf6d7d"/>
<node CREATED="1497588415182" ID="ID_654565823" MODIFIED="1497588416165" TEXT="July 2015">
<node CREATED="1497588421802" LINK="https://arxiv.org/abs/1507.02672v2" MODIFIED="1497588421802" TEXT="https://arxiv.org/abs/1507.02672v2"/>
</node>
</node>
</node>
<node CREATED="1477711720696" ID="ID_648716725" MODIFIED="1477711730358" POSITION="left" TEXT="Berkeley Class on Youtube">
<node CREATED="1492225728351" ID="ID_41617932" MODIFIED="1492225748257" TEXT="cs188 2016">
<node CREATED="1492225734144" ID="ID_1034156465" MODIFIED="1492225796324" TEXT="Pieter Abbeel"/>
<node CREATED="1492225743781" ID="ID_791233781" MODIFIED="1492225745561" TEXT="Playlist">
<node CREATED="1492225746007" LINK="https://www.youtube.com/playlist?list=PLIeooNSdhQE5kRrB71yu5yP9BRCJCSbMt" MODIFIED="1492225746007" TEXT="https://www.youtube.com/playlist?list=PLIeooNSdhQE5kRrB71yu5yP9BRCJCSbMt"/>
</node>
</node>
<node CREATED="1492225717935" FOLDED="true" ID="ID_66568195" MODIFIED="1492225749161" TEXT="cs188 2013">
<node CREATED="1477711800731" ID="ID_1086340829" MODIFIED="1477711808560" TEXT="Lecture 1">
<node CREATED="1477711804452" ID="ID_1021466296" LINK="https://www.youtube.com/watch?v=tONNlv6osG4" MODIFIED="1477711804452" TEXT="https://www.youtube.com/watch?v=tONNlv6osG4"/>
</node>
<node CREATED="1477711749670" ID="ID_1043190249" LINK="https://www.youtube.com/channel/UCshmLD2MsyqAKBx8ctivb5Q" MODIFIED="1477711749670" TEXT="https://www.youtube.com/channel/UCshmLD2MsyqAKBx8ctivb5Q"/>
</node>
</node>
<node CREATED="1482988564891" ID="ID_1879159831" MODIFIED="1482988573698" POSITION="right" TEXT="Adarsh&apos;s notes on MLConf 2016">
<node CREATED="1482988578987" LINK="https://github.com/adarsh0806/ODSCWest/blob/master/MLConf.md" MODIFIED="1482988578987" TEXT="https://github.com/adarsh0806/ODSCWest/blob/master/MLConf.md"/>
</node>
<node CREATED="1494115683470" ID="ID_544992217" MODIFIED="1494115686237" POSITION="right" TEXT="Papers">
<node CREATED="1494115688072" ID="ID_158612020" MODIFIED="1494121663572" TEXT="in iBooks">
<cloud/>
</node>
<node CREATED="1494115700207" ID="ID_1734578294" MODIFIED="1494115702430" TEXT="One shot learning of simple visual concepts">
<node CREATED="1494115709145" LINK="http://web.mit.edu/jgross/Public/lake_etal_cogsci2011.pdf" MODIFIED="1494115709145" TEXT="web.mit.edu &gt; Jgross &gt; Public &gt; Lake etal cogsci2011"/>
<node CREATED="1494115716998" ID="ID_1317231270" MODIFIED="1494115717545" TEXT="Brenden M. Lake, Ruslan Salakhutdinov, Jason Gross, and Joshua B. Tenenbaum"/>
<node CREATED="1494115722410" ID="ID_1253682519" MODIFIED="1494115723157" TEXT="Department of Brain and Cognitive Sciences Massachusetts Institute of Technology"/>
<node CREATED="1494115750537" ID="ID_1206492464" MODIFIED="1494115779670" TEXT="One shot learning"/>
<node CREATED="1494115745474" ID="ID_1351203746" MODIFIED="1494115749990" TEXT="Generative model">
<node CREATED="1494115774094" ID="ID_714051897" MODIFIED="1494115777613" TEXT="Stroke model"/>
</node>
</node>
<node CREATED="1505401092642" ID="ID_1425961357" MODIFIED="1505401481363" TEXT="Cooperative Inverse Reinforcement Learning">
<node CREATED="1505401108211" LINK="https://people.eecs.berkeley.edu/~dhm/papers/CIRL_NIPS_16.pdf" MODIFIED="1505401108211" TEXT="https://people.eecs.berkeley.edu/~dhm/papers/CIRL_NIPS_16.pdf"/>
<node CREATED="1505401169451" ID="ID_1840809135" MODIFIED="1505401170056" TEXT="Dylan Hadfield-Menell">
<node CREATED="1505401184137" LINK="http://humancompatible.ai/spotlights" MODIFIED="1505401184137" TEXT="humancompatible.ai &gt; Spotlights"/>
</node>
</node>
<node CREATED="1505401510208" ID="ID_1348961663" MODIFIED="1505401518457" TEXT="Self-Modification of Policy and Utility Function in Rational Agents    ">
<node CREATED="1505401525015" ID="ID_87466666" MODIFIED="1505401525642" TEXT="Daniel Filan">
<node CREATED="1505401530153" LINK="http://danielfilan.com/" MODIFIED="1505401530153" TEXT="danielfilan.com"/>
<node CREATED="1505401545194" ID="ID_1428805529" MODIFIED="1505401547380" TEXT="also">
<node CREATED="1505401548328" ID="ID_1666033214" MODIFIED="1505401549216" TEXT="Thesis: Resource-bounded Complexity-based Priors for Agents">
<node CREATED="1505401561282" ID="ID_740123567" MODIFIED="1505401562363" TEXT="Supervised by Marcus Hutter in 2015"/>
</node>
</node>
</node>
</node>
</node>
</node>
</map>
