<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1378077681682" ID="ID_1208022065" MODIFIED="1378271448862" STYLE="fork" TEXT="Python Language&#xa;Version 2.7">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="14"/>
<node CREATED="1378079757635" HGAP="32" ID="ID_1637077322" MODIFIED="1378271448854" POSITION="left" TEXT="compare to" VSHIFT="-219">
<node CREATED="1378079760801" ID="ID_921158681" LINK="http://freemind.sourceforge.net/wiki/extensions/freemind/flashwindow.php?startCollapsedToLevel=4&amp;initLoadFile=/wiki/images/1/1a/Python_WebLinks.mm&amp;mm_title=Python%202.5%20Computer%20Language%20-%20Contents" MODIFIED="1378271448854" TEXT="freemind.sourceforge.net &gt; Wiki &gt; Extensions &gt; Freemind &gt; Flashwindow.php?startCollapsedToLevel=4&amp;initLoadFile= &gt; Wiki &gt; Images &gt; 1 &gt; 1a &gt; Python WebLinks.mm&amp;mm title=Python 2"/>
</node>
<node BACKGROUND_COLOR="#ffff66" CREATED="1378078311501" HGAP="29" ID="ID_597249686" MODIFIED="1378271448855" POSITION="left" STYLE="bubble" TEXT="Sources" VSHIFT="1">
<cloud/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378078021318" ID="ID_185375866" LINK="http://python.org/download/releases/2.7.5/" MODIFIED="1378079473706" STYLE="bubble" TEXT="python.org &gt; Download &gt; Releases &gt; 2.7.5">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378080922369" ID="ID_298549817" LINK="http://docs.python.org/2.7/" MODIFIED="1378080930855" STYLE="bubble" TEXT="docs.python.org &gt; 2.7">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1379802330874" ID="ID_358959351" MODIFIED="1379802344241" STYLE="bubble" TEXT="scoping">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1379802340597" ID="ID_396447389" LINK="http://en.wikipedia.org/wiki/Scope_" MODIFIED="1379802347781" STYLE="bubble" TEXT="en.wikipedia.org &gt; Wiki &gt; Scope (programming)#Python">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1379802373074" ID="ID_178752876" MODIFIED="1379802382860" STYLE="bubble" TEXT="LEGB rule (Local, Enclosing, Global, Built-in)">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
</node>
<node BACKGROUND_COLOR="#ffff66" CREATED="1378078217451" ID="ID_884190582" MODIFIED="1378080135652" POSITION="left" STYLE="bubble" TEXT="Uses">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<cloud/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378078088576" ID="ID_304345614" MODIFIED="1378079487071" STYLE="bubble" TEXT="Google Apps Engine uses">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378078095462" ID="ID_1871296000" LINK="https://developers.google.com/appengine/docs/python/gettingstartedpython27/introduction" MODIFIED="1378079495358" STYLE="bubble" TEXT="https://developers.google.com/appengine/docs/python/gettingstartedpython27/introduction">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378078096112" ID="ID_858925374" MODIFIED="1378079497445" STYLE="bubble" TEXT=" 9/1/13">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node BACKGROUND_COLOR="#ffff66" CREATED="1378078311501" ID="ID_1330663188" MODIFIED="1378271448855" POSITION="left" STYLE="bubble" TEXT="Built-in Data Types&#xa;">
<cloud/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378079008619" ID="ID_1067052396" MODIFIED="1378079350482" STYLE="bubble" TEXT="Sequence Type">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378079019860" ID="ID_731063620" MODIFIED="1378079096508" TEXT="Immutable">
<node BACKGROUND_COLOR="#ccccff" CREATED="1378079024284" ID="ID_195864611" LINK="Python_2_7_strings.mm" MODIFIED="1378271474340" TEXT="Strings"/>
</node>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378079008619" ID="ID_12168908" MODIFIED="1378265889187" STYLE="bubble" TEXT="Mapping Type">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378266050464" ID="ID_883087854" LINK="Python_2_7_dictionaries.mm" MODIFIED="1378271498165" STYLE="bubble" TEXT="Dictionaries">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
</node>
<node CREATED="1378081007716" HGAP="17" ID="ID_1381249244" MODIFIED="1378274557444" POSITION="right" TEXT="TEMPLATE" VSHIFT="-181">
<node BACKGROUND_COLOR="#ffff66" CREATED="1378078311501" HGAP="29" ID="ID_915551802" MODIFIED="1378271448855" STYLE="bubble" TEXT="" VSHIFT="1">
<cloud/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378079008619" ID="ID_1692684887" MODIFIED="1378081034480" STYLE="bubble" TEXT="">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378079019860" ID="ID_157196591" MODIFIED="1378080989251" TEXT="">
<node BACKGROUND_COLOR="#ccccff" CREATED="1378080995980" ID="ID_1073047176" MODIFIED="1378080999127" STYLE="bubble" TEXT="">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
</node>
</node>
</node>
<node BACKGROUND_COLOR="#ffff66" CREATED="1378078311501" FOLDED="true" HGAP="29" ID="ID_923327166" MODIFIED="1378274562394" POSITION="right" STYLE="bubble" TEXT="Comparisons" VSHIFT="1">
<cloud/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node CREATED="1378274436687" ID="ID_1384069048" MODIFIED="1378274510085">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <table style="font-variant: normal; white-space: normal; line-height: normal; color: rgb(0, 0, 0); border: 0px solid rgb(221, 204, 238); font-style: normal; text-align: start; text-indent: 0px; background-color: rgb(255, 255, 255); font-size: 15.555556297302246px; text-transform: none; font-weight: normal; word-spacing: 0px; font-family: sans-serif; letter-spacing: normal" charset="utf-8" border="1" class="docutils">
      <colgroup>
      <col width="27%" />
      <col width="16%" />
      </colgroup>
      

      <tr valign="bottom">
        <th style="border-top-width: 1px; padding-top: 2px; background-color: rgb(238, 221, 238); padding-bottom: 2px; text-align: center; padding-right: 5px; border-left-width: 0px; padding-left: 5px" class="head">
          <font size="3">Operation </font>
        </th>
        <th style="border-top-width: 1px; padding-top: 2px; background-color: rgb(238, 221, 238); padding-bottom: 2px; text-align: center; padding-right: 5px; border-left-width: 0px; padding-left: 5px" class="head">
          <font size="3">Meaning </font>
        </th>
      </tr>
      <tr valign="top">
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          <font size="0.95em"><tt style="padding-top: 0px; padding-bottom: 0px; padding-right: 1px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-left: 1px" class="docutils literal">&lt;</tt></font><font size="3">&#160;</font>
        </td>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          <font size="3">strictly less than </font>
        </td>
      </tr>
      <tr>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          <font size="0.95em"><tt style="padding-top: 0px; padding-bottom: 0px; padding-right: 1px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-left: 1px" class="docutils literal">&lt;=</tt></font><font size="3">&#160;</font>
        </td>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          <font size="3">less than or equal </font>
        </td>
      </tr>
      <tr>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          <font size="0.95em"><tt style="padding-top: 0px; padding-bottom: 0px; padding-right: 1px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-left: 1px" class="docutils literal">&gt;</tt></font><font size="3">&#160;</font>
        </td>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          <font size="3">strictly greater than </font>
        </td>
      </tr>
      <tr>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          <font size="0.95em"><tt style="padding-top: 0px; padding-bottom: 0px; padding-right: 1px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-left: 1px" class="docutils literal">&gt;=</tt></font><font size="3">&#160;</font>
        </td>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          <font size="3">greater than or equal </font>
        </td>
      </tr>
      <tr>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          <font size="0.95em"><tt style="padding-top: 0px; padding-bottom: 0px; padding-right: 1px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-left: 1px" class="docutils literal">==</tt></font><font size="3">&#160;</font>
        </td>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          <font size="3">equal </font>
        </td>
      </tr>
      <tr>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          <font size="0.95em"><tt style="padding-top: 0px; padding-bottom: 0px; padding-right: 1px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-left: 1px" class="docutils literal">!=</tt></font><font size="3">&#160;</font>
        </td>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          <font size="3">not equal </font>
        </td>
      </tr>
      <tr>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          <font size="0.95em"><tt style="padding-top: 0px; padding-bottom: 0px; padding-right: 1px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-left: 1px" class="docutils literal">is</tt></font><font size="3">&#160;</font>
        </td>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          <font size="3">object identity </font>
        </td>
      </tr>
      <tr>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          <font size="0.95em"><tt style="padding-top: 0px; padding-bottom: 0px; padding-right: 1px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-left: 1px" class="docutils literal">is</tt><tt style="padding-top: 0px; padding-bottom: 0px; padding-right: 1px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-left: 1px" class="docutils literal">&#160;not</tt></font><font size="3">&#160;</font>
        </td>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          <font size="3">negated object identity </font>
        </td>
      </tr>
    </table>
    <br class="Apple-interchange-newline" />
  </body>
</html></richcontent>
</node>
</node>
<node BACKGROUND_COLOR="#ffff66" CREATED="1378319033634" ID="ID_1433566688" MODIFIED="1378319040184" POSITION="right" STYLE="bubble" TEXT="Copying">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378319125307" ID="ID_1805258848" MODIFIED="1378319224600" STYLE="bubble" TEXT="copy module">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378319167880" ID="ID_1143160211" MODIFIED="1378319229055" STYLE="bubble" TEXT="copy.copy(x)">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node BACKGROUND_COLOR="#ccccff" CREATED="1378319167881" ID="ID_336380176" MODIFIED="1378319245018" STYLE="bubble" TEXT="Return a shallow copy of x.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378319167882" ID="ID_966951584" MODIFIED="1378319229056" STYLE="bubble" TEXT="copy.deepcopy(x)">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node BACKGROUND_COLOR="#ccccff" CREATED="1378319167882" ID="ID_1556068463" MODIFIED="1378319245017" STYLE="bubble" TEXT="Return a deep copy of x.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
<node BACKGROUND_COLOR="#ccccff" CREATED="1378319217058" ID="ID_240775042" MODIFIED="1378319245017" STYLE="bubble" TEXT="The deepcopy() function avoids these problems by:">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node CREATED="1378319217058" ID="ID_626085917" MODIFIED="1378319324670" TEXT="keeping a &#x201c;memo&#x201d; dictionary of &#xa;objects already copied during &#xa;the current copying pass; and"/>
<node CREATED="1378319217059" ID="ID_1395872068" MODIFIED="1378319337130" TEXT="letting user-defined classes override &#xa;the copying operation or &#xa;the set of components copied."/>
</node>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378319385251" ID="ID_1806181530" MODIFIED="1378319427589" STYLE="bubble" TEXT="this dict copy is shallow">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node COLOR="#338800" CREATED="1378319400891" ID="ID_1538423002" MODIFIED="1378319427588" TEXT="dict.copy()"/>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378319403066" ID="ID_220969892" MODIFIED="1378319428284" STYLE="bubble" TEXT="this list copy is deep">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node COLOR="#338800" CREATED="1378319418826" ID="ID_1544241601" MODIFIED="1378319428284" TEXT=" copied_list = original_list[:]"/>
</node>
</node>
</node>
</node>
</map>
