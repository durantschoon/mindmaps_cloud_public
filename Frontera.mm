<map version="0.8.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1259690414538" ID="Freemind_Link_1725970853" LINK="../../Documents/mindmaps/MetaMap.mm" MODIFIED="1259690435398" TEXT="Frontera.mm">
<node CREATED="1259690466234" ID="_" MODIFIED="1259690470358" POSITION="right" TEXT="The sheer scale of the MCC should be considered before understanding its challenges. Cheney notes three scale effects of large cooperative organizations: &#xa;&#xa;(1) very few cooperatives, and no large ones (that is, with several thousand or more employees) achieve anything close to economic equality in terms of salaries and benefits; (2) growth and bureaucratization (almost) inevitably lead to great disparities in decisional power between upper-level  managers (whatever they may be called) and rank-and-file workers; and (3) large cooperative systems tend to develop quasi-unions even where organized is proscribed or seen as irrelevant. (Cheney 2006, p. 194)"/>
<node CREATED="1292821228493" LINK="http://www.fonterra.com/wps/wcm/connect/fonterracom/fonterra.com/Home/" MODIFIED="1292821228493" POSITION="left" TEXT="fonterra.com &gt; Wps &gt; Wcm &gt; Connect &gt; Fonterracom &gt; Fonterra.com &gt; Home"/>
</node>
</map>
