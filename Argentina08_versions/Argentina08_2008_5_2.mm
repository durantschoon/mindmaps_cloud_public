<map version="0.8.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node COLOR="#000000" CREATED="1204130609122" ID="Freemind_Link_1543202329" LINK="../../Documents/mindmaps/MetaMap.mm" MODIFIED="1204208412173" TEXT="Argentina08">
<font NAME="SansSerif" SIZE="20"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties"/>
<node COLOR="#0033ff" CREATED="1204130705493" FOLDED="true" ID="Freemind_Link_1152458909" MODIFIED="1207186409325" POSITION="left" TEXT="Plane tickets">
<font NAME="SansSerif" SIZE="18"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#00b439" CREATED="1205613286330" ID="Freemind_Link_1049101795" MODIFIED="1205613287090" TEXT="vio">
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1205597434956" ID="Freemind_Link_882493849" MODIFIED="1205613292558" TEXT="Check frequent flyer miles">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1205597426081" ID="Freemind_Link_609350179" MODIFIED="1205613292587" TEXT="BA travel agent">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1205613274573" ID="Freemind_Link_1989178600" MODIFIED="1205613292590" TEXT="Carbone">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1205613299106" ID="Freemind_Link_1918879382" MODIFIED="1205613300466" TEXT="4/15"/>
<node COLOR="#111111" CREATED="1207074118392" ID="Freemind_Link_423492983" MODIFIED="1207074123080" TEXT="Buy">
<node COLOR="#111111" CREATED="1207074123922" ID="Freemind_Link_32599655" MODIFIED="1207074125770" TEXT="4/25"/>
</node>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1204208241680" ID="Freemind_Link_1988181911" MODIFIED="1204208412050" POSITION="left" TEXT="Buenos Aires">
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1204130619516" FOLDED="true" ID="Freemind_Link_1089484230" MODIFIED="1207186341042" TEXT="Room to rent in BsAs">
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#990000" CREATED="1205598718824" ID="Freemind_Link_258193499" MODIFIED="1205598724673" TEXT="space for yoga">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1205598728021" ID="Freemind_Link_1557579057" MODIFIED="1205598731092" TEXT="wireless internet">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1205598748845" ID="Freemind_Link_1429003991" MODIFIED="1205598751795" TEXT="cooking">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1205598752354" ID="Freemind_Link_876490673" MODIFIED="1205598754555" TEXT="natural light">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1204152892922" FOLDED="true" ID="_" MODIFIED="1208042567054" POSITION="left" TEXT="spanish classes on skype!">
<font NAME="SansSerif" SIZE="18"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#00b439" CREATED="1205613366745" ID="Freemind_Link_1090849409" MODIFIED="1205613367966" TEXT="duro">
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1204152987361" ID="Freemind_Link_1319155090" MODIFIED="1208041591812" TEXT="monica fridays at 11">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1208041592830" ID="Freemind_Link_979065692" MODIFIED="1208041600095" TEXT="things to learn">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1205613371444" ID="Freemind_Link_1385108921" MODIFIED="1208041602189" TEXT="how to negotiate the price of a phone">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1208041604519" ID="Freemind_Link_1924504526" MODIFIED="1208041607348" TEXT="foods"/>
<node COLOR="#111111" CREATED="1208041614355" ID="Freemind_Link_162190641" MODIFIED="1208041633812" TEXT="shopping at the fmkt"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1204130624600" ID="Freemind_Link_1890408025" MODIFIED="1204208412079" POSITION="left" TEXT="Sublet JP place">
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1208041640139" ID="Freemind_Link_967564360" MODIFIED="1208041644941" TEXT="website">
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1208041645880" ID="Freemind_Link_1631937921" MODIFIED="1208041651887" TEXT="http://www.viand.net/sublet.html">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1208041655074" ID="Freemind_Link_751650102" MODIFIED="1208041675900" TEXT="advertising">
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1208041677324" ID="Freemind_Link_1429935025" MODIFIED="1208041679179" TEXT="done">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1208041658723" ID="Freemind_Link_318620143" MODIFIED="1208041692296" TEXT="queer agenda 4.10">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1208041693387" ID="Freemind_Link_115458444" MODIFIED="1208041695795" TEXT="mrap 4.10"/>
<node COLOR="#111111" CREATED="1208041696795" ID="Freemind_Link_1829488263" MODIFIED="1208041701253" TEXT="jamey"/>
<node COLOR="#111111" CREATED="1208041702329" ID="Freemind_Link_548123270" MODIFIED="1208041705952" TEXT="siv"/>
<node COLOR="#111111" CREATED="1208041707087" ID="Freemind_Link_946623835" MODIFIED="1208041711480" TEXT="debbie"/>
<node COLOR="#111111" CREATED="1207074144533" ID="Freemind_Link_772849895" MODIFIED="1208100565504" TEXT="Joanna">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1208041680386" ID="Freemind_Link_116204872" MODIFIED="1208041682198" TEXT="not yet">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1208041713548" ID="Freemind_Link_333873722" MODIFIED="1208041716809" TEXT="soc grads"/>
<node COLOR="#111111" CREATED="1208041717992" ID="Freemind_Link_539764342" MODIFIED="1208041722028" TEXT="craigslist"/>
<node COLOR="#111111" CREATED="1208041723184" ID="Freemind_Link_106710830" MODIFIED="1208041730600" TEXT="tango students"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1207074231979" ID="Freemind_Link_448259766" MODIFIED="1207074232971" TEXT="done">
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1207074233693" ID="Freemind_Link_667262950" MODIFIED="1207074236148" TEXT="talked to">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1205613327149" ID="Freemind_Link_1562443580" MODIFIED="1207074238505" TEXT="Chirsham">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1207074240205" ID="Freemind_Link_319002843" MODIFIED="1207074242286" TEXT="Chris Tilly"/>
<node COLOR="#111111" CREATED="1207074249906" ID="Freemind_Link_1584360205" MODIFIED="1207074254814" TEXT="Duro Friends &amp; Family"/>
<node COLOR="#111111" CREATED="1204986769827" ID="Freemind_Link_555328795" MODIFIED="1208041997055" TEXT="Aja?">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1208041939888" ID="Freemind_Link_1491171204" MODIFIED="1208041944382" POSITION="left" TEXT="mail person for JP">
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1208041945398" ID="Freemind_Link_770741453" MODIFIED="1208041947252" TEXT="al">
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1208041948498" ID="Freemind_Link_1353683449" MODIFIED="1208041971851" TEXT="in exchange for 5 hours of privates">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1207186356166" ID="Freemind_Link_1125369246" MODIFIED="1207186359602" POSITION="left" TEXT="Locks">
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1205596408173" ID="Freemind_Link_689525138" MODIFIED="1207186375716" TEXT="call locksmith">
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1204130632882" ID="Freemind_Link_1780454399" MODIFIED="1207186379722" TEXT="Lock Aja&apos;s room?">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1204130667402" ID="Freemind_Link_1884717852" MODIFIED="1207186379723" TEXT="keys made for filing cabinets">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1204130638863" ID="Freemind_Link_997855109" MODIFIED="1208104803761" TEXT="talk to chris &amp; marie">
<edge COLOR="#808080" WIDTH="thin"/>
<cloud COLOR="#ffffff"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1204130646138" ID="Freemind_Link_1392664153" MODIFIED="1207186379731" TEXT="cable and lock around gray files">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1204643796749" FOLDED="true" ID="Freemind_Link_1553138250" MODIFIED="1207186382593" TEXT="Steelcase">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1205613334155" ID="Freemind_Link_898639079" MODIFIED="1207186382594" TEXT="duro">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1204643800597" ID="Freemind_Link_678176394" MODIFIED="1207186367791" TEXT="contact about extra keys">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1204643809296" ID="Freemind_Link_84179471" MODIFIED="1205613340613" TEXT="have serial # ready and V&apos;s">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1205597451061" ID="Freemind_Link_1122741976" MODIFIED="1205597465538" POSITION="right" TEXT="arrangements for queer tango class/practica">
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1208042114493" ID="Freemind_Link_1370627548" MODIFIED="1208042122055" TEXT="al willing to do with someone else">
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1208042123355" ID="Freemind_Link_1769203678" MODIFIED="1208042130338" TEXT="jamey&apos;s friend richard?">
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1208042134275" ID="Freemind_Link_1375393138" MODIFIED="1208042136922" TEXT="talk to spontaneous">
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1208042138113" ID="Freemind_Link_460637248" MODIFIED="1208042142974" TEXT="substitute">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1208042144082" ID="Freemind_Link_196385774" MODIFIED="1208042147984" TEXT="tuesdays in the fall">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1204959891700" FOLDED="true" ID="Freemind_Link_310431799" MODIFIED="1205598977113" POSITION="right" TEXT="email people">
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1204954646563" ID="Freemind_Link_1480144738" MODIFIED="1205598981838" TEXT="Frank Lupo">
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1204959768105" ID="Freemind_Link_1150324881" MODIFIED="1205598987696" TEXT="Argentine friends">
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1205805399850" ID="Freemind_Link_1406902854" MODIFIED="1205805406526" TEXT="Pablo Rojas">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1205805410655" ID="Freemind_Link_1252916223" MODIFIED="1205805414664" TEXT="Christian">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1205805415602" ID="Freemind_Link_88984192" MODIFIED="1205805420332" TEXT="Gonzalo">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1205805423923" ID="Freemind_Link_1536187541" MODIFIED="1205805425721" TEXT="Marcello">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1204959903416" ID="Freemind_Link_56590722" MODIFIED="1205598890899" TEXT="vio">
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1204959905091" ID="Freemind_Link_1322096264" MODIFIED="1208042009606" TEXT="schwee">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1204959907734" ID="Freemind_Link_742106011" MODIFIED="1208042109380" TEXT="pteri">
<edge COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1204922752410" ID="Freemind_Link_1504300501" MODIFIED="1204922756960" POSITION="right" TEXT="duro thesis">
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1204696334718" ID="Freemind_Link_1076118413" MODIFIED="1208104773585" TEXT="IRB">
<cloud COLOR="#ffffff"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1204643049600" ID="Freemind_Link_362932494" MODIFIED="1204922759152" TEXT="GRGA reimbursement requirements&#xa;(Graduate Research Grant Award)">
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1204643120900" ID="Freemind_Link_250800014" MODIFIED="1204922759153" TEXT="Student Travel Authorization">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1204643154728" ID="Freemind_Link_1420456682" MODIFIED="1204922759153" TEXT="due 3 weeks (days?) before departure!">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1204643238705" ID="Freemind_Link_119582394" MODIFIED="1204922759156" TEXT="page 7">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1204643125756" ID="Freemind_Link_1865198675" MODIFIED="1204922759161" TEXT="Liability Waiver">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1204643258799" ID="Freemind_Link_1068417690" MODIFIED="1204922759165" TEXT="page 8">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1205599277328" FOLDED="true" ID="Freemind_Link_1379853773" MODIFIED="1205599278168" TEXT="url">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1204643249790" ID="Freemind_Link_113945465" LINK="http://www.uml.edu/gsa/Forms/GRGA_Application_2007-2008.pdf" MODIFIED="1205599280211" TEXT="uml.edu &gt; Gsa &gt; Forms &gt; GRGA Application 2007-2008">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node COLOR="#00b439" CREATED="1205613102305" ID="Freemind_Link_1556266592" MODIFIED="1205613105526" TEXT="query letter">
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1205613106185" ID="Freemind_Link_1499184097" MODIFIED="1205613110477" TEXT="write in English">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1205613111121" ID="Freemind_Link_1078321676" MODIFIED="1205613116302" TEXT="translate to Spanish">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1209505467670" ID="Freemind_Link_1503469895" MODIFIED="1209505478008" TEXT="test recording audio only with video camera">
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1209505480264" ID="Freemind_Link_1190179249" MODIFIED="1209505492281" TEXT="plan for failure scenarios of recording equipment">
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1209505493023" ID="Freemind_Link_1887975087" MODIFIED="1209505496400" TEXT="batteries">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1209505497191" ID="Freemind_Link_518868614" MODIFIED="1209505498741" TEXT="power">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1209505499468" ID="Freemind_Link_666623875" MODIFIED="1209505510917" TEXT="recoding media">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1204922734100" ID="Freemind_Link_36934785" MODIFIED="1207074078275" POSITION="right" TEXT="prepare for">
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1204922738913" ID="Freemind_Link_1107475118" MODIFIED="1207074078275" TEXT="electricity">
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1208042203328" ID="Freemind_Link_594662770" MODIFIED="1208042206328" TEXT="power strip">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1208042207285" ID="Freemind_Link_810530669" MODIFIED="1208042209269" TEXT="converters">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1204922745034" ID="Freemind_Link_58726211" MODIFIED="1207074078283" TEXT="bread">
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1204916830870" ID="Freemind_Link_1031083673" MODIFIED="1207074086319" TEXT="before leaving" VSHIFT="3">
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1205597550382" ID="Freemind_Link_1974846363" MODIFIED="1207074086320" TEXT="both">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1205507155639" ID="Freemind_Link_424993120" MODIFIED="1207074086321" TEXT="list bills to pay on-line">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1204916836956" ID="Freemind_Link_875100434" MODIFIED="1208105171733" TEXT="suspend cell phones">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1207169962707" ID="Freemind_Link_329023053" MODIFIED="1207169970900" TEXT="pre-pay chris &amp; marie">
<node COLOR="#111111" CREATED="1207169972705" ID="Freemind_Link_205654334" MODIFIED="1207169979846" TEXT="check with their schedule!"/>
</node>
</node>
<node COLOR="#990000" CREATED="1205597541425" ID="Freemind_Link_928881736" MODIFIED="1207074086337" TEXT="duro">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1204954628168" ID="Freemind_Link_500144586" MODIFIED="1207074086338" TEXT="put $ in citibank account">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1205504208138" ID="Freemind_Link_227807078" MODIFIED="1205597559118" TEXT="from bank accts">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1205504212425" ID="Freemind_Link_634835225" MODIFIED="1205597559122" TEXT="for tango money">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1207074037978" ID="Freemind_Link_45413083" MODIFIED="1207074040713" TEXT="add two names"/>
</node>
<node COLOR="#111111" CREATED="1208105123009" ID="Freemind_Link_832449894" MODIFIED="1208105127256" TEXT="suspend netflix"/>
<node COLOR="#111111" CREATED="1207073900214" ID="Freemind_Link_1819807365" MODIFIED="1207074086348" TEXT="go through file folder from last time">
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1207073906556" ID="Freemind_Link_726657573" MODIFIED="1207073909356" TEXT="coin purse"/>
<node COLOR="#111111" CREATED="1207073909984" ID="Freemind_Link_998248372" MODIFIED="1207073916736" TEXT="phone numbers of people"/>
<node COLOR="#111111" CREATED="1207073917460" ID="Freemind_Link_1986718796" MODIFIED="1207073922008" TEXT="electrical converters"/>
</node>
<node COLOR="#111111" CREATED="1207404744550" ID="Freemind_Link_902871997" MODIFIED="1207404752596" TEXT="car (disconnect battery?)"/>
<node COLOR="#111111" CREATED="1208637925916" ID="Freemind_Link_193916878" MODIFIED="1208637933276" TEXT="hard drive">
<node COLOR="#111111" CREATED="1208637934048" ID="Freemind_Link_262123169" MODIFIED="1208637936341" TEXT="make space"/>
<node COLOR="#111111" CREATED="1208637943659" ID="Freemind_Link_1584458141" MODIFIED="1208637951354" TEXT="on laptop and external"/>
</node>
</node>
<node COLOR="#990000" CREATED="1205599217916" ID="Freemind_Link_714005169" MODIFIED="1207074086373" TEXT="vio">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1205597487759" ID="Freemind_Link_738965930" MODIFIED="1208105214361" TEXT="see if nokia will work? x2">
<cloud COLOR="#ccccff"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1205598494442" ID="Freemind_Link_1681432778" MODIFIED="1205598500830" POSITION="right" TEXT="shopping">
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1205598871829" ID="Freemind_Link_1529329793" MODIFIED="1205598872688" TEXT="duro">
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1204959781732" ID="Freemind_Link_1500723169" MODIFIED="1205598877131" TEXT="chopped garlic for daniela arcuri">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1205613139484" ID="Freemind_Link_427238677" MODIFIED="1205613141445" TEXT="Ask Jamey"/>
<node COLOR="#111111" CREATED="1204959833476" ID="Freemind_Link_1716153461" MODIFIED="1205598877132" TEXT="vaccuum seal it ourselves?">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1205597527908" ID="Freemind_Link_1265480321" MODIFIED="1205597561097" TEXT="buy more contact lenses">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1205599158385" ID="Freemind_Link_606100567" MODIFIED="1205599159353" TEXT="vio">
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1204986685932" ID="Freemind_Link_1120362040" MODIFIED="1208042449863" TEXT="5.1.08">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1204986695555" ID="Freemind_Link_1060682473" MODIFIED="1208042462220" TEXT="order lipgloss, eyeliner, mascara">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1207097480862" ID="Freemind_Link_1912928146" MODIFIED="1208042472060" TEXT="tampons">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1208042477374" ID="Freemind_Link_1789055406" MODIFIED="1208042481624" TEXT="toothpaste and floss">
<node COLOR="#111111" CREATED="1208100523888" ID="Freemind_Link_312343252" MODIFIED="1208100534359" TEXT="new toothpaste 4.6.08"/>
<node COLOR="#111111" CREATED="1208100535278" ID="Freemind_Link_79843497" MODIFIED="1208100547415" TEXT="vio new floss 4.13.08"/>
</node>
</node>
<node COLOR="#990000" CREATED="1205597475174" ID="Freemind_Link_1600010089" MODIFIED="1205599207683" TEXT="skirts from debbie">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1205613170984" ID="Freemind_Link_814914362" MODIFIED="1205613172638" TEXT="both">
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1205613173153" ID="Freemind_Link_681616218" MODIFIED="1205613176020" TEXT="video camera">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1205613176487" ID="Freemind_Link_1814442081" MODIFIED="1205613178263" TEXT="5/1"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1205598491215" ID="Freemind_Link_994593005" MODIFIED="1205598493042" POSITION="right" TEXT="packing">
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1205598504359" ID="Freemind_Link_1313802243" MODIFIED="1205598506209" TEXT="duro">
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1204643925859" ID="Freemind_Link_1427467941" MODIFIED="1205598513388" TEXT="see palm pilot">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1204643930778" ID="Freemind_Link_1733721361" MODIFIED="1205598513389" TEXT="jacket lining, etc">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1204951551618" ID="Freemind_Link_151049932" MODIFIED="1205598517351" TEXT="bring">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1204951554195" ID="Freemind_Link_1621797805" MODIFIED="1205598517352" TEXT="naming the enemy">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1204951559633" ID="Freemind_Link_1526502362" MODIFIED="1205598517357" TEXT="global revolt">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1204986744145" ID="Freemind_Link_1901370176" MODIFIED="1205598517363" TEXT="Jaynes book">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1205613187099" ID="Freemind_Link_267560775" MODIFIED="1205613198057" TEXT="Statistics book"/>
<node COLOR="#111111" CREATED="1205126022989" ID="Freemind_Link_1103346522" MODIFIED="1205598517367" TEXT="Questionable">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1205126027624" ID="Freemind_Link_1700557458" MODIFIED="1205126032684" TEXT="goth clothes?"/>
</node>
<node COLOR="#111111" CREATED="1205598462500" ID="Freemind_Link_995888195" MODIFIED="1205598517382" TEXT="earplugs">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1205674918175" ID="Freemind_Link_1678123112" MODIFIED="1205674923795" TEXT="orange and blue nuevo hat"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1205598810769" ID="Freemind_Link_263903355" MODIFIED="1205598811587" TEXT="both">
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1205598812087" ID="Freemind_Link_787212293" MODIFIED="1205598815508" TEXT="video cameras">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1205598815998" ID="Freemind_Link_987701056" MODIFIED="1205598818036" TEXT="batteries">
<node COLOR="#111111" CREATED="1205675181636" ID="Freemind_Link_1273988496" MODIFIED="1205675186359" TEXT="AA rechargeable">
<node COLOR="#111111" CREATED="1205675188064" ID="Freemind_Link_1121194287" MODIFIED="1205675189684" TEXT="charger"/>
</node>
</node>
</node>
<node COLOR="#990000" CREATED="1205613210362" ID="Freemind_Link_524832125" MODIFIED="1205613214032" TEXT="canon digital camera">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1207074343507" ID="Freemind_Link_1836623443" MODIFIED="1207074350165" TEXT="if no wireless">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1207074353173" ID="Freemind_Link_679125666" MODIFIED="1207074356283" TEXT="splitter for cable"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1205599166141" ID="Freemind_Link_1022157233" MODIFIED="1205599167001" TEXT="vio">
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1205509914807" ID="Freemind_Link_1694486755" MODIFIED="1205599188725" TEXT="bring">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1205509917075" ID="Freemind_Link_1403486931" MODIFIED="1205599188735" TEXT="shoes file">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1205613233825" ID="Freemind_Link_285950373" MODIFIED="1205613236165" POSITION="right" TEXT="leaving prep">
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1205613241103" ID="Freemind_Link_1245760656" MODIFIED="1205613243443" TEXT="rearrange house">
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1208042426317" ID="Freemind_Link_1527703411" MODIFIED="1208042434492" TEXT="fix the bed">
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1205613243942" ID="Freemind_Link_325600668" MODIFIED="1205613250861" TEXT="train subletter">
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1205613251364" ID="Freemind_Link_1913870801" MODIFIED="1205613252641" TEXT="mail">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1205613253239" ID="Freemind_Link_944219832" MODIFIED="1205613254470" TEXT="internet">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1205613255099" ID="Freemind_Link_1445051279" MODIFIED="1205613255840" TEXT="edd">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1208042398919" ID="Freemind_Link_544318191" MODIFIED="1208042421951" TEXT="fridge ice &amp; vacuum">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1204911860967" ID="Freemind_Link_1457609" MODIFIED="1204911864576" POSITION="left" TEXT="make a budget">
<font NAME="SansSerif" SIZE="18"/>
</node>
<node COLOR="#0033ff" CREATED="1204957045553" FOLDED="true" ID="Freemind_Link_1525474349" MODIFIED="1204957056059" POSITION="left" TEXT="make schedules">
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1208041804124" ID="Freemind_Link_820334271" MODIFIED="1208041806585" TEXT="daily">
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1208041808953" ID="Freemind_Link_132450534" MODIFIED="1208041810854" TEXT="novel">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1208041811861" ID="Freemind_Link_55567911" MODIFIED="1208041813593" TEXT="work">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1208041814699" ID="Freemind_Link_565586643" MODIFIED="1208041816349" TEXT="duro">
<node COLOR="#111111" CREATED="1208041817491" ID="Freemind_Link_129589268" MODIFIED="1208041819511" TEXT="thesis"/>
<node COLOR="#111111" CREATED="1208041820603" ID="Freemind_Link_358016057" MODIFIED="1208041824644" TEXT="statistics"/>
</node>
<node COLOR="#111111" CREATED="1208041825369" ID="Freemind_Link_1886684347" MODIFIED="1208041827183" TEXT="vio">
<node COLOR="#111111" CREATED="1208041828401" ID="Freemind_Link_1061229976" MODIFIED="1208041849251" TEXT="self-help book"/>
</node>
</node>
<node COLOR="#990000" CREATED="1208041850369" ID="Freemind_Link_763968715" MODIFIED="1208041853096" TEXT="tango class">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1208041854195" ID="Freemind_Link_1736579781" MODIFIED="1208041858731" TEXT="make calendar before we go"/>
<node COLOR="#111111" CREATED="1208041883512" ID="Freemind_Link_704758387" MODIFIED="1208041885284" TEXT="DNI"/>
<node COLOR="#111111" CREATED="1208041886395" ID="Freemind_Link_451047439" MODIFIED="1208041888343" TEXT="queer"/>
<node COLOR="#111111" CREATED="1208041889241" ID="Freemind_Link_401348070" MODIFIED="1208041893511" TEXT="copelllo"/>
<node COLOR="#111111" CREATED="1208041894663" ID="Freemind_Link_253902891" MODIFIED="1208041897903" TEXT="chaos"/>
</node>
<node COLOR="#990000" CREATED="1208041874594" ID="Freemind_Link_1496211626" MODIFIED="1208041882363" TEXT="practicas y milongas">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1208041899112" ID="Freemind_Link_1203376397" MODIFIED="1208041905029" TEXT="ask aja"/>
<node COLOR="#111111" CREATED="1208041906015" ID="Freemind_Link_1847876619" MODIFIED="1208041913136" TEXT="la marshall"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1204957056887" ID="Freemind_Link_294733395" MODIFIED="1204957060438" TEXT="weekly">
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1208041783190" ID="Freemind_Link_1946755751" MODIFIED="1208041787025" TEXT="mercados">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1208041788066" ID="Freemind_Link_1209284629" MODIFIED="1208041791349" TEXT="el galp&#xf3;n">
<node COLOR="#111111" CREATED="1208041915025" ID="Freemind_Link_1143651628" MODIFIED="1208041918105" TEXT="W am"/>
<node COLOR="#111111" CREATED="1208041919348" ID="Freemind_Link_701139635" MODIFIED="1208041922323" TEXT="S am"/>
</node>
<node COLOR="#111111" CREATED="1208041792256" ID="Freemind_Link_763749630" MODIFIED="1208041796598" TEXT="other"/>
</node>
<node COLOR="#990000" CREATED="1208041800585" ID="Freemind_Link_251748615" MODIFIED="1208041802457" TEXT="laundry">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1208041867657" ID="Freemind_Link_1030833771" MODIFIED="1208041870268" TEXT="practicas">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1208041871397" ID="Freemind_Link_545396503" MODIFIED="1208041932831" TEXT="yoga">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1204957061196" ID="Freemind_Link_1769592255" MODIFIED="1204957063027" TEXT="monthly">
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1207073979582" ID="Freemind_Link_1221188734" MODIFIED="1208042554040" POSITION="left" TEXT="research">
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1205598449666" ID="Freemind_Link_1925520285" MODIFIED="1207073984641" TEXT="travel">
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1205598451813" ID="Freemind_Link_783345860" MODIFIED="1207073984642" TEXT="getting to/from airport">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1207074004484" ID="Freemind_Link_1873419712" MODIFIED="1207074007353" TEXT="Chicago">
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1207074012443" ID="Freemind_Link_326365912" MODIFIED="1207074015222" TEXT="neighborhood">
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1207097462291" ID="Freemind_Link_517117945" MODIFIED="1208105004963" TEXT="map/print citibanks">
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1204208253827" ID="Freemind_Link_551371172" MODIFIED="1205599031837" TEXT="find a translator">
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#990000" CREATED="1204208266704" ID="Freemind_Link_988286412" MODIFIED="1205599019917" TEXT="Judy">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1205599006909" ID="Freemind_Link_333143099" MODIFIED="1205599010208" TEXT="Millie"/>
</node>
<node COLOR="#990000" CREATED="1208042172385" ID="Freemind_Link_939806650" MODIFIED="1208042179240" TEXT="meeting juliano&apos;s friends may 20">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1205598525302" ID="Freemind_Link_1296664995" MODIFIED="1205598526938" POSITION="left" TEXT="once there">
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1205598668330" ID="Freemind_Link_1798633709" MODIFIED="1205598669209" TEXT="both">
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1205598669770" ID="Freemind_Link_162055372" MODIFIED="1205598699321" TEXT="food shopping &amp; exploration">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1205598678846" ID="Freemind_Link_524197925" MODIFIED="1205598690703" TEXT="tango lessons x6">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1205597507235" ID="Freemind_Link_387806599" MODIFIED="1208042366864" TEXT="buying yerba mate in BA">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1208042258332" ID="Freemind_Link_786194635" MODIFIED="1208042324256" TEXT="phoenician_trading@sinectis.com.ar"/>
<node COLOR="#111111" CREATED="1208042325585" ID="Freemind_Link_1940081471" MODIFIED="1208042356347" TEXT="jorge newbery 2523 Piso 10&#x2022; Oficina C 1426 BA"/>
</node>
<node COLOR="#990000" CREATED="1205613124908" ID="Freemind_Link_519140197" MODIFIED="1205613127381" TEXT="fabio shoes">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1209388320351" ID="Freemind_Link_1830349255" MODIFIED="1209388334881" TEXT="have tango suits made">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1208042385943" ID="Freemind_Link_468431275" MODIFIED="1208042389891" TEXT="buy music">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1205598528191" ID="Freemind_Link_281910511" MODIFIED="1205598529261" TEXT="duro">
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1204763064866" ID="Freemind_Link_1955910754" MODIFIED="1204986761431" TEXT="clean teeth">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1204763070323" ID="Freemind_Link_1804913413" MODIFIED="1204986762907" TEXT="laser">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1205598537648" ID="Freemind_Link_945265440" MODIFIED="1205598541537" TEXT="repair jacket lining">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1209389738345" ID="Freemind_Link_410290763" MODIFIED="1209389739614" TEXT="work">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1209389740353" ID="Freemind_Link_1961162924" MODIFIED="1209389743705" TEXT="Stats book"/>
<node COLOR="#111111" CREATED="1209389747381" ID="Freemind_Link_1276361117" MODIFIED="1209389758648" TEXT="write the microcity memos"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1208042181893" ID="Freemind_Link_1769091720" MODIFIED="1208042194035" POSITION="right" TEXT="list of things gina can help us with">
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1208042216504" ID="Freemind_Link_1406595457" MODIFIED="1208042222940" TEXT="where to get cell phones">
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1208042224881" ID="Freemind_Link_923585774" MODIFIED="1208042228392" TEXT="US style power strip">
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1208042229432" ID="Freemind_Link_1621479095" MODIFIED="1208042231831" TEXT="yoga mats">
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1208042233349" ID="Freemind_Link_1579967948" MODIFIED="1208042238415" TEXT="bread pots">
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1208042239837" ID="Freemind_Link_1022354987" MODIFIED="1208042242768" TEXT="mirror">
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1208042244586" ID="Freemind_Link_839422168" MODIFIED="1208042249986" TEXT="LUMI bus guide">
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
</node>
</map>
