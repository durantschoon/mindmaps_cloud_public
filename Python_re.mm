<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1385428513891" ID="ID_1357125807" LINK="MetaMap.mm" MODIFIED="1385428776274" TEXT="Python_re">
<node CREATED="1385428897506" ID="ID_1854407004" MODIFIED="1385428905590" POSITION="right" TEXT="special (? ... ) patterns">
<node CREATED="1385428982931" ID="ID_1779017333" MODIFIED="1385428984415" TEXT="set flag">
<node CREATED="1385428920794" FOLDED="true" ID="ID_340863965" MODIFIED="1511205922919" TEXT="(?iLmsux)">
<node CREATED="1385428934707" ID="ID_1708841156" MODIFIED="1385429038430" TEXT="eg.">
<node CREATED="1385428936316" ID="ID_1453408282" MODIFIED="1385428940230" TEXT="(?i)"/>
</node>
<node CREATED="1385428949819" ID="ID_112421126" MODIFIED="1385428951343" TEXT="(One or more letters from the set &apos;i&apos;, &apos;L&apos;, &apos;m&apos;, &apos;s&apos;, &apos;u&apos;, &apos;x&apos;.) The group matches the empty string; the letters set the corresponding flags: re.I (ignore case), re.L (locale dependent), re.M (multi-line), re.S (dot matches all), re.U (Unicode dependent), and re.X (verbose), for the entire regular expression. (The flags are described in Module Contents.) This is useful if you wish to include the flags as part of the regular expression, instead of passing a flag argument to the re.compile() function."/>
<node CREATED="1385428966819" ID="ID_1544814096" MODIFIED="1385428967511" TEXT="Note that the (?x) flag changes how the expression is parsed. It should be used first in the expression string, or after one or more whitespace characters. If there are non-whitespace characters before the flag, the results are undefined."/>
</node>
</node>
<node CREATED="1385428987244" ID="ID_637792758" MODIFIED="1385429001381" TEXT="non-capturing">
<node CREATED="1385428972051" ID="ID_617327406" MODIFIED="1386522767951" TEXT="(?: ... )">
<node CREATED="1385429029611" ID="ID_1034963931" MODIFIED="1385429030110" TEXT="A non-capturing version of regular parentheses. Matches whatever regular expression is inside the parentheses, but the substring matched by the group cannot be retrieved after performing a match or referenced later in the pattern."/>
</node>
</node>
<node CREATED="1385429054028" ID="ID_385514174" MODIFIED="1385429056302" TEXT="name group">
<node CREATED="1385429192355" ID="ID_754374053" MODIFIED="1385429193453" TEXT="(?P&lt;name&gt;...)">
<node CREATED="1385429247755" ID="ID_10860575" MODIFIED="1385429561501" TEXT="Similar to regular parentheses, but the substring matched by the group is accessible via the symbolic group name name. Group names must be valid Python identifiers, and each group name must be defined only once within a regular expression. A symbolic group is also a numbered group, just as if the group were not named.  "/>
<node CREATED="1385429561504" ID="ID_1663479622" MODIFIED="1385429561506" TEXT="Named groups can be referenced in three contexts. If the pattern is (?P&lt;quote&gt;[&apos;&quot;]).*?(?P=quote) (i.e. matching a string quoted with either single or double quotes):"/>
<node CREATED="1385429226068" ID="ID_703284538" MODIFIED="1385429516093">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <table style="font-variant: normal; word-spacing: 0px; letter-spacing: normal; line-height: 20px; font-family: sans-serif; text-align: justify; font-style: normal; white-space: normal; color: rgb(0, 0, 0); background-color: rgb(255, 255, 255); text-indent: 0px; font-size: 10px; font-weight: normal; text-transform: none" charset="utf-8" border="1" class="last docutils">
      <colgroup>
      <col width="53%" />
      <col width="47%" />
      </colgroup>
      

      <tr valign="bottom">
        <th style="border-top-width: 1px; padding-left: 5px; padding-right: 5px; border-left-width: 0px; text-align: center; padding-top: 2px; border-top-style: solid; background-color: rgb(238, 221, 238); padding-bottom: 2px; border-top-color: rgb(204, 170, 204)" class="head">
          Context of reference to group &#8220;quote&#8221;
        </th>
        <th style="border-top-width: 1px; padding-left: 5px; padding-right: 5px; border-left-width: 0px; text-align: center; padding-top: 2px; border-top-style: solid; background-color: rgb(238, 221, 238); padding-bottom: 2px; border-top-color: rgb(204, 170, 204)" class="head">
          Ways to reference it
        </th>
      </tr>
      <tr valign="top">
        <td style="padding-right: 5px; padding-left: 5px; text-align: left; padding-bottom: 2px; background-color: rgb(238, 238, 255); padding-top: 2px; border-left-width: 0px">
          in the same pattern itself
        </td>
        <td style="padding-right: 5px; padding-left: 5px; text-align: left; background-color: rgb(238, 238, 255); padding-bottom: 2px; padding-top: 2px; border-left-width: 0px">
          <ul style="margin-top: 0; margin-bottom: 10px" class="first last simple">
            <li style="line-height: 20px; text-align: justify">
              <tt style="padding-left: 1px; padding-right: 1px; padding-top: 0px; background-color: rgb(236, 240, 243); padding-bottom: 0px; font-size: 0.95em" class="docutils literal"><font size="0.95em">(?P=quote)</font></tt>&#160;(as shown)
            </li>
            <li style="line-height: 20px; text-align: justify">
              <tt style="padding-left: 1px; padding-right: 1px; padding-top: 0px; background-color: rgb(236, 240, 243); padding-bottom: 0px; font-size: 0.95em" class="docutils literal"><font size="0.95em">\1</font></tt>
            </li>
          </ul>
        </td>
      </tr>
      <tr>
        <td style="padding-right: 5px; padding-left: 5px; text-align: left; background-color: rgb(238, 238, 255); padding-bottom: 2px; padding-top: 2px; border-left-width: 0px">
          when processing match object&#160;<tt style="padding-left: 1px; padding-right: 1px; padding-top: 0px; background-color: rgb(236, 240, 243); padding-bottom: 0px; font-size: 0.95em" class="docutils literal"><font size="0.95em">m</font></tt>
        </td>
        <td style="padding-right: 5px; padding-left: 5px; text-align: left; background-color: rgb(238, 238, 255); padding-bottom: 2px; padding-top: 2px; border-left-width: 0px">
          <ul style="margin-top: 0; margin-bottom: 10px" class="first last simple">
            <li style="line-height: 20px; text-align: justify">
              <tt style="padding-left: 1px; padding-right: 1px; padding-top: 0px; background-color: rgb(236, 240, 243); padding-bottom: 0px; font-size: 0.95em" class="docutils literal"><font size="0.95em">m.group('quote')</font></tt>
            </li>
            <li style="line-height: 20px; text-align: justify">
              <tt style="padding-left: 1px; padding-right: 1px; padding-top: 0px; background-color: rgb(236, 240, 243); padding-bottom: 0px; font-size: 0.95em" class="docutils literal"><font size="0.95em">m.end('quote')</font></tt>&#160;(etc.)
            </li>
          </ul>
        </td>
      </tr>
      <tr>
        <td style="padding-right: 5px; padding-left: 5px; text-align: left; background-color: rgb(238, 238, 255); padding-bottom: 2px; padding-top: 2px; border-left-width: 0px">
          in a string passed to the&#160;<tt style="padding-left: 1px; padding-right: 1px; padding-top: 0px; background-color: rgb(236, 240, 243); padding-bottom: 0px; font-size: 0.95em" class="docutils literal"><font size="0.95em">repl</font></tt>&#160;argument of&#160;<tt style="padding-left: 1px; padding-right: 1px; padding-top: 0px; background-color: rgb(236, 240, 243); padding-bottom: 0px; font-size: 0.95em" class="docutils literal"><font size="0.95em">re.sub()</font></tt>
        </td>
        <td style="padding-right: 5px; padding-left: 5px; text-align: left; background-color: rgb(238, 238, 255); padding-bottom: 2px; padding-top: 2px; border-left-width: 0px">
          <ul style="margin-top: 0; margin-bottom: 10px" class="first last simple">
            <li style="line-height: 20px; text-align: justify">
              <tt style="padding-left: 1px; padding-right: 1px; padding-top: 0px; background-color: rgb(236, 240, 243); padding-bottom: 0px; font-size: 0.95em" class="docutils literal"><font size="0.95em">\g&lt;quote&gt;</font></tt>
            </li>
            <li style="line-height: 20px; text-align: justify">
              <tt style="padding-left: 1px; padding-right: 1px; padding-top: 0px; background-color: rgb(236, 240, 243); padding-bottom: 0px; font-size: 0.95em" class="docutils literal"><font size="0.95em">\g&lt;1&gt;</font></tt>
            </li>
            <li style="line-height: 20px; text-align: justify">
              <tt style="padding-left: 1px; padding-right: 1px; padding-top: 0px; background-color: rgb(236, 240, 243); padding-bottom: 0px; font-size: 0.95em" class="docutils literal"><font size="0.95em">\1</font></tt>
            </li>
          </ul>
        </td>
      </tr>
    </table>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1385429601818" ID="ID_1717897974" MODIFIED="1385429605116" TEXT="reference named group">
<node CREATED="1385429605713" ID="ID_301464986" MODIFIED="1385429606492" TEXT="(?P=name)"/>
</node>
<node CREATED="1385429616306" ID="ID_229055869" MODIFIED="1385429618396" TEXT="comment">
<node CREATED="1385429618865" ID="ID_826772545" MODIFIED="1385429619837" TEXT="(?#...)"/>
</node>
<node CREATED="1385429634106" ID="ID_299025594" MODIFIED="1385429636789" TEXT="match if not">
<node CREATED="1385429637267" ID="ID_1481589923" MODIFIED="1385429637837" TEXT="(?!...)">
<node CREATED="1385429646171" ID="ID_1651798514" MODIFIED="1385429646989" TEXT="Matches if ... doesn&#x2019;t match next. This is a negative lookahead assertion. For example, Isaac (?!Asimov) will match &apos;Isaac &apos; only if it&#x2019;s not followed by &apos;Asimov&apos;."/>
</node>
</node>
<node CREATED="1385429680157" ID="ID_1959706573" MODIFIED="1385429685602" TEXT="positive look behind assertion">
<node CREATED="1385429686296" ID="ID_839518175" MODIFIED="1385429687002" TEXT="(?&lt;=...)">
<node CREATED="1385429808652" ID="ID_715461785" MODIFIED="1385429810708" TEXT="This example looks for a word following a hyphen:"/>
<node CREATED="1385429756162" ID="ID_396273456" MODIFIED="1385429787758">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div charset="utf-8" class="last highlight-python">
      <div style="background-repeat: repeat; background-color: rgb(238, 255, 204); background-position: initial initial" class="highlight">
        <pre style="border-top-width: 1px; padding-left: 5px; border-bottom-width: 1px; padding-right: 5px; line-height: 15.555556297302246px; border-left-style: none; border-bottom-style: solid; padding-top: 5px; border-bottom-color: rgb(170, 204, 153); border-right-style: none; color: rgb(51, 51, 51); background-color: rgb(238, 255, 204); border-top-style: solid; padding-bottom: 5px; border-top-color: rgb(170, 204, 153)"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font>m <font color="rgb(102, 102, 102)">=</font> re<font color="rgb(102, 102, 102)">.</font>search(<font color="rgb(64, 112, 160)">'(?&lt;=-)\w+'</font>, <font color="rgb(64, 112, 160)">'spam-egg'</font>)
<font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font>m<font color="rgb(102, 102, 102)">.</font>group(<font color="rgb(32, 128, 80)">0</font>)
<font color="rgb(48, 48, 48)">'egg'</font>
</pre>
      </div>
    </div>
    <br class="Apple-interchange-newline" />
    
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1385429832314" ID="ID_1888647112" MODIFIED="1385429832613" TEXT="negative lookbehind assertion">
<node CREATED="1385429686296" ID="ID_608843944" MODIFIED="1385429966921" TEXT="(?&lt;!...)">
<node CREATED="1385430882371" ID="ID_526231329" MODIFIED="1385430927865" TEXT="words with b where &apos;a&apos; is not before b"/>
<node CREATED="1385429756162" ID="ID_1201159730" MODIFIED="1385430874442">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div charset="utf-8" class="last highlight-python">
      <div style="background-repeat: repeat; background-color: rgb(238, 255, 204); background-position: initial initial" class="highlight">
        <pre style="border-top-width: 1px; padding-left: 5px; border-bottom-width: 1px; padding-right: 5px; line-height: 15.555556297302246px; border-left-style: none; border-bottom-style: solid; padding-top: 5px; border-bottom-color: rgb(170, 204, 153); border-right-style: none; color: rgb(51, 51, 51); background-color: rgb(238, 255, 204); border-top-style: solid; padding-bottom: 5px; border-top-color: rgb(170, 204, 153)"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; re.findall('(\w*(?&lt;!a)b\w*)', 'cab bed fabulous debt slab')</b></font>
['bed', 'debt']</pre>
      </div>
    </div>
    <br class="Apple-interchange-newline" />
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</node>
</map>
