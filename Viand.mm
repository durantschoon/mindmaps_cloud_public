<map version="0.8.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1272289937001" ID="Freemind_Link_1062493822" LINK="../../Documents/mindmaps/MetaMap.mm" MODIFIED="1272289969223" TEXT="Viand.mm">
<node CREATED="1272289975559" ID="_" MODIFIED="1272289978518" POSITION="right" TEXT="recipe ideas">
<node CREATED="1272289990752" ID="Freemind_Link_224703017" MODIFIED="1272290032197" TEXT="at Vineria Gualterio de Bolivar&#xa;Friday, 4/23/10">
<node CREATED="1272289985823" MODIFIED="1272289985823" TEXT="bacon and egg yolk and red wine"/>
<node CREATED="1272289985825" MODIFIED="1272289985825" TEXT="clove on potatoes"/>
<node CREATED="1272289985825" MODIFIED="1272289985825" TEXT="first few dishes should come out fast"/>
<node CREATED="1272289985828" MODIFIED="1272289985828" TEXT="pulverized food for textural variety, including cookies"/>
</node>
<node CREATED="1301027749417" ID="Freemind_Link_610151015" MODIFIED="1301027756885" TEXT="what about candied chili pepper"/>
<node CREATED="1301712920671" ID="Freemind_Link_1465933186" MODIFIED="1301712920671" TEXT="bites:">
<node CREATED="1301027746019" MODIFIED="1301027746019" TEXT="bite of dried fruit with candied ginger"/>
<node CREATED="1301712932213" MODIFIED="1301712932213" TEXT="salted cashews and dried papaya (on a spoon of something creamy)"/>
<node CREATED="1301712932214" MODIFIED="1301712932214" TEXT="salted toasted walnuts and dried pineapple"/>
</node>
</node>
</node>
</map>
