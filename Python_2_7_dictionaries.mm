<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1378265929669" ID="ID_628442559" LINK="Python_2_7.mm" MODIFIED="1378271507906">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <center>
      <p>
        Python Dictionary Data Type
      </p>
      Version 2.7
    </center>
  </body>
</html></richcontent>
<font BOLD="true" NAME="Helvetica" SIZE="14"/>
<node BACKGROUND_COLOR="#ffff66" CREATED="1378266264582" ID="ID_367248246" MODIFIED="1378266299672" POSITION="left" STYLE="bubble" TEXT="Unordered">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
</node>
<node BACKGROUND_COLOR="#ffff66" CREATED="1378266303917" ID="ID_1533395138" MODIFIED="1378266480009" POSITION="left" STYLE="bubble" TEXT="Mutable">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
</node>
<node BACKGROUND_COLOR="#ffff66" CREATED="1378266319470" ID="ID_1831884104" MODIFIED="1378266480018" POSITION="left" STYLE="bubble" TEXT="AKA">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378266323438" ID="ID_772812137" MODIFIED="1378266487739" STYLE="bubble" TEXT="Mappings">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
</node>
</node>
<node BACKGROUND_COLOR="#ffff66" CREATED="1378266329534" ID="ID_1492531577" MODIFIED="1378266480017" POSITION="left" STYLE="bubble" TEXT="Key : Value pairs">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
</node>
<node BACKGROUND_COLOR="#ffff66" CREATED="1378266340686" ID="ID_12094411" MODIFIED="1378266480016" POSITION="left" STYLE="bubble" TEXT="Enclosed by">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378266346318" ID="ID_476718877" MODIFIED="1378266488251" STYLE="bubble" TEXT="Curly braces">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378266350046" ID="ID_408734225" MODIFIED="1378266715774" STYLE="bubble" TEXT="{ ... }">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
</node>
<node BACKGROUND_COLOR="#ffff66" CREATED="1378266363903" ID="ID_1198822390" MODIFIED="1378266480015" POSITION="left" STYLE="bubble" TEXT="Variable Length">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
</node>
<node BACKGROUND_COLOR="#ffff66" CREATED="1378266367591" FOLDED="true" ID="ID_1548181523" MODIFIED="1511390517512" POSITION="left" STYLE="bubble" TEXT="Nestable">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378272520931" ID="ID_795685869" MODIFIED="1378272546528" STYLE="bubble" TEXT="Multidimensional example">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node COLOR="#338800" CREATED="1378272527667" ID="ID_665531884" MODIFIED="1378273153683">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <pre><code>matrix = { (2,3,4) : 88,
           (7,8,9) : 99 }

# if the key is a tuple, you can access without parentheses ()
print matrix[2,3,4] 
# 88

# but it works this way too
print matrix[(2,3,4)]
# 88

matrix[1,1,1] = 66

print matrix
# {(1, 1, 1): 66, (2, 3, 4): 88, (7, 8, 9): 99}

x,y,z = 7,8,9
print matrix[x,y,z]
# 99

# try to print a value for a key that does not exist
# but specify a default of 0
print matrix.get((9,9,9),0)
# 0</code></pre>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node BACKGROUND_COLOR="#ffff66" CREATED="1378266373887" ID="ID_1770864097" MODIFIED="1378266480014" POSITION="left" STYLE="bubble" TEXT="Hetorogenous">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378266764543" ID="ID_1402995738" MODIFIED="1378266783139" STYLE="bubble" TEXT="Can contain">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378266771496" ID="ID_1409657375" MODIFIED="1378266785562" STYLE="bubble" TEXT="Data types">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node BACKGROUND_COLOR="#ccccff" CREATED="1378266773776" ID="ID_1607926281" MODIFIED="1378266794587" STYLE="bubble" TEXT="different">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
<node BACKGROUND_COLOR="#ccccff" CREATED="1378266776088" ID="ID_597274575" MODIFIED="1378266796674" STYLE="bubble" TEXT="simultaneously">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378272134154" ID="ID_1360189395" MODIFIED="1378272170045" STYLE="bubble" TEXT="Keys must be immutable">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378272146754" ID="ID_799802070" MODIFIED="1378272175741" STYLE="bubble" TEXT="OK">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node BACKGROUND_COLOR="#ccccff" CREATED="1378272152682" ID="ID_1767433768" MODIFIED="1378272183165" STYLE="bubble" TEXT="string">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
<node BACKGROUND_COLOR="#ccccff" CREATED="1378272154426" ID="ID_1301757959" MODIFIED="1378272183166" STYLE="bubble" TEXT="number">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
<node BACKGROUND_COLOR="#ccccff" CREATED="1378272155810" ID="ID_387363193" MODIFIED="1378272183166" STYLE="bubble" TEXT="tuple">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378272147714" ID="ID_84876457" MODIFIED="1378272175742" STYLE="bubble" TEXT="NOT OK">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node BACKGROUND_COLOR="#ccccff" CREATED="1378272163450" ID="ID_1751919184" MODIFIED="1378272199261" STYLE="bubble" TEXT="list">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
<node BACKGROUND_COLOR="#ccccff" CREATED="1378272164274" ID="ID_293663390" MODIFIED="1378272200005" STYLE="bubble" TEXT="dictionary">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
</node>
</node>
<node BACKGROUND_COLOR="#ffff66" CREATED="1378266465329" ID="ID_264163982" MODIFIED="1378266480013" POSITION="left" STYLE="bubble" TEXT="NOT a sequence">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378266721078" ID="ID_227624992" MODIFIED="1378266731809" STYLE="bubble" TEXT="Don&apos;t work">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378266724862" ID="ID_1474347724" MODIFIED="1378266733825" STYLE="bubble" TEXT="Slicing">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
</node>
<node BACKGROUND_COLOR="#ffff66" CREATED="1378078311501" HGAP="29" ID="ID_1478823078" MODIFIED="1378266976032" POSITION="left" STYLE="bubble" TEXT="Implementation" VSHIFT="1">
<cloud/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378079008619" ID="ID_140605059" MODIFIED="1378266984712" STYLE="bubble" TEXT="CPython">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378266990837" ID="ID_1165971125" MODIFIED="1378266995606" STYLE="bubble" TEXT="Hash Table">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
</node>
</node>
<node BACKGROUND_COLOR="#ffff66" CREATED="1378267000864" ID="ID_1638228653" MODIFIED="1378271078318" POSITION="right" STYLE="bubble" TEXT="Constructor">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378267007213" FOLDED="true" ID="ID_32012354" MODIFIED="1511390230679" STYLE="bubble" TEXT="dict()">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node CREATED="1378267089511" ID="ID_1508891348" MODIFIED="1378267154481">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <pre style="font-variant: normal; border-top-width: 1px; line-height: 15.555556297302246px; color: rgb(51, 51, 51); padding-top: 5px; font-style: normal; text-align: justify; padding-bottom: 5px; padding-right: 5px; border-bottom-width: 1px; text-indent: 0px; background-color: rgb(238, 255, 204); padding-left: 5px; text-transform: none; font-weight: normal; word-spacing: 0px; letter-spacing: normal" charset="utf-8"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font>a&#xa0;<font color="rgb(102, 102, 102)">=</font>&#xa0;<font color="rgb(0, 112, 32)">dict</font>(one<font color="rgb(102, 102, 102)">=</font><font color="rgb(32, 128, 80)">1</font>,&#xa0;two<font color="rgb(102, 102, 102)">=</font><font color="rgb(32, 128, 80)">2</font>,&#xa0;three<font color="rgb(102, 102, 102)">=</font><font color="rgb(32, 128, 80)">3</font>)
<font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font>b <font color="rgb(102, 102, 102)">=</font> {<font color="rgb(64, 112, 160)">'one'</font>: <font color="rgb(32, 128, 80)">1</font>, <font color="rgb(64, 112, 160)">'two'</font>: <font color="rgb(32, 128, 80)">2</font>, <font color="rgb(64, 112, 160)">'three'</font>: <font color="rgb(32, 128, 80)">3</font>}
<font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font>c <font color="rgb(102, 102, 102)">=</font> <font color="rgb(0, 112, 32)">dict</font>(<font color="rgb(0, 112, 32)">zip</font>([<font color="rgb(64, 112, 160)">'one'</font>, <font color="rgb(64, 112, 160)">'two'</font>, <font color="rgb(64, 112, 160)">'three'</font>], [<font color="rgb(32, 128, 80)">1</font>, <font color="rgb(32, 128, 80)">2</font>, <font color="rgb(32, 128, 80)">3</font>]))
<font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font>d <font color="rgb(102, 102, 102)">=</font> <font color="rgb(0, 112, 32)">dict</font>([(<font color="rgb(64, 112, 160)">'two'</font>, <font color="rgb(32, 128, 80)">2</font>), (<font color="rgb(64, 112, 160)">'one'</font>, <font color="rgb(32, 128, 80)">1</font>), (<font color="rgb(64, 112, 160)">'three'</font>, <font color="rgb(32, 128, 80)">3</font>)])
<font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font>e <font color="rgb(102, 102, 102)">=</font> <font color="rgb(0, 112, 32)">dict</font>({<font color="rgb(64, 112, 160)">'three'</font>: <font color="rgb(32, 128, 80)">3</font>, <font color="rgb(64, 112, 160)">'one'</font>: <font color="rgb(32, 128, 80)">1</font>, <font color="rgb(64, 112, 160)">'two'</font>: <font color="rgb(32, 128, 80)">2</font>})
<font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font>a <font color="rgb(102, 102, 102)">==</font> b <font color="rgb(102, 102, 102)">==</font> c <font color="rgb(102, 102, 102)">==</font> d <font color="rgb(102, 102, 102)">==</font> e
<font color="rgb(48, 48, 48)">True</font></pre>
  </body>
</html></richcontent>
</node>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378267010605" ID="ID_1466825411" MODIFIED="1378271067896" STYLE="bubble" TEXT="d = {}&#xa;d = {&apos;first&apos;: 1, &apos;second&apos;: 2}">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
</node>
</node>
<node CREATED="1378271939101" ID="ID_1244791626" MODIFIED="1378271939101" POSITION="right" TEXT="">
<node BACKGROUND_COLOR="#ffff66" CREATED="1378271001511" ID="ID_1950839722" MODIFIED="1378271005841" STYLE="bubble" TEXT="Assignment">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378268839948" FOLDED="true" ID="ID_1803480500" MODIFIED="1511390237349" STYLE="bubble" TEXT="d[key] = value">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839949" ID="ID_1320752611" MODIFIED="1378268927257" STYLE="bubble" TEXT="Set d[key] to value.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
</node>
<node BACKGROUND_COLOR="#ffff66" CREATED="1378270888228" ID="ID_1600466025" MODIFIED="1378270894956" STYLE="bubble" TEXT="Deleting">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378268839950" FOLDED="true" ID="ID_280054793" MODIFIED="1511390239544" STYLE="bubble" TEXT="del d[key]">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839950" ID="ID_627433532" MODIFIED="1378268927258" STYLE="bubble" TEXT="Remove d[key] from d. Raises a KeyError if key is not in the map.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
</node>
</node>
<node CREATED="1378271952630" ID="ID_806013288" MODIFIED="1378271952630" POSITION="right" TEXT="">
<node BACKGROUND_COLOR="#ffff66" CREATED="1378270466491" ID="ID_1069393214" MODIFIED="1378270499476" STYLE="bubble" TEXT="Keys">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378268839976" FOLDED="true" ID="ID_1094714536" MODIFIED="1378360169212" STYLE="bubble" TEXT="keys()">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839977" ID="ID_180191539" MODIFIED="1378268907026" STYLE="bubble" TEXT="Return a copy of the dictionary&#x2019;s list of keys. See the note for dict.items().">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378268839970" FOLDED="true" ID="ID_1504433477" MODIFIED="1378271358035" STYLE="bubble" TEXT="iterkeys()">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839971" ID="ID_1091457034" MODIFIED="1378268916758" STYLE="bubble" TEXT="Return an iterator over the dictionary&#x2019;s keys. See the note for dict.items().">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839972" ID="ID_1862162247" MODIFIED="1378268916757" STYLE="bubble" TEXT="Using iterkeys() while adding or deleting entries in the dictionary may raise a RuntimeError or fail to iterate over all entries.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839973" ID="ID_921542726" MODIFIED="1378268916754" STYLE="bubble" TEXT="New in version 2.2.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378268839954" FOLDED="true" ID="ID_1944688017" MODIFIED="1378272049787" STYLE="bubble" TEXT="iter(d)">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839955" ID="ID_1625649610" MODIFIED="1378268927261" STYLE="bubble" TEXT="Return an iterator over the keys of the dictionary. This is a shortcut for iterkeys().">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378268839951" FOLDED="true" ID="ID_1004821645" MODIFIED="1378360172274" STYLE="bubble" TEXT="key in d">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839951" ID="ID_830595344" MODIFIED="1378268927259" STYLE="bubble" TEXT="Return True if d has a key key, else False.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839952" ID="ID_402140079" MODIFIED="1378268927259" STYLE="bubble" TEXT="New in version 2.2.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378268839953" FOLDED="true" ID="ID_904700020" MODIFIED="1378360173570" STYLE="bubble" TEXT="key not in d">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839953" ID="ID_397885037" MODIFIED="1378268927261" STYLE="bubble" TEXT="Equivalent to not key in d.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839954" ID="ID_798264261" MODIFIED="1378268927260" STYLE="bubble" TEXT="New in version 2.2.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
</node>
<node BACKGROUND_COLOR="#ffff66" CREATED="1378270467971" ID="ID_862778814" MODIFIED="1378270499478" STYLE="bubble" TEXT="Values">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378268839973" FOLDED="true" ID="ID_515196089" MODIFIED="1378272063535" STYLE="bubble" TEXT="itervalues()">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839974" ID="ID_1542438374" MODIFIED="1378268916762" STYLE="bubble" TEXT="Return an iterator over the dictionary&#x2019;s values. See the note for dict.items().">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839974" ID="ID_1332375044" MODIFIED="1378268916761" STYLE="bubble" TEXT="Using itervalues() while adding or deleting entries in the dictionary may raise a RuntimeError or fail to iterate over all entries.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839976" ID="ID_1283802924" MODIFIED="1378268907026" STYLE="bubble" TEXT="New in version 2.2.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378268713695" FOLDED="true" ID="ID_1986545522" MODIFIED="1378270929993" STYLE="bubble" TEXT="d[key]">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268713696" ID="ID_1599596861" MODIFIED="1378268927256" STYLE="bubble" TEXT="Return the item of d with key key. Raises a KeyError if key is not in the map. New in version 2.5: If a subclass of dict defines a method __missing__(), if the key key is not present, the d[key] operation calls that method with the key key as argument. The d[key] operation then returns or raises whatever is returned or raised by the __missing__(key) call if the key is not present. No other operations or methods invoke __missing__(). If __missing__() is not defined, KeyError is raised. __missing__() must be a method; it cannot be an instance variable. For an example, see collections.defaultdict.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378268839988" FOLDED="true" ID="ID_1377948274" MODIFIED="1378269515160" STYLE="bubble" TEXT="values()">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839988" ID="ID_906862056" MODIFIED="1378268907055" STYLE="bubble" TEXT="Return a copy of the dictionary&#x2019;s list of values. See the note for dict.items().">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
</node>
<node BACKGROUND_COLOR="#ffff66" CREATED="1378270471043" ID="ID_1196952659" MODIFIED="1378270499477" STYLE="bubble" TEXT="Items">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378268839962" FOLDED="true" ID="ID_1270634341" MODIFIED="1378269436979" STYLE="bubble" TEXT="items()">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839962" ID="ID_814633584" MODIFIED="1378268916748" STYLE="bubble" TEXT="Return a copy of the dictionary&#x2019;s list of (key, value) pairs.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839963" ID="ID_181664733" MODIFIED="1378268916747" STYLE="bubble" TEXT="CPython implementation detail: Keys and values are listed in an arbitrary order which is non-random, varies across Python implementations, and depends on the dictionary&#x2019;s history of insertions and deletions.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839965" ID="ID_149566134" MODIFIED="1378268916742" STYLE="bubble" TEXT="If items(), keys(), values(), iteritems(), iterkeys(), and itervalues() are called with no intervening modifications to the dictionary, the lists will directly correspond. This allows the creation of (value, key) pairs using zip(): pairs = zip(d.values(), d.keys()). The same relationship holds for the iterkeys() and itervalues() methods: pairs = zip(d.itervalues(), d.iterkeys()) provides the same value for pairs. Another way to create the same list is pairs = [(v, k) for (k, v) in d.iteritems()].">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378268839967" FOLDED="true" ID="ID_1146990366" MODIFIED="1378269455655" STYLE="bubble" TEXT="iteritems()">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839968" ID="ID_861184655" MODIFIED="1378268916753" STYLE="bubble" TEXT="Return an iterator over the dictionary&#x2019;s (key, value) pairs. See the note for dict.items().">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839968" ID="ID_174040474" MODIFIED="1378268916752" STYLE="bubble" TEXT="Using iteritems() while adding or deleting entries in the dictionary may raise a RuntimeError or fail to iterate over all entries.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839970" ID="ID_1243909663" MODIFIED="1378268916749" STYLE="bubble" TEXT="New in version 2.2.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378268839980" FOLDED="true" ID="ID_463267415" MODIFIED="1378269491524" STYLE="bubble" TEXT="popitem()">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839980" ID="ID_286814856" MODIFIED="1378268907037" STYLE="bubble" TEXT="Remove and return an arbitrary (key, value) pair from the dictionary.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839981" ID="ID_290442959" MODIFIED="1378268907036" STYLE="bubble" TEXT="popitem() is useful to destructively iterate over a dictionary, as often used in set algorithms. If the dictionary is empty, calling popitem() raises a KeyError.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
</node>
<node BACKGROUND_COLOR="#ffff66" CREATED="1378270473691" ID="ID_1517440849" MODIFIED="1378271801207" STYLE="bubble" TEXT="Views">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378268839989" FOLDED="true" ID="ID_1429310870" MODIFIED="1378271803933" STYLE="bubble" TEXT="viewitems()">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839989" ID="ID_716757501" MODIFIED="1378268907059" STYLE="bubble" TEXT="Return a new view of the dictionary&#x2019;s items ((key, value) pairs). See below for documentation of view objects.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839991" ID="ID_1167135532" MODIFIED="1378268907060" STYLE="bubble" TEXT="New in version 2.7.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378268839991" FOLDED="true" ID="ID_695628985" MODIFIED="1378270806830" STYLE="bubble" TEXT="viewkeys()">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839992" ID="ID_404134787" MODIFIED="1378268907061" STYLE="bubble" TEXT="Return a new view of the dictionary&#x2019;s keys. See below for documentation of view objects.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839992" ID="ID_208456013" MODIFIED="1378268907060" STYLE="bubble" TEXT="New in version 2.7.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378268839993" FOLDED="true" ID="ID_316638867" MODIFIED="1378270807842" STYLE="bubble" TEXT="viewvalues()">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839993" ID="ID_1232152155" MODIFIED="1378268907061" STYLE="bubble" TEXT="Return a new view of the dictionary&#x2019;s values. See below for documentation of view objects.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
</node>
</node>
<node BACKGROUND_COLOR="#ffff66" CREATED="1378268693691" ID="ID_135353901" MODIFIED="1378273087863" POSITION="right" STYLE="bubble" TEXT="Other Methods">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378268713693" FOLDED="true" ID="ID_1072607795" MODIFIED="1378271925405" STYLE="bubble" TEXT="len(d)">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268713694" ID="ID_1555515150" MODIFIED="1378268927249" STYLE="bubble" TEXT="Return the number of items in the dictionary d.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378268839955" FOLDED="true" ID="ID_649013759" MODIFIED="1378269383753" STYLE="bubble" TEXT="clear()">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839956" ID="ID_808638722" MODIFIED="1378268927262" STYLE="bubble" TEXT="Remove all items from the dictionary.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378268839956" FOLDED="true" ID="ID_457699800" MODIFIED="1378273067743" STYLE="bubble" TEXT="copy()">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839956" ID="ID_1345282709" MODIFIED="1378268927263" STYLE="bubble" TEXT="Return a shallow copy of the dictionary.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378268839957" FOLDED="true" ID="ID_366255045" MODIFIED="1511390275639" STYLE="bubble" TEXT="fromkeys(seq[, value])">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839957" ID="ID_344574190" MODIFIED="1378268927264" STYLE="bubble" TEXT="Create a new dictionary with keys from seq and values set to value.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839958" ID="ID_1136027212" MODIFIED="1378271742544" STYLE="bubble" TEXT="fromkeys() is a class method that returns a new dictionary.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<arrowlink DESTINATION="ID_1136027212" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Arrow_ID_881918015" STARTARROW="None" STARTINCLINATION="0;0;"/>
<linktarget COLOR="#b0b0b0" DESTINATION="ID_1136027212" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Arrow_ID_881918015" SOURCE="ID_1136027212" STARTARROW="None" STARTINCLINATION="0;0;"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378271742544" ID="ID_1902920202" MODIFIED="1378271746643" STYLE="bubble" TEXT="value defaults to None.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839958" ID="ID_1062455542" MODIFIED="1378268927264" STYLE="bubble" TEXT="New in version 2.3.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378268839984" FOLDED="true" ID="ID_67608850" MODIFIED="1378321769054" STYLE="bubble" TEXT="update([other])">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839984" ID="ID_1207012146" MODIFIED="1378268907054" STYLE="bubble" TEXT="Update the dictionary with the key/value pairs from other, overwriting existing keys. Return None.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839985" ID="ID_506313318" MODIFIED="1378268907053" STYLE="bubble" TEXT="update() accepts either another dictionary object or an iterable of key/value pairs (as tuples or other iterables of length two). If keyword arguments are specified, the dictionary is then updated with those key/value pairs: d.update(red=1, blue=2).">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839986" ID="ID_945014958" MODIFIED="1378268907048" STYLE="bubble" TEXT="Changed in version 2.4: Allowed the argument to be an iterable of key/value pairs and allowed keyword arguments.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
<node CREATED="1378270959622" ID="ID_1739249398" MODIFIED="1378271995807" STYLE="fork" TEXT="Defaults">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<arrowlink DESTINATION="ID_1739249398" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Arrow_ID_496678705" STARTARROW="None" STARTINCLINATION="0;0;"/>
<linktarget COLOR="#b0b0b0" DESTINATION="ID_1739249398" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Arrow_ID_496678705" SOURCE="ID_1739249398" STARTARROW="None" STARTINCLINATION="0;0;"/>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378268839959" FOLDED="true" ID="ID_519104336" MODIFIED="1378269397146" STYLE="bubble" TEXT="get(key[, default])">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839959" ID="ID_1378632870" MODIFIED="1378268916734" STYLE="bubble" TEXT="Return the value for key if key is in the dictionary, else default. If default is not given, it defaults to None, so that this method never raises a KeyError.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378268839977" FOLDED="true" ID="ID_153435708" MODIFIED="1378269505180" STYLE="bubble" TEXT="pop(key[, default])">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839978" ID="ID_1243770061" MODIFIED="1378268907032" STYLE="bubble" TEXT="If key is in the dictionary, remove it and return its value, else return default. If default is not given and key is not in the dictionary, a KeyError is raised.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839979" ID="ID_4362085" MODIFIED="1378268907027" STYLE="bubble" TEXT="New in version 2.3.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378268839982" FOLDED="true" ID="ID_1403030852" MODIFIED="1378269352945" STYLE="bubble" TEXT="setdefault(key[, default])">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378268839982" ID="ID_1921112380" MODIFIED="1378268907040" STYLE="bubble" TEXT="If key is in the dictionary, return its value. If not, insert key with a value of default and return default. default defaults to None.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
</node>
</node>
<node CREATED="1378271291790" ID="ID_1395795062" MODIFIED="1378271291790" POSITION="right" TEXT="">
<node BACKGROUND_COLOR="#ffff66" CREATED="1378269696049" FOLDED="true" ID="ID_1102069611" MODIFIED="1511390359741" STYLE="bubble" TEXT="dictionary views">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378269775880" ID="ID_616739302" LINK="http://docs.python.org/2.7/library/stdtypes.html#dictionary-view-objects" MODIFIED="1378269780351" STYLE="bubble" TEXT="docs.python.org &gt; 2.7 &gt; Library &gt; Stdtypes">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378269905430" ID="ID_727038277" MODIFIED="1378271199976" STYLE="bubble" TEXT="views">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378269805555" ID="ID_71834400" MODIFIED="1378269939240" STYLE="bubble" TEXT="update automatically when keys, values and items are modified">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378270224613" ID="ID_1778406419" MODIFIED="1378270230887" STYLE="bubble" TEXT="iteration">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node BACKGROUND_COLOR="#ccccff" CREATED="1378270247529" FOLDED="true" ID="ID_1768532898" MODIFIED="1378271211263" STYLE="bubble" TEXT="len(dictview)">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node CREATED="1378270247530" MODIFIED="1378270259160" TEXT="Return the number of entries in the dictionary."/>
</node>
<node BACKGROUND_COLOR="#ccccff" CREATED="1378270247531" FOLDED="true" ID="ID_846871565" MODIFIED="1378271211263" STYLE="bubble" TEXT="iter(dictview)">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node CREATED="1378270247531" ID="ID_512576702" MODIFIED="1378270259179" TEXT="Return an iterator over the keys, values or items (represented as tuples of (key, value)) in the dictionary."/>
</node>
<node BACKGROUND_COLOR="#ccccff" CREATED="1378270277422" FOLDED="true" ID="ID_739724769" MODIFIED="1378271211263" STYLE="bubble" TEXT="keys and values will zip in order">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node CREATED="1378270247533" ID="ID_1960184784" MODIFIED="1378270322777" STYLE="fork" TEXT="Keys and values are iterated over in an arbitrary order which is non-random, varies across Python implementations, and depends on the dictionary&#x2019;s history of insertions and deletions. If keys, values and items views are iterated over with no intervening modifications to the dictionary, the order of items will directly correspond. This allows the creation of (value, key) pairs using zip(): pairs = zip(d.values(), d.keys()). Another way to create the same list is pairs = [(v, k) for (k, v) in d.items()].">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<arrowlink DESTINATION="ID_1960184784" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Arrow_ID_1740436945" STARTARROW="None" STARTINCLINATION="0;0;"/>
<linktarget COLOR="#b0b0b0" DESTINATION="ID_1960184784" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Arrow_ID_1740436945" SOURCE="ID_1960184784" STARTARROW="None" STARTINCLINATION="0;0;"/>
</node>
</node>
<node BACKGROUND_COLOR="#ccccff" CREATED="1378270338296" FOLDED="true" ID="ID_1528212558" MODIFIED="1378271211263" STYLE="bubble" TEXT="don&apos;t modify during">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node CREATED="1378270247535" ID="ID_175100435" MODIFIED="1378271136656" STYLE="fork" TEXT="Iterating views while adding or deleting entries in the dictionary may raise a RuntimeError or fail to iterate over all entries.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<arrowlink DESTINATION="ID_175100435" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Arrow_ID_1326930774" STARTARROW="None" STARTINCLINATION="0;0;"/>
<linktarget COLOR="#b0b0b0" DESTINATION="ID_175100435" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Arrow_ID_1326930774" SOURCE="ID_175100435" STARTARROW="None" STARTINCLINATION="0;0;"/>
</node>
</node>
<node BACKGROUND_COLOR="#ccccff" CREATED="1378270247537" FOLDED="true" ID="ID_1369580892" MODIFIED="1378271211264" STYLE="bubble" TEXT="x in dictview">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node CREATED="1378270247537" ID="ID_1056156330" MODIFIED="1378270259162" TEXT="Return True if x is in the underlying dictionary&#x2019;s keys, values or items (in the latter case, x should be a (key, value) tuple)."/>
</node>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378269927446" ID="ID_1138659912" MODIFIED="1378269940073" STYLE="bubble" TEXT="have set-like behavior">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node BACKGROUND_COLOR="#ccccff" CREATED="1378270159136" FOLDED="true" ID="ID_707951817" MODIFIED="1378271206550" STYLE="bubble" TEXT="dictview &amp; other">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node CREATED="1378270159138" MODIFIED="1378270171486" TEXT="Return the intersection of the dictview and the other object as a new set."/>
</node>
<node BACKGROUND_COLOR="#ccccff" CREATED="1378270159139" FOLDED="true" ID="ID_859993238" MODIFIED="1378271206551" STYLE="bubble" TEXT="dictview | other">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node CREATED="1378270159139" ID="ID_1336315741" MODIFIED="1378270171493" TEXT="Return the union of the dictview and the other object as a new set."/>
</node>
<node BACKGROUND_COLOR="#ccccff" CREATED="1378270159140" FOLDED="true" ID="ID_1319427268" MODIFIED="1378271206551" STYLE="bubble" TEXT="dictview - other">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node CREATED="1378270159140" MODIFIED="1378270171490" TEXT="Return the difference between the dictview and the other object (all elements in dictview that aren&#x2019;t in other) as a new set."/>
</node>
<node BACKGROUND_COLOR="#ccccff" CREATED="1378270159142" FOLDED="true" ID="ID_1544276198" MODIFIED="1378271206551" STYLE="bubble" TEXT="dictview ^ other">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node CREATED="1378270159143" MODIFIED="1378270171487" TEXT="Return the symmetric difference (all elements either in dictview or other, but not in both) of the dictview and the other object as a new set."/>
</node>
</node>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378269859341" ID="ID_1987804366" MODIFIED="1378270395225" STYLE="bubble" TEXT="examples">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node CREATED="1378267089511" ID="ID_66555337" MODIFIED="1378270395219">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <pre style="font-variant: normal; border-top-width: 1px; line-height: 15.555556297302246px; color: rgb(51, 51, 51); padding-top: 5px; font-style: normal; text-align: start; padding-bottom: 5px; padding-right: 5px; border-bottom-width: 1px; text-indent: 0px; background-color: rgb(238, 255, 204); padding-left: 5px; text-transform: none; font-weight: normal; word-spacing: 0px; letter-spacing: normal" charset="utf-8"><font color="rgb(198, 93, 9)"><span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><b>&gt;&gt;&gt; </b></span></font><span class="n">dishes</span> <font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">=</span></font> <span class="p">{</span><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'eggs'</span></font><span class="p">:</span> <font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mi">2</span></font><span class="p">,</span> <font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'sausage'</span></font><span class="p">:</span> <font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mi">1</span></font><span class="p">,</span> <font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'bacon'</span></font><span class="p">:</span> <font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mi">1</span></font><span class="p">,</span> <font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'spam'</span></font><span class="p">:</span> <font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mi">500</span></font><span class="p">}</span>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><span class="n">keys</span> <font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">=</span></font> <span class="n">dishes</span><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">.</span></font><span class="n">viewkeys</span><span class="p">()</span>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><span class="n">values</span> <font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">=</span></font> <span class="n">dishes</span><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">.</span></font><span class="n">viewvalues</span><span class="p">()</span>

<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><i><font color="rgb(64, 128, 144)"><span style="color: rgb(64, 128, 144); font-style: italic" class="c"># iteration</span></font></i>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><span class="n">n</span> <font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">=</span></font> <font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mi">0</span></font>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><b><font color="rgb(0, 112, 32)"><span style="color: rgb(0, 112, 32); font-weight: bold" class="k">for</span></font></b> <span class="n">val</span> <span style="color: rgb(0, 112, 32); font-weight: bold" class="ow"><font color="rgb(0, 112, 32)"><b>in</b></font></span> <span class="n">values</span><span class="p">:</span>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>... </b></font></span>    <span class="n">n</span> <font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">+=</span></font> <span class="n">val</span>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><b><font color="rgb(0, 112, 32)"><span style="color: rgb(0, 112, 32); font-weight: bold" class="k">print</span></font></b><span class="p">(</span><span class="n">n</span><span class="p">)</span>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">504</span></font>

<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><i><font color="rgb(64, 128, 144)"><span style="color: rgb(64, 128, 144); font-style: italic" class="c"># keys and values are iterated over in the same order</span></font></i>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><font color="rgb(0, 112, 32)"><span style="color: rgb(0, 112, 32)" class="nb">list</span></font><span class="p">(</span><span class="n">keys</span><span class="p">)</span>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">['eggs', 'bacon', 'sausage', 'spam']</span></font>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><font color="rgb(0, 112, 32)"><span style="color: rgb(0, 112, 32)" class="nb">list</span></font><span class="p">(</span><span class="n">values</span><span class="p">)</span>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">[2, 1, 1, 500]</span></font>

<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><i><font color="rgb(64, 128, 144)"><span style="color: rgb(64, 128, 144); font-style: italic" class="c"># view objects are dynamic and reflect dict changes</span></font></i>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><b><font color="rgb(0, 112, 32)"><span style="color: rgb(0, 112, 32); font-weight: bold" class="k">del</span></font></b> <span class="n">dishes</span><span class="p">[</span><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'eggs'</span></font><span class="p">]</span>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><b><font color="rgb(0, 112, 32)"><span style="color: rgb(0, 112, 32); font-weight: bold" class="k">del</span></font></b> <span class="n">dishes</span><span class="p">[</span><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'sausage'</span></font><span class="p">]</span>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><font color="rgb(0, 112, 32)"><span style="color: rgb(0, 112, 32)" class="nb">list</span></font><span class="p">(</span><span class="n">keys</span><span class="p">)</span>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">['spam', 'bacon']</span></font>

<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><i><font color="rgb(64, 128, 144)"><span style="color: rgb(64, 128, 144); font-style: italic" class="c"># set operations</span></font></i>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><span class="n">keys</span> <font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">&amp;</span></font> <span class="p">{</span><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'eggs'</span></font><span class="p">,</span> <font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'bacon'</span></font><span class="p">,</span> <font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'salad'</span></font><span class="p">}</span>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">{'bacon'}</span></font></pre>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node BACKGROUND_COLOR="#ffff66" CREATED="1378267287595" ID="ID_477853068" MODIFIED="1511390374369" STYLE="bubble" TEXT="collections.defaultdict">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378267298987" ID="ID_729375399" LINK="http://docs.python.org/2.7/library/collections.html#collections.defaultdict" MODIFIED="1378267805405" STYLE="bubble" TEXT="docs.python.org &gt; 2.7 &gt; Library &gt; Collections.html#collections">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378267442502" FOLDED="true" ID="ID_1937871949" MODIFIED="1511390472293" STYLE="bubble" TEXT="dictionary with &#xa;default type for value&#xa;so you don&apos;t have to check&#xa;the first time you use it">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378267463167" ID="ID_719450926" MODIFIED="1378267810552" STYLE="bubble" TEXT="example &#xa;setting the default value &#xa;to an empty list&#xa;so you can append immediately">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node CREATED="1378267089511" ID="ID_1937361766" MODIFIED="1378267810548">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <pre style="font-variant: normal; border-top-width: 1px; line-height: 15.555556297302246px; color: rgb(51, 51, 51); padding-top: 5px; font-style: normal; text-align: start; padding-bottom: 5px; padding-right: 5px; border-bottom-width: 1px; text-indent: 0px; background-color: rgb(238, 255, 204); padding-left: 5px; text-transform: none; font-weight: normal; word-spacing: 0px; letter-spacing: normal" charset="utf-8"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font><b><font color="rgb(0, 112, 32)">from</font></b>&#xa0;<font color="rgb(14, 132, 181)"><b>collections</b></font>&#xa0;<font color="rgb(0, 112, 32)"><b>import</b></font>&#xa0;defaultdict
<font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font>s&#xa0;<font color="rgb(102, 102, 102)">=</font>&#xa0;[(<font color="rgb(64, 112, 160)">'yellow'</font>,&#xa0;<font color="rgb(32, 128, 80)">1</font>),&#xa0;(<font color="rgb(64, 112, 160)">'blue'</font>,&#xa0;<font color="rgb(32, 128, 80)">2</font>),&#xa0;(<font color="rgb(64, 112, 160)">'yellow'</font>,&#xa0;<font color="rgb(32, 128, 80)">3</font>),&#xa0;(<font color="rgb(64, 112, 160)">'blue'</font>,&#xa0;<font color="rgb(32, 128, 80)">4</font>),&#xa0;(<font color="rgb(64, 112, 160)">'red'</font>,&#xa0;<font color="rgb(32, 128, 80)">1</font>)]
<font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font>d&#xa0;<font color="rgb(102, 102, 102)">=</font>&#xa0;defaultdict(<font color="rgb(0, 112, 32)">list</font>)
<font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font><b><font color="rgb(0, 112, 32)">for</font></b> k, v <font color="rgb(0, 112, 32)"><b>in</b></font> s:
<font color="rgb(198, 93, 9)"><b>... </b></font>    d[k]<font color="rgb(102, 102, 102)">.</font>append(v)
<font color="rgb(198, 93, 9)"><b>...</b></font>
<font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font>d<font color="rgb(102, 102, 102)">.</font>items()
<font color="rgb(48, 48, 48)">[('blue', [2, 4]), ('red', [1]), ('yellow', [1, 3])]</font></pre>
  </body>
</html></richcontent>
</node>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378267782262" ID="ID_1114901866" MODIFIED="1378267811466" STYLE="bubble" TEXT="int">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node CREATED="1378267089511" ID="ID_1640830169" MODIFIED="1378267985962">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <pre style="font-variant: normal; border-top-width: 1px; line-height: 15.555556297302246px; color: rgb(51, 51, 51); padding-top: 5px; font-style: normal; text-align: start; padding-bottom: 5px; padding-right: 5px; border-bottom-width: 1px; text-indent: 0px; background-color: rgb(238, 255, 204); padding-left: 5px; text-transform: none; font-weight: normal; word-spacing: 0px; letter-spacing: normal" charset="utf-8"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font><b><font color="rgb(0, 112, 32)">from</font></b>&#xa0;<font color="rgb(14, 132, 181)"><b>collections</b></font>&#xa0;<font color="rgb(0, 112, 32)"><b>import</b></font>&#xa0;defaultdict
<font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font>s&#xa0;<font color="rgb(102, 102, 102)">=</font>&#xa0;<font color="rgb(64, 112, 160)">'mississippi'</font>
<font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font>d <font color="rgb(102, 102, 102)">=</font> defaultdict(<font color="rgb(0, 112, 32)">int</font>)
<font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font><b><font color="rgb(0, 112, 32)">for</font></b> k <font color="rgb(0, 112, 32)"><b>in</b></font> s:
<font color="rgb(198, 93, 9)"><b>... </b></font>    d[k] <font color="rgb(102, 102, 102)">+=</font> <font color="rgb(32, 128, 80)">1</font>
<font color="rgb(198, 93, 9)"><b>...</b></font>
<font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font>d<font color="rgb(102, 102, 102)">.</font>items()
<font color="rgb(48, 48, 48)">[('i', 4), ('p', 2), ('s', 4), ('m', 1)]</font></pre>
  </body>
</html></richcontent>
</node>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378267922001" ID="ID_252985104" MODIFIED="1378267926515" STYLE="bubble" TEXT="set">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node CREATED="1378267089511" ID="ID_1969647359" MODIFIED="1511390406462">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <pre charset="utf-8" style="letter-spacing: normal; padding-left: 5px; padding-bottom: 5px; font-style: normal; border-top-width: 1px; text-align: start; border-bottom-width: 1px; color: rgb(51, 51, 51); background-color: rgb(238, 255, 204); padding-top: 5px; padding-right: 5px; word-spacing: 0px; text-transform: none; font-variant: normal; font-weight: normal; text-indent: 0px; line-height: 15.555556297302246px"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font><b><font color="rgb(0, 112, 32)">from</font></b>&#160;<font color="rgb(14, 132, 181)"><b>collections</b></font>&#160;<font color="rgb(0, 112, 32)"><b>import</b></font>&#160;defaultdict
<font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font>s&#160;<font color="rgb(102, 102, 102)">=</font>&#160;[(<font color="rgb(64, 112, 160)">'red'</font>,&#160;<font color="rgb(32, 128, 80)">1</font>),&#160;(<font color="rgb(64, 112, 160)">'blue'</font>,&#160;<font color="rgb(32, 128, 80)">2</font>),&#160;(<font color="rgb(64, 112, 160)">'red'</font>,&#160;<font color="rgb(32, 128, 80)">3</font>),&#160;(<font color="rgb(64, 112, 160)">'blue'</font>,&#160;<font color="rgb(32, 128, 80)">4</font>),&#160;(<font color="rgb(64, 112, 160)">'red'</font>,&#160;<font color="rgb(32, 128, 80)">1</font>),&#160;(<font color="rgb(64, 112, 160)">'blue'</font>,&#160;<font color="rgb(32, 128, 80)">4</font>)]
<font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font>d <font color="rgb(102, 102, 102)">=</font> defaultdict(<font color="rgb(0, 112, 32)">set</font>)
<font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font><b><font color="rgb(0, 112, 32)">for</font></b> k, v <font color="rgb(0, 112, 32)"><b>in</b></font> s:
<font color="rgb(198, 93, 9)"><b>... </b></font>    d[k]<font color="rgb(102, 102, 102)">.</font>add(v)
<font color="rgb(198, 93, 9)"><b>...</b></font>
<font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font>d<font color="rgb(102, 102, 102)">.</font>items()
<font color="rgb(48, 48, 48)">[('blue', set([2, 4])), ('red', set([1, 3]))]</font></pre>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
</node>
</node>
</map>
