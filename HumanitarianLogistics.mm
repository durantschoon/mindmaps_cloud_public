<map version="0.8.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1296576051640" ID="Freemind_Link_697798014" LINK="../../Documents/mindmaps/MetaMap.mm" MODIFIED="1296576070220" TEXT="HumanitarianLogistics.mm">
<node CREATED="1296576081233" ID="_" MODIFIED="1296576083532" POSITION="right" TEXT="MIT course">
<node CREATED="1296576085024" MODIFIED="1296576085024" TEXT="1) Spring Humanitarian Logistics course with projects analyzing the Haiti response"/>
<node CREATED="1296576085024" MODIFIED="1296576085024" TEXT="ESD.934 Humanitarian Logistics"/>
<node CREATED="1296576085024" MODIFIED="1296576085024" TEXT="6 Units (2-0-4)"/>
<node CREATED="1296576085024" MODIFIED="1296576085024" TEXT="Spring Semester 2011"/>
<node CREATED="1296576085024" MODIFIED="1296576085024" TEXT="Tuesdays 4:00-7:00pm (1 Feb &#x2013; 29 Mar)"/>
<node CREATED="1296576085024" MODIFIED="1296576085024" TEXT="Room E51-057"/>
<node CREATED="1296576085024" MODIFIED="1296576085024" TEXT="Logistics management plays a key role in responding to humanitarian crises stemming from natural disasters, armed conflicts, epidemics, and famine.  Supply chains also provide the backbone for sustainable humanitarian programs and international development. This course explores how logistics management principles apply in dynamic, resource-constrained contexts."/>
<node CREATED="1296576085025" MODIFIED="1296576085025" TEXT="The class sessions begin with an overview of humanitarian operations by introducing the challenging context in which they take place, the products and services needed, and the organizations striving to meet these needs (e.g. donors, UN agencies, NGOs, government agencies and private companies). Sessions then dive into the strategies, technologies, and management approaches used by these organizations for effective logistics performance.  Class sessions combine interactive presentations, case discussions, and guest speakers from humanitarian organizations."/>
<node CREATED="1296576085025" MODIFIED="1296576085025" TEXT="A key component of the course is a team project focusing on the 2010 Haiti earthquake response.  We will utilize data and information directly from sources such as the UN, US government, and NGOs. Expected projects include:">
<node CREATED="1296576085025" MODIFIED="1296576085025" TEXT="- Demand Characterization: Analyzing data from an extensive survey conducted March-July."/>
<node CREATED="1296576085025" MODIFIED="1296576085025" TEXT="- Distribution Management: Tracking distribution of shelter and non-food items across organizations."/>
<node CREATED="1296576085026" MODIFIED="1296576085026" TEXT="- Transportation Coordination: Assessing utilization of critical resources (airport, port) and/or services."/>
<node CREATED="1296576085026" MODIFIED="1296576085026" TEXT="- Information Management: Leveraging new sources of information, such as text messages."/>
<node CREATED="1296576085026" MODIFIED="1296576085026" TEXT="- Supply Chain Integration: Combining funds and in-kind donations to support operations."/>
<node CREATED="1296576085026" MODIFIED="1296576085026" TEXT="- Additional project options are being defined; and student proposals will be considered."/>
</node>
<node CREATED="1296576085026" MODIFIED="1296576085026" TEXT="Students will have the option to continue interesting projects for credit in the second half of the semester.  Grades will be based on class participation, brief individual writeups and the team project."/>
<node CREATED="1296576085026" MODIFIED="1296576085026" TEXT="The course is designed to accommodate students from various backgrounds, including those with limited logistics and supply chain experience. In fact, we welcome a diverse group of students and expect participation from various MIT programs as well as Tufts and Harvard."/>
<node CREATED="1296576085026" LINK="mailto:(goentzel@mit.edu)." MODIFIED="1296576085026" TEXT="If you have any questions, please contact Jarrod Goentzel (goentzel@mit.edu)."/>
</node>
</node>
</map>
