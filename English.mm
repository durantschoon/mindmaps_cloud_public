<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1339541165400" ID="ID_1091352983" LINK="../../Documents/mindmaps/MetaMap.mm" MODIFIED="1339541183744" TEXT="English">
<node CREATED="1339541186314" ID="ID_315926577" MODIFIED="1339541189597" POSITION="right" TEXT="affect and effect">
<node CREATED="1339541209271" ID="ID_411696041" LINK="http://grammar.quickanddirtytips.com/affect-versus-effect.aspx" MODIFIED="1339541209271" TEXT="grammar.quickanddirtytips.com &gt; Affect-versus-effect"/>
<node CREATED="1339541228737" ID="ID_698467942" MODIFIED="1339541336260" TEXT="more common">
<node CREATED="1339541202074" ID="ID_1623163944" MODIFIED="1339541210589" TEXT="The arrows affected Aardvark. The effect was eye-popping.">
<node CREATED="1339541221249" ID="ID_1854385569" MODIFIED="1339541227045" TEXT="affect = verb"/>
<node CREATED="1339541238153" ID="ID_491533505" MODIFIED="1339541244126" TEXT="effect = noun"/>
</node>
</node>
<node CREATED="1339541230673" ID="ID_1934806572" MODIFIED="1339541236222" TEXT="rarer">
<node CREATED="1339541262170" ID="ID_1677155880" MODIFIED="1339541312743" TEXT="On rare occasion, the aardvark displayed a happy affect. The aardvark hoped to effect change within the burrow.">
<node CREATED="1339541318251" ID="ID_357018024" MODIFIED="1339541321328" TEXT="affect = noun"/>
<node CREATED="1339541321931" ID="ID_751523978" MODIFIED="1339541324736" TEXT="effect = verb"/>
</node>
</node>
</node>
</node>
</map>
