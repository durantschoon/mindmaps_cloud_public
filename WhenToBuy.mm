<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1326857905001" ID="Freemind_Link_1394103154" LINK="../../Documents/mindmaps/MetaMap.mm" MODIFIED="1326857918872" TEXT="whentobuy.mm">
<node CREATED="1326858092933" ID="Freemind_Link_827833010" MODIFIED="1326858092933" POSITION="right" TEXT="First Quarter (Jan/Feb/March)">
<node CREATED="1326858126309" FOLDED="true" ID="Freemind_Link_555336358" MODIFIED="1326858126309" TEXT="Gas Grills &amp; Air Conditioners:">
<node CREATED="1326858126309" MODIFIED="1326858126309" TEXT="They&apos;re obviously off-season (at least in the colder states), and unless you&apos;re a serious air conditioning enthusiast, you probably aren&apos;t itching to get the latest and greatest."/>
</node>
<node CREATED="1326858126309" FOLDED="true" ID="Freemind_Link_1716601397" MODIFIED="1326858126309" TEXT="Wedding Supplies:">
<node CREATED="1326858126309" MODIFIED="1326858126309" TEXT="Everyone wants a wedding in the spring, which means it&apos;s a lot easier to find a venue and negotiate prices for a service in the winter."/>
</node>
<node CREATED="1326858385478" ID="Freemind_Link_1633831341" MODIFIED="1326858385478" TEXT="">
<node CREATED="1326857845184" ID="_" MODIFIED="1326857847090" TEXT="January">
<node CREATED="1326857847490" FOLDED="true" ID="Freemind_Link_117695108" MODIFIED="1326857847490" TEXT="Bicycles &amp; Sporting Goods:">
<node CREATED="1326857847490" MODIFIED="1326857847490" TEXT="The dead of winter isn&apos;t when most people are buying their summer gear, so you&apos;re likely to get some good deals. Again, this could vary if you&apos;re in a warmer climate."/>
</node>
<node CREATED="1326857862074" FOLDED="true" ID="Freemind_Link_356084256" MODIFIED="1326857862074" TEXT="Broadway Tickets:">
<node CREATED="1326857862074" MODIFIED="1326857862074" TEXT="Broadway&apos;s slower months are in the winter and fall, so January&apos;s a great time to seek out some discounts. You probably won&apos;t find them for the more popular shows, but if you&apos;re looking to branch out, this is the time to do it."/>
</node>
<node CREATED="1326857847490" FOLDED="true" ID="Freemind_Link_284223713" MODIFIED="1326857847490" TEXT="Calendars:">
<node CREATED="1326857847490" MODIFIED="1326857847490" TEXT="Paper calendars make a great stocking stuffer, so if you aren&apos;t too picky about what you get, grab them after the holidays for some bargain bin deals."/>
</node>
<node CREATED="1326857847490" FOLDED="true" ID="Freemind_Link_476525865" MODIFIED="1326857847490" TEXT="Carpeting &amp; Flooring:">
<node CREATED="1326857847490" MODIFIED="1326857847490" TEXT="According to Carpet SuperSite, most people buy carpets at the end of the year in preparation for the holidays. Once January rolls around, prices will go down."/>
</node>
<node CREATED="1326857847491" FOLDED="true" ID="Freemind_Link_762678722" MODIFIED="1326859754364" TEXT="Digital Cameras:">
<edge COLOR="#808080" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="bookmark"/>
<node CREATED="1326857847491" MODIFIED="1326857847491" TEXT="After the big trade shows like CES come around in mid-January, you&apos;ll see that older model cameras drop in price to prepare for the newly-announced ones."/>
</node>
<node CREATED="1326857847491" FOLDED="true" ID="Freemind_Link_390561919" MODIFIED="1326857978950" TEXT="Furniture:">
<icon BUILTIN="bookmark"/>
<node CREATED="1326857847491" MODIFIED="1326857847491" TEXT="Most furniture companies release new products twice a in February and August. That means they&apos;ll be giving some sweet deals in January to make room for the new products next month."/>
</node>
<node CREATED="1326857847491" FOLDED="true" ID="Freemind_Link_276624807" MODIFIED="1326857847491" TEXT="Gift Cards:">
<node CREATED="1326857847491" MODIFIED="1326857847491" TEXT="You wouldn&apos;t think there would be a best time to buy gift cards, but you&apos;d be wrong. Just after the holidays, you&apos;re bound to find better deals on gift card exchange sites like Plastic Jungle, where everyone&apos;s trading in their unwanted cards for cash."/>
</node>
<node CREATED="1326857847491" FOLDED="true" ID="Freemind_Link_1656081352" MODIFIED="1326858001800" TEXT="HDTVs &amp; Home Theater Gear:">
<icon BUILTIN="bookmark"/>
<node CREATED="1326857847491" MODIFIED="1326857847491" TEXT="These stay on sale pretty much right up until the super bowl, so even if you aren&apos;t a football fan, you can take advantage of the sales brought on by extra competition."/>
</node>
<node CREATED="1326857847491" FOLDED="true" ID="Freemind_Link_1730387470" MODIFIED="1326857998396" TEXT="Digital Frames:">
<icon BUILTIN="bookmark"/>
<node CREATED="1326857847491" MODIFIED="1326857847491" TEXT="Because this is one of those things everybody seems to give for Christmas."/>
</node>
<node CREATED="1326857847491" FOLDED="true" ID="Freemind_Link_319815645" MODIFIED="1326857847491" TEXT="Linens and Bedding:">
<node CREATED="1326857847491" MODIFIED="1326857847491" TEXT="Ever since John Wannamaker started the first &quot;white sale&quot; in 1878, January has been the best month to get bedding and other linens. Of course, just as with clothing, keep an eye out all year round, since retailers will put last season&apos;s stuff on sale when new products come out."/>
</node>
<node CREATED="1326857847491" FOLDED="true" ID="Freemind_Link_937139784" MODIFIED="1326857847491" TEXT="Motorcycles:">
<node CREATED="1326857847492" MODIFIED="1326857847492" TEXT="It&apos;s really cold outside. Not to mention icy. It doesn&apos;t make for great cycling weather, but if you&apos;ve been mulling over a motorcycle purchase, get it in January and then wait to ride it when the snow melts."/>
</node>
<node CREATED="1326857847492" FOLDED="true" ID="Freemind_Link_979147092" MODIFIED="1326858026551" TEXT="Office Furniture:">
<icon BUILTIN="bookmark"/>
<node CREATED="1326857847492" MODIFIED="1326857847492" TEXT="Business is best for office furniture retailers in January, likely due to new offices buying up stock for the new year. Grab your new ergonomic office chair now, or in April after tax day."/>
</node>
<node CREATED="1326857847492" FOLDED="true" ID="Freemind_Link_249453577" MODIFIED="1326857847492" TEXT="Video Games:">
<node CREATED="1326857847492" MODIFIED="1326857847492" TEXT="Most new video games come out in late fall for the holiday season, and they&apos;ll go on sale after the holidays are over. Check out Steam and other gaming retailers for discounted prices."/>
</node>
</node>
<node CREATED="1326858075275" ID="Freemind_Link_1439771988" MODIFIED="1326858075275" TEXT="February">
<node CREATED="1326858263993" FOLDED="true" ID="Freemind_Link_1869018571" MODIFIED="1326858269596" TEXT="Boats:">
<icon BUILTIN="bookmark"/>
<node CREATED="1326858263993" MODIFIED="1326858263993" TEXT="February is boat show season, and since manufacturers are clearing out the old models, you can often get some pretty good deals. Some people recommend waiting until after the actual boat shows, so you don&apos;t have the other expenses they carries with them."/>
</node>
<node CREATED="1326858263994" FOLDED="true" ID="Freemind_Link_1605387237" MODIFIED="1326858263994" TEXT="Broadway Tickets:">
<node CREATED="1326858263994" MODIFIED="1326858263994" TEXT="February still falls into the slow season for Broadway, so grab discounts on tickets to the less popular shows."/>
</node>
<node CREATED="1326858263994" ID="Freemind_Link_1477942144" MODIFIED="1326859367269" TEXT="Digital Cameras:">
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="bookmark"/>
<node CREATED="1326858263994" MODIFIED="1326858263994" TEXT="Since the newest cameras will have just come out post-CES, you can grab last year&apos;s models for less."/>
</node>
<node CREATED="1326858263994" FOLDED="true" ID="Freemind_Link_478934061" MODIFIED="1326858281098" TEXT="HDTVs &amp; Home Theaters:">
<icon BUILTIN="bookmark"/>
<node CREATED="1326858263994" MODIFIED="1326858263994" TEXT="Up until the Super Bowl, grab great prices on those new TVs."/>
</node>
<node CREATED="1326858263994" FOLDED="true" ID="Freemind_Link_1866536208" MODIFIED="1326858263994" TEXT="Video Games:">
<node CREATED="1326858263994" MODIFIED="1326858263994" TEXT="Again, since video games come out in early winter for the holidays, you can grab a lot of those new games at a discounted price now."/>
</node>
</node>
<node CREATED="1326858082987" ID="Freemind_Link_473437526" MODIFIED="1326858082987" TEXT="March">
<node CREATED="1326858172069" FOLDED="true" ID="Freemind_Link_18654429" MODIFIED="1513982419852" TEXT="Boats:">
<icon BUILTIN="bookmark"/>
<node CREATED="1326858172069" MODIFIED="1326858172069" TEXT="As we mentioned above, once boat show season is over, you can still grab good deals on last year&apos;s models before the brand new ones come out."/>
</node>
<node CREATED="1326858172069" FOLDED="true" ID="Freemind_Link_1798963474" MODIFIED="1326858172069" TEXT="Frozen Foods:">
<node CREATED="1326858172069" MODIFIED="1326858172069" TEXT="It&apos;s National Frozen Food Month. It may be strange, but I&apos;m not about to argue with them."/>
</node>
<node CREATED="1326858172069" FOLDED="true" ID="Freemind_Link_479109226" MODIFIED="1326858172069" TEXT="Gardening Tools:">
<node CREATED="1326858172069" MODIFIED="1326858172069" TEXT="It may not be time to break them out yet, but if you shop for those gardening tools ahead of time, you can usually get a sweet price in late winter/early spring."/>
</node>
<node CREATED="1326858172069" FOLDED="true" ID="Freemind_Link_1417967602" MODIFIED="1326858172069" TEXT="Golf Clubs:">
<node CREATED="1326858172069" MODIFIED="1326858172069" TEXT="New models are coming out for the summer, so your local golf shop is pushing the old ones out the door. Grab last year&apos;s set cheaply and you&apos;ll forget all about how awesome the new ones are."/>
</node>
<node CREATED="1326858172069" FOLDED="true" ID="Freemind_Link_769530972" MODIFIED="1513982426335" TEXT="Luggage:">
<font ITALIC="true" NAME="Skia" SIZE="24"/>
<icon BUILTIN="bookmark"/>
<node CREATED="1326858172069" MODIFIED="1326858172069" TEXT="It&apos;s in between vacation seasons, so shops are dropping prices. Grab any extra bags you might need for the summer in March."/>
</node>
</node>
</node>
</node>
<node CREATED="1326858300722" ID="Freemind_Link_1171246105" MODIFIED="1326858300722" POSITION="right" TEXT="Second Quarter (April/May/June)">
<node CREATED="1326858326844" FOLDED="true" ID="Freemind_Link_1466598621" MODIFIED="1326858334327" TEXT="Televisions and Other Electronics:">
<icon BUILTIN="bookmark"/>
<node CREATED="1326858326844" MODIFIED="1326858326844" TEXT="Japanese manufacturers&apos; fiscal year ends in March, so they&apos;re eager to get rid of old stock."/>
</node>
<node CREATED="1326858326844" FOLDED="true" ID="Freemind_Link_869418674" MODIFIED="1513982439747" TEXT="Houses:">
<icon BUILTIN="bookmark"/>
<node CREATED="1326858326844" MODIFIED="1326858326844" TEXT="Home buying is a tough process, but if you do it in between winter and summer, you&apos;ll find a better deal than you would in the middle of the big seasons."/>
</node>
<node CREATED="1326858326844" FOLDED="true" ID="Freemind_Link_82328081" MODIFIED="1326858344577" TEXT="Boots &amp; Winter Wear:">
<icon BUILTIN="bookmark"/>
<node CREATED="1326858326844" MODIFIED="1326858326844" TEXT="Because who thinks about winter clothes when it&apos;s finally getting warm again?"/>
</node>
<node CREATED="1326858326845" FOLDED="true" ID="Freemind_Link_903645046" MODIFIED="1326858367989" TEXT="Cookware &amp; Kitchen Accessories:">
<icon BUILTIN="bookmark"/>
<node CREATED="1326858326845" MODIFIED="1326858326845" TEXT="Graduations are coming up, so even if you&apos;re well out of school, take advantage of the grad-based sales for everything kitchen-related."/>
</node>
<node CREATED="1326858326845" FOLDED="true" ID="Freemind_Link_1720169480" MODIFIED="1326858326845" TEXT="Vacuum Cleaners:">
<node CREATED="1326858326845" MODIFIED="1326858326845" TEXT="New vacuums come out in June. This is pretty convenient, since you can buy a vacuum for a low price beforehand and get started on your spring cleaning."/>
</node>
<node CREATED="1326858394958" ID="Freemind_Link_658307442" MODIFIED="1326858394958" TEXT="">
<node CREATED="1326858404721" ID="Freemind_Link_150013848" MODIFIED="1326858404721" TEXT="April">
<node CREATED="1326858446101" FOLDED="true" ID="Freemind_Link_994155309" MODIFIED="1326858446101" TEXT="Car Accessories &amp; Parts:">
<node CREATED="1326858446101" MODIFIED="1326858446101" TEXT="Since most people head out to fix their cars in the summer, you can get in on the game early for a good price. Some of you have pointed out some good places to get car parts online, too."/>
</node>
<node CREATED="1326858446101" FOLDED="true" ID="Freemind_Link_277019567" MODIFIED="1336178832119" TEXT="Cruises:">
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="bookmark"/>
<node CREATED="1326858446101" MODIFIED="1326858446101" TEXT="Since it isn&apos;t a traditional vacation month, you can often grab a cruise last-minute for less."/>
</node>
<node CREATED="1326858446101" FOLDED="true" ID="Freemind_Link_1046966619" MODIFIED="1336178831463" TEXT="Laptops:">
<icon BUILTIN="bookmark"/>
<node CREATED="1326858446101" MODIFIED="1326858446101" TEXT="Laptop prices are even lower in April than they are during the back to school season. For no apparent reason."/>
</node>
<node CREATED="1326858446101" FOLDED="true" ID="Freemind_Link_890826401" MODIFIED="1326858503475" TEXT="Office Furniture:">
<icon BUILTIN="bookmark"/>
<node CREATED="1326858446101" MODIFIED="1326858446101" TEXT="If you shop after tax day&#x2014;when most home businesses start up&#x2014;you can usually find some great deals on those office chairs and desks."/>
</node>
<node CREATED="1326858446102" FOLDED="true" ID="Freemind_Link_1846047535" MODIFIED="1326858446102" TEXT="Sneakers:">
<node CREATED="1326858446102" MODIFIED="1326858446102" TEXT="As the weather starts getting warmer, the less serious start getting back into the swing of working out, which means it&apos;s a great time to take advantage of spring sales at your favorite running or footwear store."/>
</node>
<node CREATED="1326858446102" FOLDED="true" ID="Freemind_Link_689555627" MODIFIED="1326858446102" TEXT="Snowblowers:">
<node CREATED="1326858446102" MODIFIED="1326858446102" TEXT="Obviously, no one&apos;s buying snowblowers after it&apos;s all melted. If you got snowed in this year, though, you might want to pre-emptively buy one for next year for a discounted price."/>
</node>
</node>
<node CREATED="1326858409706" ID="Freemind_Link_874290353" MODIFIED="1326858409706" TEXT="May">
<node CREATED="1326858557018" FOLDED="true" ID="Freemind_Link_1034886427" MODIFIED="1326858557018" TEXT="Mattresses:">
<node CREATED="1326858557018" MODIFIED="1326858557018" TEXT="Manufacturers are rolling out new products in the summer, so the best time to buy a mattress is now, while they&apos;re making room for all the new ones.It should still be easy to pick a great one with a bit of pre-shopping research."/>
</node>
<node CREATED="1326858557018" FOLDED="true" ID="Freemind_Link_785540253" MODIFIED="1326858557018" TEXT="Patio Furniture:">
<node CREATED="1326858557018" MODIFIED="1326858557018" TEXT="Warm weather cometh, along with new patio furniture. Stores are going to start clearing out any old stuff to make room, so get the deals while you can. You might want to check out garage sales before you do, though."/>
</node>
<node CREATED="1326858557018" FOLDED="true" ID="Freemind_Link_1573269324" MODIFIED="1326858557018" TEXT="Party &amp; Picnic Supplies:">
<node CREATED="1326858557019" MODIFIED="1326858557019" TEXT="The season of outdoor eats is on its way, and even if you aren&apos;t having a barbecue until July, stock up now while the deals are good."/>
</node>
<node CREATED="1326858557019" FOLDED="true" ID="Freemind_Link_1777761183" MODIFIED="1326858557019" TEXT="Refrigerators:">
<node CREATED="1326858557019" MODIFIED="1326858557019" TEXT="People are cleaning and upgrading their homes, and new refrigerators are coming out soon. Grab good deals on an older-model icebox in May."/>
</node>
</node>
<node CREATED="1326858415145" ID="Freemind_Link_1253025099" MODIFIED="1326858415145" TEXT="June">
<node CREATED="1326858602126" FOLDED="true" ID="Freemind_Link_1075516856" MODIFIED="1326858653304" TEXT="Champagne:">
<icon BUILTIN="bookmark"/>
<node CREATED="1326858602126" MODIFIED="1326858602126" TEXT="Since wedding season comes around at this time, champagne makers are in furious competition with one another, and you can grab a good deal."/>
</node>
<node CREATED="1326858602126" FOLDED="true" ID="Freemind_Link_451797924" MODIFIED="1326858688057" TEXT="Dishware:">
<icon BUILTIN="bookmark"/>
<node CREATED="1326858602126" MODIFIED="1326858602126" TEXT="Wedding season brings deals on dishes to everyone, including those not getting hitched."/>
</node>
<node CREATED="1326858602126" FOLDED="true" ID="Freemind_Link_1886817893" MODIFIED="1326858602126" TEXT="Gym Memberships:">
<node CREATED="1326858602126" MODIFIED="1326858602126" TEXT="Everyone&apos;s forgotten their New Year&apos;s resolutions, so gyms are a bit more desperate for members (not to mention everyone&apos;s working out outside). Haggle yourself a good price."/>
</node>
<node CREATED="1326858602126" FOLDED="true" ID="Freemind_Link_1469534492" MODIFIED="1326858602126" TEXT="Paint:">
<node CREATED="1326858602126" MODIFIED="1326858602126" TEXT="The summer heat means death to painters, but it also means low prices&#x2014;so if you&apos;re considering a new coat of interior or exterior paint, find your shade now and buy it for later."/>
</node>
<node CREATED="1326858602126" FOLDED="true" ID="Freemind_Link_1886200926" MODIFIED="1326858643737" TEXT="Tools:">
<icon BUILTIN="bookmark"/>
<node CREATED="1326858602126" MODIFIED="1326858602126" TEXT="It&apos;s hard to complain about gender stereotypes when power tool prices are this low. Shop before Father&apos;s Day for a good sale."/>
</node>
<node CREATED="1326858602126" FOLDED="true" ID="Freemind_Link_1433096609" MODIFIED="1326858646706" TEXT="Suits:">
<icon BUILTIN="bookmark"/>
<node CREATED="1326858602126" MODIFIED="1326858602126" TEXT="Like power tools, Father&apos;s Day brings low prices on the suits every dad must have."/>
</node>
</node>
</node>
</node>
<node CREATED="1326858055322" LINK="http://lifehacker.com/5872627/the-best-time-to-buy-anything-in-2012" MODIFIED="1326858055322" POSITION="left" TEXT="lifehacker.com &gt; 5872627 &gt; The-best-time-to-buy-anything-in-2012"/>
<node CREATED="1326858711575" ID="Freemind_Link_468899404" MODIFIED="1326858711575" POSITION="left" TEXT="Third Quarter (July/Aug/Sept)">
<node CREATED="1326858731520" FOLDED="true" ID="Freemind_Link_1659473236" MODIFIED="1326858731520" TEXT="Computers:">
<node CREATED="1326858731520" MODIFIED="1326858731520" TEXT="Intel and AMD start ramping up for new stuff in July, and back-to-school sales get pretty good here too. If your computer store of choice requires proof that you&apos;re a student, you can always fudge it if you have flexible ethics."/>
</node>
<node CREATED="1326858731520" FOLDED="true" ID="Freemind_Link_1619881166" MODIFIED="1326858731520" TEXT="Big Appliances:">
<node CREATED="1326858731520" MODIFIED="1326858731520" TEXT="Newer appliances are coming out at the end of the year, so you can get great discounts on the bigger ones all quarter."/>
</node>
<node CREATED="1326858731520" FOLDED="true" ID="Freemind_Link_1598142005" MODIFIED="1326858731520" TEXT="MP3 Players:">
<node CREATED="1326858731520" MODIFIED="1326858731520" TEXT="In the later two thirds of the quarter, you can often find some pretty good deals on MP3 players, not the least of which is the illustrious iPod, which always has its refresh in the fall."/>
</node>
<node CREATED="1326858796369" ID="Freemind_Link_1611840059" MODIFIED="1326858796369" TEXT="">
<node CREATED="1326858805517" ID="Freemind_Link_280726891" MODIFIED="1326858805517" TEXT="July">
<node CREATED="1326858840325" FOLDED="true" ID="Freemind_Link_1565103303" MODIFIED="1326858840325" TEXT="Broadway Tickets:">
<node CREATED="1326858840325" MODIFIED="1326858840325" TEXT="Once again, tickets are a bit cheaper in July when fewer people are seeing shows. Even if you&apos;re buying tickets for later in the year, you can get a pretty good discount on them now."/>
</node>
<node CREATED="1326858840325" FOLDED="true" ID="Freemind_Link_938505197" MODIFIED="1326858852064" TEXT="Furniture:">
<icon BUILTIN="bookmark"/>
<node CREATED="1326858840325" MODIFIED="1326858840325" TEXT="August is the second time of year that new furniture comes out, so once again you can get good deals the month before as manufacturers are trying to get rid of the old stuff."/>
</node>
<node CREATED="1326858840325" FOLDED="true" ID="Freemind_Link_1179950026" MODIFIED="1326858840325" TEXT="Grills:">
<node CREATED="1326858840325" MODIFIED="1326858840325" TEXT="Borrow your neighbor&apos;s grill for Independence Day, then grab a great deal after the 4th instead."/>
</node>
<node CREATED="1326858840325" FOLDED="true" ID="Freemind_Link_370400725" MODIFIED="1326858840325" TEXT="Paint:">
<node CREATED="1326858840325" MODIFIED="1326858840325" TEXT="As noted above, it&apos;s still pretty hot outside, so grab your paint now while it&apos;s in low demand."/>
</node>
</node>
<node CREATED="1326858811044" ID="Freemind_Link_772219446" MODIFIED="1326858811044" TEXT="August">
<node CREATED="1326858913443" FOLDED="true" ID="Freemind_Link_604881945" MODIFIED="1326858913443" TEXT="Linens &amp; Storage Containers:">
<node CREATED="1326858913443" MODIFIED="1326858913443" TEXT="As the kids head off to college, lots of stores have sales on new dorm supplies."/>
</node>
<node CREATED="1326858913443" FOLDED="true" ID="Freemind_Link_164837372" MODIFIED="1326858928996" TEXT="Outdoor Toys &amp; Camping Equipment:">
<icon BUILTIN="bookmark"/>
<node CREATED="1326858913443" MODIFIED="1326858913443" TEXT="The weather won&apos;t be warm for much longer, and stores want to get rid of these summer toys that take up tons of space. You&apos;ll find some pretty great deals here if you shop around."/>
</node>
<node CREATED="1326858913443" FOLDED="true" ID="Freemind_Link_883061529" MODIFIED="1326858913443" TEXT="Kids&apos; Clothing:">
<node CREATED="1326858913443" MODIFIED="1326858913443" TEXT="It&apos;s back to school season, so lots of more kid-oriented stores are having sales."/>
</node>
<node CREATED="1326858913443" FOLDED="true" ID="Freemind_Link_1842774469" MODIFIED="1326858913443" TEXT="School Supplies:">
<node CREATED="1326858913444" MODIFIED="1326858913444" TEXT="Again, back to school sales mean good deals on lots of office supplies. You can either buy now, when the sales are going on, or later in September, when stores are getting rid of leftovers."/>
</node>
<node CREATED="1326858913444" FOLDED="true" ID="Freemind_Link_1668802218" MODIFIED="1326858913444" TEXT="Wine:">
<node CREATED="1326858913444" MODIFIED="1326858913444" TEXT="It&apos;s still early in the harvest, but you can fine some smaller-run wines for cheap during the month of August."/>
</node>
</node>
<node CREATED="1326858818005" ID="Freemind_Link_1133669064" MODIFIED="1326858818005" TEXT="September">
<node CREATED="1326858964691" FOLDED="true" ID="Freemind_Link_599249325" MODIFIED="1326858964691" TEXT="Bicycles:">
<node CREATED="1326858964691" MODIFIED="1326858964691" TEXT="New models come out at the end of the month, which means you can get discounts on the old beaters before winter comes around."/>
</node>
<node CREATED="1326858964691" FOLDED="true" ID="Freemind_Link_1530113681" MODIFIED="1326858995931" TEXT="Cars:">
<icon BUILTIN="bookmark"/>
<node CREATED="1326858964692" MODIFIED="1326858964692" TEXT="New cars usually come out at the end of the summer, so lots will be clearing out their 2012 models around this time. It should be easier to haggle down the price, too. Buy at the end of the month for extra savings."/>
</node>
<node CREATED="1326858964692" FOLDED="true" ID="Freemind_Link_940047476" MODIFIED="1326858964692" TEXT="Grills &amp; Lawnmowers:">
<node CREATED="1326858964692" MODIFIED="1326858964692" TEXT="Stores have to make room for all their winter gear, which means the big summer staples will be on sale."/>
</node>
<node CREATED="1326858964692" FOLDED="true" ID="Freemind_Link_1352392771" MODIFIED="1326859032574" TEXT="Holiday Airfare:">
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="bookmark"/>
<node CREATED="1326858964692" MODIFIED="1326858964692" TEXT="We&apos;ve mentioned before that you should buy plane tickets two months in advance, which means now is the time to prepare your holiday visits to friends and family."/>
</node>
<node CREATED="1326858964692" FOLDED="true" ID="Freemind_Link_165705695" MODIFIED="1326858964692" TEXT="Patio Furniture:">
<node CREATED="1326858964692" MODIFIED="1326858964692" TEXT="Like the grills and lawnmowers, out with the summer, in with the winter."/>
</node>
<node CREATED="1326858964692" FOLDED="true" ID="Freemind_Link_1603205133" MODIFIED="1326858964692" TEXT="School Supplies:">
<node CREATED="1326858964692" MODIFIED="1326858964692" TEXT="As mentioned above, stores&apos;ll be clearing out their inventory from all the back to school sales. Anything you don&apos;t need the first few weeks of school can be bought in September for even less."/>
</node>
<node CREATED="1326858964692" FOLDED="true" ID="Freemind_Link_1315900767" MODIFIED="1326858964692" TEXT="Wine:">
<node CREATED="1326858964692" MODIFIED="1326858964692" TEXT="It&apos;s harvest time, which means this is the best time to stock up on wine."/>
</node>
</node>
</node>
</node>
<node CREATED="1326859048680" ID="Freemind_Link_1523845437" MODIFIED="1326859048680" POSITION="left" TEXT="Fourth Quarter (Oct/Nov/Dec)">
<node CREATED="1326859069400" FOLDED="true" ID="Freemind_Link_111114818" MODIFIED="1326859079267" TEXT="Cars:">
<icon BUILTIN="bookmark"/>
<node CREATED="1326859069400" MODIFIED="1326859069400" TEXT="As we said in September, now that the new models are out, the old models are going for less. Haggle your way down to good prices on 2012 models for the rest of the year."/>
</node>
<node CREATED="1326859069400" FOLDED="true" ID="Freemind_Link_1158312261" MODIFIED="1326859088766" TEXT="Cookware &amp; Kitchen Accessories:">
<icon BUILTIN="bookmark"/>
<node CREATED="1326859069400" MODIFIED="1326859069400" TEXT="Holiday deals start pretty early, and cookware is one of the more popular items that you&apos;ll find on sale all quarter."/>
</node>
<node CREATED="1326859069400" FOLDED="true" ID="Freemind_Link_1201427908" MODIFIED="1326859069400" TEXT="Gas Grills &amp; Air Conditioners:">
<node CREATED="1326859069400" MODIFIED="1326859069400" TEXT="Again, we drift into the cold off-season, and stores start to push their older stuff."/>
</node>
<node CREATED="1326859069401" FOLDED="true" ID="Freemind_Link_1920567466" MODIFIED="1326859069401" TEXT="Shrubs, Bushes, Bulbs, etc.:">
<node CREATED="1326859069401" MODIFIED="1326859069401" TEXT="If you have a cellar or other area that can store plants until the spring, you can pick them up now pretty cheaply."/>
</node>
<node CREATED="1326859069401" FOLDED="true" ID="Freemind_Link_1321373360" MODIFIED="1326859069401" TEXT="Toys &amp; Games:">
<node CREATED="1326859069401" MODIFIED="1326859069401" TEXT="Again, the Christmas sales start early at places like Toys R Us, and will continue through most of the holiday season."/>
</node>
<node CREATED="1326859069401" FOLDED="true" ID="Freemind_Link_1995378582" MODIFIED="1326859069401" TEXT="Wedding Supplies:">
<node CREATED="1326859069401" MODIFIED="1326859069401" TEXT="Winter&apos;s back, which means finding a venue and negotiating those services is going to be a lot easier. The more deeply into winter you wait, the better it gets."/>
</node>
<node CREATED="1326859071061" ID="Freemind_Link_1201331901" MODIFIED="1326859071061" TEXT="">
<node CREATED="1326859119574" ID="Freemind_Link_1802723119" MODIFIED="1326859119574" TEXT="October">
<node CREATED="1326859147382" FOLDED="true" ID="Freemind_Link_329947760" MODIFIED="1326859147382" TEXT="Broadway Tickets:">
<node CREATED="1326859147382" MODIFIED="1326859147382" TEXT="Yet another off-month for Broadway shows, so grab your tickets now for less."/>
</node>
<node CREATED="1326859147382" FOLDED="true" ID="Freemind_Link_1567193256" MODIFIED="1326859147382" TEXT="Health Insurance:">
<node CREATED="1326859147382" MODIFIED="1326859147382" TEXT="If you&apos;re lucky enough to have a choice in when you buy, October is when most health plans accept new members, so shop around and see if you can find better benefits elsewhere."/>
</node>
<node CREATED="1326859147382" FOLDED="true" ID="Freemind_Link_256298834" MODIFIED="1326859147382" TEXT="Jeans:">
<node CREATED="1326859147382" MODIFIED="1326859147382" TEXT="Hit the mall in between back to school season and holiday shopping season for some mad prices on jeans."/>
</node>
</node>
<node CREATED="1326859124814" ID="Freemind_Link_825033817" MODIFIED="1326859124814" TEXT="November">
<node CREATED="1326859192239" FOLDED="true" ID="Freemind_Link_566146605" MODIFIED="1326859192239" TEXT="Aluminum Foil &amp; Plastic Wrap:">
<node CREATED="1326859192239" MODIFIED="1326859192239" TEXT="Yeah, it&apos;s weird."/>
</node>
<node CREATED="1326859192239" FOLDED="true" ID="Freemind_Link_1554302414" MODIFIED="1326859192239" TEXT="Televisions &amp; Other Electronics:">
<node CREATED="1326859192239" MODIFIED="1326859192239" TEXT="Hit the stores during Black Friday and Cyber Monday for insane deals on all sorts of electronics."/>
</node>
</node>
<node CREATED="1326859129582" ID="Freemind_Link_490156086" MODIFIED="1326859129582" TEXT="December">
<node CREATED="1326859248250" FOLDED="true" ID="Freemind_Link_1735450534" MODIFIED="1326859301754" TEXT="Champagne:">
<icon BUILTIN="bookmark"/>
<node CREATED="1326859248250" MODIFIED="1326859248250" TEXT="Again, this is one of those few times that prices drop because demand is so high. Champagne companies are all trying to compete with each other, so grab it now for your holiday celebrations."/>
</node>
<node CREATED="1326859248251" FOLDED="true" ID="Freemind_Link_1544009056" MODIFIED="1326859248251" TEXT="Golf Clubs:">
<node CREATED="1326859248251" MODIFIED="1326859248251" TEXT="It&apos;s the off-season, so golf clubs are cheaper to come by."/>
</node>
<node CREATED="1326859248251" FOLDED="true" ID="Freemind_Link_1789695989" MODIFIED="1326859248251" TEXT="Pools:">
<node CREATED="1326859248251" MODIFIED="1326859248251" TEXT="It may seem like eons away, but if you buy a pool now, you can get a great discount on next summer&apos;s fun. There are a few smaller sales during the summer, but you&apos;d have to keep a sharp eye out to catch them."/>
</node>
<node CREATED="1326859248251" FOLDED="true" ID="Freemind_Link_232420419" MODIFIED="1326859306345" TEXT="Televisions &amp; Other Electronics:">
<icon BUILTIN="bookmark"/>
<node CREATED="1326859248251" MODIFIED="1326859248251" TEXT="The sales continue after Black Friday and Cyber Monday, usually all the way up until the super bowl."/>
</node>
<node CREATED="1326859248251" FOLDED="true" ID="Freemind_Link_1031819624" MODIFIED="1326859296245" TEXT="Tools:">
<icon BUILTIN="bookmark"/>
<node CREATED="1326859248251" MODIFIED="1326859248251" TEXT="Holiday sales are perfect for grabbing cheap tools, especially if you have a winter-related home repair you need to make."/>
</node>
</node>
</node>
</node>
<node CREATED="1326859450610" ID="Freemind_Link_1937818625" MODIFIED="1326859450610" POSITION="left" TEXT="General Buying Tips for Any Time of Year">
<node CREATED="1326859551245" ID="Freemind_Link_393839223" MODIFIED="1326859551245" TEXT="Appliances">
<node CREATED="1326859598044" FOLDED="true" ID="Freemind_Link_452638102" MODIFIED="1326859714228" TEXT="Sundays">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1326859551245" ID="Freemind_Link_403047825" MODIFIED="1326859596838" TEXT="tend to be cheap on Sundays, when most people are out buying new ones. In addition, you&apos;re bound to find great deals on any major holiday, like Memorial Day or Independence day."/>
</node>
</node>
<node CREATED="1326859551245" ID="Freemind_Link_1932029331" MODIFIED="1326859551245" TEXT="Computers &amp; other electronics">
<node CREATED="1326859604020" FOLDED="true" ID="Freemind_Link_8423395" MODIFIED="1326859715035" TEXT="Mondays">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1326859551245" MODIFIED="1326859551245" TEXT="are cheapest on Mondays, when manufacturers apply their rebates. This applies to TVs, cameras, and video games as well."/>
</node>
</node>
<node CREATED="1326859551245" ID="Freemind_Link_725876505" MODIFIED="1326859551245" TEXT="Entertainment venues">
<node CREATED="1326859609516" FOLDED="true" ID="Freemind_Link_247119246" MODIFIED="1326859715659" TEXT="mid-week">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1326859551245" MODIFIED="1326859551245" TEXT="like museums and amusement parks often have discounts during the middle of the week, when they&apos;re less crowded. Plus, smaller crowds are always nice. Some museums might even have free admission days, so check out the venues you&apos;re interested in for more."/>
</node>
</node>
<node CREATED="1326859551245" ID="Freemind_Link_766192624" MODIFIED="1326859551245" TEXT="Gas">
<node CREATED="1326859650473" FOLDED="true" ID="Freemind_Link_1530945264" MODIFIED="1326859728484" TEXT="Wednesday morning when colder">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1326859551245" MODIFIED="1326859551245" TEXT="prices are higher on the weekends, hitting their peak mid-morning on Thursday to anticipate all the weekend drivers and travelers. Fill up on Wednesdays or early morning Thursdays for the biggest savings. Also, if you fill up in the morning when it&apos;s cold, your gas will be a bit denser and you&apos;ll get a bit more fuel for your money."/>
</node>
</node>
<node CREATED="1326859551245" ID="Freemind_Link_4356387" MODIFIED="1326859551245" TEXT="Jewelry">
<node CREATED="1326859676434" FOLDED="true" ID="Freemind_Link_901682370" MODIFIED="1326859683765" TEXT="Wednesdays, avoid holidays">
<node CREATED="1326859551246" MODIFIED="1326859551246" TEXT="is best bought on Wednesdays, when most people tend to shop for it. However, stay away from gift giving months like Valentine&apos;s Day, Mother&apos;s Day, and Christmas."/>
</node>
</node>
<node CREATED="1326859551246" ID="Freemind_Link_5783611" MODIFIED="1326859551246" TEXT="Chocolate">
<node CREATED="1326859690226" FOLDED="true" ID="Freemind_Link_917103921" MODIFIED="1326859725539" TEXT="post-holidays">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1326859551246" MODIFIED="1326859551246" TEXT="is great to buy after any holiday that involves chocolate: Halloween, Christmas, Easter, and Valentines&apos;s Day, for example. They&apos;ll be clearing out all their holiday-themed candy, so you can grab them up if your kids already ate their chocolate rabbits and Santas."/>
</node>
</node>
</node>
</node>
</map>
