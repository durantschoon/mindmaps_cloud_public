<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1273388046201" ID="Freemind_Link_1623211675" LINK="../../Documents/mindmaps/MetaMap.mm" MODIFIED="1273388092129" TEXT="Photography.mm">
<node CREATED="1273388107429" MODIFIED="1273388107429" POSITION="right" TEXT="SLR has the mirror so you can see what the lense sees"/>
<node CREATED="1273388119115" ID="_" MODIFIED="1273388119139" POSITION="right" TEXT="Micro 4/3&apos;s ">
<node CREATED="1273388107434" ID="Freemind_Link_443333356" MODIFIED="1273388150777" TEXT="only allows viewing what the digital sensor sees, &#xa;but pictures will be as awesome &#xa;(smaller form factor than SLR).">
<node CREATED="1273388107445" LINK="http://en.wikipedia.org/wiki/Micro_Four_Thirds_system" MODIFIED="1273388107445" TEXT="en.wikipedia.org &gt; Wiki &gt; Micro Four Thirds system"/>
</node>
</node>
<node CREATED="1336178267403" ID="ID_339122541" LINK="http://www.lytro.com/" MODIFIED="1336178267403" POSITION="left" TEXT="lytro.com"/>
</node>
</map>
