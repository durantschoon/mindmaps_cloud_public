<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1373997602985" ID="ID_1266528637" LINK="../../Documents/mindmaps/MetaMap.mm" MODIFIED="1373997620889" TEXT="QuantumMechanics">
<node CREATED="1373997624925" ID="ID_802477270" MODIFIED="1373997631260" POSITION="left" TEXT="prerequisites">
<node CREATED="1373997631656" ID="ID_885480688" MODIFIED="1373997634637" TEXT="linear algebra">
<node CREATED="1373997655372" ID="ID_1958609630" MODIFIED="1373997658185" TEXT="matricies"/>
<node CREATED="1373997658549" ID="ID_1900651418" MODIFIED="1373997660610" TEXT="vectors"/>
</node>
<node CREATED="1373997634913" ID="ID_1741552391" MODIFIED="1373997638655" TEXT="complex numbers">
<node CREATED="1373997665417" ID="ID_1970505456" MODIFIED="1373997668889" TEXT="spherical coordinates"/>
</node>
<node CREATED="1373997681521" ID="ID_1011678857" MODIFIED="1373997684014" TEXT="probability"/>
<node CREATED="1374184250795" ID="ID_1684625692" MODIFIED="1374184251816" TEXT="logic">
<node CREATED="1374184252147" ID="ID_122477988" MODIFIED="1374184255054" TEXT="truth tables"/>
<node CREATED="1374184255298" ID="ID_1788758893" MODIFIED="1374184256174" TEXT="gates"/>
</node>
</node>
<node CREATED="1374184224411" ID="ID_1206181479" MODIFIED="1374184224411" POSITION="right" TEXT="">
<node CREATED="1374184204584" ID="ID_849177884" MODIFIED="1374184215754" TEXT="see Learning for reading list">
<node CREATED="1374183736945" ID="ID_1751610349" LINK="Learning.mm" MODIFIED="1375595120222" STYLE="bubble" TEXT="Learning">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
<node CREATED="1374184218828" ID="ID_1561774964" MODIFIED="1374184223384" TEXT="this file is for notes"/>
</node>
<node CREATED="1374027752783" ID="ID_999932152" MODIFIED="1374027755372" POSITION="right" TEXT="university programs">
<node CREATED="1374027755938" ID="ID_55269424" LINK="http://iqc.uwaterloo.ca/" MODIFIED="1374027755938" TEXT="iqc.uwaterloo.ca"/>
</node>
<node CREATED="1496295480652" ID="ID_991662074" MODIFIED="1496295569687" POSITION="right" TEXT="The Logic of Experimental Tests, &#xa;Particularly of Everettian Quantum Theory">
<node CREATED="1496295510526" ID="ID_377759195" MODIFIED="1496295514015" TEXT="David Deutsch">
<node CREATED="1496295574570" ID="ID_1844503091" MODIFIED="1496295580465" TEXT="Constructor Theory">
<node CREATED="1496295586381" LINK="http://constructortheory.org/" MODIFIED="1496295586381" TEXT="constructortheory.org"/>
</node>
</node>
<node CREATED="1496295515251" ID="ID_1854714088" LINK="http://constructortheory.org/portfolio/logic-experimental-tests/" MODIFIED="1496295515251" TEXT="constructortheory.org &gt; Portfolio &gt; Logic-experimental-tests"/>
</node>
</node>
</map>
