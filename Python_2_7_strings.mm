<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1378079802778" ID="ID_416378775" LINK="Python_2_7.mm" MODIFIED="1378271488901" STYLE="fork">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <center>
      <p>
        Python String Data Type
      </p>
      Version 2.7
    </center>
  </body>
</html></richcontent>
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="14"/>
<node BACKGROUND_COLOR="#ffff66" CREATED="1378078311501" HGAP="38" ID="ID_229325077" MODIFIED="1378271445125" POSITION="right" STYLE="bubble" TEXT="Raw Strings" VSHIFT="-141">
<cloud/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378079008619" ID="ID_1910526844" MODIFIED="1378081071001" STYLE="bubble" TEXT="r">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378079019860" ID="ID_156047299" MODIFIED="1378081169587" TEXT="Before">
<node BACKGROUND_COLOR="#ccccff" CREATED="1378080995980" ID="ID_686623608" MODIFIED="1378081176507" STYLE="bubble" TEXT="Opening Quotes">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378079008619" ID="ID_535395027" MODIFIED="1378081084537" STYLE="bubble" TEXT="Turns Off">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378081108974" ID="ID_184762190" MODIFIED="1378081120232" STYLE="bubble" TEXT="Escaping">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node BACKGROUND_COLOR="#ccccff" CREATED="1378081111161" ID="ID_1040096404" MODIFIED="1378081128329" STYLE="bubble" TEXT="Via">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node CREATED="1378081133423" ID="ID_831892991" MODIFIED="1378081156663" TEXT="\"/>
</node>
</node>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378079008619" ID="ID_230377098" MODIFIED="1378081094321" STYLE="bubble" TEXT="Useful for">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378079019860" ID="ID_307365327" MODIFIED="1378081340863" TEXT="Paths">
<node BACKGROUND_COLOR="#ccccff" CREATED="1378080995980" ID="ID_1158735346" MODIFIED="1378081677950" STYLE="bubble" TEXT="e.g.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node CREATED="1378081373164" ID="ID_1163622315" MODIFIED="1378081376393" TEXT="Normal">
<node CREATED="1378081597389" ID="ID_1893186729" MODIFIED="1378081597389" TEXT="&quot;C:\\Dir1\\Dir2\\Dir3\\Dir4&quot;"/>
<node CREATED="1378081612001" ID="ID_276015896" MODIFIED="1378081618614" TEXT="Backslashes require escaping"/>
</node>
<node CREATED="1378081376660" ID="ID_370056893" MODIFIED="1378081377305" TEXT="Raw">
<node CREATED="1378081650492" ID="ID_810795953" MODIFIED="1378081653543" TEXT="r&quot;C:\Dir1\Dir2\Dir3\Dir4&quot;"/>
</node>
</node>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378081343828" ID="ID_706381152" MODIFIED="1378081704592" STYLE="bubble" TEXT="Regular Expressions">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node BACKGROUND_COLOR="#ccccff" CREATED="1378081250161" ID="ID_885885827" LINK="http://docs.python.org/2.7/library/re.html#raw-string-notation" MODIFIED="1378081711705" STYLE="bubble" TEXT="docs.python.org &gt; 2.7 &gt; Library &gt; Re">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node BACKGROUND_COLOR="#ffff66" CREATED="1378078311501" HGAP="29" ID="ID_915551802" MODIFIED="1378271445125" POSITION="left" STYLE="bubble" TEXT="Immutable" VSHIFT="1">
<cloud/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378079008619" ID="ID_1692684887" MODIFIED="1378081775320" STYLE="bubble" TEXT="Operations">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378079019860" ID="ID_157196591" MODIFIED="1378081806945" TEXT="Concatenation"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378079019860" ID="ID_1134413522" MODIFIED="1378081820421" TEXT="Create">
<node BACKGROUND_COLOR="#ccccff" CREATED="1378080995980" ID="ID_1332085226" MODIFIED="1378081826041" STYLE="bubble" TEXT="New String">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
</node>
</node>
<node BACKGROUND_COLOR="#ffff66" CREATED="1378078311501" HGAP="29" ID="ID_1160126960" MODIFIED="1378440358276" POSITION="left" STYLE="bubble" TEXT="Sequence Type Methods" VSHIFT="1">
<cloud/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node CREATED="1378084219397" ID="ID_1320664416" MODIFIED="1378085711098">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <table style="border-style: solid; border-top-width: 0; width: 80%; border-bottom-width: 0; border-right-width: 0; border-left-width: 0" border="0">
      <tr>
        <th style="border-top-width: 1px; padding-top: 2px; background-color: rgb(238, 221, 238); padding-bottom: 2px; text-align: center; padding-right: 5px; border-left-width: 0px; padding-left: 5px" class="head">
          Operation
        </th>
        <th style="border-top-width: 1px; padding-top: 2px; background-color: rgb(238, 221, 238); padding-bottom: 2px; text-align: center; padding-right: 5px; border-left-width: 0px; padding-left: 5px" class="head">
          Result
        </th>
      </tr>
      <tr>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          <font size="0.95em"><tt style="padding-top: 0px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-bottom: 0px; padding-right: 1px; padding-left: 1px" class="docutils literal">x</tt><tt style="padding-top: 0px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-bottom: 0px; padding-right: 1px; padding-left: 1px" class="docutils literal">&#xa0;in&#xa0;s</tt></font>
        </td>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          <font size="0.95em"><tt style="padding-top: 0px; font-weight: bold; font-size: 0.95em; padding-bottom: 0px; padding-right: 1px; padding-left: 1px" class="xref docutils literal"><b>True</b></tt></font>&#xa0;if an item of&#xa0;<em>s</em>&#xa0;is equal to&#xa0;<em>x</em>, else&#xa0;<font size="0.95em"><tt style="padding-top: 0px; font-weight: bold; font-size: 0.95em; padding-bottom: 0px; padding-right: 1px; padding-left: 1px" class="xref docutils literal"><b>False</b></tt></font>
        </td>
      </tr>
      <tr>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          <span class="pre"><font size="0.95em"><tt style="padding-top: 0px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-bottom: 0px; padding-right: 1px; padding-left: 1px" class="docutils literal">x</tt></font></span><font size="0.95em"><span class="Apple-converted-space"><tt style="padding-top: 0px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-bottom: 0px; padding-right: 1px; padding-left: 1px" class="docutils literal">&#xa0;</tt></span><span class="pre"><tt style="padding-top: 0px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-bottom: 0px; padding-right: 1px; padding-left: 1px" class="docutils literal">not</tt></span><span class="Apple-converted-space"><tt style="padding-top: 0px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-bottom: 0px; padding-right: 1px; padding-left: 1px" class="docutils literal">&#xa0;</tt></span><span class="pre"><tt style="padding-top: 0px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-bottom: 0px; padding-right: 1px; padding-left: 1px" class="docutils literal">in</tt></span><span class="Apple-converted-space"><tt style="padding-top: 0px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-bottom: 0px; padding-right: 1px; padding-left: 1px" class="docutils literal">&#xa0;</tt></span><span class="pre"><tt style="padding-top: 0px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-bottom: 0px; padding-right: 1px; padding-left: 1px" class="docutils literal">s</tt></span></font>
        </td>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          <span class="pre"><font size="0.95em"><tt style="padding-top: 0px; font-weight: bold; font-size: 0.95em; padding-bottom: 0px; padding-right: 1px; padding-left: 1px" class="xref docutils literal"><b>False</b></tt></font></span><span class="Apple-converted-space">&#xa0;</span>if an item of<span class="Apple-converted-space">&#xa0;</span><em>s</em><span class="Apple-converted-space">&#xa0;</span>is equal to<span class="Apple-converted-space">&#xa0;</span><em>x</em>, else<span class="Apple-converted-space">&#xa0;</span><span class="pre"><font size="0.95em"><tt style="padding-top: 0px; font-weight: bold; font-size: 0.95em; padding-bottom: 0px; padding-right: 1px; padding-left: 1px" class="xref docutils literal"><b>True</b></tt></font></span>
        </td>
      </tr>
      <tr>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          <span class="pre"><font size="0.95em"><tt style="padding-top: 0px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-bottom: 0px; padding-right: 1px; padding-left: 1px" class="docutils literal">s</tt></font></span><font size="0.95em"><span class="Apple-converted-space"><tt style="padding-top: 0px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-bottom: 0px; padding-right: 1px; padding-left: 1px" class="docutils literal">&#xa0;</tt></span><span class="pre"><tt style="padding-top: 0px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-bottom: 0px; padding-right: 1px; padding-left: 1px" class="docutils literal">+</tt></span><span class="Apple-converted-space"><tt style="padding-top: 0px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-bottom: 0px; padding-right: 1px; padding-left: 1px" class="docutils literal">&#xa0;</tt></span><span class="pre"><tt style="padding-top: 0px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-bottom: 0px; padding-right: 1px; padding-left: 1px" class="docutils literal">t</tt></span></font>
        </td>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          the concatenation of<span class="Apple-converted-space">&#xa0;</span><em>s</em><span class="Apple-converted-space">&#xa0;</span>and<span class="Apple-converted-space">&#xa0;</span><em>t</em>
        </td>
      </tr>
      <tr>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          <span class="pre"><font size="0.95em"><tt style="padding-top: 0px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-bottom: 0px; padding-right: 1px; padding-left: 1px" class="docutils literal">s</tt></font></span><font size="0.95em"><span class="Apple-converted-space"><tt style="padding-top: 0px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-bottom: 0px; padding-right: 1px; padding-left: 1px" class="docutils literal">&#xa0;</tt></span><span class="pre"><tt style="padding-top: 0px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-bottom: 0px; padding-right: 1px; padding-left: 1px" class="docutils literal">*</tt></span><span class="Apple-converted-space"><tt style="padding-top: 0px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-bottom: 0px; padding-right: 1px; padding-left: 1px" class="docutils literal">&#xa0;</tt></span><span class="pre"><tt style="padding-top: 0px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-bottom: 0px; padding-right: 1px; padding-left: 1px" class="docutils literal">n,</tt></span><span class="Apple-converted-space"><tt style="padding-top: 0px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-bottom: 0px; padding-right: 1px; padding-left: 1px" class="docutils literal">&#xa0;</tt></span><span class="pre"><tt style="padding-top: 0px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-bottom: 0px; padding-right: 1px; padding-left: 1px" class="docutils literal">n</tt></span><span class="Apple-converted-space"><tt style="padding-top: 0px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-bottom: 0px; padding-right: 1px; padding-left: 1px" class="docutils literal">&#xa0;</tt></span><span class="pre"><tt style="padding-top: 0px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-bottom: 0px; padding-right: 1px; padding-left: 1px" class="docutils literal">*</tt></span><span class="Apple-converted-space"><tt style="padding-top: 0px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-bottom: 0px; padding-right: 1px; padding-left: 1px" class="docutils literal">&#xa0;</tt></span><span class="pre"><tt style="padding-top: 0px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-bottom: 0px; padding-right: 1px; padding-left: 1px" class="docutils literal">s</tt></span></font>
        </td>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          <em>n</em><span class="Apple-converted-space">&#xa0;</span>shallow copies of<span class="Apple-converted-space">&#xa0;</span><em>s</em><span class="Apple-converted-space">&#xa0;</span>concatenated
        </td>
      </tr>
      <tr>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          <span class="pre"><font size="0.95em"><tt style="padding-top: 0px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-bottom: 0px; padding-right: 1px; padding-left: 1px" class="docutils literal">s[i]</tt></font></span>
        </td>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          <em>i</em>th item of<span class="Apple-converted-space">&#xa0;</span><em>s</em>, origin 0
        </td>
      </tr>
      <tr>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          <span class="pre"><font size="0.95em"><tt style="padding-top: 0px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-bottom: 0px; padding-right: 1px; padding-left: 1px" class="docutils literal">s[i:j]</tt></font></span>
        </td>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          slice of<span class="Apple-converted-space">&#xa0;</span><em>s</em><span class="Apple-converted-space">&#xa0;</span>from<span class="Apple-converted-space">&#xa0;</span><em>i</em><span class="Apple-converted-space">&#xa0;</span>to<span class="Apple-converted-space">&#xa0;</span><em>j</em>
        </td>
      </tr>
      <tr>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          <span class="pre"><font size="0.95em"><tt style="padding-top: 0px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-bottom: 0px; padding-right: 1px; padding-left: 1px" class="docutils literal">s[i:j:k]</tt></font></span>
        </td>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          slice of<span class="Apple-converted-space">&#xa0;</span><em>s</em><span class="Apple-converted-space">&#xa0;</span>from<span class="Apple-converted-space">&#xa0;</span><em>i</em><span class="Apple-converted-space">&#xa0;</span>to<span class="Apple-converted-space">&#xa0;</span><em>j</em><span class="Apple-converted-space">&#xa0;</span>with step<span class="Apple-converted-space">&#xa0;</span><em>k</em>
        </td>
      </tr>
      <tr>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          <span class="pre"><font size="0.95em"><tt style="padding-top: 0px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-bottom: 0px; padding-right: 1px; padding-left: 1px" class="docutils literal">len(s)</tt></font></span>
        </td>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          length of<span class="Apple-converted-space">&#xa0;</span><em>s</em>
        </td>
      </tr>
      <tr>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          <span class="pre"><font size="0.95em"><tt style="padding-top: 0px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-bottom: 0px; padding-right: 1px; padding-left: 1px" class="docutils literal">min(s)</tt></font></span>
        </td>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          smallest item of<span class="Apple-converted-space">&#xa0;</span><em>s</em>
        </td>
      </tr>
      <tr>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          <span class="pre"><font size="0.95em"><tt style="padding-top: 0px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-bottom: 0px; padding-right: 1px; padding-left: 1px" class="docutils literal">max(s)</tt></font></span>
        </td>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          largest item of<span class="Apple-converted-space">&#xa0;</span><em>s</em>
        </td>
      </tr>
      <tr>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          <span class="pre"><font size="0.95em"><tt style="padding-top: 0px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-bottom: 0px; padding-right: 1px; padding-left: 1px" class="docutils literal">s.index(i)</tt></font></span>
        </td>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          index of the first occurrence of<span class="Apple-converted-space">&#xa0;</span><em>i</em><span class="Apple-converted-space">&#xa0;</span>in<span class="Apple-converted-space">&#xa0;</span><em>s</em>
        </td>
      </tr>
      <tr>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          <span class="pre"><font size="0.95em"><tt style="padding-top: 0px; background-color: rgb(236, 240, 243); font-size: 0.95em; padding-bottom: 0px; padding-right: 1px; padding-left: 1px" class="docutils literal">s.count(i)</tt></font></span>
        </td>
        <td style="padding-top: 2px; background-color: rgb(238, 238, 255); padding-bottom: 2px; text-align: left; padding-right: 5px; border-left-width: 0px; padding-left: 5px">
          total number of occurrences of<span class="Apple-converted-space">&#xa0;</span><em>i</em><span class="Apple-converted-space">&#xa0;</span>in<span class="Apple-converted-space">&#xa0;</span><em>s</em>
        </td>
      </tr>
    </table>
  </body>
</html></richcontent>
</node>
</node>
<node BACKGROUND_COLOR="#ffff66" CREATED="1378078311501" FOLDED="true" HGAP="29" ID="ID_1157431591" MODIFIED="1379967843399" POSITION="left" STYLE="bubble" TEXT="Constants" VSHIFT="1">
<cloud/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378082545167" FOLDED="true" ID="ID_1337336390" MODIFIED="1378273216050" STYLE="bubble" TEXT="ascii_letters">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node CREATED="1378082640049" ID="ID_1853699241" MODIFIED="1378082687429" STYLE="fork" TEXT="The concatenation of the ascii_lowercase and ascii_uppercase constants described below. This value is not locale-dependent.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378082552245" FOLDED="true" ID="ID_476350441" MODIFIED="1379967836096" STYLE="bubble" TEXT="ascii_lowercase">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node CREATED="1378082647144" ID="ID_1445371478" MODIFIED="1378082701273" STYLE="fork" TEXT="The lowercase letters &apos;abcdefghijklmnopqrstuvwxyz&apos;. This value is not locale-dependent and will not change.  ">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378082626139" FOLDED="true" ID="ID_1737391832" MODIFIED="1378273218154" STYLE="bubble" TEXT="ascii_uppercase">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node CREATED="1378082626141" ID="ID_921702231" MODIFIED="1378082701261" STYLE="fork" TEXT="The uppercase letters &apos;ABCDEFGHIJKLMNOPQRSTUVWXYZ&apos;. This value is not locale-dependent and will not change.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378082626143" FOLDED="true" ID="ID_1788395430" MODIFIED="1378273219306" STYLE="bubble" TEXT="digits">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node CREATED="1378082626144" ID="ID_578400844" MODIFIED="1378082701257" STYLE="fork" TEXT="The string &apos;0123456789&apos;.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378082626145" FOLDED="true" ID="ID_1338375408" MODIFIED="1378273220546" STYLE="bubble" TEXT="hexdigits">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node CREATED="1378082626145" ID="ID_1082754833" MODIFIED="1378082701256" STYLE="fork" TEXT="The string &apos;0123456789abcdefABCDEF&apos;.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378082626146" FOLDED="true" ID="ID_17548953" MODIFIED="1378273222947" STYLE="bubble" TEXT="letters">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node CREATED="1378082626147" ID="ID_714253626" MODIFIED="1378082701255" STYLE="fork" TEXT="The concatenation of the strings lowercase and uppercase described below. The specific value is locale-dependent, and will be updated when locale.setlocale() is called.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378082626149" FOLDED="true" ID="ID_700229908" MODIFIED="1379967840922" STYLE="bubble" TEXT="lowercase">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node CREATED="1378082626150" ID="ID_659258816" MODIFIED="1378082701249" STYLE="fork" TEXT="A string containing all the characters that are considered lowercase letters. On most systems this is the string &apos;abcdefghijklmnopqrstuvwxyz&apos;. The specific value is locale-dependent, and will be updated when locale.setlocale() is called.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378082626152" FOLDED="true" ID="ID_902860629" MODIFIED="1378273227726" STYLE="bubble" TEXT="octdigits">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node CREATED="1378082626152" ID="ID_1881129514" MODIFIED="1378082701245" STYLE="fork" TEXT="The string &apos;01234567&apos;.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378082626153" FOLDED="true" ID="ID_434084902" MODIFIED="1378273230227" STYLE="bubble" TEXT="punctuation">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node CREATED="1378082626153" ID="ID_72866777" MODIFIED="1378082701244" STYLE="fork" TEXT="String of ASCII characters which are considered punctuation characters in the C locale.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378082626154" FOLDED="true" ID="ID_1200015720" MODIFIED="1378273235631" STYLE="bubble" TEXT="printable">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node CREATED="1378082626154" ID="ID_1872773579" MODIFIED="1378082701243" STYLE="fork" TEXT="String of characters which are considered printable. This is a combination of digits, letters, punctuation, and whitespace.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378082626156" FOLDED="true" ID="ID_1660704152" MODIFIED="1378273237931" STYLE="bubble" TEXT="uppercase">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node CREATED="1378082626157" FOLDED="true" ID="ID_349550555" MODIFIED="1378082746926" STYLE="fork" TEXT="A string containing all the characters that are considered uppercase letters. On most systems this is the string &apos;ABCDEFGHIJKLMNOPQRSTUVWXYZ&apos;. The specific value is locale-dependent, and will be updated when locale.setlocale() is called.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node CREATED="1378082653616" ID="ID_871729356" MODIFIED="1378082701234" TEXT="foo"/>
</node>
</node>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378082626159" FOLDED="true" ID="ID_1456127377" MODIFIED="1378273240215" STYLE="bubble" TEXT="whitespace">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node CREATED="1378082772996" ID="ID_1621339127" MODIFIED="1378082780592" STYLE="fork" TEXT="A string containing all characters that are considered whitespace. On most systems this includes the characters space, tab, linefeed, return, formfeed, and vertical tab.">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
</node>
<node BACKGROUND_COLOR="#ffff66" CREATED="1378078311501" HGAP="29" ID="ID_749897728" MODIFIED="1378274648387" POSITION="left" STYLE="bubble" TEXT="Formatting" VSHIFT="1">
<cloud/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#a2eb8d" CREATED="1378079008619" ID="ID_656420503" MODIFIED="1384582349724" STYLE="bubble" TEXT="examples">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<font BOLD="true" NAME="Helvetica" SIZE="12"/>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378083165671" ID="ID_879306814" MODIFIED="1384974379074" STYLE="bubble" TEXT="Accessing arguments by position">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node CREATED="1378084166988" ID="ID_1448466392" MODIFIED="1378084182983">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <pre style="font-variant: normal; border-top-width: 1px; line-height: 15.555556297302246px; color: rgb(51, 51, 51); padding-top: 5px; font-style: normal; text-align: start; padding-bottom: 5px; padding-right: 5px; border-bottom-width: 1px; text-indent: 0px; background-color: rgb(238, 255, 204); padding-left: 5px; text-transform: none; font-weight: normal; word-spacing: 0px; letter-spacing: normal" charset="utf-8"><font color="rgb(198, 93, 9)"><span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><b>&gt;&gt;&gt; </b></span></font><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'{0}, {1}, {2}'</span></font><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">.</span></font><span class="n">format</span><span class="p">(</span><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'a'</span></font><span class="p">,</span> <font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'b'</span></font><span class="p">,</span> <font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'c'</span></font><span class="p">)</span>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">'a, b, c'</span></font>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'{}, {}, {}'</span></font><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">.</span></font><span class="n">format</span><span class="p">(</span><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'a'</span></font><span class="p">,</span> <font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'b'</span></font><span class="p">,</span> <font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'c'</span></font><span class="p">)</span>  <i><font color="rgb(64, 128, 144)"><span style="color: rgb(64, 128, 144); font-style: italic" class="c"># 2.7+ only</span></font></i>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">'a, b, c'</span></font>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'{2}, {1}, {0}'</span></font><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">.</span></font><span class="n">format</span><span class="p">(</span><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'a'</span></font><span class="p">,</span> <font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'b'</span></font><span class="p">,</span> <font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'c'</span></font><span class="p">)</span>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">'c, b, a'</span></font>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'{2}, {1}, {0}'</span></font><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">.</span></font><span class="n">format</span><span class="p">(</span><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">*</span></font><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'abc'</span></font><span class="p">)</span>      <i><font color="rgb(64, 128, 144)"><span style="color: rgb(64, 128, 144); font-style: italic" class="c"># unpacking argument sequence</span></font></i>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">'c, b, a'</span></font>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'{0}{1}{0}'</span></font><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">.</span></font><span class="n">format</span><span class="p">(</span><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'abra'</span></font><span class="p">,</span> <font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'cad'</span></font><span class="p">)</span>   <i><font color="rgb(64, 128, 144)"><span style="color: rgb(64, 128, 144); font-style: italic" class="c"># arguments' indices can be repeated</span></font></i>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">'abracadabra'</span></font></pre>
  </body>
</html></richcontent>
</node>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378083165673" ID="ID_402149396" MODIFIED="1384974379112" STYLE="bubble" TEXT="Accessing arguments by name">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node CREATED="1378084219397" ID="ID_955386783" MODIFIED="1378084240374">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <pre style="font-variant: normal; border-top-width: 1px; line-height: 15.555556297302246px; color: rgb(51, 51, 51); padding-top: 5px; font-style: normal; text-align: start; padding-bottom: 5px; padding-right: 5px; border-bottom-width: 1px; text-indent: 0px; background-color: rgb(238, 255, 204); padding-left: 5px; text-transform: none; font-weight: normal; word-spacing: 0px; letter-spacing: normal" charset="utf-8"><font color="rgb(198, 93, 9)"><span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><b>&gt;&gt;&gt; </b></span></font><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'Coordinates: {latitude}, {longitude}'</span></font><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">.</span></font><span class="n">format</span><span class="p">(</span><span class="n">latitude</span><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">=</span></font><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'37.24N'</span></font><span class="p">,</span> <span class="n">longitude</span><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">=</span></font><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'-115.81W'</span></font><span class="p">)</span>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">'Coordinates: 37.24N, -115.81W'</span></font>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><span class="n">coord</span> <font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">=</span></font> <span class="p">{</span><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'latitude'</span></font><span class="p">:</span> <font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'37.24N'</span></font><span class="p">,</span> <font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'longitude'</span></font><span class="p">:</span> <font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'-115.81W'</span></font><span class="p">}</span>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'Coordinates: {latitude}, {longitude}'</span></font><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">.</span></font><span class="n">format</span><span class="p">(</span><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">**</span></font><span class="n">coord</span><span class="p">)</span>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">'Coordinates: 37.24N, -115.81W'</span></font></pre>
  </body>
</html></richcontent>
</node>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378084270415" ID="ID_933831799" MODIFIED="1384974379178" STYLE="bubble" TEXT="Accessing arguments&#x2019; attributes">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node CREATED="1378084219397" ID="ID_1312404536" MODIFIED="1378084299765">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <pre style="font-variant: normal; border-top-width: 1px; line-height: 15.555556297302246px; color: rgb(51, 51, 51); padding-top: 5px; font-style: normal; text-align: start; padding-bottom: 5px; padding-right: 5px; border-bottom-width: 1px; text-indent: 0px; background-color: rgb(238, 255, 204); padding-left: 5px; text-transform: none; font-weight: normal; word-spacing: 0px; letter-spacing: normal" charset="utf-8"><font color="rgb(198, 93, 9)"><span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><b>&gt;&gt;&gt; </b></span></font><span class="n">c</span> <font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">=</span></font> <font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mi">3</span></font><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">-</span></font><font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mi">5</span></font><span class="n">j</span>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><span class="p">(</span><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'The complex number {0} is formed from the real part {0.real} '</span></font>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>... </b></font></span> <font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'and the imaginary part {0.imag}.'</span></font><span class="p">)</span><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">.</span></font><span class="n">format</span><span class="p">(</span><span class="n">c</span><span class="p">)</span>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">'The complex number (3-5j) is formed from the real part 3.0 and the imaginary part -5.0.'</span></font>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><b><font color="rgb(0, 112, 32)"><span style="color: rgb(0, 112, 32); font-weight: bold" class="k">class</span></font></b> <span style="color: rgb(14, 132, 181); font-weight: bold" class="nc"><font color="rgb(14, 132, 181)"><b>Point</b></font></span><span class="p">(</span><font color="rgb(0, 112, 32)"><span style="color: rgb(0, 112, 32)" class="nb">object</span></font><span class="p">):</span>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>... </b></font></span>    <span style="color: rgb(0, 112, 32); font-weight: bold" class="k"><font color="rgb(0, 112, 32)"><b>def</b></font></span> <font color="rgb(6, 40, 126)"><span style="color: rgb(6, 40, 126)" class="nf">__init__</span></font><span class="p">(</span><font color="rgb(0, 112, 32)"><span style="color: rgb(0, 112, 32)" class="bp">self</span></font><span class="p">,</span> <span class="n">x</span><span class="p">,</span> <span class="n">y</span><span class="p">):</span>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>... </b></font></span>        <font color="rgb(0, 112, 32)"><span style="color: rgb(0, 112, 32)" class="bp">self</span></font><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">.</span></font><span class="n">x</span><span class="p">,</span> <font color="rgb(0, 112, 32)"><span style="color: rgb(0, 112, 32)" class="bp">self</span></font><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">.</span></font><span class="n">y</span> <font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">=</span></font> <span class="n">x</span><span class="p">,</span> <span class="n">y</span>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>... </b></font></span>    <span style="color: rgb(0, 112, 32); font-weight: bold" class="k"><font color="rgb(0, 112, 32)"><b>def</b></font></span> <font color="rgb(6, 40, 126)"><span style="color: rgb(6, 40, 126)" class="nf">__str__</span></font><span class="p">(</span><font color="rgb(0, 112, 32)"><span style="color: rgb(0, 112, 32)" class="bp">self</span></font><span class="p">):</span>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>... </b></font></span>        <span style="color: rgb(0, 112, 32); font-weight: bold" class="k"><font color="rgb(0, 112, 32)"><b>return</b></font></span> <font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'Point({self.x}, {self.y})'</span></font><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">.</span></font><span class="n">format</span><span class="p">(</span><font color="rgb(0, 112, 32)"><span style="color: rgb(0, 112, 32)" class="bp">self</span></font><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">=</span></font><font color="rgb(0, 112, 32)"><span style="color: rgb(0, 112, 32)" class="bp">self</span></font><span class="p">)</span>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>...</b></font></span>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><font color="rgb(0, 112, 32)"><span style="color: rgb(0, 112, 32)" class="nb">str</span></font><span class="p">(</span><span class="n">Point</span><span class="p">(</span><font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mi">4</span></font><span class="p">,</span> <font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mi">2</span></font><span class="p">))</span>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">'Point(4, 2)'</span></font></pre>
  </body>
</html></richcontent>
</node>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378083165674" ID="ID_635478377" MODIFIED="1384974379187" STYLE="bubble" TEXT="Accessing arguments&#x2019; items">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node CREATED="1378084219397" ID="ID_1882462396" MODIFIED="1378084315693">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <pre style="font-variant: normal; border-top-width: 1px; line-height: 15.555556297302246px; color: rgb(51, 51, 51); padding-top: 5px; font-style: normal; text-align: start; padding-bottom: 5px; padding-right: 5px; border-bottom-width: 1px; text-indent: 0px; background-color: rgb(238, 255, 204); padding-left: 5px; text-transform: none; font-weight: normal; word-spacing: 0px; letter-spacing: normal" charset="utf-8"><font color="rgb(198, 93, 9)"><span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><b>&gt;&gt;&gt; </b></span></font><span class="n">coord</span> <font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">=</span></font> <span class="p">(</span><font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mi">3</span></font><span class="p">,</span> <font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mi">5</span></font><span class="p">)</span>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'X: {0[0]};  Y: {0[1]}'</span></font><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">.</span></font><span class="n">format</span><span class="p">(</span><span class="n">coord</span><span class="p">)</span>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">'X: 3;  Y: 5'</span></font></pre>
  </body>
</html></richcontent>
</node>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378083165675" ID="ID_453790981" MODIFIED="1384974379194" STYLE="bubble" TEXT="Replacing %s and %r">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node CREATED="1378084219397" ID="ID_731837522" MODIFIED="1378084349324">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <pre style="font-variant: normal; border-top-width: 1px; line-height: 15.555556297302246px; color: rgb(51, 51, 51); padding-top: 5px; font-style: normal; text-align: start; padding-bottom: 5px; padding-right: 5px; border-bottom-width: 1px; text-indent: 0px; background-color: rgb(238, 255, 204); padding-left: 5px; text-transform: none; font-weight: normal; word-spacing: 0px; letter-spacing: normal" charset="utf-8"><font color="rgb(198, 93, 9)"><span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><b>&gt;&gt;&gt; </b></span></font><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">"repr() shows quotes: {!r}; str() doesn't: {!s}"</span></font><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">.</span></font><span class="n">format</span><span class="p">(</span><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'test1'</span></font><span class="p">,</span> <font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'test2'</span></font><span class="p">)</span>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">"repr() shows quotes: 'test1'; str() doesn't: test2"</span></font></pre>
  </body>
</html></richcontent>
</node>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378083165675" ID="ID_834615763" MODIFIED="1384974379210" STYLE="bubble" TEXT="Aligning the text and specifying a width">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node CREATED="1378084219397" ID="ID_627713647" MODIFIED="1378084388518">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <pre style="font-variant: normal; border-top-width: 1px; line-height: 15.555556297302246px; color: rgb(51, 51, 51); padding-top: 5px; font-style: normal; text-align: start; padding-bottom: 5px; padding-right: 5px; border-bottom-width: 1px; text-indent: 0px; background-color: rgb(238, 255, 204); padding-left: 5px; text-transform: none; font-weight: normal; word-spacing: 0px; letter-spacing: normal" charset="utf-8"><font color="rgb(198, 93, 9)"><span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><b>&gt;&gt;&gt; </b></span></font><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'{:&lt;30}'</span></font><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">.</span></font><span class="n">format</span><span class="p">(</span><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'left aligned'</span></font><span class="p">)</span>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">'left aligned                  '</span></font>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'{:&gt;30}'</span></font><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">.</span></font><span class="n">format</span><span class="p">(</span><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'right aligned'</span></font><span class="p">)</span>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">'                 right aligned'</span></font>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'{:^30}'</span></font><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">.</span></font><span class="n">format</span><span class="p">(</span><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'centered'</span></font><span class="p">)</span>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">'           centered           '</span></font>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'{:*^30}'</span></font><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">.</span></font><span class="n">format</span><span class="p">(</span><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'centered'</span></font><span class="p">)</span>  <i><font color="rgb(64, 128, 144)"><span style="color: rgb(64, 128, 144); font-style: italic" class="c"># use '*' as a fill char</span></font></i>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">'***********centered***********'</span></font></pre>
  </body>
</html></richcontent>
</node>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378083165676" ID="ID_343889035" MODIFIED="1384974379241" STYLE="bubble" TEXT="Replacing %+f, %-f, and % f and specifying a sign">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node CREATED="1378084219397" ID="ID_475281605" MODIFIED="1378084399756">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <pre style="font-variant: normal; border-top-width: 1px; line-height: 15.555556297302246px; color: rgb(51, 51, 51); padding-top: 5px; font-style: normal; text-align: start; padding-bottom: 5px; padding-right: 5px; border-bottom-width: 1px; text-indent: 0px; background-color: rgb(238, 255, 204); padding-left: 5px; text-transform: none; font-weight: normal; word-spacing: 0px; letter-spacing: normal" charset="utf-8"><font color="rgb(198, 93, 9)"><span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><b>&gt;&gt;&gt; </b></span></font><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'{:+f}; {:+f}'</span></font><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">.</span></font><span class="n">format</span><span class="p">(</span><font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mf">3.14</span></font><span class="p">,</span> <font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">-</span></font><font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mf">3.14</span></font><span class="p">)</span>  <i><font color="rgb(64, 128, 144)"><span style="color: rgb(64, 128, 144); font-style: italic" class="c"># show it always</span></font></i>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">'+3.140000; -3.140000'</span></font>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'{: f}; {: f}'</span></font><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">.</span></font><span class="n">format</span><span class="p">(</span><font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mf">3.14</span></font><span class="p">,</span> <font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">-</span></font><font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mf">3.14</span></font><span class="p">)</span>  <i><font color="rgb(64, 128, 144)"><span style="color: rgb(64, 128, 144); font-style: italic" class="c"># show a space for positive numbers</span></font></i>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">' 3.140000; -3.140000'</span></font>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'{:-f}; {:-f}'</span></font><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">.</span></font><span class="n">format</span><span class="p">(</span><font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mf">3.14</span></font><span class="p">,</span> <font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">-</span></font><font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mf">3.14</span></font><span class="p">)</span>  <i><font color="rgb(64, 128, 144)"><span style="color: rgb(64, 128, 144); font-style: italic" class="c"># show only the minus -- same as '{:f}; {:f}'</span></font></i>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">'3.140000; -3.140000'</span></font></pre>
  </body>
</html></richcontent>
</node>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378083165676" ID="ID_697647064" MODIFIED="1384974379254" STYLE="bubble" TEXT="Replacing %x and %o and converting the value to different bases">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node CREATED="1378084219397" ID="ID_317953452" MODIFIED="1378084407669">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <pre style="font-variant: normal; border-top-width: 1px; line-height: 15.555556297302246px; color: rgb(51, 51, 51); padding-top: 5px; font-style: normal; text-align: start; padding-bottom: 5px; padding-right: 5px; border-bottom-width: 1px; text-indent: 0px; background-color: rgb(238, 255, 204); padding-left: 5px; text-transform: none; font-weight: normal; word-spacing: 0px; letter-spacing: normal" charset="utf-8"><font color="rgb(198, 93, 9)"><span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><b>&gt;&gt;&gt; </b></span></font><i><span style="color: rgb(64, 128, 144); font-style: italic" class="c"><font color="rgb(64, 128, 144)"># format also supports binary numbers</font></span></i>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">"int: {0:d};  hex: {0:x};  oct: {0:o};  bin: {0:b}"</span></font><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">.</span></font><span class="n">format</span><span class="p">(</span><font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mi">42</span></font><span class="p">)</span>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">'int: 42;  hex: 2a;  oct: 52;  bin: 101010'</span></font>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><i><font color="rgb(64, 128, 144)"><span style="color: rgb(64, 128, 144); font-style: italic" class="c"># with 0x, 0o, or 0b as prefix:</span></font></i>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">"int: {0:d};  hex: {0:#x};  oct: {0:#o};  bin: {0:#b}"</span></font><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">.</span></font><span class="n">format</span><span class="p">(</span><font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mi">42</span></font><span class="p">)</span>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">'int: 42;  hex: 0x2a;  oct: 0o52;  bin: 0b101010'</span></font></pre>
  </body>
</html></richcontent>
</node>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378083165677" ID="ID_547095219" MODIFIED="1384974379263" STYLE="bubble" TEXT="Using the comma as a thousands separator">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node CREATED="1378084219397" ID="ID_1829360258" MODIFIED="1378084421015">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <pre style="font-variant: normal; border-top-width: 1px; line-height: 15.555556297302246px; color: rgb(51, 51, 51); padding-top: 5px; font-style: normal; text-align: start; padding-bottom: 5px; padding-right: 5px; border-bottom-width: 1px; text-indent: 0px; background-color: rgb(238, 255, 204); padding-left: 5px; text-transform: none; font-weight: normal; word-spacing: 0px; letter-spacing: normal" charset="utf-8"><font color="rgb(198, 93, 9)"><span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><b>&gt;&gt;&gt; </b></span></font><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'{:,}'</span></font><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">.</span></font><span class="n">format</span><span class="p">(</span><font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mi">1234567890</span></font><span class="p">)</span>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">'1,234,567,890'</span></font></pre>
  </body>
</html></richcontent>
</node>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378083165678" ID="ID_1920161240" MODIFIED="1384974379276" STYLE="bubble" TEXT="Expressing a percentage">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node CREATED="1378084219397" ID="ID_1427012257" MODIFIED="1378084427634">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <pre style="font-variant: normal; border-top-width: 1px; line-height: 15.555556297302246px; color: rgb(51, 51, 51); padding-top: 5px; font-style: normal; text-align: start; padding-bottom: 5px; padding-right: 5px; border-bottom-width: 1px; text-indent: 0px; background-color: rgb(238, 255, 204); padding-left: 5px; text-transform: none; font-weight: normal; word-spacing: 0px; letter-spacing: normal" charset="utf-8"><font color="rgb(198, 93, 9)"><span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><b>&gt;&gt;&gt; </b></span></font><span class="n">points</span> <font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">=</span></font> <font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mf">19.5</span></font>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><span class="n">total</span> <font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">=</span></font> <font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mi">22</span></font>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'Correct answers: {:.2%}'</span></font><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">.</span></font><span class="n">format</span><span class="p">(</span><span class="n">points</span><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">/</span></font><span class="n">total</span><span class="p">)</span>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">'Correct answers: 88.64%'</span></font></pre>
  </body>
</html></richcontent>
</node>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378083165678" ID="ID_1155431202" MODIFIED="1384974379298" STYLE="bubble" TEXT="Using type-specific formatting">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node CREATED="1378084219397" ID="ID_1546999941" MODIFIED="1378084436175">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <pre style="font-variant: normal; border-top-width: 1px; line-height: 15.555556297302246px; color: rgb(51, 51, 51); padding-top: 5px; font-style: normal; text-align: start; padding-bottom: 5px; padding-right: 5px; border-bottom-width: 1px; text-indent: 0px; background-color: rgb(238, 255, 204); padding-left: 5px; text-transform: none; font-weight: normal; word-spacing: 0px; letter-spacing: normal" charset="utf-8"><font color="rgb(198, 93, 9)"><span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><b>&gt;&gt;&gt; </b></span></font><b><span style="color: rgb(0, 112, 32); font-weight: bold" class="kn"><font color="rgb(0, 112, 32)">import</font></span></b> <span style="color: rgb(14, 132, 181); font-weight: bold" class="nn"><font color="rgb(14, 132, 181)"><b>datetime</b></font></span>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><span class="n">d</span> <font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">=</span></font> <span class="n">datetime</span><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">.</span></font><span class="n">datetime</span><span class="p">(</span><font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mi">2010</span></font><span class="p">,</span> <font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mi">7</span></font><span class="p">,</span> <font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mi">4</span></font><span class="p">,</span> <font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mi">12</span></font><span class="p">,</span> <font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mi">15</span></font><span class="p">,</span> <font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mi">58</span></font><span class="p">)</span>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'{:%Y-%m-</span></font><i><font color="rgb(112, 160, 208)"><span style="color: rgb(112, 160, 208); font-style: italic" class="si">%d</span></font></i><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s"> %H:%M:%S}'</span></font><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">.</span></font><span class="n">format</span><span class="p">(</span><span class="n">d</span><span class="p">)</span>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">'2010-07-04 12:15:58'</span></font></pre>
  </body>
</html></richcontent>
</node>
</node>
<node BACKGROUND_COLOR="#99ebff" CREATED="1378083165679" ID="ID_988388471" MODIFIED="1384974379345" STYLE="bubble" TEXT="Nesting arguments and more complex examples">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
<node CREATED="1378084076162" ID="ID_1463495699" MODIFIED="1378084103506">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <pre style="font-variant: normal; border-top-width: 1px; line-height: 15.555556297302246px; color: rgb(51, 51, 51); padding-top: 5px; font-style: normal; text-align: start; padding-bottom: 5px; padding-right: 5px; border-bottom-width: 1px; text-indent: 0px; background-color: rgb(238, 255, 204); padding-left: 5px; text-transform: none; font-weight: normal; word-spacing: 0px; letter-spacing: normal" charset="utf-8"><font color="rgb(198, 93, 9)"><span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><b>&gt;&gt;&gt; </b></span></font><b><span style="color: rgb(0, 112, 32); font-weight: bold" class="k"><font color="rgb(0, 112, 32)">for</font></span></b> <span class="n">align</span><span class="p">,</span> <span class="n">text</span> <span style="color: rgb(0, 112, 32); font-weight: bold" class="ow"><font color="rgb(0, 112, 32)"><b>in</b></font></span> <font color="rgb(0, 112, 32)"><span style="color: rgb(0, 112, 32)" class="nb">zip</span></font><span class="p">(</span><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'&lt;^&gt;'</span></font><span class="p">,</span> <span class="p">[</span><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'left'</span></font><span class="p">,</span> <font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'center'</span></font><span class="p">,</span> <font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'right'</span></font><span class="p">]):</span>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>... </b></font></span>    <font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'{0:{fill}{align}16}'</span></font><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">.</span></font><span class="n">format</span><span class="p">(</span><span class="n">text</span><span class="p">,</span> <span class="n">fill</span><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">=</span></font><span class="n">align</span><span class="p">,</span> <span class="n">align</span><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">=</span></font><span class="n">align</span><span class="p">)</span>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>...</b></font></span>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">'left&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;'</span></font>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">'^^^^^center^^^^^'</span></font>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">'&gt;&gt;&gt;&gt;&gt;&gt;&gt;&gt;&gt;&gt;&gt;right'</span></font>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">&gt;&gt;&gt;</span></font>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><span class="n">octets</span> <font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">=</span></font> <span class="p">[</span><font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mi">192</span></font><span class="p">,</span> <font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mi">168</span></font><span class="p">,</span> <font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mi">0</span></font><span class="p">,</span> <font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mi">1</span></font><span class="p">]</span>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'{:02X}{:02X}{:02X}{:02X}'</span></font><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">.</span></font><span class="n">format</span><span class="p">(</span><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">*</span></font><span class="n">octets</span><span class="p">)</span>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">'C0A80001'</span></font>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><font color="rgb(0, 112, 32)"><span style="color: rgb(0, 112, 32)" class="nb">int</span></font><span class="p">(</span><span class="n">_</span><span class="p">,</span> <font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mi">16</span></font><span class="p">)</span>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">3232235521</span></font>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">&gt;&gt;&gt;</span></font>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><span class="n">width</span> <font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">=</span></font> <font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mi">5</span></font>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><b><font color="rgb(0, 112, 32)"><span style="color: rgb(0, 112, 32); font-weight: bold" class="k">for</span></font></b> <span class="n">num</span> <span style="color: rgb(0, 112, 32); font-weight: bold" class="ow"><font color="rgb(0, 112, 32)"><b>in</b></font></span> <font color="rgb(0, 112, 32)"><span style="color: rgb(0, 112, 32)" class="nb">range</span></font><span class="p">(</span><font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mi">5</span></font><span class="p">,</span><font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mi">12</span></font><span class="p">):</span>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>... </b></font></span>    <span style="color: rgb(0, 112, 32); font-weight: bold" class="k"><font color="rgb(0, 112, 32)"><b>for</b></font></span> <span class="n">base</span> <span style="color: rgb(0, 112, 32); font-weight: bold" class="ow"><font color="rgb(0, 112, 32)"><b>in</b></font></span> <font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'dXob'</span></font><span class="p">:</span>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>... </b></font></span>        <span style="color: rgb(0, 112, 32); font-weight: bold" class="k"><font color="rgb(0, 112, 32)"><b>print</b></font></span> <font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s">'{0:{width}{base}}'</span></font><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">.</span></font><span class="n">format</span><span class="p">(</span><span class="n">num</span><span class="p">,</span> <span class="n">base</span><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">=</span></font><span class="n">base</span><span class="p">,</span> <span class="n">width</span><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">=</span></font><span class="n">width</span><span class="p">),</span>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>... </b></font></span>    <span style="color: rgb(0, 112, 32); font-weight: bold" class="k"><font color="rgb(0, 112, 32)"><b>print</b></font></span>
<span style="color: rgb(198, 93, 9); font-weight: bold" class="gp"><font color="rgb(198, 93, 9)"><b>...</b></font></span>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">    5     5     5   101</span></font>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">    6     6     6   110</span></font>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">    7     7     7   111</span></font>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">    8     8    10  1000</span></font>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">    9     9    11  1001</span></font>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">   10     A    12  1010</span></font>
<font color="rgb(48, 48, 48)"><span style="color: rgb(48, 48, 48)" class="go">   11     B    13  1011</span></font></pre>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1378083212036" ID="ID_340018798" MODIFIED="1378271445126" POSITION="right" TEXT="html">
<node CREATED="1378084219397" ID="ID_451383089" MODIFIED="1378271445126">
<richcontent TYPE="NODE"><html>
  

  <head>

  </head>
  <body>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1378440422268" ID="ID_1373339606" MODIFIED="1378440423370" POSITION="right" TEXT="todo">
<node CREATED="1378440423523" ID="ID_473532480" MODIFIED="1378440428146" TEXT="sequences">
<node CREATED="1378440428380" ID="ID_428306428" MODIFIED="1378440430698" TEXT="reversed()"/>
</node>
<node CREATED="1378440432347" ID="ID_230499898" MODIFIED="1378440434170" TEXT="decorators"/>
</node>
</node>
</map>
