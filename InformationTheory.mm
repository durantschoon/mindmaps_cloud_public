<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1480482845439" ID="ID_1314379757" LINK="MetaMap.mm" MODIFIED="1480482874269" TEXT="InformationTheory.mm">
<node CREATED="1511201757583" HGAP="19" ID="ID_948424751" LINK="http://colah.github.io/posts/2015-09-Visual-Information/" MODIFIED="1511201773718" POSITION="right" TEXT="colah.github.io &gt; Posts &gt; 2015-09-Visual-Information" VSHIFT="-32"/>
<node CREATED="1480482897751" ID="ID_192536773" LINK="https://en.wikipedia.org/wiki/Information_theory" MODIFIED="1511201764495" POSITION="right" STYLE="fork" TEXT="https://en.wikipedia.org/wiki/Information_theory">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
<node CREATED="1480482910447" ID="ID_1572870776" LINK="https://en.wikipedia.org/wiki/Information_geometry" MODIFIED="1511201765310" POSITION="right" STYLE="fork" TEXT="https://en.wikipedia.org/wiki/Information_geometry">
<edge COLOR="#808080" STYLE="bezier" WIDTH="thin"/>
</node>
</node>
</map>
