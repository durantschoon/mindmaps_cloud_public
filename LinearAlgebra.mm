<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1375595178101" ID="ID_693463061" LINK="../../Documents/mindmaps/MetaMap.mm" MODIFIED="1375595196860" TEXT="LinearAlgebra">
<node CREATED="1375595201030" ID="ID_1509731783" MODIFIED="1375595204518" POSITION="right" TEXT="MIT OCW">
<node CREATED="1375595204519" ID="ID_737884533" MODIFIED="1375595207666" TEXT="Video Lectures">
<node CREATED="1375595207846" ID="ID_1273988531" MODIFIED="1375595208882" TEXT="Strang">
<node CREATED="1375595214513" ID="ID_1938522198" LINK="http://ocw.mit.edu/courses/mathematics/18-06-linear-algebra-spring-2010/video-lectures/" MODIFIED="1375595214513" TEXT="ocw.mit.edu &gt; Courses &gt; Mathematics &gt; 18-06-linear-algebra-spring-2010 &gt; Video-lectures"/>
<node CREATED="1375595249767" ID="ID_682450417" MODIFIED="1375595252467" TEXT="34 lectures">
<node CREATED="1375595343962" ID="ID_1194875986" MODIFIED="1375595343962">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <a class="bullet medialink" href="http://ocw.mit.edu/courses/mathematics/18-06-linear-algebra-spring-2010/video-lectures/lecture-33-left-and-right-inverses-pseudoinverse">Lecture 33: Left and right inverses; pseudoinverse</a>
  </body>
</html></richcontent>
<node CREATED="1375595345713" ID="ID_1589615327" MODIFIED="1375595351057" TEXT="The diagram!">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1475815972508" ID="ID_73211510" MODIFIED="1475815975309" TEXT=" fundamental subspaces ">
<node CREATED="1475815949030" ID="ID_900426335" LINK="https://en.wikipedia.org/wiki/Fundamental_theorem_of_linear_algebra" MODIFIED="1475815949030" TEXT="https://en.wikipedia.org/wiki/Fundamental_theorem_of_linear_algebra"/>
</node>
<node CREATED="1475816061781" LINK="http://home.engineering.iastate.edu/~julied/classes/CE570/Notes/strangpaper.pdf" MODIFIED="1475816061781" TEXT="home.engineering.iastate.edu &gt; Julied &gt; Classes &gt; CE570 &gt; Notes &gt; Strangpaper"/>
</node>
</node>
</node>
<node CREATED="1376174091927" ID="ID_1259738685" MODIFIED="1376174093371" POSITION="left" TEXT="play">
<node CREATED="1376173961471" ID="ID_1842298033" MODIFIED="1511374219538" TEXT="Tree of primitive Pythagorean triples">
<node CREATED="1376173952154" ID="ID_991415428" LINK="http://en.wikipedia.org/wiki/Tree_of_primitive_Pythagorean_triples" MODIFIED="1376173967029" TEXT="en.wikipedia.org &gt; Wiki &gt; Tree of primitive Pythagorean triples"/>
<node CREATED="1376174198799" ID="ID_1331376595" LINK="http://en.wikipedia.org/wiki/Formulas_for_generating_Pythagorean_triples#Pythagorean_triples_by_use_of_matrices_and_linear_transformations" MODIFIED="1376174198799" TEXT="en.wikipedia.org &gt; Wiki &gt; Formulas for generating Pythagorean triples#Pythagorean triples by use of matrices and linear transformations"/>
</node>
</node>
<node CREATED="1444798343744" ID="ID_41169758" MODIFIED="1466359393005" POSITION="right" TEXT="Grassmann Algebras&#xa;&quot;Geometric Algebras&quot;">
<node CREATED="1468289827408" ID="ID_987125797" MODIFIED="1468289831320" TEXT="Start Here">
<node CREATED="1468289840041" LINK="http://geocalc.clas.asu.edu/pdf/OerstedMedalLecture.pdf" MODIFIED="1468289840041" TEXT="geocalc.clas.asu.edu &gt; Pdf &gt; OerstedMedalLecture"/>
<node CREATED="1468289909938" ID="ID_670477098" MODIFIED="1468289911283" TEXT="See">
<node CREATED="1468289912002" ID="ID_241062466" MODIFIED="1468289923360" TEXT="/Users/durantschoon/DocumentsOthers/grassman_geometric_algebra"/>
</node>
<node CREATED="1492025461282" ID="ID_18328837" MODIFIED="1492025463813" TEXT="book">
<node CREATED="1492025467371" ID="ID_1081089210" MODIFIED="1492025468224" TEXT="David Hestenes&#x2019; New Foundations for Classical Mechanics"/>
<node CREATED="1492025549555" ID="ID_321982953" MODIFIED="1492025557848" TEXT="eBook">
<node CREATED="1492025557849" ID="ID_1076989759" MODIFIED="1492025560483" TEXT="$85"/>
<node CREATED="1492025552152" ID="ID_1205163657" LINK="http://www.springer.com/us/book/9780792353027" MODIFIED="1492025552152" TEXT="springer.com &gt; Us &gt; Book &gt; 9780792353027"/>
</node>
</node>
</node>
<node CREATED="1475438983422" ID="ID_995024347" MODIFIED="1475439022307" TEXT="Python">
<node CREATED="1475438987505" ID="ID_998840930" LINK="https://github.com/brombo/galgebra" MODIFIED="1475438987505" TEXT="https://github.com/brombo/galgebra">
<node CREATED="1475439010155" FOLDED="true" ID="ID_1188018877" MODIFIED="1475439018275" TEXT="part of sympy">
<node CREATED="1475439015456" LINK="http://www.sympy.org/en/index.html" MODIFIED="1475439015456" TEXT="sympy.org &gt; En &gt; Index"/>
</node>
</node>
</node>
<node CREATED="1444798349195" ID="ID_743954302" MODIFIED="1444798381512" TEXT="exterior product, &quot;wedge&quot; product">
<node CREATED="1444798393081" ID="ID_641748828" LINK="http://math.stackexchange.com/questions/21614/is-there-a-definition-of-determinants-that-does-not-rely-on-how-they-are-calcula" MODIFIED="1444798393081" TEXT="math.stackexchange.com &gt; Questions &gt; 21614 &gt; Is-there-a-definition-of-determinants-that-does-not-rely-on-how-they-are-calcula"/>
</node>
<node CREATED="1494014297202" ID="ID_1869921817" MODIFIED="1494014315172" TEXT="visual explanation of unit bivectors">
<node CREATED="1494014316957" LINK="https://www.youtube.com/watch?v=Bb03hIYJbd8" MODIFIED="1494014316957" TEXT="https://www.youtube.com/watch?v=Bb03hIYJbd8"/>
<node CREATED="1494014434925" ID="ID_1920562135" MODIFIED="1494014438076" TEXT="part of">
<node CREATED="1494014436882" LINK="https://www.youtube.com/watch?v=2cqDVtHcCoE" MODIFIED="1494014436882" TEXT="https://www.youtube.com/watch?v=2cqDVtHcCoE"/>
</node>
<node CREATED="1494014406459" ID="ID_1073937264" MODIFIED="1494014409094" TEXT="outer product"/>
</node>
<node CREATED="1471760549670" ID="ID_1790888910" LINK="http://geometry.mrao.cam.ac.uk/" MODIFIED="1471760553103" TEXT="geometry.mrao.cam.ac.uk">
<node CREATED="1471760728138" ID="ID_299226902" MODIFIED="1471760729911" TEXT="lectures">
<node CREATED="1471760876163" ID="ID_1905547578" LINK="http://geometry.mrao.cam.ac.uk/2015/10/geometric-algebra-2015/" MODIFIED="1471760876163" TEXT="geometry.mrao.cam.ac.uk &gt; 2015 &gt; 10 &gt; Geometric-algebra-2015"/>
<node CREATED="1471760731463" ID="ID_588209891" LINK="http://geometry.mrao.cam.ac.uk/2015/10/ga2015-lecture-1/" MODIFIED="1471760878089" TEXT="geometry.mrao.cam.ac.uk &gt; 2015 &gt; 10 &gt; Ga2015-lecture-1"/>
</node>
</node>
<node CREATED="1471762745413" ID="ID_1019649420" MODIFIED="1492026808481" TEXT="books">
<node CREATED="1492026809394" ID="ID_1781488645" MODIFIED="1492190555642" TEXT="Hestenes, David">
<node CREATED="1492026545691" FOLDED="true" ID="ID_688421742" MODIFIED="1492026572360" TEXT="New Foundations for Classical Mechanics">
<node CREATED="1471762746911" ID="ID_188373018" LINK="http://geometry.mrao.cam.ac.uk/2007/01/geometric-algebra-for-physicists/" MODIFIED="1492026547660" TEXT="geometry.mrao.cam.ac.uk &gt; 2007 &gt; 01 &gt; Geometric-algebra-for-physicists"/>
<node CREATED="1492026560179" ID="ID_582831450" MODIFIED="1492026562365" TEXT="eBook">
<node CREATED="1492026562776" ID="ID_571652855" MODIFIED="1492026564330" TEXT="$109"/>
</node>
</node>
<node CREATED="1492026525627" FOLDED="true" ID="ID_1206592123" MODIFIED="1492026571685" TEXT="Clifford Algebra to Geometric Calculus">
<node CREATED="1492026534978" ID="ID_515344807" LINK="http://www.springer.com/us/book/9789027716736" MODIFIED="1492026534978" TEXT="springer.com &gt; Us &gt; Book &gt; 9789027716736"/>
<node CREATED="1492026560179" ID="ID_1396349231" MODIFIED="1492026562365" TEXT="eBook">
<node CREATED="1492026562776" ID="ID_1683792772" MODIFIED="1492026564330" TEXT="$109"/>
</node>
</node>
</node>
<node CREATED="1492026823201" ID="ID_1336035156" MODIFIED="1492190551082" TEXT="Perwass, Christian">
<node CREATED="1492026829755" ID="ID_1536640583" MODIFIED="1492026830451" TEXT="Geometric Algebra with Applications in Engineering">
<node CREATED="1492026836122" ID="ID_489617880" MODIFIED="1492026837967" TEXT="eBook">
<node CREATED="1492026837968" ID="ID_1500147108" MODIFIED="1492026840513" TEXT="$69"/>
</node>
<node CREATED="1492026842181" LINK="http://www.springer.com/us/book/9783540890676" MODIFIED="1492026842181" TEXT="springer.com &gt; Us &gt; Book &gt; 9783540890676"/>
<node CREATED="1492026857997" ID="ID_129269695" MODIFIED="1492026859188" TEXT="use with">
<node CREATED="1492026901592" FOLDED="true" ID="ID_43143538" MODIFIED="1492026906007" TEXT="CluViz">
<node CREATED="1492026861013" ID="ID_1075136378" LINK="http://www.clucalc.info/" MODIFIED="1492026861013" TEXT="clucalc.info">
<node CREATED="1492026894784" ID="ID_509357893" MODIFIED="1492026896133" TEXT="windows"/>
<node CREATED="1492026896365" ID="ID_293601563" MODIFIED="1492026897378" TEXT="linux"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1492190607805" ID="ID_1129334970" LINK="http://grassmannalgebra.com/" MODIFIED="1492190607805" TEXT="grassmannalgebra.com">
<node CREATED="1492190811340" ID="ID_1861439873" MODIFIED="1492190818370" TEXT="with mathematica package"/>
<node CREATED="1492190823550" ID="ID_1692447975" MODIFIED="1492190826570" TEXT="October 2012"/>
</node>
</node>
<node CREATED="1461033226906" ID="ID_876819889" MODIFIED="1478673503911" TEXT="Clifford Algebras">
<node CREATED="1461033233935" ID="ID_483018847" MODIFIED="1461033240348" TEXT="C++ libraries">
<node CREATED="1461033244846" ID="ID_1836442206" LINK="http://versor.mat.ucsb.edu/" MODIFIED="1461033244846" TEXT="versor.mat.ucsb.edu"/>
<node CREATED="1464655443145" ID="ID_1623296072" MODIFIED="1474833827597" TEXT="Masters Thesis UCSB">
<node CREATED="1464655451963" ID="ID_952647426" MODIFIED="1474833827598" TEXT="VERSOR&#xa;Spatial Computing with Conformal Geometric&#xa;Algebra">
<node CREATED="1464655464499" LINK="http://wolftype.com/versor/colapinto_masters_final_02.pdf" MODIFIED="1464655464499" TEXT="wolftype.com &gt; Versor &gt; Colapinto masters final 02"/>
</node>
</node>
</node>
<node CREATED="1469589269273" ID="ID_734660816" MODIFIED="1469589275140" TEXT="Physics">
<node CREATED="1469589275143" ID="ID_436601834" MODIFIED="1469589277816" TEXT="Adinkras">
<node CREATED="1469589279180" LINK="https://www.onbeing.org/sites/onbeing.org/files/gates-symbolsofpower.pdf" MODIFIED="1469589279180" TEXT="https://www.onbeing.org/sites/onbeing.org/files/gates-symbolsofpower.pdf"/>
<node CREATED="1469589343945" ID="ID_1060948322" MODIFIED="1469589344508" TEXT="James Gates"/>
</node>
</node>
<node CREATED="1477459001315" ID="ID_886572707" MODIFIED="1477459020756" TEXT="clifford fourier transform">
<node CREATED="1477459029588" ID="ID_750126265" LINK="https://arxiv.org/abs/1003.0689" MODIFIED="1477459031924" TEXT="https://arxiv.org/abs/1003.0689"/>
<node CREATED="1477459003629" ID="ID_1512383898" LINK="https://en.wikipedia.org/wiki/Clifford_analysis" MODIFIED="1477459003629" TEXT="https://en.wikipedia.org/wiki/Clifford_analysis"/>
</node>
</node>
<node CREATED="1478673498857" ID="ID_163742067" MODIFIED="1478673526856" TEXT="Multilinear algebra">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1478673502981" ID="ID_1865629648" LINK="https://en.wikipedia.org/wiki/Multilinear_algebra" MODIFIED="1478673502981" TEXT="https://en.wikipedia.org/wiki/Multilinear_algebra">
<node CREATED="1478673541258" ID="ID_581279386" MODIFIED="1478673543511" TEXT="In a vector space of dimension n, one usually considers only the vectors. According to Hermann Grassmann and others, this presumption misses the complexity of considering the structures of pairs, triples,[clarification needed] and general multivectors. "/>
<node CREATED="1478673560639" ID="ID_1477114445" MODIFIED="1478673561814" TEXT="A major advance in multilinear algebra came in the work of Gregorio Ricci-Curbastro and Tullio Levi-Civita"/>
<node CREATED="1478673641038" LINK="https://books.google.com/books/about/Algebra_I.html?id=STS9aZ6F204C" MODIFIED="1478673641038" TEXT="https://books.google.com/books/about/Algebra_I.html?id=STS9aZ6F204C"/>
</node>
<node CREATED="1478673744256" LINK="https://en.wikipedia.org/wiki/Multivector" MODIFIED="1478673744256" TEXT="https://en.wikipedia.org/wiki/Multivector"/>
<node CREATED="1478673780664" LINK="https://en.wikipedia.org/wiki/Determinant#Abstract_formulation" MODIFIED="1478673780664" TEXT="https://en.wikipedia.org/wiki/Determinant#Abstract_formulation"/>
</node>
<node CREATED="1466360103531" ID="ID_265136202" MODIFIED="1466360107310" TEXT="youtube.com">
<node CREATED="1466360107311" ID="ID_1652112705" MODIFIED="1466360115427" TEXT="watch list">
<node CREATED="1466360115784" ID="ID_262241459" MODIFIED="1466360117635" TEXT="durant.schoon"/>
</node>
<node CREATED="1466363265264" ID="ID_797142432" MODIFIED="1466363269520" TEXT="playlists">
<node CREATED="1466363307131" ID="ID_1974032279" MODIFIED="1466363309960" TEXT="Exterior Algebra aka Grassmann Algebra and the Wedge Product">
<node CREATED="1466363276191" ID="ID_649038134" LINK="https://www.youtube.com/playlist?list=PL6oNjS6Kc-nQmqvWjRzLYLk1WlMdFudJa" MODIFIED="1466363276191" TEXT="https://www.youtube.com/playlist?list=PL6oNjS6Kc-nQmqvWjRzLYLk1WlMdFudJa"/>
</node>
<node CREATED="1466363288112" ID="ID_1274654917" MODIFIED="1466363289792" TEXT="Exterior Algebra Covariant Derivative">
<node CREATED="1466363295228" LINK="https://www.youtube.com/playlist?list=PLFA19188032C5F11C" MODIFIED="1466363295228" TEXT="https://www.youtube.com/playlist?list=PLFA19188032C5F11C"/>
</node>
</node>
</node>
<node CREATED="1479083043666" ID="ID_1267540114" MODIFIED="1479083059940" TEXT="SymPy">
<node CREATED="1479083038365" ID="ID_575647113" LINK="http://docs.sympy.org/latest/modules/diffgeom.html?highlight=wedge" MODIFIED="1479083038365" TEXT="docs.sympy.org &gt; Latest &gt; Modules &gt; Diffgeom ? ..."/>
</node>
</node>
<node CREATED="1478675828781" ID="ID_1688802354" MODIFIED="1478675835760" POSITION="right" TEXT="covariance matrix example">
<node CREATED="1478675821385" ID="ID_522112548" LINK="http://stattrek.com/matrix-algebra/covariance-matrix.aspx" MODIFIED="1478675821385" TEXT="stattrek.com &gt; Matrix-algebra &gt; Covariance-matrix"/>
</node>
</node>
</map>
