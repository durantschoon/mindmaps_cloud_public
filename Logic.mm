<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1437948941617" ID="ID_1186727913" LINK="../../Documents/mindmaps/MetaMap.mm" MODIFIED="1437948997489" TEXT="Logic">
<node CREATED="1437949000112" ID="ID_597172839" MODIFIED="1438066919219" POSITION="right" TEXT="from Wikipedia">
<node CREATED="1437950938839" ID="ID_396336252" MODIFIED="1437951161258" TEXT=" Sentential logic">
<node CREATED="1437950946692" ID="ID_1030205562" MODIFIED="1437950947660" TEXT="Propositional calculus">
<node CREATED="1437950951463" LINK="https://en.wikipedia.org/wiki/Propositional_calculus" MODIFIED="1437950951463" TEXT="https://en.wikipedia.org/wiki/Propositional_calculus"/>
<node CREATED="1437950974483" ID="ID_201827404" MODIFIED="1437951122258" TEXT="mathematical logic concerned with the study of propositions (whether they are true or false))"/>
<node CREATED="1437950996140" ID="ID_455479584" MODIFIED="1437950997359" TEXT="that are formed by other propositions with the use of logical connectives"/>
<node CREATED="1437951013821" ID="ID_202956221" MODIFIED="1437951014303" TEXT="some examples are &quot;and&quot; (conjunction), &quot;or&quot; (disjunction), &quot;not&#x201d; (negation) and &quot;if&quot; (but only when used to denote material conditional)">
<node CREATED="1437951031597" ID="ID_683044615" MODIFIED="1437951032216" TEXT="Material conditional">
<node CREATED="1437951065882" ID="ID_1701048724" MODIFIED="1437951066673" TEXT="is a logical connective (or a binary operator) that is often symbolized by a forward arrow &quot;&#x2192;&quot;."/>
<node CREATED="1437951071011" ID="ID_1863616344" MODIFIED="1437951083541" TEXT="does not mean cause, just that when p is true, q is true too"/>
</node>
</node>
</node>
</node>
<node CREATED="1437951162192" ID="ID_355807513" MODIFIED="1437951162895" TEXT="Predicate logic">
<node CREATED="1437951250631" ID="ID_1174167820" LINK="https://en.wikipedia.org/wiki/Predicate_logic" MODIFIED="1437951252435" TEXT="https://en.wikipedia.org/wiki/Predicate_logic"/>
<node CREATED="1437951172496" ID="ID_257598177" MODIFIED="1437951237930" TEXT="First-order logic">
<node CREATED="1437951210069" ID="ID_1837661276" MODIFIED="1437951217590" TEXT="includes quantifiers, like">
<node CREATED="1438062744187" ID="ID_1565979874" MODIFIED="1438062754314" TEXT="&#x2203;">
<node CREATED="1437951217846" ID="ID_18080709" MODIFIED="1437951226357" TEXT="there exists"/>
</node>
<node CREATED="1438062760528" ID="ID_1403418903" MODIFIED="1438062761796" TEXT="&#x2200;">
<node CREATED="1437951226805" ID="ID_1636600275" MODIFIED="1437951227954" TEXT="for all"/>
</node>
</node>
</node>
</node>
<node CREATED="1437949015273" ID="ID_1010540359" MODIFIED="1437951288224" TEXT="Modal logics">
<node CREATED="1437949020446" ID="ID_106640244" LINK="https://en.wikipedia.org/wiki/Modal_logic" MODIFIED="1437949020446" TEXT="https://en.wikipedia.org/wiki/Modal_logic"/>
<node CREATED="1437949952017" ID="ID_909658378" MODIFIED="1437949952017" TEXT="">
<node CREATED="1437949779437" ID="ID_718039837" MODIFIED="1437949782933" TEXT="temporal?">
<node CREATED="1437949028328" ID="ID_1549940587" MODIFIED="1437949031168" TEXT="&quot;usually&quot;">
<node CREATED="1437949033594" ID="ID_403396995" MODIFIED="1437949034447" TEXT="For example, the statement &quot;John is happy&quot; might be qualified by saying that John is usually happy, in which case the term &quot;usually&quot; is functioning as a modal."/>
</node>
</node>
<node CREATED="1437949722482" ID="ID_420754493" MODIFIED="1437949724258" TEXT="alethic">
<node CREATED="1437949722504" ID="ID_778774980" MODIFIED="1437949722504" TEXT="modalities of truth, include">
<node CREATED="1437949722505" ID="ID_250563320" MODIFIED="1437949722505" TEXT="possibility (&quot;Possibly, p&quot;, &quot;It is possible that p&quot;),">
<node CREATED="1437949722506" ID="ID_315929181" MODIFIED="1437949722506" TEXT="impossibility (&quot;Impossibly, p&quot;, &quot;It is impossible that p&quot;)"/>
</node>
<node CREATED="1437949722505" ID="ID_1299311392" MODIFIED="1437949722505" TEXT="necessity (&quot;Necessarily, p&quot;, &quot;It is necessary that p&quot;), and"/>
<node CREATED="1437949925981" ID="ID_1519050396" MODIFIED="1437949929471" TEXT="contingency"/>
</node>
</node>
<node CREATED="1437949672073" ID="ID_1603655064" MODIFIED="1437949672073" TEXT="epistemic">
<node CREATED="1437949672074" MODIFIED="1437949672074" TEXT="modalities of knowledge (&quot;It is known that p&quot;)"/>
</node>
<node CREATED="1437949672095" ID="ID_502369921" MODIFIED="1437949672095" TEXT="doxastic">
<node CREATED="1437949672095" MODIFIED="1437949672095" TEXT="modalities of belief (&quot;It is believed that p&quot;)"/>
</node>
<node CREATED="1438066066489" ID="ID_452287447" MODIFIED="1438066067322" TEXT="Intuitionistic logic">
<node CREATED="1438064544681" ID="ID_1685797271" MODIFIED="1438066084173" TEXT="constructive logic">
<node CREATED="1438064550597" LINK="https://en.wikipedia.org/wiki/Intuitionistic_logic" MODIFIED="1438064550597" TEXT="https://en.wikipedia.org/wiki/Intuitionistic_logic"/>
<node CREATED="1438064590232" ID="ID_86551885" MODIFIED="1509025326390" TEXT="For example, in classical logic, propositional formulae are always assigned a truth value from the two element set of trivial propositions {&#x22a4;, &#x22a5;} (&quot;true&quot; and &quot;false&quot; respectively) regardless of whether we have direct evidence for either case."/>
<node CREATED="1438064602074" ID="ID_545552831" MODIFIED="1438064603058" TEXT="In contrast, propositional formulae in intuitionistic logic are not assigned any definite truth value at all and instead only considered &quot;true&quot; when we have direct evidence, hence proof."/>
<node CREATED="1438064658511" ID="ID_572618392" MODIFIED="1438064663991" TEXT="no 3rd truth value">
<node CREATED="1438064668269" ID="ID_1881867070" MODIFIED="1438064679426" TEXT="statements just remain unproved"/>
</node>
<node CREATED="1438064724353" ID="ID_744113987" MODIFIED="1438064749609" TEXT="From a proof-theoretic perspective,&#xa;intuitionistic logic is a restriction of classical logic in which &#xa;the law of excluded middle and double negation elimination &#xa;are not admitted as axioms.">
<node CREATED="1438064766729" ID="ID_1891130859" MODIFIED="1438064767578" TEXT="Law of excluded middle">
<node CREATED="1438064771524" LINK="https://en.wikipedia.org/wiki/Law_of_excluded_middle" MODIFIED="1438064771524" TEXT="https://en.wikipedia.org/wiki/Law_of_excluded_middle"/>
<node CREATED="1438064845877" ID="ID_1060022288" MODIFIED="1438064846744" TEXT="either that proposition is true, or its negation is tru"/>
</node>
<node CREATED="1438064858121" ID="ID_134393298" MODIFIED="1438064859208" TEXT="Double negative elimination">
<node CREATED="1438064863328" LINK="https://en.wikipedia.org/wiki/Double_negation#Double_negative_elimination" MODIFIED="1438064863328" TEXT="https://en.wikipedia.org/wiki/Double_negation#Double_negative_elimination"/>
<node CREATED="1438064886269" ID="ID_1395187696" MODIFIED="1438064903172" TEXT="if A is true, then not not-A is true and its converse, that, &#xa;if not not-A is true, then A is true"/>
</node>
</node>
</node>
<node CREATED="1438066101170" ID="ID_258247" MODIFIED="1438066102258" TEXT="realizability interpretation">
<node CREATED="1438066108552" LINK="https://en.wikipedia.org/wiki/Brouwer%E2%80%93Heyting%E2%80%93Kolmogorov_interpretation" MODIFIED="1438066108552" TEXT="https://en.wikipedia.org/wiki/Brouwer%E2%80%93Heyting%E2%80%93Kolmogorov_interpretation"/>
</node>
<node CREATED="1509025766334" ID="ID_630422376" MODIFIED="1509025852458" TEXT="Linear Logic">
<node CREATED="1509025799675" LINK="https://en.wikipedia.org/wiki/Linear_logic" MODIFIED="1509025799675" TEXT="https://en.wikipedia.org/wiki/Linear_logic"/>
<node CREATED="1509025878676" ID="ID_1311719181" MODIFIED="1509025882821" TEXT="reasoning about resources">
<node CREATED="1509025887294" FOLDED="true" ID="ID_1327381999" LINK="https://ghc.haskell.org/trac/ghc/wiki/LinearTypes" MODIFIED="1509025985241" TEXT="https://ghc.haskell.org/trac/ghc/wiki/LinearTypes">
<node CREATED="1509025982542" ID="ID_138842066" MODIFIED="1509025983585" TEXT="With linear types we can give a functional interface to streams. (Note that in the example below we keep the output as an old-style file, only the input streams are changed). In essence, linear types allow writing in the style of lazy effects, but without resource leaks due to late garbage collection."/>
</node>
</node>
</node>
</node>
<node CREATED="1437949059576" ID="ID_711661643" MODIFIED="1437949967838" TEXT="Temporal logic&#xa;(is a modal logic)">
<node CREATED="1437949063556" LINK="https://en.wikipedia.org/wiki/Temporal_logic" MODIFIED="1437949063556" TEXT="https://en.wikipedia.org/wiki/Temporal_logic"/>
<node CREATED="1437949080306" ID="ID_711706918" MODIFIED="1437949089578" TEXT="&quot;always&quot;">
<node CREATED="1437949089579" ID="ID_295771661" MODIFIED="1437949094999" TEXT="relates to time">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1437949095589" ID="ID_704751679" MODIFIED="1437949096812" TEXT="In a temporal logic we can then express statements like &quot;I am always hungry&quot;, &quot;I will eventually be hungry&quot;, or &quot;I will be hungry until I eat something&quot;. "/>
</node>
</node>
<node CREATED="1437949187448" ID="ID_1878660950" MODIFIED="1437949481366" TEXT="Deontic logic&#xa;(is a modal logic)">
<node CREATED="1437949192053" LINK="https://en.wikipedia.org/wiki/Deontic_logic" MODIFIED="1437949192053" TEXT="https://en.wikipedia.org/wiki/Deontic_logic"/>
<node CREATED="1437949192629" ID="ID_837993060" MODIFIED="1437949260176" TEXT="&quot;must&quot;">
<node CREATED="1437949267136" ID="ID_978059576" MODIFIED="1437949269145" TEXT="mine">
<node CREATED="1437949260517" ID="ID_1742090501" MODIFIED="1437949266513" TEXT="You must stop at a red lightt"/>
</node>
<node CREATED="1437949273627" ID="ID_217133290" MODIFIED="1437949274474" TEXT="it is obligatory that A, given B"/>
<node CREATED="1437949293739" ID="ID_150989634" MODIFIED="1437949295123" TEXT="field of philosophical logic that is concerned with obligation, permission, and related concepts."/>
<node CREATED="1437949335244" ID="ID_1414324980" MODIFIED="1437949335757" TEXT="The term deontic is derived from the ancient Greek d&#xe9;on - &#x3b4;&#x3ad;&#x3bf;&#x3bd; (gen.: &#x3b4;&#x3ad;&#x3bf;&#x3bd;&#x3c4;&#x3bf;&#x3c2;), meaning, roughly, that which is binding or proper."/>
</node>
</node>
<node CREATED="1438064321059" ID="ID_1270432266" MODIFIED="1438066818191" TEXT="Japaridze&apos;s Polymodal Logic">
<node CREATED="1438064326948" ID="ID_1772054341" LINK="https://en.wikipedia.org/wiki/Japaridze%27s_Polymodal_Logic" MODIFIED="1438064326948" TEXT="https://en.wikipedia.org/wiki/Japaridze%27s_Polymodal_Logic"/>
<node CREATED="1509025441615" ID="ID_299475789" MODIFIED="1509025442215" TEXT="is a system of provability logic with infinitely many modal (provability) operators. This system has played an important role in some applications of provability algebras in proof theory, and has been extensively studied since the late 1980s. "/>
<node CREATED="1509025653168" ID="ID_1427741403" LINK="https://en.wikipedia.org/wiki/Giorgi_Japaridze" MODIFIED="1509025653168" TEXT="https://en.wikipedia.org/wiki/Giorgi_Japaridze">
<node CREATED="1509025645009" ID="ID_1342538886" MODIFIED="1509025657008" TEXT="Japaridze is best known[citation needed] for founding Computability Logic in 2003"/>
<node CREATED="1509025700103" ID="ID_1542606614" LINK="https://en.wikipedia.org/wiki/Computability_logic" MODIFIED="1509025700103" TEXT="https://en.wikipedia.org/wiki/Computability_logic">
<node CREATED="1509026348795" LINK="https://en.wikipedia.org/wiki/Independence-friendly_logic" MODIFIED="1509026348795" TEXT="https://en.wikipedia.org/wiki/Independence-friendly_logic"/>
</node>
<node CREATED="1509026837760" LINK="https://wikivisually.com/wiki/Cirquent_calculus" MODIFIED="1509026837760" TEXT="https://wikivisually.com/wiki/Cirquent_calculus"/>
<node CREATED="1509026941854" LINK="https://wikivisually.com/wiki/Giorgi_Japaridze" MODIFIED="1509026941854" TEXT="https://wikivisually.com/wiki/Giorgi_Japaridze"/>
</node>
</node>
</node>
</node>
<node CREATED="1438063675490" ID="ID_1935733068" MODIFIED="1438063678398" TEXT="definitions">
<node CREATED="1438063678730" ID="ID_1597081968" MODIFIED="1438063796234" TEXT="Soundness">
<node CREATED="1438063687271" ID="ID_617800701" LINK="https://en.wikipedia.org/wiki/Soundness" MODIFIED="1438063687271" TEXT="https://en.wikipedia.org/wiki/Soundness"/>
<node CREATED="1438063701891" ID="ID_1408678465" MODIFIED="1438063719143" TEXT="In mathematical logic, a logical system has the soundness property&#xa;if and only if its inference rules prove only formulas that are valid with respect to its semantics">
<node CREATED="1438063737237" ID="ID_1374487855" MODIFIED="1438063756868" TEXT="All organisms with wings can fly. &#xa;Penguins have wings.&#xa;Therefore, penguins can fly. &#xa;&#xa;Since the first premise is actually false, the argument, though valid, is not sound."/>
</node>
</node>
<node CREATED="1438063790398" ID="ID_1272189568" MODIFIED="1438063792547" TEXT="Completeness">
<node CREATED="1438063801830" LINK="https://en.wikipedia.org/wiki/Completeness_" MODIFIED="1438063801830" TEXT="https://en.wikipedia.org/wiki/Completeness_(logic)"/>
<node CREATED="1438063813445" ID="ID_1358872635" MODIFIED="1438063900837" TEXT="In mathematical logic and metalogic, &#xa;a formal system is called complete with respect to a particular property&#xa;if every formula having the property can be derived using that system, &#xa;i.e. is one of its theorems; otherwise the system is said to be incomplete."/>
</node>
<node CREATED="1438065195882" ID="ID_1904783824" MODIFIED="1438065197456" TEXT="Curry&#x2013;Howard correspondence">
<node CREATED="1438065202856" LINK="https://en.wikipedia.org/wiki/Curry%E2%80%93Howard_correspondence" MODIFIED="1438065202856" TEXT="https://en.wikipedia.org/wiki/Curry%E2%80%93Howard_correspondence"/>
<node CREATED="1438065218306" ID="ID_1843801849" MODIFIED="1438065218803" TEXT="the direct relationship between computer programs and mathematical proofs"/>
<node CREATED="1438065350028" ID="ID_1225943962" MODIFIED="1438065459104" TEXT="a proof is a program, &#xa;the formula it proves is a type for the program">
<node CREATED="1438065408763" ID="ID_984938246" MODIFIED="1438065447972" TEXT="This sets a form of logic programming &#xa;on a rigorous foundation">
<node CREATED="1438065414801" ID="ID_837118349" MODIFIED="1438065415496" TEXT="proofs can be represented as programs"/>
<node CREATED="1438065422231" ID="ID_1099189980" MODIFIED="1438065423054" TEXT="proofs can be run"/>
</node>
</node>
<node CREATED="1438065886838" ID="ID_892418869" MODIFIED="1438065888425" TEXT="coq">
<node CREATED="1438065888795" LINK="https://en.wikipedia.org/wiki/Coq" MODIFIED="1438065888795" TEXT="https://en.wikipedia.org/wiki/Coq"/>
</node>
</node>
<node CREATED="1438066820121" ID="ID_886649787" MODIFIED="1438066821458" TEXT="Principle of explosion">
<node CREATED="1438066826516" LINK="https://en.wikipedia.org/wiki/Principle_of_explosion" MODIFIED="1438066826516" TEXT="https://en.wikipedia.org/wiki/Principle_of_explosion"/>
<node CREATED="1438066909389" ID="ID_1556332339" MODIFIED="1438066910591" TEXT="(Latin: ex falso quodlibet, &quot;from a falsehood, anything follows&quot;, or  ex contradictione sequitur quodlibet, &quot;from a contradiction, anything follows&quot;), or  the principle of Pseudo-Scotus, is the law of    classical logic,    intuitionistic logic and    similar logical systems, according to which  any statement can be proven from a contradiction."/>
<node CREATED="1438066968628" ID="ID_1109383896" MODIFIED="1438066980206" TEXT="That is, once a contradiction has been asserted,&#xa;any proposition (or its negation) can be inferred from it."/>
</node>
</node>
<node CREATED="1438067618809" ID="ID_1993686261" MODIFIED="1438067620024" TEXT="later">
<node CREATED="1438067627455" LINK="https://en.wikipedia.org/wiki/Proof_theory" MODIFIED="1438067627455" TEXT="https://en.wikipedia.org/wiki/Proof_theory"/>
<node CREATED="1438067689267" ID="ID_1628456221" LINK="https://en.wikipedia.org/wiki/Realizability" MODIFIED="1438067689267" TEXT="https://en.wikipedia.org/wiki/Realizability">
<node CREATED="1438067805827" ID="ID_1335185292" MODIFIED="1438067840525" TEXT="Modified realizability is one way to show that &#xa;Markov&apos;s principle is not derivable in intuitionistic logic   "/>
</node>
<node CREATED="1438067752071" LINK="https://en.wikipedia.org/wiki/Proof_mining" MODIFIED="1438067752071" TEXT="https://en.wikipedia.org/wiki/Proof_mining"/>
<node CREATED="1438067924902" LINK="https://en.wikipedia.org/wiki/Type_theory" MODIFIED="1438067924902" TEXT="https://en.wikipedia.org/wiki/Type_theory"/>
<node CREATED="1438068556636" LINK="https://en.wikipedia.org/wiki/Laws_of_Form" MODIFIED="1438068556636" TEXT="https://en.wikipedia.org/wiki/Laws_of_Form"/>
<node CREATED="1438068576845" LINK="http://users.clas.ufl.edu/jzeman/" MODIFIED="1438068576845" TEXT="users.clas.ufl.edu &gt; Jzeman"/>
</node>
</node>
<node CREATED="1454480934954" ID="ID_1187935301" MODIFIED="1454480938102" POSITION="left" TEXT="GGP Notes">
<node CREATED="1454480943596" LINK="http://logic.stanford.edu/ggp/chapters/cover.html" MODIFIED="1454480943596" TEXT="logic.stanford.edu &gt; Ggp &gt; Chapters &gt; Cover"/>
</node>
<node CREATED="1495182950532" ID="ID_1407928108" MODIFIED="1495182951634" POSITION="left" TEXT="Markov logic network">
<node CREATED="1495182958429" LINK="https://en.wikipedia.org/wiki/Markov_logic_network" MODIFIED="1495182958429" TEXT="https://en.wikipedia.org/wiki/Markov_logic_network"/>
<node CREATED="1495182967630" LINK="https://en.wikipedia.org/wiki/Pedro_Domingos" MODIFIED="1495182967630" TEXT="https://en.wikipedia.org/wiki/Pedro_Domingos"/>
<node CREATED="1495182997410" ID="ID_1078599898" MODIFIED="1495182997983" TEXT="Work in this area began in 2003 by Pedro Domingos and Matt Richardson">
<node CREATED="1495183056572" ID="ID_968819852" MODIFIED="1495183057261" TEXT="Matthew Richardson">
<node CREATED="1495183061327" LINK="https://www.microsoft.com/en-us/research/people/mattri/" MODIFIED="1495183061327" TEXT="https://www.microsoft.com/en-us/research/people/mattri/"/>
</node>
</node>
</node>
<node CREATED="1498801614906" ID="ID_342903685" MODIFIED="1498801618516" POSITION="left" TEXT="Logic Programming">
<node CREATED="1498801619014" ID="ID_387761656" MODIFIED="1498801621118" TEXT="miniKanren-WEByrd.pdf">
<node CREATED="1498801643810" LINK="http://pqdtopen.proquest.com/#abstract?dispub=3380156" MODIFIED="1498801643810" TEXT="pqdtopen.proquest.com &gt; #abstract ? ..."/>
<node CREATED="1498801644830" ID="ID_1152883536" MODIFIED="1498801647742" TEXT="also downloaded"/>
<node CREATED="1499636052392" ID="ID_453503909" MODIFIED="1499636063005" TEXT="PhD dissertation of">
<node CREATED="1499636061129" ID="ID_843148665" MODIFIED="1499636061571" TEXT="William E. Byrd"/>
</node>
<node CREATED="1499636064836" ID="ID_133038251" MODIFIED="1499636075443" TEXT="now part of a book">
<node CREATED="1499637985145" ID="ID_91356243" MODIFIED="1499637986516" TEXT="The Reasoned Schemer"/>
<node CREATED="1499636079892" LINK="https://mitpress.mit.edu/books/reasoned-schemer" MODIFIED="1499636079892" TEXT="https://mitpress.mit.edu/books/reasoned-schemer"/>
<node CREATED="1499636081848" ID="ID_200516956" MODIFIED="1499637984232" TEXT="extending logic programming to functional programming (scheme)  "/>
<node CREATED="1499636013598" ID="ID_656316142" MODIFIED="1499636105895" TEXT="coauthors">
<node CREATED="1499636116117" ID="ID_1420217720" MODIFIED="1499636127536" TEXT="Daniel P. Friedman"/>
<node CREATED="1499636127537" ID="ID_834156095" MODIFIED="1499636139986" TEXT="William E. Byrd"/>
<node CREATED="1499636139988" ID="ID_91293252" MODIFIED="1499636168006" TEXT="Oleg Kiselyov">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1499636148210" LINK="http://okmij.org/ftp/" MODIFIED="1499636148210" TEXT="okmij.org &gt; Ftp"/>
<node CREATED="1499636155476" ID="ID_893340700" MODIFIED="1499636155955" TEXT="Program Generation for High-Performance Computing"/>
<node CREATED="1499636168491" LINK="http://okmij.org/ftp/meta-programming/HPC.html#shonan-challenge" MODIFIED="1499636168491" TEXT="okmij.org &gt; Ftp &gt; Meta-programming &gt; HPC"/>
<node CREATED="1499636338943" ID="ID_360027075" MODIFIED="1499636341154" TEXT="code generation">
<node CREATED="1499636386675" ID="ID_695897914" LINK="http://okmij.org/ftp/meta-programming/" MODIFIED="1499636388560" TEXT="okmij.org &gt; Ftp &gt; Meta-programming"/>
<node CREATED="1499637194077" ID="ID_1605436632" MODIFIED="1499637204962" TEXT="BER MetaOCaml">
<node CREATED="1499637213803" ID="ID_812005067" MODIFIED="1499637229269" TEXT="based on MetaOCaml&#xa;by Walid Taha">
<node CREATED="1499637239029" ID="ID_1561815489" MODIFIED="1499637240360" TEXT="A Gentle Introduction to Multi-stage Programming"/>
<node CREATED="1499637245672" LINK="http://www.cs.rice.edu/~taha/publications/journal/dspg04a.pdf" MODIFIED="1499637245672" TEXT="cs.rice.edu &gt; Taha &gt; Publications &gt; Journal &gt; Dspg04a"/>
<node CREATED="1499637285006" ID="ID_1771252842" MODIFIED="1499637285894" TEXT="Multi-stage programming (MSP) is a paradigm for developing generic software that does not pay a runtime penalty for this generality."/>
</node>
</node>
<node CREATED="1499637824745" ID="ID_673534484" MODIFIED="1499637860868" TEXT="this is about the efficiency of abstracted code (e.g. functional programming)"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1498835450791" LINK="http://probcomp.csail.mit.edu/" MODIFIED="1498835450791" TEXT="probcomp.csail.mit.edu"/>
<node CREATED="1498835500389" ID="ID_529583430" MODIFIED="1498835516889" TEXT="Picture: &#xa;A Probabilistic Programming Language &#xa;for Scene Perception">
<node CREATED="1498835489267" ID="ID_1311756197" LINK="http://mrkulk.github.io/www_cvpr15/1999.pdf" MODIFIED="1498835489267" TEXT="mrkulk.github.io &gt; Www cvpr15 &gt; 1999"/>
</node>
</node>
</node>
</map>
