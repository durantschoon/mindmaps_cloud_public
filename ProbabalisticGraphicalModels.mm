<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1482125637147" ID="ID_1039594804" LINK="MetaMap.mm" MODIFIED="1486445514768" TEXT="ProbabalisticGraphicalModels.mm">
<node CREATED="1482126052843" ID="ID_753828559" MODIFIED="1482126054868" POSITION="right" TEXT="Coursera">
<node CREATED="1482126057029" ID="ID_941541804" MODIFIED="1482126059354" TEXT="2017">
<node CREATED="1482126114061" ID="ID_777329344" LINK="https://www.coursera.org/learn/probabilistic-graphical-models" MODIFIED="1482126114061" TEXT="https://www.coursera.org/learn/probabilistic-graphical-models"/>
<node CREATED="1485973734233" ID="ID_1228730376" MODIFIED="1487643389186" TEXT="TODO">
<node CREATED="1493010777021" ID="ID_360995070" MODIFIED="1493010802795" TEXT="Build a graph of my test in SAMIAM and see if it gives the same result "/>
<node CREATED="1485973735996" ID="ID_1260948259" MODIFIED="1485973739446" TEXT="collect all terms">
<node CREATED="1485973742226" ID="ID_46894985" MODIFIED="1485973746707" TEXT="in Course Resources">
<node CREATED="1485973747359" ID="ID_1665813083" MODIFIED="1485973752675" TEXT="know how to describe each concept"/>
</node>
</node>
</node>
<node CREATED="1487643993905" ID="ID_1801799518" MODIFIED="1487643999616" TEXT="All Honors Assignments">
<node CREATED="1487644002077" ID="ID_810013851" LINK="https://www.coursera.org/learn/probabilistic-graphical-models/home/assignments" MODIFIED="1487644002077" TEXT="https://www.coursera.org/learn/probabilistic-graphical-models/home/assignments"/>
</node>
<node CREATED="1485972965866" ID="ID_208524489" MODIFIED="1487643390321" TEXT="Quizzes &#xa;(now in PDFs on dropbox)">
<node CREATED="1482126139372" ID="ID_601909484" MODIFIED="1489889278242" TEXT="Intro and Overview">
<node CREATED="1482127820974" ID="ID_879585146" MODIFIED="1482127825548" TEXT="Distributions">
<node CREATED="1482127829380" ID="ID_537668935" MODIFIED="1482127837455" TEXT="Conditioning : Renormalization">
<node CREATED="1482127909233" ID="ID_963171563" MODIFIED="1482127918540" TEXT="un-normalized">
<node CREATED="1482127919255" ID="ID_288230838" MODIFIED="1482127933296" TEXT="take all the G = g1"/>
<node CREATED="1482127935058" ID="ID_951251896" MODIFIED="1482127939353" TEXT="divide by the sums"/>
</node>
</node>
</node>
<node CREATED="1482126153087" ID="ID_1752064248" MODIFIED="1482126155967" TEXT="Factors">
<node CREATED="1490072990053" ID="ID_734247364" MODIFIED="1490072994743" TEXT="Factor Product">
<node CREATED="1490072995143" ID="ID_248200784" MODIFIED="1490073058404" TEXT="Takes the union of all the (column) variables&#xa;multiplying the &quot;output&quot; when combining rows"/>
</node>
<node CREATED="1482126155968" ID="ID_1454214091" MODIFIED="1490073079543" TEXT="Factor Marginalization">
<node CREATED="1482126171381" ID="ID_521161116" MODIFIED="1482126180041" TEXT="Removes columns">
<node CREATED="1482126183417" ID="ID_223017813" MODIFIED="1482126186108" TEXT="eg. all b&apos;s"/>
<node CREATED="1482126191078" ID="ID_1968185890" MODIFIED="1482126193060" TEXT="ADD">
<node CREATED="1482126193300" ID="ID_1379972428" MODIFIED="1482126202280" TEXT="resulting values with a1, c1"/>
</node>
</node>
</node>
<node CREATED="1482126164470" ID="ID_50473369" MODIFIED="1482126170486" TEXT="Factor Reduction">
<node CREATED="1482126206347" ID="ID_1060881657" MODIFIED="1482126210206" TEXT="Removes rows">
<node CREATED="1482126218120" ID="ID_1780245569" MODIFIED="1482126221815" TEXT="eg. all c2&apos;s"/>
<node CREATED="1482126240887" ID="ID_993382399" MODIFIED="1482126251604" TEXT="SELECTION only"/>
</node>
<node CREATED="1482127587066" ID="ID_1787463961" MODIFIED="1482127592347" TEXT="like conditional probability">
<node CREATED="1482127962567" ID="ID_729013465" MODIFIED="1482127967341" TEXT="related to renormalizations"/>
</node>
</node>
</node>
<node CREATED="1482126374999" ID="ID_294060044" MODIFIED="1482126376901" TEXT="Quiz">
<node CREATED="1482126632170" ID="ID_1237839592" MODIFIED="1482126633397" TEXT="1">
<node CREATED="1482126379825" ID="ID_1048372841" MODIFIED="1482126381165" TEXT="values">
<node CREATED="1482126381165" ID="ID_342982450" MODIFIED="1482126407833" TEXT="0.56">
<node CREATED="1482126400581" MODIFIED="1482126400581" TEXT="0.7 * 0.8"/>
</node>
<node CREATED="1482126432636" ID="ID_1148325186" MODIFIED="1482126433676" TEXT="0.0">
<node CREATED="1482126437681" ID="ID_1194246467" MODIFIED="1482126442604" TEXT="0.1 * 0.0"/>
</node>
<node CREATED="1482126444013" ID="ID_1418685210" MODIFIED="1482126476399" TEXT="0.2">
<node CREATED="1482126476399" ID="ID_1491237710" MODIFIED="1482126477390" TEXT="0.4 * 0.5"/>
</node>
</node>
</node>
<node CREATED="1482126635093" ID="ID_324177214" MODIFIED="1482126635726" TEXT="2">
<node CREATED="1482126636119" ID="ID_564495662" MODIFIED="1482126638464" TEXT="values">
<node CREATED="1482126638465" ID="ID_756997045" MODIFIED="1482126639900" TEXT="14 60 4 59">
<node CREATED="1482126639901" ID="ID_474884746" MODIFIED="1482126655927" TEXT="where Y column = 1"/>
</node>
</node>
</node>
<node CREATED="1482126684443" ID="ID_664936320" MODIFIED="1482126766211" TEXT="3">
<node CREATED="1482126731246" ID="ID_1090031612" MODIFIED="1482126733267" TEXT="true">
<node CREATED="1482126719565" ID="ID_93347860" MODIFIED="1482126720752" TEXT="P(B|A)=P(B)"/>
<node CREATED="1482126725027" ID="ID_1252489832" MODIFIED="1482126726130" TEXT="P(A|B)=P(A)"/>
<node CREATED="1482126737968" ID="ID_562737347" MODIFIED="1482126740824" TEXT="A,B indep"/>
</node>
</node>
<node CREATED="1482126764365" ID="ID_1319327730" MODIFIED="1482126765264" TEXT="4">
<node CREATED="1482126871301" ID="ID_1115000788" MODIFIED="1482126873951" TEXT="values">
<node CREATED="1482126885837" ID="ID_949381375" MODIFIED="1482126888103" TEXT="108">
<node CREATED="1482126873952" ID="ID_771094078" MODIFIED="1482126879523" TEXT="68+40"/>
</node>
<node CREATED="1482126917774" ID="ID_1433487" MODIFIED="1482126919239" TEXT="135">
<node CREATED="1482126919526" ID="ID_1846405737" MODIFIED="1482126920719" TEXT="95+40"/>
</node>
<node CREATED="1482126958416" ID="ID_865592684" MODIFIED="1482126959148" TEXT="79">
<node CREATED="1482126959550" ID="ID_1458417924" MODIFIED="1482126960413" TEXT="65+14"/>
</node>
<node CREATED="1482126987245" ID="ID_1936688842" MODIFIED="1482126992970" TEXT="141">
<node CREATED="1482126992971" ID="ID_1980444098" MODIFIED="1482126994081" TEXT="63+78"/>
</node>
<node CREATED="1482127001733" ID="ID_1048444144" MODIFIED="1482127006094" TEXT="108 135 79 141"/>
</node>
</node>
</node>
</node>
<node CREATED="1487643626229" ID="ID_813172093" MODIFIED="1487643627880" TEXT="Wk 1">
<node CREATED="1487643474124" ID="ID_1342737247" MODIFIED="1487643475105" TEXT="Bayesian Network (Directed Models)">
<node CREATED="1485972998969" ID="ID_1827694454" MODIFIED="1487643516071" TEXT="Bayesian Network Fundamentals">
<node CREATED="1484034728500" FOLDED="true" ID="ID_1846225785" MODIFIED="1489371759134" TEXT="Quiz">
<node CREATED="1484034731181" ID="ID_678418833" MODIFIED="1484034732003" TEXT="1">
<node CREATED="1484034811134" ID="ID_404296648" MODIFIED="1484034813582" TEXT="Factorization">
<node CREATED="1484034745228" ID="ID_1372840336" MODIFIED="1484034805548" TEXT="Decomposition of  Joint Distribution&#xa;P(A,B,C,D)&#xa;Graph includes E as a child of A, C"/>
</node>
</node>
<node CREATED="1484034732565" ID="ID_514291162" MODIFIED="1484034732939" TEXT="2">
<node CREATED="1484034821497" ID="ID_496850584" MODIFIED="1484034822130" TEXT="Independent parameters">
<node CREATED="1484034852204" ID="ID_1198825413" MODIFIED="1484034859375" TEXT="counting independent parameters"/>
</node>
</node>
<node CREATED="1484034733455" ID="ID_1037764572" MODIFIED="1484034733872" TEXT="3">
<node CREATED="1484034832991" ID="ID_568352665" MODIFIED="1484034833585" TEXT="Inter-causal reasoning">
<node CREATED="1484034870867" ID="ID_828722093" MODIFIED="1484034873791" TEXT="Calculate"/>
</node>
</node>
</node>
</node>
<node CREATED="1484507910208" ID="ID_778774971" MODIFIED="1487643438024" TEXT="Bayesian Network Independencies">
<node CREATED="1484507912585" FOLDED="true" ID="ID_1700916983" MODIFIED="1489371758116" TEXT="Quiz">
<node CREATED="1484507914415" ID="ID_1854700903" MODIFIED="1484507915815" TEXT="1">
<node CREATED="1484507922335" ID="ID_739755233" MODIFIED="1484507922906" TEXT="Independencies in a graph.  ">
<node CREATED="1484510127623" ID="ID_1774115026" MODIFIED="1484510129015" TEXT="A,B"/>
</node>
</node>
<node CREATED="1484508242977" ID="ID_934492978" MODIFIED="1484508244433" TEXT="2">
<node CREATED="1484508239971" ID="ID_1653301084" MODIFIED="1484508240837" TEXT="*Independencies in a graph.">
<node CREATED="1484510120581" ID="ID_1385316020" MODIFIED="1484510122001" TEXT="None - given E, there are no pairs of variables that are independent."/>
</node>
</node>
<node CREATED="1484508256231" ID="ID_1517899972" MODIFIED="1484508256546" TEXT="3">
<node CREATED="1484508257406" ID="ID_996199354" MODIFIED="1484508257888" TEXT="I-maps">
<node CREATED="1484508822839" ID="ID_1042311217" MODIFIED="1484508825713" TEXT="T or F">
<node CREATED="1484508965702" ID="ID_1381614378" MODIFIED="1484508966908" TEXT="T">
<node CREATED="1484509022246" ID="ID_609967582" MODIFIED="1484509032325" TEXT="must be by elimination">
<node CREATED="1484508828383" ID="ID_45214366" MODIFIED="1484509036541" TEXT="A graph K is an I-map for a graph G if and only if all of the independencies encoded by K are also encoded by G."/>
</node>
</node>
<node CREATED="1484508967296" ID="ID_1805766438" MODIFIED="1484508967848" TEXT="F">
<node CREATED="1484508840810" ID="ID_320273426" MODIFIED="1484508981559" TEXT="A graph K is an I-map for a graph G if and only if K and G are identical, i.e., they have exactly the same nodes and edges."/>
<node CREATED="1484508848121" ID="ID_1673851962" MODIFIED="1484508998721" TEXT="An I-map is a function f that maps a graph G to itself, i.e., f(G)=G."/>
<node CREATED="1484508856425" ID="ID_1279734627" MODIFIED="1484509011454" TEXT="The graph K that is the same as the graph G, except that all of the edges are oriented in the opposite direction as the corresponding edges in G, is always an I-map for G, regardless of the structure of G."/>
</node>
</node>
<node CREATED="1484508891658" ID="ID_417233369" MODIFIED="1484508893868" TEXT="haha">
<node CREATED="1484508866074" ID="ID_1601954589" MODIFIED="1484508901407" TEXT="I-maps are Apple&apos;s answer to Google Maps."/>
</node>
</node>
</node>
<node CREATED="1484509777811" ID="ID_1937322934" MODIFIED="1484509778312" TEXT="4">
<node CREATED="1484509779059" ID="ID_1362889159" MODIFIED="1484509779668" TEXT="*Naive Bayes.">
<node CREATED="1484509789331" ID="ID_841356702" MODIFIED="1484509791499" TEXT="T">
<node CREATED="1484509791853" ID="ID_1148282810" MODIFIED="1484509799104" TEXT="Say we observe that 1000 people have the flu, out of which 500 people have a headache (and possibly other symptoms) and 500 have a fever (and possibly other symptoms).&#xa;&#xa;We would expect that approximately 250 people with the flu also have both a headache and fever."/>
</node>
<node CREATED="1484509800883" ID="ID_1360337258" MODIFIED="1484509818992" TEXT="vs F for this conclusion">
<node CREATED="1484509824654" ID="ID_347481356" MODIFIED="1484509825208" TEXT="We can conclude that exactly 250 people with the flu also have both a headache and fever."/>
</node>
</node>
</node>
<node CREATED="1484510059555" ID="ID_1915726411" MODIFIED="1484510060194" TEXT="5">
<node CREATED="1484510060636" ID="ID_1491057447" MODIFIED="1484510067183" TEXT="I-maps">
<node CREATED="1484510067184" ID="ID_655354937" MODIFIED="1484510175391" TEXT="T">
<icon BUILTIN="button_cancel"/>
<node CREATED="1484510076641" ID="ID_290190730" MODIFIED="1484510102509" TEXT="Suppose (A&#x22a5;B)&#x2208;I(P), and G is an I-map of P, where G is a Bayesian network and P is a probability distribution. Is it necessarily true that (A&#x22a5;B)&#x2208;IG)?"/>
<node CREATED="1484510177236" ID="ID_975011838" MODIFIED="1484510180263" TEXT="This should not be selected  Since G is an I-map of P, all independencies in G are also in P. However, this doesn&apos;t mean that all independencies in P are also in G. An easy way to remember this is that the complete graph, which has no independencies, is an I-map of all distributions.  "/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1487643486905" ID="ID_652851339" MODIFIED="1487643487386" TEXT="Bayesian Networks: Knowledge Engineering"/>
</node>
<node CREATED="1487643815583" ID="ID_1244472433" MODIFIED="1487643817016" TEXT="Optional Honors Content">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1487643831393" ID="ID_23779338" MODIFIED="1487643831771" TEXT="Honors Programming Assignment: Simple BN Knowledge Engineering"/>
</node>
</node>
<node CREATED="1487643630315" ID="ID_327415929" MODIFIED="1487643632209" TEXT="Wk 2">
<node CREATED="1487643632809" ID="ID_1023894941" MODIFIED="1487643633261" TEXT="Template Models for Bayesian Networks">
<node CREATED="1487643638802" ID="ID_1117638783" MODIFIED="1487643639414" TEXT="Template Models"/>
</node>
<node CREATED="1487643654245" ID="ID_322601589" MODIFIED="1487643850126" TEXT="Structured CPDs for Bayesian Networks">
<node CREATED="1487643660174" ID="ID_970669040" MODIFIED="1487643660497" TEXT="Structured CPDs">
<node CREATED="1487643666134" ID="ID_602083905" MODIFIED="1487643704615" TEXT="Conditional Probability Distributions"/>
</node>
</node>
<node CREATED="1487643815583" ID="ID_13515357" MODIFIED="1487643817016" TEXT="Optional Honors Content">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1487643877082" ID="ID_1319906369" MODIFIED="1487643877663" TEXT="Honors Programming Assignment: BNs for Genetic Inheritance"/>
<node CREATED="1487643949754" ID="ID_1907862005" MODIFIED="1487643950527" TEXT="HONORS QUIZ BNs for Genetic Inheritance PA Quiz 11 Questions  "/>
</node>
</node>
<node CREATED="1487643747917" ID="ID_1615274756" MODIFIED="1487643749167" TEXT="Wk 3">
<node CREATED="1487643749585" ID="ID_822005524" MODIFIED="1487643750051" TEXT="Markov Networks (Undirected Models) ">
<node CREATED="1487643755356" ID="ID_1296716496" MODIFIED="1487643755694" TEXT="Markov Network Fundamentals"/>
<node CREATED="1487643760618" ID="ID_152881624" MODIFIED="1487643760816" TEXT="Independencies in Markov Networks and Bayesian Networks"/>
<node CREATED="1487643765601" ID="ID_1513266914" MODIFIED="1487643766518" TEXT="Local Structure in Markov Networks"/>
</node>
<node CREATED="1487643815583" ID="ID_1918522756" MODIFIED="1487643817016" TEXT="Optional Honors Content">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1487644026336" ID="ID_1381708250" MODIFIED="1489368334892" TEXT="Honors Programming Assignment: Markov Networks for OCR  "/>
</node>
</node>
<node CREATED="1487643776619" ID="ID_989981033" MODIFIED="1487643777546" TEXT="Wk 4">
<node CREATED="1487643777918" ID="ID_991555715" MODIFIED="1487643778398" TEXT="Decision Making">
<node CREATED="1487643783050" ID="ID_689716576" MODIFIED="1487643783961" TEXT="Decision Theory"/>
</node>
<node CREATED="1487643815583" ID="ID_909072572" MODIFIED="1487643817016" TEXT="Optional Honors Content">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1487644064691" ID="ID_280883924" MODIFIED="1487644065457" TEXT="Honors Programming Assignment: Decision Making"/>
<node CREATED="1487644078598" ID="ID_776425152" MODIFIED="1487644079149" TEXT="HONORS QUIZ Decision Making PA Quiz"/>
</node>
</node>
<node CREATED="1487643794597" ID="ID_1318709427" MODIFIED="1487643795971" TEXT="Wk 5">
<node CREATED="1487643796336" ID="ID_1698569885" MODIFIED="1487643797480" TEXT="Knowledge Engineering &amp; Summary">
<node CREATED="1487644136209" ID="ID_888090547" MODIFIED="1487644137126" TEXT="QUIZ Representation Final Exam"/>
</node>
</node>
</node>
<node CREATED="1485973074183" ID="ID_801600957" MODIFIED="1485973079120" TEXT="Course Resources">
<node CREATED="1485973079451" ID="ID_610407299" LINK="https://www.coursera.org/learn/probabilistic-graphical-models/resources/7ct5V" MODIFIED="1485973079451" TEXT="https://www.coursera.org/learn/probabilistic-graphical-models/resources/7ct5V"/>
<node CREATED="1485973659537" ID="ID_9253544" MODIFIED="1487643401354" TEXT="Intro">
<node CREATED="1485973097308" FOLDED="true" ID="ID_1848653926" MODIFIED="1489368347879" TEXT="Motivation and Overview">
<node CREATED="1485973127923" ID="ID_1882330350" LINK="http://spark-university.s3.amazonaws.com/stanford-pgm/slides/1.1.1-Intro-overview.pdf" MODIFIED="1485973127923" TEXT="spark-university.s3.amazonaws.com &gt; Stanford-pgm &gt; Slides &gt; 1.1.1-Intro-overview"/>
<node CREATED="1485973286821" ID="ID_143303985" MODIFIED="1487643409413" TEXT="2/17">
<node CREATED="1485973289102" ID="ID_1893649305" MODIFIED="1485973292130" TEXT="Labelling"/>
</node>
<node CREATED="1485973250833" ID="ID_727367902" MODIFIED="1487643409415" TEXT="7/17">
<node CREATED="1485973225102" FOLDED="true" ID="ID_785467185" MODIFIED="1487643404339" TEXT="Random Variables ">
<node CREATED="1485973313338" ID="ID_360080793" MODIFIED="1485973315661" TEXT="binary valued"/>
</node>
<node CREATED="1485973312127" FOLDED="true" ID="ID_303940906" MODIFIED="1487643404340" TEXT="&amp; Joint distributions">
<node CREATED="1485973321879" ID="ID_1213831507" MODIFIED="1485973326797" TEXT="2^n possible states"/>
</node>
</node>
<node CREATED="1485973399571" ID="ID_438560515" MODIFIED="1487643409418" TEXT="10/17">
<node CREATED="1486268783713" FOLDED="true" ID="ID_513770092" MODIFIED="1487643404340" TEXT="Graphical Representation&#xa;Sparse parameterazation">
<node CREATED="1485973401602" FOLDED="true" ID="ID_1172323475" MODIFIED="1487643403780" TEXT="feasible elicitation">
<node CREATED="1485973405468" ID="ID_716109075" MODIFIED="1485973406096" TEXT="In requirements engineering, requirements elicitation is the practice of collecting the requirements of a system from users, customers and other stakeholders. The practice is also sometimes referred to as &quot;requirement gathering&quot;."/>
<node CREATED="1485973413393" ID="ID_870094515" MODIFIED="1486268735194" TEXT="done by hand"/>
</node>
<node CREATED="1485973417458" FOLDED="true" ID="ID_1853396329" MODIFIED="1487643403780" TEXT="learning from data">
<node CREATED="1485973423010" ID="ID_1214883481" MODIFIED="1485973426634" TEXT="automatically"/>
</node>
</node>
</node>
<node CREATED="1485973467350" ID="ID_68729639" MODIFIED="1487643409419" TEXT="14/17">
<node CREATED="1485973469976" FOLDED="true" ID="ID_1689666308" MODIFIED="1487643404343" TEXT="Textual Information Extraction ">
<node CREATED="1485973477366" ID="ID_1898524458" MODIFIED="1485973484477" TEXT="label the parts of speech in a sentence"/>
</node>
</node>
</node>
<node CREATED="1485973669143" ID="ID_735757668" MODIFIED="1487744695854" TEXT="Preliminaries:&#x9; Distributions&#x9;">
<node CREATED="1485973684741" ID="ID_1596253930" LINK="http://spark-university.s3.amazonaws.com/stanford-pgm/slides/1.1.2-Intro-distributions.pdf" MODIFIED="1485973684741" TEXT="spark-university.s3.amazonaws.com &gt; Stanford-pgm &gt; Slides &gt; 1.1.2-Intro-distributions"/>
<node CREATED="1486268613795" ID="ID_847364393" MODIFIED="1487643409420" TEXT="Joint Distributions">
<node CREATED="1486268618864" FOLDED="true" ID="ID_440120748" MODIFIED="1489368369102" TEXT="2/6">
<node CREATED="1486268631432" ID="ID_1901280894" MODIFIED="1486268642707" TEXT="i \in i{0,1}"/>
<node CREATED="1486268646271" ID="ID_47584965" MODIFIED="1486268653853" TEXT="d \in d{0,1}"/>
<node CREATED="1486268654402" ID="ID_615121887" MODIFIED="1486268662760" TEXT="g \in g{0,1,2}"/>
<node CREATED="1486268663339" FOLDED="true" ID="ID_1018662381" MODIFIED="1489368366459" TEXT="parameters">
<node CREATED="1486268675584" ID="ID_1144564180" MODIFIED="1486268680523" TEXT="2x2x3 = 12"/>
</node>
<node CREATED="1486268681719" FOLDED="true" ID="ID_1442563127" MODIFIED="1489368366460" TEXT="independent parameters">
<node CREATED="1486268687790" ID="ID_251597569" MODIFIED="1486268690741" TEXT="12 - 1 = 11"/>
</node>
</node>
</node>
<node CREATED="1485974026422" ID="ID_585819062" MODIFIED="1487643409422" TEXT="Conditioning">
<node CREATED="1485974061786" FOLDED="true" ID="ID_16080727" MODIFIED="1489368386643" TEXT="3/6">
<node CREATED="1485974063342" FOLDED="true" ID="ID_1635686499" MODIFIED="1489368385122" TEXT="Condition on g1">
<node CREATED="1485974075472" FOLDED="true" ID="ID_965310485" MODIFIED="1489368384419" TEXT="means">
<node CREATED="1485974077530" ID="ID_430331082" MODIFIED="1486268299365" TEXT="filter for only rows with g1"/>
</node>
</node>
</node>
<node CREATED="1485974094667" FOLDED="true" ID="ID_1492313925" MODIFIED="1489368419022" TEXT="5/6">
<node CREATED="1485974097026" FOLDED="true" ID="ID_157397601" MODIFIED="1489368417700" TEXT="Renormalization">
<node CREATED="1485974101909" ID="ID_1458211363" MODIFIED="1486268330239" TEXT="divide by sum"/>
<node CREATED="1486268347927" ID="ID_705840705" MODIFIED="1486268374346" TEXT="P(I,D,g1) is unnormalized"/>
<node CREATED="1486268356291" FOLDED="true" ID="ID_998141378" MODIFIED="1489368416949" TEXT="P(I,D | g1) should be normalized">
<node CREATED="1486268549108" ID="ID_504440099" MODIFIED="1486268560139" TEXT="divide by sum of all probabilities"/>
</node>
</node>
</node>
</node>
<node CREATED="1485974029546" ID="ID_417562860" MODIFIED="1490072870173" TEXT="Marginalization">
<node CREATED="1485974041416" ID="ID_954457788" MODIFIED="1489368439013" TEXT="6/6">
<node CREATED="1486268469748" ID="ID_324961209" MODIFIED="1487744700690" TEXT="1">
<node CREATED="1486268437554" ID="ID_1724615209" MODIFIED="1486268455215" TEXT="P(I,D) is over i{0,1} x d{0,1}"/>
</node>
<node CREATED="1486268475093" ID="ID_1422175232" MODIFIED="1487744700691" TEXT="2">
<node CREATED="1486268459078" ID="ID_1244848274" MODIFIED="1487744720840" TEXT="marginalize over I">
<node CREATED="1486268510055" ID="ID_310754082" MODIFIED="1486268520719" TEXT="Column I disappears">
<node CREATED="1490072888206" ID="ID_1625825134" MODIFIED="1490072895097" TEXT="you end up with fewer variables"/>
</node>
<node CREATED="1487744721329" ID="ID_527086306" MODIFIED="1487744728888" TEXT="&quot;Summing out I in P&quot;">
<node CREATED="1487744761953" ID="ID_463666776" LINK="http://www.cs.toronto.edu/~urtasun/courses/GraphicalModels/lecture7.pdf" MODIFIED="1487744763829" TEXT="cs.toronto.edu &gt; Urtasun &gt; Courses &gt; GraphicalModels &gt; Lecture7"/>
<node CREATED="1487744756592" ID="ID_1696032470" MODIFIED="1487744759955" TEXT="Slide 14"/>
</node>
</node>
</node>
<node CREATED="1486268488945" ID="ID_705706285" MODIFIED="1487744700693" TEXT="3">
<node CREATED="1486268490145" ID="ID_1891181888" MODIFIED="1486268496504" TEXT="P(D) is over d{0,1}"/>
</node>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1487643286254" ID="ID_1529127206" MODIFIED="1487643290859" TEXT="Course sections">
<node CREATED="1487643575836" ID="ID_1622343855" MODIFIED="1487643577295" TEXT="Bayesian Network (Directed Models)">
<node CREATED="1484034726804" ID="ID_1514231962" MODIFIED="1484034728151" TEXT="Bayesian Network Fundamentals">
<node CREATED="1485973035411" FOLDED="true" ID="ID_1264712885" MODIFIED="1489368471410" TEXT="Combined Slides">
<node CREATED="1485973040747" ID="ID_1039845485" LINK="http://spark-university.s3.amazonaws.com/stanford-pgm/slides/Section-2-Representation-Bayes-Nets.pdf" MODIFIED="1485973040747" TEXT="spark-university.s3.amazonaws.com &gt; Stanford-pgm &gt; Slides &gt; Section-2-Representation-Bayes-Nets"/>
</node>
<node CREATED="1484035048896" FOLDED="true" ID="ID_609415929" MODIFIED="1489368476551" TEXT="To keep in notes/learn well">
<node CREATED="1484034875911" ID="ID_1973527221" MODIFIED="1484035097984" TEXT="Learn the different types &#xa;of reasoning (save screen shots)&#xa;eg. inter-causal"/>
<node CREATED="1484035103736" ID="ID_668279212" MODIFIED="1484035126669" TEXT="Use Feynman&apos;s methods to learn v-structure"/>
<node CREATED="1484035136572" ID="ID_1032761630" MODIFIED="1484035141393" TEXT="Glossary for chapter"/>
<node CREATED="1484035144394" ID="ID_742512232" MODIFIED="1484035147824" TEXT="Honors track?"/>
</node>
<node CREATED="1484377196107" FOLDED="true" ID="ID_128133464" MODIFIED="1489368545570" TEXT="Reasoning Patterns">
<node CREATED="1486270116840" ID="ID_1704542613" MODIFIED="1486270122627" TEXT="15/59">
<node CREATED="1484377206351" ID="ID_7421786" MODIFIED="1484377233522" TEXT="Causal Reasoning">
<node CREATED="1484377245634" ID="ID_951409083" MODIFIED="1484377251357" TEXT="reasoning &quot;down&quot; the graph"/>
<node CREATED="1484377447141" ID="ID_841135088" MODIFIED="1489368538242" TEXT="example">
<node CREATED="1484377448712" ID="ID_1753362057" MODIFIED="1486268877427" TEXT="P(l1) ~= 0.5"/>
<node CREATED="1484377448712" ID="ID_1620248022" MODIFIED="1486268887258" TEXT="P(l1 | i0) ~= 0.39">
<node CREATED="1484427791748" ID="ID_899689655" MODIFIED="1484427804578" TEXT="P(l1 | i0) "/>
</node>
<node CREATED="1484377448712" ID="ID_1182898993" MODIFIED="1486268898733" TEXT="P(l1 | i0, d0) ~= 0.51">
<node CREATED="1484377537920" ID="ID_71151663" MODIFIED="1484377638636" TEXT="P(g1 | i0, d0)">
<node CREATED="1484377639484" ID="ID_224007354" MODIFIED="1484428685230" TEXT="0.3"/>
</node>
<node CREATED="1484377537920" ID="ID_1017166827" MODIFIED="1484377659093" TEXT="P(g2 | i0, d0)">
<node CREATED="1484377639484" ID="ID_507135096" MODIFIED="1484428688828" TEXT="0.4"/>
</node>
<node CREATED="1484377537920" ID="ID_812597229" MODIFIED="1484377684028" TEXT="P(g3 | i0, d0)">
<node CREATED="1484377639484" ID="ID_1786904441" MODIFIED="1484428685230" TEXT="0.3"/>
</node>
<node CREATED="1484427680735" ID="ID_862843216" MODIFIED="1484427739207" TEXT="">
<node CREATED="1484427479548" ID="ID_1326435084" MODIFIED="1484428714780" TEXT="i0d0 = np.array([0.3, 0.4, 0.3])"/>
<node CREATED="1484427690048" ID="ID_377807662" MODIFIED="1484428721795" TEXT="l1  = np.array([0.9, 0.6, 0.01])"/>
<node CREATED="1484427697558" ID="ID_510619944" MODIFIED="1484427698147" TEXT="sum(i0d0 * l1)">
<node CREATED="1484427702471" ID="ID_1703573118" MODIFIED="1484427708308" TEXT="0.513"/>
</node>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1484377234661" ID="ID_891880680" MODIFIED="1484377239784" TEXT="Evidential Reasoning">
<node CREATED="1484377252818" ID="ID_1746594440" MODIFIED="1484377258394" TEXT="reasoning &quot;up&quot; the graph"/>
</node>
<node CREATED="1484377210511" ID="ID_129376627" MODIFIED="1484377244436" TEXT="Inter-causal Reasoning">
<node CREATED="1484377282161" ID="ID_1473932421" MODIFIED="1484377291491" TEXT="reasoning &quot;zig-zag&quot; across the graph"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1487643304487" ID="ID_732511872" MODIFIED="1487643305800" TEXT="By Week">
<node CREATED="1484375884843" ID="ID_1138446803" MODIFIED="1487643299913" TEXT="Week1">
<node CREATED="1484375887105" ID="ID_234523950" MODIFIED="1484375890969" TEXT="Forums">
<node CREATED="1484375908753" FOLDED="true" ID="ID_231149451" MODIFIED="1489371915583" TEXT="BN Fundamental quiz - readings">
<node CREATED="1484375926003" ID="ID_101730078" MODIFIED="1484433463615" TEXT="BN Fundamental quiz - readings Tom&#xe1;&#x161; Bouda&#xa;&#xa;Week 1 &#xb7; 4 months ago I got stuck in computing marginal probabilities question.  If the same happens to you, I recommend Bayesian Reasoning and Machine Learning, by David Barber (free to download), page 35-36 contains nice example including a lot of reasoning.  Hope this helps.&#xa;&#xa;Note Durant: 2016 version for p35-36"/>
<node CREATED="1484376400988" FOLDED="true" ID="ID_177390513" MODIFIED="1489371912595" TEXT="Jonathan Stone&#xa; &#xb7; 3 months ago&#xa;I worked through the wikipedia example about grass, rain, and sprinklers --&#xa;&#xa;https://en.wikipedia.org/wiki/Bayesian_network#Example&#xa;&#xa;It was still rather challenging. I found a tool&#xa;&#xa;http://aispace.org/bayes/&#xa;&#xa;which walks you through step-by-step how each sub-step is derived from the previous. I was able to use this to get to the answers on the Evidential Reasoning slide: P(g3 | d1) = 0.63.&#xa;&#xa;I think it would be more helpful to see a worked example with the algebra as given on the wikipedia page. I know that in practice computers will be solving these for us but for the most part I am going to use the algebra on the quiz so I want to understand it very well enough to produce it on my own in a &quot;clean room&quot; setting, using Bayes&apos; rule and other probability shortcuts.">
<node CREATED="1484376435352" ID="ID_1044938645" LINK="https://en.wikipedia.org/wiki/Bayesian_network#Example" MODIFIED="1484376440485" TEXT="https://en.wikipedia.org/wiki/Bayesian_network#Example"/>
<node CREATED="1484376425310" ID="ID_1375687338" LINK="http://aispace.org/bayes/" MODIFIED="1484376425310" TEXT="aispace.org &gt; Bayes">
<node CREATED="1484443676551" ID="ID_1170846073" MODIFIED="1484443684401" TEXT="Has interesting Java Applets Apps"/>
</node>
</node>
</node>
</node>
<node CREATED="1484447023491" FOLDED="true" ID="ID_563183923" MODIFIED="1489368300095" TEXT="d-separation">
<node CREATED="1484447027074" ID="ID_1382150707" LINK="http://web.mit.edu/jmn/www/6.034/d-separation.pdf" MODIFIED="1484447027074" TEXT="web.mit.edu &gt; Jmn &gt; Www &gt; 6.034 &gt; D-separation"/>
<node CREATED="1484459487521" ID="ID_1787231225" MODIFIED="1484459498611" TEXT="Draw ancestral graph &#xa;Moralize &#xa;Disorient &#xa;Delete givens"/>
</node>
<node CREATED="1484508271118" ID="ID_695658177" MODIFIED="1496167745496" TEXT="I-maps">
<node CREATED="1484508280659" ID="ID_1380454685" MODIFIED="1484511642850" TEXT="I-maps can also be defined directly&#xa; on graphs as follows.">
<node CREATED="1484511629178" ID="ID_1983145753" MODIFIED="1484511649587" TEXT="Let I(G) be the set of independencies encoded by a graph G. &#xa;Then G1 is an I-map for G2 if I(G1)&#x2286;I(G2)."/>
</node>
<node CREATED="1484511404589" ID="ID_1381895233" MODIFIED="1484511409659" TEXT="Missed on Quiz.">
<node CREATED="1484511409659" ID="ID_745762874" MODIFIED="1484511417266" TEXT="Valuable information!">
<node CREATED="1496167784202" ID="ID_1025248200" MODIFIED="1496167787387" TEXT="Q">
<node CREATED="1496167788519" ID="ID_1407393914" MODIFIED="1496168198188" TEXT="Suppose (A &#x22a5; B) &#x2208; I(P), and G is an I-map of P, where G is a Bayesian network and P is a probability distribution. Is it necessarily true that (A &#x22a5; B) &#x2208; I(G)?  "/>
</node>
<node CREATED="1496167780463" ID="ID_321123534" MODIFIED="1496167781889" TEXT="A">
<node CREATED="1484511417267" ID="ID_1462868714" MODIFIED="1484511445676" TEXT="This should not be selected  Since G is an I-map of P, all independencies in G are also in P. &#xa;&#xa;However, this doesn&apos;t mean that all independencies in P are also in G. &#xa;&#xa;An easy way to remember this is that the complete graph, which has no independencies, is an I-map of all distributions.   ">
<node CREATED="1484511543689" ID="ID_1491608811" MODIFIED="1484511551797" TEXT="re: quiz question">
<node CREATED="1484511517528" ID="ID_1265961590" MODIFIED="1484511518101" TEXT="Suppose (A&#x22a5;B)&#x2208;I(P), and G is an I-map of P, where G is a Bayesian network and P is a probability distribution. Is it necessarily true that (A&#x22a5;B)&#x2208;IG)? "/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1496168259530" ID="ID_1929012101" MODIFIED="1496168272647" TEXT="If G is an I-map for probability distribution P">
<node CREATED="1496168274019" ID="ID_1522951695" MODIFIED="1496168285082" TEXT="all independencies in G exist in P"/>
<node CREATED="1496168285970" ID="ID_1851806986" MODIFIED="1496168309216" TEXT="but not necesssarily all the independencies of P are in G"/>
<node CREATED="1496168317656" ID="ID_1478234339" MODIFIED="1496168726656" TEXT="So remember that G can have no independencies (fully connected) and still be an I-map of P  "/>
<node CREATED="1496168834935" ID="ID_31565521" MODIFIED="1496168844392" TEXT="My conclusion">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1496168809521" ID="ID_1249572277" MODIFIED="1496168827017" TEXT="G cannot be an I-map of P if G has independencies which DO NOT exist in P"/>
<node CREATED="1496168914551" ID="ID_806089264" MODIFIED="1496168918814" TEXT="mnemonic">
<node CREATED="1496168919072" ID="ID_649388487" MODIFIED="1496168925618" TEXT="P is a bucket of independencies"/>
<node CREATED="1496168927178" ID="ID_328048692" MODIFIED="1496169020554" TEXT="G is a valid I-map of P if G contains only (and any amount of) independencies in that bucket, but none from anywhere else.  "/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1485657342179" ID="ID_1294429437" MODIFIED="1485657344806" TEXT="Week 3">
<node CREATED="1496621638067" ID="ID_200085701" MODIFIED="1496621642677" TEXT="Gibbs Distribution">
<node CREATED="1496622320328" ID="ID_1713146739" MODIFIED="1496622338997" TEXT="Gibbs distribution represents distribution as a product of factors "/>
<node CREATED="1496622338998" ID="ID_1548112907" MODIFIED="1496623940751">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Induced Markov network connects&#160; <u>every pair of node</u>s that are in the same factor&#160;
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1496622371155" ID="ID_1873458150" MODIFIED="1496623931457">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Markov network structure&#160; <u>doesn&#8217;</u>t fully specify the factorization of P&#160;
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1496622384970" ID="ID_491085058" MODIFIED="1496622384973" TEXT="But active trails depend only on graph structure"/>
</node>
<node CREATED="1496621720338" ID="ID_1972688151" MODIFIED="1496621730165" TEXT="Conditional Random Fields">
<node CREATED="1496622058093" ID="ID_245601034" MODIFIED="1496622058996" TEXT="A CRF is parameterized the same as a Gibbs distribution, but normalized differently "/>
<node CREATED="1496622288117" ID="ID_1877180821" MODIFIED="1496622289346" TEXT="Don&#x2019;t need to model distribution over variables we don&#x2019;t care about "/>
<node CREATED="1496622297581" ID="ID_337694620" MODIFIED="1496622298186" TEXT="Allows models with highly expressive features, without worrying about wrong independencies "/>
</node>
<node CREATED="1496622596016" ID="ID_176412818" MODIFIED="1496622599392" TEXT="I-Map">
<node CREATED="1496622753590" ID="ID_1345631266" MODIFIED="1496622755712" TEXT="Graph H"/>
<node CREATED="1496622617949" ID="ID_1493121724" MODIFIED="1496622619189" TEXT="I(H) = {(X &#x22a5; Y | Z) : sepH(X, Y | Z)} "/>
<node CREATED="1496622604364" ID="ID_807583003" MODIFIED="1496622605296" TEXT="If P satisfies I(H), we say that H is an I-map (independency map) of P"/>
<node CREATED="1496622653834" ID="ID_735947979" MODIFIED="1496622654555" TEXT="Theorem: If P factorizes over H, then H is an I-map of P"/>
<node CREATED="1496622711315" ID="ID_176203666" MODIFIED="1496622717057" TEXT="Summary">
<node CREATED="1496622723813" ID="ID_1265245408" MODIFIED="1496622724486" TEXT="Two equivalent* views of graph structure:">
<node CREATED="1496622730297" ID="ID_1838117337" MODIFIED="1496622731348" TEXT="Factorization: H allows P to be represented"/>
<node CREATED="1496622737016" ID="ID_1962024692" MODIFIED="1496622737586" TEXT="I-map: Independencies encoded by H hold in P "/>
</node>
<node CREATED="1496622767881" ID="ID_1271267007" MODIFIED="1496622768730" TEXT="If P factorizes over a graph H, we can read from the graph independencies that must hold in P (an independency map)"/>
</node>
</node>
<node CREATED="1496622847453" ID="ID_306400047" MODIFIED="1496622847753" TEXT="Capturing Independencies in P">
<node CREATED="1496622887761" ID="ID_1002097491" MODIFIED="1496622980856" TEXT="I(G) &#x2286; I(P)">
<node CREATED="1496622909630" ID="ID_41106160" MODIFIED="1496622915995" TEXT="all the independencies in G can be found in P"/>
</node>
</node>
<node CREATED="1485657344807" ID="ID_1297636526" MODIFIED="1496374849568" TEXT="I-Equivalence">
<node CREATED="1485657349823" ID="ID_1255367829" MODIFIED="1485657395566" TEXT="G &amp; H are I-equivalent if there are no extra independencies in either graph G &amp; H"/>
<node CREATED="1496623699934" ID="ID_1224938348" MODIFIED="1496623700865" TEXT="Note">
<node CREATED="1496623701075" ID="ID_1443773953" MODIFIED="1496623716547" TEXT="Most graphs have many I-equivalent variants"/>
</node>
</node>
<node CREATED="1496623758002" ID="ID_276800215" MODIFIED="1496623773904" TEXT="I-maps and Perfect Maps">
<node CREATED="1496623736912" ID="ID_1108670975" MODIFIED="1496623737334" TEXT="Summary">
<node CREATED="1496623743783" ID="ID_1493136788" MODIFIED="1496623744355" TEXT="Graphs that capture more of I(P) are more compact and provide more insight "/>
<node CREATED="1496623809487" ID="ID_1005864215" MODIFIED="1496623820475" TEXT="A minimal I-map may fail to capture a lot of structure even if present  (and be representable as a PGM)"/>
<node CREATED="1496623835280" ID="ID_1736755988" MODIFIED="1496623835888" TEXT="A perfect map is great, but may not exist "/>
<node CREATED="1496623871491" ID="ID_593868172" MODIFIED="1496623872102" TEXT="Converting BNs &#x2194; MNs loses independencies">
<node CREATED="1496623879635" ID="ID_1815325908" MODIFIED="1496623887442">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      BN to MN: loses independencies in <u>v-structure</u>s
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1496623895472" ID="ID_890530990" MODIFIED="1496623918383">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      MN to BN: must add <u>triangulating</u>&#160;&#160;edges to loops
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1496624187076" ID="ID_1986676355" MODIFIED="1496624189139" TEXT="MRF">
<node CREATED="1496624189374" ID="ID_408991645" MODIFIED="1496624198532" TEXT="Markov Random Field">
<node CREATED="1496624205402" LINK="https://en.wikipedia.org/wiki/Markov_random_field" MODIFIED="1496624205402" TEXT="https://en.wikipedia.org/wiki/Markov_random_field"/>
<node CREATED="1496624233331" ID="ID_27541331" MODIFIED="1496624234340" TEXT="The prototypical Markov random field is the Ising model"/>
</node>
<node CREATED="1496624331627" ID="ID_331060676" MODIFIED="1496624332996" TEXT="Metric MRFs">
<node CREATED="1496624338440" ID="ID_13478224" MODIFIED="1496624340928" TEXT="distance function">
<node CREATED="1496624351497" ID="ID_971948432" MODIFIED="1496624352525" TEXT="metric">
<node CREATED="1496624354380" ID="ID_1134707003" MODIFIED="1496624355146" TEXT="&#x2013; Reflexivity: &#xb5;(v,v) = 0 for all v "/>
<node CREATED="1496624361993" ID="ID_545518188" MODIFIED="1496624362474" TEXT="&#x2013; Symmetry: &#xb5;(v1,v2) = &#xb5;(v2,v1) for all v1, v2"/>
<node CREATED="1496624368398" ID="ID_1323474251" MODIFIED="1496624368758" TEXT="&#x2013; Triangle inequality: &#xb5;(v1,v2) &#x2264; &#xb5;(v1,v3) + &#xb5;(v3, v2) for all v1, v2, v3"/>
</node>
<node CREATED="1496624428713" ID="ID_1106441011" MODIFIED="1496624437002" TEXT="lower distance &lt;-&gt; higher probability"/>
</node>
<node CREATED="1496624493234" ID="ID_1766087694" MODIFIED="1496624496060" TEXT="denoising"/>
</node>
</node>
<node CREATED="1496624535359" ID="ID_85685957" MODIFIED="1496624559063" TEXT="Shared Features in Log-Linear Models">
<node CREATED="1496624572155" ID="ID_473999270" MODIFIED="1496624572725" TEXT="Ising Models ">
<node CREATED="1496624630523" ID="ID_488661574" MODIFIED="1496624631206" TEXT="same weight for every adjacent pair"/>
</node>
<node CREATED="1496624643755" ID="ID_684622155" MODIFIED="1496624644005" TEXT="Natural Language Processing">
<node CREATED="1496624654441" ID="ID_1731035375" MODIFIED="1496624654916" TEXT="Same energy terms wkfk(Xi ,Yi ) repeat for all positions i in the sequence"/>
<node CREATED="1496624662922" ID="ID_1192533861" MODIFIED="1496624686267" TEXT="Same energy terms wmfm(Yi ,Yi+1) and repeat for all positions i"/>
</node>
<node CREATED="1496624714029" ID="ID_976397848" MODIFIED="1496624714458" TEXT="Image Segmentation">
<node CREATED="1496624719126" ID="ID_273729880" MODIFIED="1496624720627" TEXT="Same features and weights for all superpixels in the image "/>
</node>
<node CREATED="1496624795641" ID="ID_1992746530" MODIFIED="1496624799101" TEXT="Summary">
<node CREATED="1496624804162" ID="ID_540049" MODIFIED="1496624804641" TEXT="Same feature &amp; weight can be used for multiple subsets of variables ">
<node CREATED="1496624809622" ID="ID_940711963" MODIFIED="1496624810259" TEXT="Pairs of adjacent pixels/atoms/words "/>
<node CREATED="1496624815885" ID="ID_444873002" MODIFIED="1496624816279" TEXT="Occurrences of same word in document"/>
</node>
<node CREATED="1496624823279" ID="ID_1319426062" MODIFIED="1496624823534" TEXT="Can provide a single template for multiple MNs">
<node CREATED="1496624829644" ID="ID_541578229" MODIFIED="1496624830299" TEXT="Different images"/>
<node CREATED="1496624835249" ID="ID_1743119639" MODIFIED="1496624845764" TEXT="Different sentences "/>
</node>
<node CREATED="1496624858244" ID="ID_459898467" MODIFIED="1496624859259" TEXT="Parameters and structure are reused within an MN and across different MNs"/>
<node CREATED="1496624867220" ID="ID_1554675058" MODIFIED="1496624867446" TEXT="Need to specify set of scopes for each feature"/>
</node>
</node>
</node>
<node CREATED="1496626884496" ID="ID_1751500673" MODIFIED="1496626886366" TEXT="Week 4">
<node CREATED="1496626886756" ID="ID_606298896" MODIFIED="1496626946305" TEXT="Need to rewatch">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1496626892727" ID="ID_191456877" MODIFIED="1496626894791" TEXT="Decision Theory"/>
</node>
</node>
</node>
<node CREATED="1487643316043" ID="ID_1867263281" MODIFIED="1487643322525" TEXT="Honors Programming Quizzes"/>
<node CREATED="1493006316526" ID="ID_1611518834" MODIFIED="1493006321355" TEXT="Forums">
<node CREATED="1493006322343" ID="ID_1187918092" MODIFIED="1493006404845" TEXT="PGMs in python">
<node CREATED="1493006423382" FOLDED="true" ID="ID_85077752" MODIFIED="1493006429850" TEXT="forum thread">
<node CREATED="1493006416311" ID="ID_1367246360" LINK="https://www.coursera.org/learn/probabilistic-graphical-models/discussions/forums/FS3L_ANiEeawFQ5rY6ZGGQ/threads/I52rQIaAEeapVg7XPVK6Cw" MODIFIED="1493006422593" TEXT="https://www.coursera.org/learn/probabilistic-graphical-models/discussions/forums/FS3L_ANiEeawFQ5rY6ZGGQ/threads/I52rQIaAEeapVg7XPVK6Cw"/>
</node>
<node CREATED="1493006329343" ID="ID_1411430100" MODIFIED="1493006330288" TEXT="book">
<node CREATED="1493006332930" ID="ID_296390529" LINK="https://www.packtpub.com/big-data-and-business-intelligence/mastering-probabilistic-graphical-models-using-python" MODIFIED="1493006332930" TEXT="https://www.packtpub.com/big-data-and-business-intelligence/mastering-probabilistic-graphical-models-using-python"/>
<node CREATED="1493006340267" ID="ID_1050307739" MODIFIED="1493006341702" TEXT="Mastering Probabilistic Graphical Models Using Python"/>
<node CREATED="1493006344632" ID="ID_937199207" MODIFIED="1493006347511" TEXT="Aug 2015"/>
</node>
<node CREATED="1493006406067" ID="ID_422363030" LINK="http://pgmpy.org/" MODIFIED="1493006406067" TEXT="pgmpy.org"/>
<node CREATED="1493009099088" ID="ID_819788535" MODIFIED="1493009102626" TEXT="with pandas">
<node CREATED="1493008961631" ID="ID_1267279162" LINK="https://github.com/KKostya/GraphicalModelsPython/tree/master/Week1" MODIFIED="1493008961631" TEXT="https://github.com/KKostya/GraphicalModelsPython/tree/master/Week1"/>
</node>
</node>
<node CREATED="1493007867282" ID="ID_1210645034" MODIFIED="1493007935564" TEXT="this person wrote up all the solutions an a PDF">
<node CREATED="1493007885091" LINK="https://github.com/rockkingjy/CourseraProbabilisticGraphicModels/blob/master/PGM.pdf" MODIFIED="1493007885091" TEXT="https://github.com/rockkingjy/CourseraProbabilisticGraphicModels/blob/master/PGM.pdf"/>
</node>
<node CREATED="1493008488911" ID="ID_1216466186" MODIFIED="1493008508039" TEXT="Bartek Wilczynski &#xa;Using Python to Find a Bayesian Network Describing Your Data ">
<node CREATED="1493008494320" LINK="https://www.youtube.com/watch?v=WD7b1jLeZZ4" MODIFIED="1493008494320" TEXT="https://www.youtube.com/watch?v=WD7b1jLeZZ4"/>
</node>
<node CREATED="1493009514003" ID="ID_1268362807" MODIFIED="1493009519212" TEXT="Figaro (ie. Scala)">
<node CREATED="1493009519803" LINK="https://gist.github.com/mtncork/635e22f00a7b34534d8f8b08c07b3f66" MODIFIED="1493009519803" TEXT="https://gist.github.com/mtncork/635e22f00a7b34534d8f8b08c07b3f66"/>
</node>
</node>
</node>
</node>
<node CREATED="1482128963729" ID="ID_602684865" MODIFIED="1482128970263" POSITION="right" TEXT="U Toronto">
<node CREATED="1482128971171" LINK="http://www.cs.toronto.edu/~urtasun/courses/GraphicalModels/lecture7.pdf" MODIFIED="1482128971171" TEXT="cs.toronto.edu &gt; Urtasun &gt; Courses &gt; GraphicalModels &gt; Lecture7"/>
</node>
<node CREATED="1482129171492" ID="ID_474855795" MODIFIED="1482129173425" POSITION="right" TEXT="Misc">
<node CREATED="1482129174382" LINK="http://www.bayesserver.com/Visualization.aspx" MODIFIED="1482129174382" TEXT="bayesserver.com &gt; Visualization"/>
</node>
<node CREATED="1486446879403" ID="ID_1806114594" MODIFIED="1486446880688" POSITION="left" TEXT="MIT">
<node CREATED="1486446881624" ID="ID_1868142232" LINK="https://metacademy.org/roadmaps/rgrosse/mit_6_438" MODIFIED="1486446881624" TEXT="https://metacademy.org/roadmaps/rgrosse/mit_6_438"/>
</node>
<node CREATED="1489353244597" ID="ID_1073610060" MODIFIED="1489353247007" POSITION="left" TEXT="ubc.ca">
<node CREATED="1489353237253" ID="ID_917045321" MODIFIED="1489353278407" TEXT="A Brief Introduction to &#xa;Graphical Models and &#xa;Bayesian Networks">
<node CREATED="1489353247931" ID="ID_428573735" MODIFIED="1489353248975" TEXT="Kevin Murphy"/>
<node CREATED="1489353261570" ID="ID_1584753221" LINK="http://people.cs.ubc.ca/~murphyk/Bayes/bnintro.html" MODIFIED="1489353261570" TEXT="people.cs.ubc.ca &gt; Murphyk &gt; Bayes &gt; Bnintro"/>
<node CREATED="1489368655247" FOLDED="true" ID="ID_240682361" MODIFIED="1492742651644" TEXT="exercise: Summing probabilities with Bayes&apos; Rule">
<node CREATED="1489368695769" ID="ID_325789542" MODIFIED="1489375167979" TEXT="P(W=1)">
<node CREATED="1489375086822" ID="ID_930733369" MODIFIED="1489375088366" TEXT="=">
<node CREATED="1489375089818" ID="ID_45797185" MODIFIED="1489375090440" TEXT="+">
<node CREATED="1489375091193" MODIFIED="1489375091193" TEXT="c0 = 0.5"/>
<node CREATED="1489375091194" MODIFIED="1489375091194" TEXT="c1 = 0.5"/>
<node CREATED="1489375091194" MODIFIED="1489375091194" TEXT="c0s0 = 0.5"/>
<node CREATED="1489375091194" MODIFIED="1489375091194" TEXT="c1s0 = 0.9"/>
<node CREATED="1489375091195" MODIFIED="1489375091195" TEXT="c0s1 = 0.5"/>
<node CREATED="1489375091195" MODIFIED="1489375091195" TEXT="c1s1 = 0.1"/>
<node CREATED="1489375091195" MODIFIED="1489375091195" TEXT="c0r0 = 0.8"/>
<node CREATED="1489375091196" MODIFIED="1489375091196" TEXT="c1r0 = 0.2"/>
<node CREATED="1489375091196" MODIFIED="1489375091196" TEXT="c0r1 = 0.2"/>
<node CREATED="1489375091196" MODIFIED="1489375091196" TEXT="c1r1 = 0.8"/>
<node CREATED="1489375091197" MODIFIED="1489375091197" TEXT="c0s0r0w1 = c0 * c0s0 * c0r0 * (0.0)"/>
<node CREATED="1489375091197" MODIFIED="1489375091197" TEXT="c1s0r0w1 = c1 * c1s0 * c1r0 * (0.0)"/>
<node CREATED="1489375091197" MODIFIED="1489375091197" TEXT="c0s1r0w1 = c0 * c0s1 * c0r0 * (0.9)"/>
<node CREATED="1489375091197" MODIFIED="1489375091197" TEXT="c1s1r0w1 = c1 * c1s1 * c1r0 * (0.9)"/>
<node CREATED="1489375091198" MODIFIED="1489375091198" TEXT="c0s0r1w1 = c0 * c0s0 * c0r1 * (0.9)"/>
<node CREATED="1489375091198" MODIFIED="1489375091198" TEXT="c1s0r1w1 = c1 * c1s0 * c1r1 * (0.9)"/>
<node CREATED="1489375091198" MODIFIED="1489375091198" TEXT="c0s1r1w1 = c0 * c0s1 * c0r1 * (0.99)"/>
<node CREATED="1489375091198" ID="ID_1818846722" MODIFIED="1489375091198" TEXT="c1s1r1w1 = c1 * c1s1 * c1r1 * (0.99)"/>
<node CREATED="1489375132669" ID="ID_1573704382" MODIFIED="1489375133560" TEXT="w1 = c0s0r0w1 + c1s0r0w1 + c0s1r0w1 + c1s1r0w1 + c0s0r1w1 + c1s0r1w1 + c0s1r1w1 + c1s1r1w1"/>
<node CREATED="1489375091202" ID="ID_800003670" MODIFIED="1489375091202" TEXT="# w1 = 0.6471"/>
<node CREATED="1489377268355" ID="ID_155777768" MODIFIED="1489377318908" TEXT="If we want to add up the operations&#xa;1. for each value in column Wet=True&#xa;2. mulitply by the all the ways to reach FF, FT, TF, TT&#xa;   Note: we might be able to think of this as a single variable with 4 states&#xa;3. there are 2 paths to FF because Sprinkler and Rain are fixed, but Cloudy varies&#xa;so 4 * 2 = 8 summands&#xa;if Cloudy had 3 values, this would be 4 * 3 = 12 (summands, or &quot;rows to add&quot;)&#xa;And if trivalued Cloudy depended on a CloudControlMachine with on or off &#xa;we&apos;d have 4 * 3 * 2 = 24 (summands)&#xa;Now how many multiplicands per summand?&#xa;There are 4 variables so 4 things are multiplied, ie 4 multiplicands&#xa;Trivalued Cloudy depending on a CloudControlMachine would mean 5 multiplicands&#xa;so 24 rows to add of 5 things multiplied together"/>
</node>
</node>
</node>
</node>
<node CREATED="1492742914950" ID="ID_603192313" MODIFIED="1492742917688" TEXT="PDF version">
<node CREATED="1492742924063" LINK="http://people.cs.ubc.ca/~murphyk/Bayes/bayes_tutorial.pdf" MODIFIED="1492742924063" TEXT="people.cs.ubc.ca &gt; Murphyk &gt; Bayes &gt; Bayes tutorial"/>
</node>
<node CREATED="1492742897187" ID="ID_1230506705" MODIFIED="1492742900357" TEXT="Tutorial">
<node CREATED="1492742907584" LINK="http://people.cs.ubc.ca/~murphyk/Papers/intro_gm.pdf" MODIFIED="1492742907584" TEXT="people.cs.ubc.ca &gt; Murphyk &gt; Papers &gt; Intro gm"/>
</node>
<node CREATED="1493762382251" ID="ID_1606990185" MODIFIED="1493762383333" TEXT="Bayes Net Toolbox for Matlab">
<node CREATED="1493762388186" LINK="https://github.com/bayesnet/bnt" MODIFIED="1493762388186" TEXT="https://github.com/bayesnet/bnt"/>
</node>
</node>
</node>
<node CREATED="1492981160909" ID="ID_120375570" MODIFIED="1492981163103" POSITION="left" TEXT="cmu">
<node CREATED="1492981179581" ID="ID_1443241964" MODIFIED="1492981193660" TEXT="Artificial Intelligence 15-381 &#xa;Mar 27, 2007&#xa;Bayesian Networks 1">
<node CREATED="1492981165225" ID="ID_57786496" LINK="https://www.cs.cmu.edu/afs/cs/academic/class/15381-s07/www/slides/032707bayesNets1.pdf" MODIFIED="1492981165225" TEXT="https://www.cs.cmu.edu/afs/cs/academic/class/15381-s07/www/slides/032707bayesNets1.pdf"/>
<node CREATED="1492981213819" ID="ID_230290115" MODIFIED="1492981214182" TEXT="Calculating probabilities using the joint distribution"/>
</node>
</node>
<node CREATED="1493763520977" ID="ID_1834563806" MODIFIED="1493763529599" POSITION="left" TEXT="Code Libraries">
<node CREATED="1493763532232" ID="ID_1233621002" MODIFIED="1493763535091" TEXT="C++">
<node CREATED="1493763535947" LINK="http://dlib.net/bayes.html" MODIFIED="1493763535947" TEXT="dlib.net &gt; Bayes"/>
</node>
<node CREATED="1493763541919" ID="ID_981917048" MODIFIED="1493763543613" TEXT="Python">
<node CREATED="1493006406067" ID="ID_1870102075" LINK="http://pgmpy.org/" MODIFIED="1493006406067" TEXT="pgmpy.org"/>
<node CREATED="1493009099088" ID="ID_1630025755" MODIFIED="1493009102626" TEXT="with pandas">
<node CREATED="1493008961631" ID="ID_548019990" LINK="https://github.com/KKostya/GraphicalModelsPython/tree/master/Week1" MODIFIED="1493008961631" TEXT="https://github.com/KKostya/GraphicalModelsPython/tree/master/Week1"/>
</node>
</node>
</node>
</node>
</map>
