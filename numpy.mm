<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1481411959667" ID="ID_1383997673" LINK="MetaMap.mm" MODIFIED="1481411993576" TEXT="numpy.mm">
<node CREATED="1481411996509" ID="ID_1543603459" MODIFIED="1481412000642" POSITION="right" TEXT="arange vs linspace">
<node CREATED="1481412011159" ID="ID_1810538315" MODIFIED="1481412011935" TEXT="x = np.arange(-2.0, 6.0, 0.1)"/>
<node CREATED="1481412020104" ID="ID_583650508" MODIFIED="1481412020846" TEXT="x = np.linspace(-2.0, 6.0, num=80, endpoint=False)"/>
<node CREATED="1481412052699" LINK="https://docs.scipy.org/doc/numpy/reference/generated/numpy.linspace.html" MODIFIED="1481412052699" TEXT="https://docs.scipy.org/doc/numpy/reference/generated/numpy.linspace.html"/>
</node>
<node CREATED="1481433358768" ID="ID_1534059539" MODIFIED="1481433381122" POSITION="right" TEXT="reshape with -1 infers the right number for the last dimension."/>
</node>
</map>
