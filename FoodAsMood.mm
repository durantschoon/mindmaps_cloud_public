<map version="0.8.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1325972631414" ID="Freemind_Link_1833542643" LINK="../../Documents/mindmaps/MetaMap.mm" MODIFIED="1325972658392" TEXT="FoodAsMood.mm">
<node CREATED="1325972684516" ID="_" MODIFIED="1325972689655" POSITION="right" TEXT="rules I&apos;m learning about myself">
<node CREATED="1325973245558" ID="Freemind_Link_484369260" MODIFIED="1325973246489" TEXT="common">
<node CREATED="1325972691779" ID="Freemind_Link_57794381" MODIFIED="1325972698542" TEXT="wine">
<node CREATED="1325972698954" ID="Freemind_Link_1431381695" MODIFIED="1325972706086" TEXT="only before bed"/>
<node CREATED="1325972706323" ID="Freemind_Link_142945234" MODIFIED="1325972708838" TEXT="never before tango"/>
</node>
<node CREATED="1325972743369" ID="Freemind_Link_460631821" MODIFIED="1325972761212" TEXT="sugar&#xa;(non-fruit)">
<node CREATED="1325972747817" ID="Freemind_Link_699834583" MODIFIED="1325972754908" TEXT="usually before sleeping"/>
<node CREATED="1325973282467" ID="Freemind_Link_100523717" MODIFIED="1325973293894" TEXT="only eat fruit at home unless going to sleep"/>
<node CREATED="1325972762744" ID="Freemind_Link_417000442" MODIFIED="1325972764475" TEXT="or with coffee"/>
</node>
<node CREATED="1325973000418" ID="Freemind_Link_1264013178" MODIFIED="1325973003213" TEXT="coffee">
<node CREATED="1325973003505" ID="Freemind_Link_746576121" MODIFIED="1325973006301" TEXT="just one"/>
<node CREATED="1325973006793" ID="Freemind_Link_1768496731" MODIFIED="1325973010501" TEXT="not too late unless dancing"/>
</node>
</node>
<node CREATED="1325973241862" ID="Freemind_Link_132974538" MODIFIED="1325973242905" TEXT="rare">
<node CREATED="1325973011889" ID="Freemind_Link_65361375" MODIFIED="1325973013637" TEXT="doughnuts">
<node CREATED="1325973013953" ID="Freemind_Link_1210209174" MODIFIED="1325973019956" TEXT="1/2 max, ever, seriously"/>
</node>
</node>
</node>
<node CREATED="1325973142942" ID="Freemind_Link_1368317105" MODIFIED="1325973147706" POSITION="left" TEXT="allergies/effects">
<node CREATED="1325972711922" ID="Freemind_Link_642090268" MODIFIED="1325972734013" TEXT="Chicken">
<node CREATED="1325972734539" ID="Freemind_Link_1730322029" MODIFIED="1325972739749" TEXT="Especially not before exercise">
<node CREATED="1325973037832" ID="Freemind_Link_336705475" MODIFIED="1325973054371" TEXT="Serious danger"/>
</node>
</node>
<node CREATED="1325972775191" ID="Freemind_Link_1452925835" MODIFIED="1325972778331" TEXT="Garlic">
<node CREATED="1325972781511" ID="Freemind_Link_1025528538" MODIFIED="1325972788235" TEXT="better with enzymes, but eat it"/>
</node>
<node CREATED="1325972963935" ID="Freemind_Link_9663502" MODIFIED="1325973064004" TEXT="Cabbage/Lentils/Beans">
<node CREATED="1325972971671" ID="Freemind_Link_1431662808" MODIFIED="1325972973891" TEXT="gassy"/>
<node CREATED="1325972974127" ID="Freemind_Link_233099771" MODIFIED="1325973071370" TEXT="best before (outdoor) hiking, ha, ha"/>
</node>
</node>
<node CREATED="1325973153718" ID="Freemind_Link_693102398" MODIFIED="1325973159704" POSITION="left" TEXT="tastes good or not">
<node CREATED="1325973083257" ID="Freemind_Link_1981020019" MODIFIED="1325973084228" TEXT="fats">
<node CREATED="1325973088065" ID="Freemind_Link_1006247612" MODIFIED="1325973092772" TEXT="bacon grease">
<node CREATED="1325973092984" ID="Freemind_Link_288332455" MODIFIED="1325973095612" TEXT="not good with">
<node CREATED="1325973096096" ID="Freemind_Link_1085723147" MODIFIED="1325973099708" TEXT="zuchinni"/>
</node>
</node>
</node>
</node>
</node>
</map>
